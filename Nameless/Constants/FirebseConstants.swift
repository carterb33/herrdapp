//
//  FirebseConstants.swift
//  Herrd
//
//  Created by DG on 17/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation

struct FirebaseConst {
    struct Collections {
        static let Users = "users"
        static let Admin = "Admin"
        static let Venues = "VenueChannels"
        static let TopicChannels = "TopicChannels"
        static let Events = "Events"
        static let UserChatCount = "UserChatReadCount"
        static let BlockedUsers = "BlockedUsers"
        static let Names = "names"
        
        //Topic & Sub Topics
        static let Topics = "Topics"
        static let SubTopics = "SubTopics"
        
        //Backup collections
        static let OldTopics = "OldTopics"
        static let OldSubTopics = "OldSubTopics"
        
        //App Settings
        static let AppSettings = "Settings"
    }
}
