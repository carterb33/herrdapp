//
//  ColorConstants.swift
//  Herrd
//
//  Created by DG on 14/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit

struct AppColor {
    
    struct CommonColor {
        static let GoingButton = UIColor.init(red: 165/255.0, green: 230/255.0, blue: 100/255.0, alpha: 1.0)
        static let RefreshControl = UIColor(red:0.09, green:0.09, blue:0.09, alpha:1.0)
        
        static let cellColor = UIColor(red: 33/255, green: 34/255, blue: 35/255, alpha: 1.0)
        static let postColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        static let secondaryColor = UIColor(red: 117.0/255, green: 117.0/255, blue: 117.0/255, alpha: 1.0)
    }
    
}
