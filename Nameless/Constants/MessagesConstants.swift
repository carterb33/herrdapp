//
//  MessagesConstants.swift
//  Herrd
//
//  Created by DG on 16/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation

struct Messages {

        struct Errors {
            static let DownVoteFailure = "Something went wrong while downvoting. Please try again!"
            static let UpVoteFailure = "Something went wrong while upvoting. Please try again!"
            static let ReportFailure = "Something went wrong while reporting. Please try again!"
            static let HideFailure = "Something went wrong while hiding. Please try again!"
            static let BlockUserFailure = "Something went wrong while blocking user. Please try again!"
            static let HideSubTopicFailure = "Something went wrong while hiding sub post. Please try again!"
        }

        struct Info {
            static let ReviewPost = "This post will be reviewed within 24 hours by the Herrd team."
            static let ReviewReply = "This reply will be reviewed within 24 hours by the Herrd team."
            static let HidePost = "This post now will be hidden from your feed."
            static let HideReply = "This reply now will be hidden from your feed."
            static let BlockPost = "All posts from this user will be hidden."
            static let CameraPrivacy = "It looks like your privacy settings are preventing us from accessing your camera to capture photo. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n5. Open this app and try again."
            static let CameraPrivacyWithSetting = "It looks like your privacy settings are preventing us from accessing your camera to capture photo. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again."
            static let AnonymousPost = "Although all posts to this page are anonymous, any harmful or threatening posts can be traced back to the logged-in user to ban accounts or take other necessary actions with Boston College. Help keep this a safe place to talk about what's going on 🙂"
            static let NoRecordInReview = "No topics reported for review. Keep checking this tab for reported topics."
            static let NoRecordInSubTopicReview = "No topics/sub topics reported for review. Keep checking this tab for reported topics."
            static let NoRecordInMyTopic = "Oops... You haven't posted anything new. Create a post by clicking on the + button below"
            static let NoRecordInMySubTopic = "Oops... You haven't replied to any posts."
            static let NoRecordInTopic = "Oops! It's Empty. There are no topics available right now."
            static let NoRecordInChatTopic = "Oops! It's Empty. You do not have any direct messages in this section."
            static let AnonymousMode = "When anonynmous mode is on, your actual name will not appear on any venue guest list. When anonymous mode is off, your actual name will show up on the guest list for venue you select. All posts to the main Herrd feed, though, will ALWAYS remain anonymous no matter what you toggle on this setting."
            static let AllUserUnlocked = "All Users unblocked successfully."
        }
        struct Actions {
            static let PullToRefresh = "Pull to Sort & Refresh Posts"
        }

        struct Warning {
            static let DeletePost = "Your post will be permanentaly removed from the app."
            static let DeleteReply = "Your reply will be permanentaly removed from this post."
            static let ValidMailSignIn = "You must sign in with a valid BC email"
            static let EntryValidationMsg = "Your entry cannot be empty and cannot  start/end in a space"
            static let AccessCodeBlankValidationMsg = "Access Code should not be blank. Please enter access code."
            static let AccessCodeLengthValidationMsg = "Access Code should be only 4 charater. Please enter valid access code."
            static let AccessCodeValidationMsg = "Access code is not valid. Please enter valid access code."
        }
}
