//
//  KeyConstants.swift
//  Herrd
//
//  Created by DG on 14/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation

struct Key {

        struct UserDefaults {
            static let k_App_Running_FirstTime = "userRunningAppFirstTime"
        }

        struct Headers {
            static let Authorization = "Authorization"
            static let ContentType = "application/json"
        }
        struct Google{
            static let FCM = "key=AAAACVE7A8M:APA91bEAGAod8g3bx-D95BqkeV8_lywMHS42mizraAk4t80H_JrFVXy6QMaGeOPNkgpZ0RQQWXwcfo0aYEjWu2cwlJQrSfkmHTcLkxf3haBUUoO-i0h3ZvNUeLZ7ckfLIicdgwIjwZDf"
            static let Places = "AIzaSyBfbSabISTOKq8AUEPEckFX0jxzcddyFJU"
            static let AdMob = "ca-app-pub-8946459992553219~4985121185"
        }

    struct GIPHY {
        static let SdkKey = "hfpYZYaOdgY7j5lMor9wwL0Qm1XubrFQ"
    }
}
