//
//  URLConstants.swift
//  Herrd
//
//  Created by DG on 11/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation

struct APPURL {

//    private struct Domains {
//        static let Dev = "http://test-dev.cloudapp.net"
//        static let UAT = "http://test-UAT.com"
//        static let Local = "192.145.1.1"
//        static let QA = "testAddress.qa.com"
//    }
//
//    private  struct Routes {
//        static let Api = "/api/mobile"
//    }
//
//    private  static let Domain = Domains.Dev
//    private  static let Route = Routes.Api
//    private  static let BaseURL = Domain + Route
//
//    static var FacebookLogin: String {
//        return BaseURL  + "/auth/facebook"
//    }
    
    static let PrivacyUrl = "https://herrdapp.com/#privacypolicy"
    
    static let PolicyUrl = "https://www.websitepolicies.com/policies/view/GgAKI1AG"
    
    static let FCMNotifictionUrl = "https://fcm.googleapis.com/fcm/send"
    
    static let iTunesAppBaseUrl = "https://itunes.apple.com/lookup?bundleId="
}
