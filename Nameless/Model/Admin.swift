//
//  Admin.swift
//  Herrd
//
//  Created by DG on 16/08/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit

class Admin: NSObject {

    var UID : String?
    
    override init() {
        super.init()
    }
    
    init(data:[String:Any]) throws {
        guard let UID = data["UID"] as? String else { throw ParsingError() }
        self.UID = UID
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Admin()
        copy.UID = self.UID
        return copy
    }
}
