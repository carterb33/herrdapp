//
//  Message.swift
//  Herrd
//
//  Created by DG on 06/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import Firebase
import MessageKit
import FirebaseFirestore

struct Message: MessageType {
    var sender: SenderType
    
    var kind: MessageKind
    
    let id: String?
    let content: String
    let sentDate: Date
    let read: Bool
    
    var messageId: String {
        return id ?? UUID().uuidString
    }
    
    var image: UIImage? = nil
    var downloadURL: URL? = nil
    
    init(user: User, content: String) {
        
        if UserDefaults.standard.bool(forKey: "AnonymousMode") {
            sender = Sender(id: user.uid, displayName: "anonymous")
        } else {
            sender = Sender(id: user.uid, displayName: user.displayName ?? user.uid)
        }
        
        
        self.kind = .text(content)
        self.content = content
        sentDate = Date()
        id = nil
        read = false
    }
    
//    init(user: User, image: UIImage) {
//        sender = Sender(id: user.uid, displayName: user.displayName ?? user.uid)
//        self.kind = .photo(<#T##MediaItem#>)
//        self.image = image
//        content = ""
//        sentDate = Date()
//        id = nil
//    }
    
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()
        guard let stamp = data["created"] as? Timestamp else {
            return nil
        }
        let sentDate = stamp.dateValue()
        
        guard let senderID = data["senderID"] as? String else {
            return nil
        }
        guard let senderName = data["senderName"] as? String else {
            return nil
        }
        let isRead = data["read"] as? Bool
        
        id = document.documentID
        
        self.sentDate = sentDate
        self.read = isRead ?? false
        sender = Sender(id: senderID, displayName: senderName)
        
        if let content = data["content"] as? String {
            self.content = content
            self.kind = .text(content)
            downloadURL = nil
        } else if let urlString = data["url"] as? String, let url = URL(string: urlString) {
            downloadURL = url
            content = ""
            self.kind = .text("")
        } else {
            return nil
        }
    }
    
}

extension Message: DatabaseRepresentation {
    
    var representation: [String : Any] {
        var rep: [String : Any] = [
            "created": sentDate,
            "senderID": sender.senderId,
            "senderName": sender.displayName,
            "read": read
        ]
        
        if let url = downloadURL {
            rep["url"] = url.absoluteString
        } else {
            rep["content"] = content
        }
        
        return rep
    }
    
}

extension Message: Comparable {
    
    static func == (lhs: Message, rhs: Message) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func < (lhs: Message, rhs: Message) -> Bool {
        return lhs.sentDate < rhs.sentDate
    }
    
}

