//
//  DatabaseRepresentation.swift
//  Herrd
//
//  Created by DG on 06/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import Foundation

protocol DatabaseRepresentation {
    var representation: [String: Any] { get }
}
