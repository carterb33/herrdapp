//
//  SubTopic.swift
//  Herrd
//
//  Created by DG on 05/04/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase

class SubTopic: NSObject {

    var subtopic_id : Double?
    var topic_id : Double?
    var subtopic_title: String?
    var subtopic_desc: String?
    var difference: Int?
    var subtopic_date: Timestamp?
    var created_by: String?
    var documentId: String?
    var upVoteIds: String?
    var downVoteIds: String?
    var channelId: String?
    var msgcount: Int64 = 0
    var subReplyCount: Int64 = 0
    var reportedIds: String?
    var hidePostsUserIds: String?
    var isReplySubTopic: Bool?
    
    override init() {
        super.init()
    }
    
    init(data:[String:Any]) throws {
        guard let subtopic_id = data["subtopic_id"] as? Double else { throw ParsingError() }
        guard let topic_id = data["topic_id"] as? Double else { throw ParsingError() }
        guard let subtopic_title = data["subtopic_title"] as? String else { throw ParsingError() }
        guard let subtopic_desc = data["subtopic_desc"] as? String else { throw ParsingError() }
        guard let difference = data["difference"] as? Int else { throw ParsingError() }
        guard let subtopic_date = data["subtopic_date"] as? Timestamp else { throw ParsingError() }
        guard let created_by = data["created_by"] as? String else { throw ParsingError() }
        guard let documentId = data["documentId"] as? String else { throw ParsingError() }
        guard let upVoteIds = data["upVoteIds"] as? String else { throw ParsingError() }
        guard let downVoteIds = data["downVoteIds"] as? String else { throw ParsingError() }
        guard let channelId = data["channelId"] as? String else { throw ParsingError() }
        let msgcount = data["msgcount"]
        let subReplyCount = data["subReplyCount"]
        let reportedIds = data["reportedIds"] as? String
        let hidePostsUserIds = data["hidePostsUserIds"] as? String
        let isReplySubTopic = data["isReplySubTopic"] as? Bool
        
        self.subtopic_id = subtopic_id
        self.topic_id = topic_id
        self.subtopic_title = subtopic_title
        self.subtopic_desc = subtopic_desc
        self.difference = difference
        self.subtopic_date = subtopic_date
        self.created_by = created_by
        self.documentId = documentId
        self.upVoteIds = upVoteIds
        self.downVoteIds = downVoteIds
        self.channelId = channelId
        self.msgcount = msgcount != nil ? msgcount as! Int64 : 0
        self.subReplyCount = subReplyCount != nil ? subReplyCount as! Int64 : 0
        self.reportedIds = reportedIds
        self.hidePostsUserIds = hidePostsUserIds
        self.isReplySubTopic = isReplySubTopic
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
      let copy = SubTopic()
        copy.subtopic_id = self.subtopic_id
        copy.topic_id = self.topic_id
        copy.subtopic_title = self.subtopic_title
        copy.subtopic_desc = self.subtopic_desc
        copy.difference = self.difference
        copy.subtopic_date = self.subtopic_date
        copy.created_by = self.created_by
        copy.documentId = self.documentId
        copy.upVoteIds = self.upVoteIds
        copy.downVoteIds = self.downVoteIds
        copy.channelId = self.channelId
        copy.msgcount = self.msgcount
        copy.subReplyCount = self.subReplyCount
        copy.reportedIds = self.reportedIds
        copy.hidePostsUserIds = self.hidePostsUserIds
        copy.isReplySubTopic = self.isReplySubTopic
      return copy
    }
}
