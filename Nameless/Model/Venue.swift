//
//  Venue.swift
//  Herrd
//
//  Created by DG on 28/07/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import Foundation

struct ParsingError:Error { }

class Venue {
    var name : String?
    var address: String?
    var hours: String?
    var amount: Int64?
    var placeId: String?
    var latitude: Double?
    var longitude: Double?
    var openingHours: [String]?
    var channelId: String?
    var msgcount: Int64 = 0
    
    init(data:[String:Any]) throws {
        guard let name = data["name"] as? String else { throw ParsingError() }
        guard let address = data["address"] as? String else { throw ParsingError() }
        guard let hours = data["hours"] as? String else { throw ParsingError() }
        guard let amount = data["amount"] as? Int64 else { throw ParsingError() }
        guard let placeId = data["placeId"] as? String else { throw ParsingError() }
        guard let latitude = data["latitude"] as? Double else { throw ParsingError() }
        guard let longitude = data["longitude"] as? Double else { throw ParsingError() }
        guard let openingHours = data["openingHours"] as? [String] else { throw ParsingError() }
        guard let channelId = data["channelId"] as? String else { throw ParsingError() }
        let msgcount = data["msgcount"]
        
        self.name = name
        self.address = address
        self.hours = hours
        self.amount = amount
        self.placeId = placeId
        self.latitude = latitude
        self.longitude = longitude
        self.openingHours = openingHours
        self.channelId = channelId
        self.msgcount = msgcount != nil ? msgcount as! Int64 : 0
    }
}
