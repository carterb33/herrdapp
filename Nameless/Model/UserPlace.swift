//
//  UserPlace.swift
//  Herrd
//
//  Created by DG on 19/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import Foundation

class UserPlace {
    var UID : String?
    var UserName: String?
    var favPlaceId: String?
    
    
    init(data:[String:Any]) throws {
        guard let UID = data["UID"] as? String else { throw ParsingError() }
        guard let UserName = data["UserName"] as? String else { throw ParsingError() }
        guard let favPlaceId = data["favPlaceId"] as? String else { throw ParsingError() }
        
        self.UID = UID
        self.UserName = UserName
        self.favPlaceId = favPlaceId
    }
}
