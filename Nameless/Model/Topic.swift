//
//  Topic.swift
//  Herrd
//
//  Created by DG on 01/02/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase

class Topic: NSObject {

    var topic_id : Double?
    var topic_title: String?
    var buttonTitle: String?
    var buttonUrl: String?
    var topic_desc: String?
    var difference: Int?
    var topic_date: Timestamp?
    var topic_date_str: String?
    var created_by: String?
    var documentId: String?
    var upVoteIds: String?
    var downVoteIds: String?
    var channelId: String?
    var msgcount: Int64 = 0
    var subTopicsCount: Int64 = 0
    var imageUrl: String?
    var logoUrl: String?
    var videoUrl: String?
    var gifUrl: String?
    var gifId: String?
    var mediaType: String?
    var reportedIds: String?
    var hidePostsUserIds: String?
    var hidePostsUserIdsArr: [String] = []
    var privateChannelRef: [String]?
    var privateMsgCount: [String:Int]?
    var connected_users: [String]?
    
    override init() {
        super.init()
    }
    
    init(data:[String:Any]) throws {
        guard let topic_id = data["topic_id"] as? Double else { throw ParsingError() }
        guard let topic_title = data["topic_title"] as? String else { throw ParsingError() }
        guard let topic_desc = data["topic_desc"] as? String else { throw ParsingError() }
        guard let difference = data["difference"] as? Int else { throw ParsingError() }
        
        guard let created_by = data["created_by"] as? String else { throw ParsingError() }
        guard let documentId = data["documentId"] as? String else { throw ParsingError() }
        guard let upVoteIds = data["upVoteIds"] as? String else { throw ParsingError() }
        guard let downVoteIds = data["downVoteIds"] as? String else { throw ParsingError() }
        guard let channelId = data["channelId"] as? String else { throw ParsingError() }
        let msgcount = data["msgcount"]
        let subTopicsCount = data["subTopicsCount"]
        guard let imageUrl = data["imageUrl"] as? String else { throw ParsingError() }
        let topic_date = data["topic_date"] as? Timestamp
        let topic_date_str = data["topic_date_str"] as? String
        let videoUrl = data["videoUrl"] as? String
        let gifUrl = data["gifUrl"] as? String
        let gifId = data["gifId"] as? String
        let logoUrl = data["logoUrl"] as? String
        let mediaType = data["mediaType"] as? String
        let reportedIds = data["reportedIds"] as? String
        let hidePostsUserIds = data["hidePostsUserIds"] as? String
        let hidePostsUserIdsArr = data["hidePostsUserIdsArr"] as? [String]
        let privateChannelRef = data["privateChannelRef"] as? [String]
        let privateMsgCount = data["privateMsgCount"] as? [String:Int]
        let connected_users = data["connected_users"] as? [String]
        let buttonTitle = data["buttonTitle"] as? String
        let buttonUrl = data["buttonUrl"] as? String
        
        self.topic_id = topic_id
        self.topic_title = topic_title
        self.topic_desc = topic_desc
        self.difference = difference
        self.topic_date = topic_date
        self.topic_date_str = topic_date_str
        self.created_by = created_by
        self.documentId = documentId
        self.upVoteIds = upVoteIds
        self.downVoteIds = downVoteIds
        self.channelId = channelId
        self.msgcount = msgcount as? Int64 ?? 0
        self.subTopicsCount = subTopicsCount as? Int64 ?? 0
        self.imageUrl = imageUrl
        self.videoUrl = videoUrl
        self.gifUrl = gifUrl
        self.gifId = gifId
        self.logoUrl = logoUrl
        self.mediaType = mediaType
        self.reportedIds = reportedIds
        self.hidePostsUserIds = hidePostsUserIds
        self.hidePostsUserIdsArr = hidePostsUserIdsArr ?? []
        self.privateChannelRef = privateChannelRef
        self.privateMsgCount = privateMsgCount
        self.connected_users = connected_users
        self.buttonTitle = buttonTitle
        self.buttonUrl = buttonUrl
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
      let copy = Topic()
        copy.topic_id = self.topic_id
        copy.topic_title = self.topic_title
        copy.topic_desc = self.topic_desc
        copy.difference = self.difference
        copy.topic_date = self.topic_date
        copy.topic_date_str = self.topic_date_str
        copy.created_by = self.created_by
        copy.documentId = self.documentId
        copy.upVoteIds = self.upVoteIds
        copy.downVoteIds = self.downVoteIds
        copy.channelId = self.channelId
        copy.msgcount = self.msgcount
        copy.subTopicsCount = self.subTopicsCount
        copy.imageUrl = self.imageUrl
        copy.logoUrl = self.logoUrl
        copy.videoUrl = self.videoUrl
        copy.gifUrl = self.gifUrl
        copy.gifId = self.gifId
        copy.mediaType = self.mediaType
        copy.reportedIds = self.reportedIds
        copy.hidePostsUserIds = self.hidePostsUserIds
        copy.hidePostsUserIdsArr = self.hidePostsUserIdsArr
        copy.privateChannelRef = self.privateChannelRef
        copy.privateMsgCount = self.privateMsgCount
        copy.connected_users = self.connected_users
        copy.buttonTitle = self.buttonTitle
        copy.buttonUrl = self.buttonUrl
      return copy
    }
    
    
}

extension Topic: DatabaseRepresentation {
    
    var representation: [String : Any] {
        
        return [
            "topic_id": topic_id ?? 0,
            "topic_title": topic_title ?? "",
            "topic_desc": topic_desc ?? "",
            "difference": difference ?? 0,
            "topic_date": topic_date ?? Timestamp(),
            "topic_date_str": topic_date_str ?? "",
            "created_by": created_by ?? "",
            "documentId": documentId ?? "",
            "upVoteIds": upVoteIds ?? "",
            "downVoteIds": downVoteIds ?? "",
            "channelId": channelId ?? "",
            "msgcount": msgcount,
            "subTopicsCount": subTopicsCount,
            "imageUrl": imageUrl ?? "",
            "logoUrl": logoUrl ?? "",
            "videoUrl": videoUrl ?? "",
            "gifUrl": gifUrl ?? "",
            "gifId": gifId ?? "",
            "mediaType": mediaType ?? "",
            "reportedIds": reportedIds ?? "",
            "hidePostsUserIds": hidePostsUserIds ?? "",
            "hidePostsUserIdsArr": hidePostsUserIdsArr,
            "privateChannelRef": privateChannelRef ?? [],
            "privateMsgCount": privateMsgCount ?? [:],
            "connected_users": connected_users ?? [],
            "buttonTitle": buttonTitle ?? "",
            "buttonUrl": buttonUrl ?? ""
        ]
    }
    
}
