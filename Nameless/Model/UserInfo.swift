//
//  UserInfo.swift
//  Herrd
//
//  Created by DG on 15/02/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import Foundation
import FirebaseFirestore

class UserInfo {
    let id: String?
    var nickName: String?
    var device_id: String?
    var blockedUserIds: [String]?
    var blockedSubTopicUserIds: [String]?
    
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()

        let nickName = data["nickName"] as? String
        let device_id = data["device_id"] as? String
        let blockedUserIds = data["blockedUserIds"] as? [String]
        let blockedSubTopicUserIds = data["blockedSubTopicUserIds"] as? [String]
        
        self.id = document.documentID
        self.nickName = nickName
        self.device_id = device_id
        self.blockedUserIds = blockedUserIds
        self.blockedSubTopicUserIds = blockedSubTopicUserIds
    }
    
    init(data:[String:Any]) throws {
        let nickName = data["nickName"] as? String
        let device_id = data["device_id"] as? String
        let blockedUserIds = data["blockedUserIds"] as? [String]
        let blockedSubTopicUserIds = data["blockedSubTopicUserIds"] as? [String]
        self.id = nil
        self.nickName = nickName
        self.device_id = device_id
        self.blockedUserIds = blockedUserIds
        self.blockedSubTopicUserIds = blockedSubTopicUserIds
    }
}
