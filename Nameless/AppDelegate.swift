//
//  AppDelegate.swift
//  Herrd
//
//  Created by Carter  Beaulieu on 7/6/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import IQKeyboardManagerSwift
import GiphyUISDK
import MBProgressHUD
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var currentLocation: CLLocation?
    var currentCity: String?
    var isAdminUser = false
    var currentUserDetails: UserInfo?
    var blockedUserIdsArr: [String]?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        self.getAppSettings()
        GMSPlacesClient.provideAPIKey(Key.Google.Places)
        Giphy.configure(apiKey: Key.GIPHY.SdkKey)
        Messaging.messaging().delegate = self
        self.getRegisterForRemoteNotifications(application)
        IQKeyboardManager.shared.enable = true
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.sharedInstance().start(completionHandler: nil)
//        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "ef4b87f9ad09b792a97ef0a485e72bd8" ];
        AppStoreUpdate.shared.showAppStoreVersionUpdateAlert(isForceUpdate: false)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
            notifications.forEach { (notification) in
                if let tmpNotificationData = notification.request.content.userInfo as? [String:Any]
                {
                    DispatchQueue.main.sync {
                        self.updateMsgCount(tmpNotificationData)
                    }
                }
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0  // Remove badge count when app became active
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
    }
    
    func getAppSettings() {
        FirebaseUtility().getAppSettings { (error, doc) in
            if let err = error {
                print("Got error while reading app settings",err)
            }
        }
    }
    
    func getLoggedInUserDetails() {
        guard let currentUid = Auth.auth().currentUser?.uid else {
            return
        }
        
        FirebaseUtility().getUserDetails(userId: currentUid) { (user, error) in
            if let err = error {
                print("Got error while fetching current user info.",err)
                return
            }
            self.currentUserDetails = user
        }
    }
    
    func getRegisterForRemoteNotifications(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {

        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        if let deviceToken = UserDefaults.standard.object(forKey: "deviceId") as? String
        {
            if deviceToken != fcmToken  // Check device id mis match
            {
                addOrUpdateDeviceId(fcmToken: fcmToken ?? "") // Add or update device id on firebase
            }
        }
        else
        {
            addOrUpdateDeviceId(fcmToken: fcmToken ?? "") // Add or update device id on firebase
        }
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    // Add device id in user default and Add or update device id on firebase
    func addOrUpdateDeviceId(fcmToken : String) {
        UserDefaults.standard.set(fcmToken, forKey: "deviceId")
        UserDefaults.standard.synchronize()
        
        if let uid = Auth.auth().currentUser?.uid
        {
            FirebaseUtility().addOrUpdateDeviceTokenWithUserId(userId: uid, deviceToken: fcmToken) { (isAdded, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                }
            }
        }
    }
    
    
    // MARK: - UNUserNotificationCenter delegate
    // Call when notification come
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Function: \(#function), line: \(#line)")
        if let tmpNotificationData = notification.request.content.userInfo as? [String:Any]
        {
            //Check is from private message
            if (tmpNotificationData["isFromPrivateMsg"] != nil) == true {
                self.updateMsgCount(tmpNotificationData)
            }
        }
        //Mark notification as read
        center.removeDeliveredNotifications(withIdentifiers: [notification.request.identifier])
        
//        guard let uid = Auth.auth().currentUser?.uid else {
//            return
//        }
//
////         Check if app is in foreground
//        let state = UIApplication.shared.applicationState
//        if state == .active
//        {
//            if var tmpNotificationData = notification.request.content.userInfo as? [String:Any]
//            {
//
//                //Check is from private message
//                if (tmpNotificationData["isFromPrivateMsg"] != nil) == true {
//
//                    guard let tabbar = self.window?.rootViewController as? UITabBarController else {
//                        return
//                    }
//
//                    guard let selectedTab = tabbar.selectedViewController as? UINavigationController else {
//                        return
//                    }
//
//                    if let isSameClass = selectedTab.viewControllers.last?.isKind(of: ContainerViewController.self), isSameClass == true {
//                        //User already on chat screen so no further data fetch
//                        return
//                    }
//
//                    //Convert Data in specific types
//                    tmpNotificationData["topic_id"] = Double(tmpNotificationData["topic_id"] as! String)
//
//                    // Fetch particular topic using topic id
//                    TopicFirebaseUtility().getTopicsByTopicId(tmpNotificationData["topic_id"]) { (topic, error) in
//                        if let error = error {
//                            print("Error getting documents: \(error)")
//                        } else {
//                            if let topic = topic {
//                                if uid == topic.created_by {
//                                    self.pushtoPrivateChatList(topic)
//                                } else {
//                                    //Push to container view
//                                    self.getTopicPrivateChannel(topic)
//                                }
//                            }
//                        }
//                    }
//                }
//                // Check is reply on comment
//                else if tmpNotificationData["isReplySubTopic"] as! String == "true"
//                {
//                    // Check currently displayed sub topic and sub topic match with notification data, then reload data
//                    if let replySubTopicVC = UIApplication.shared.topViewController() as? ReplySubTopicViewController, let subTopic = replySubTopicVC.selectedSubTopic,  subTopic.subtopic_id == Double(tmpNotificationData["topic_id"] as! String) {
//                        replySubTopicVC.getSubTopics()
//                    }
//                }
//                else
//                {
//                    // Check currently displayed topic and topic match with notification data, then reload data
//                    if let subTopicVC = UIApplication.shared.topViewController() as? SubTopicViewController, let topic = subTopicVC.selectedTopic,  topic.topic_id == Double(tmpNotificationData["topic_id"] as! String) {
//                        subTopicVC.getSubTopics()
//                    }
//                }
//            }
//        }
        
        completionHandler([.alert,.sound,.badge]);
    }
    
    func updateMsgCount(_ tmpNotificationData:[String:Any]) {
        guard let tabbar = self.window?.rootViewController as? UITabBarController else {
            return
        }
        if let navController = tabbar.viewControllers?[0] as? UINavigationController {
            if let topicNewsVC = navController.viewControllers.first as? TopicNewsViewController {
                if var badgeCount = Int(topicNewsVC.lblBadgeCount.text ?? "0") {
                    badgeCount = badgeCount + 1
                    topicNewsVC.lblBadgeCount.text = "\(badgeCount)"
                    topicNewsVC.lblBadgeCount.isHidden = badgeCount > 0 ? false : true
                }
            }
            
            if let topicChatListViewController =  navController.viewControllers.last as? TopicChatListViewController {
                //Convert Data in specific types
                if let topic_id = Double(tmpNotificationData["topic_id"] as! String) {
                    topicChatListViewController.updateTableMsgCount(topic_id)
                }
            }
        }
    }
    
    func pushtoPrivateChatList(_ topic: Topic) {
        
        guard let tabbar = self.window?.rootViewController as? UITabBarController else {
            return
        }
        
        guard let selectedTab = tabbar.selectedViewController as? UINavigationController else {
            return
        }
        
        if let isSameClass = selectedTab.viewControllers.last?.isKind(of: SubTopicViewController.self), isSameClass == true {
            //Check if sub topic with private list
            if let subTopicVC = selectedTab.viewControllers.last as? SubTopicViewController, subTopicVC.isFromPrivateChatList {
                //User already on chat screen so no further data fetch
                
                if let privateNavVC = subTopicVC.children.last as? UINavigationController  {
                    if let pricateChatListVC = privateNavVC.viewControllers.last as? TopicPrivateChatListViewController {
                        pricateChatListVC.getAllChannels()
                        return
                    }
                }
            }
        }
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let subTopicVC = storyboard.instantiateViewController(withIdentifier: "SubTopicViewController") as? SubTopicViewController {
            subTopicVC.selectedTopic = topic
            subTopicVC.adminList = []
            subTopicVC.isFromPrivateChatList = true
            selectedTab.pushViewController(subTopicVC, animated: true)
        }
    }
    
    func getTopicPrivateChannel(_ topic: Topic) {
        //        MBProgressHUD.showAdded(to: self.view, animated: true)
        guard let currentId = Auth.auth().currentUser?.uid else {
            return
        }
        let channelRef = CommonUtility.getChannelRefFromTopicRefList(topic.privateChannelRef ?? [],
                                                                     currentUid: currentId)
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .whereField("channelRef", isEqualTo: channelRef)
            .getDocuments(completion: { (querySnapshot, err) in
                //                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let document = querySnapshot!.documents.first{
                        var channel = document.data()
                        channel["id"] = document.documentID
                        //Push to container view
                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        if let containerVC = storyboard.instantiateViewController(withIdentifier: "ContainerViewController") as? ContainerViewController {
                            guard let currentUser = Auth.auth().currentUser else {
                                return
                            }
                            guard let selectedChannel = Channel(data: channel) else {
                                return
                            }
                            
                            FirebaseUtility().getUserDetails(userId: topic.created_by!) { (user, error) in
                                var senderNickName = ""
                                if user != nil, user?.nickName != nil{
                                    senderNickName = user?.nickName ?? ""
                                }
                            
                                containerVC.selectedChannel = selectedChannel
                                containerVC.currentUser = currentUser
                                containerVC.topic = topic
                                if let topic_title = topic.topic_title {
                                    containerVC.title = topic_title.count > Constants.chatTitleMaxLength ? String(topic_title.prefix(Constants.chatTitleMaxLength)) + "..." : topic_title
                                }
                                containerVC.senderDisplayName = senderNickName
                                containerVC.selectedFirebaseCollection = FirebaseConst.Collections.TopicChannels
                                //User Unread counts
                                containerVC.userUnreadCounts = channel[currentUser.uid] as? Int ?? 0
                                
                                guard let tabbar = self.window?.rootViewController as? UITabBarController else {
                                    return
                                }
                                
                                guard let selectedTab = tabbar.selectedViewController as? UINavigationController else {
                                    return
                                }
                                selectedTab.show(containerVC, sender: nil)
                                self.setNavigationBarColor(selectedTab)
                            }
                        }
                    }
                }
            })
    }
    
    func setNavigationBarColor(_ selectedTab: UINavigationController) {
        let nav = selectedTab.navigationBar
        nav.barStyle = UIBarStyle.black
        nav.tintColor = UIColor.white
        nav.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        selectedTab.navigationBar.setBackgroundImage(UIImage(), for: .default)
        selectedTab.navigationBar.shadowImage = UIImage()
        selectedTab.navigationBar.isTranslucent = false
        selectedTab.view.backgroundColor = .black
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Function: \(#function), line: \(#line)")
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        if var tmpNotificationData = response.notification.request.content.userInfo as? [String:Any]
        {
            //Check is from private message
            if (tmpNotificationData["isFromPrivateMsg"] != nil) == true {
                
                guard let tabbar = self.window?.rootViewController as? UITabBarController else {
                    return
                }
                
                guard let selectedTab = tabbar.selectedViewController as? UINavigationController else {
                    return
                }
                
                if let isSameClass = selectedTab.viewControllers.last?.isKind(of: ContainerViewController.self), isSameClass == true {
                    //User already on chat screen so no further data fetch
                    return
                }
                
                //Convert Data in specific types
                tmpNotificationData["topic_id"] = Double(tmpNotificationData["topic_id"] as! String)
                
                // Fetch particular topic using topic id
                TopicFirebaseUtility().getTopicsByTopicId(tmpNotificationData["topic_id"]) { (topic, error) in
                    if let error = error {
                        print("Error getting documents: \(error)")
                    } else {
                        if let topic = topic {
                            if uid == topic.created_by {
                                self.pushtoPrivateChatList(topic)
                            } else {
                                //Push to container view
                                self.getTopicPrivateChannel(topic)
                            }
                        }
                    }
                }
            }
            // Check is reply on comment
            else if tmpNotificationData["isReplySubTopic"] as! String == "true"
            {
                // Check currently displayed sub topic and sub topic match with notification data, then reload data
                if let replySubTopicVC = UIApplication.shared.topViewController() as? ReplySubTopicViewController, let subTopic = replySubTopicVC.selectedSubTopic,  subTopic.subtopic_id == Double(tmpNotificationData["topic_id"] as! String) {
                    replySubTopicVC.getSubTopics()
                } else {    // Redirect to sub topic screen
                    if let replySubTopicVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReplySubTopicViewController") as? ReplySubTopicViewController
                    {
                        replySubTopicVC.subTopicId = Double(tmpNotificationData["topic_id"] as! String) ?? 0 // Pass sub topic id
                        
                        guard let tabBarVC = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController else { return }
                        
                        if let currentNavController = tabBarVC.selectedViewController as? UINavigationController {
                            currentNavController.pushViewController(replySubTopicVC, animated: true)
                        }
                    }
                }
            }
            else
            {
                // Check currently displayed topic and topic match with notification data, then reload data
                if let subTopicVC = UIApplication.shared.topViewController() as? SubTopicViewController, let topic = subTopicVC.selectedTopic,  topic.topic_id == Double(tmpNotificationData["topic_id"] as! String) {
                    subTopicVC.getSubTopics()
                } else {    // Redirect to topic screen
                    if let subTopicVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubTopicViewController") as? SubTopicViewController
                    {
                        subTopicVC.topicId = Double(tmpNotificationData["topic_id"] as! String) ?? 0 // Pass topic id
                        guard let tabBarVC = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController else { return }
                        
                        if let currentNavController = tabBarVC.selectedViewController as? UINavigationController {
                            currentNavController.pushViewController(subTopicVC, animated: true)
                        }
                    }
                }
            }
        }
        
        completionHandler()
    }
    
}

