//
//  ViewController.swift
//  Nameless
//
//  Created by Carter  Beaulieu on 7/6/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseUI
import GoogleSignIn
import GooglePlaces
import MBProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var anonymousSwitch: UISwitch!
    @IBOutlet weak var overallButtonPressed: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var refreshControl = UIRefreshControl()
    
    var isNewPlaceAdded = false
    
    var isAdminUser = false
    
    //Filter array for search data
    var filteredVenueList : [Venue] = []
    var userChatCountList : [[String:Any]] = []
    var venueList : [Venue] = [Venue]()
    
//    var venueList : [Venue] = [Venue]() {
//        didSet {
//            tableView.reloadData()
//        }
//    }
    
    var authUI: FUIAuth!
    var selectedPlaceId : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let layer = CAGradientLayer()
        layer.frame = view.bounds
        layer.colors = [UIColor.yellow.cgColor, UIColor.green.cgColor]
        layer.startPoint = CGPoint(x: 0,y: 0)
        layer.endPoint = CGPoint(x: 1,y: 1)
        //        searchBar.layer.addSublayer(layer)
        
        searchBar.barTintColor = UIColor.clear
        searchBar.backgroundColor = UIColor.clear
        searchBar.isTranslucent = true
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        tableView.rowHeight = 125
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: -21, width: 230, height: 80))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: -21, width: 230, height: 80))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "fullLogo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
        
        authUI = FUIAuth.defaultAuthUI()
        
        // You need to adopt a FUIAuthDelegate protocol to receive callback
        authUI?.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        
        if authUI.auth?.currentUser != nil {
            self.checkAdminUser()
            self.getFavPlace()
        }
        
        addPullToRefresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        signIn()
    }
    
    func getFavPlace() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        Firestore.firestore().collection("UserPlaces")
            .whereField("UID", isEqualTo: uid ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first?.data(){
                        print("User Data:",data)
                        self.selectedPlaceId = data["favPlaceId"] as! String
                    }else{
                        print("No Data")
                    }
                    self.getAllVenuesInSingleCall()
                }
            })
        
    }
    
    func checkAdminUser() {
        
        let user = Auth.auth().currentUser
        
        let uid = user?.uid
        Firestore.firestore().collection("Admin")
            .whereField("UID", isEqualTo: uid ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first?.data(){
                        print("User Data:",data)
                        self.isAdminUser = true
                    }else{
                        print("No Data")
                    }
                    //                    self.getVenueList()
                    self.getAllVenuesInSingleCall()
                }
            })
        
    }
    
    func getAllVenuesInSingleCall() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        Firestore.firestore().collection("VenueList")
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.venueList = []
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    querySnapshot!.documents.forEach {
                        do {
                            let documentValue = $0.data()
                            let venues = try Venue(data: documentValue)
                            self.venueList.append(venues)
                        } catch {
                            print(error)
                        }
                    }
                    self.venueList.sort(by: {
                        $0.amount! > $1.amount!
                    })
                    
                    
                    //fetch user chat count list
                    FirebaseUtility().getUserChatReadCount { (userChatList, error) in
                        if let error = error {
                            print("Error getting documents: \(error)")
                        } else {
                            print("User Chat List",userChatList)
                            self.userChatCountList = userChatList ?? []
                            
                            self.tableView.reloadData()
                            if self.refreshControl.isRefreshing {
                                self.refreshControl.endRefreshing()
                            }
                            if self.isNewPlaceAdded {
                                self.scrollTableViewToBottom()
                            }
                        }
                    }
                }
            })
    }
    
    func updatePlaceCount(_ selectedVenue : Venue) {
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: selectedVenue.placeId ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "amount": selectedVenue.amount ?? 0
                        ])
                }
        }
    }
    
    func removePlaceFromUserFav(_ selectedPlaceId : String) {
        
        Firestore.firestore().collection("UserPlaces")
            .whereField("favPlaceId", isEqualTo: selectedPlaceId )
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    querySnapshot!.documents.forEach {
                        $0.reference.updateData([
                            "favPlaceId": ""
                        ])
                    }
                }
        }
    }
    
    func updatePlaceWithChannelId(_ channelId : String, selectedVenue : Venue) {
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: selectedVenue.placeId ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "channelId": channelId
                        ])
                }
        }
    }
    
    func deleteFavPlace() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection("UserPlaces").document(uid!).delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                print("Document successfully removed!")
            }
        }
    }
    
    func addPullToRefresh() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to Sort & Refresh Venues")
        refreshControl.backgroundColor = UIColor(red:0.09, green:0.09, blue:0.09, alpha:1.0)
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(ViewController.refreshAndSortListData(sender:)), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        if selectedPlaceId.count > 0 {
            if sender.isOn {
                //Send Anonymous name
                addUpdateFavoritePlace(selectedPlaceId)
            } else {
                //Send Actual Name
            }
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.getAllVenuesInSingleCall()
    }
    
    //SIGN IN FUNCTION
    func signIn() {
        let providers: [FUIAuthProvider] = [
            FUIGoogleAuth()
        ]
        if authUI.auth?.currentUser == nil {
            self.authUI?.providers = providers
            present(authUI.authViewController(), animated: true, completion: nil)
        } else {
            tableView.isHidden = false
        }
    }
    
    //LOGGING OUT A USER
    @IBAction func logOut(_ sender: UIBarButtonItem) {
        do {
            try authUI!.signOut()
            print("^^ Successfully signed out")
            tableView.isHidden = true
            self.selectedPlaceId = ""
            self.isAdminUser = false
            self.venueList = []
            signIn()
        } catch {
            tableView.isHidden = true
            print("** ERROR: Couldn't sign out")
        }
    }
    
    //INFO BUTTON FOR ANONYMOUS MODE
    @IBAction func infoButton(_ sender: UIButton) {
        func showAlert(title: String, message: String) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        showAlert(title: "Anonymous Mode", message: "When enabled, your actual name will not appear on any guest list or in any chat rooms 👻 ")
    }
    
    //ADDING A NEW VENUE LOCATION WITH GOOGLE PLACES API
    @IBAction func addVenuePressed(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

//TABLEVIEW EXTENSION
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.searchBar.text?.isEmpty)! {
            return venueList.count
        }else{
            return filteredVenueList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MainScreenCell
        
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[indexPath.row]
        } else {
            venue = self.filteredVenueList[indexPath.row]
        }
        cell.venueName?.text = venue?.name
        cell.venueAddress?.text = venue?.address
        cell.venueHours?.text = venue?.hours
        let amount = venue?.amount
        cell.peopleAmount?.text = "\(amount ?? 0)"
        cell.goingButton.tag = indexPath.row
        cell.bubbleButton.tag = indexPath.row
        cell.placeMap.tag = indexPath.row
        cell.btnGuestList.tag = indexPath.row
        
        if selectedPlaceId == venue?.placeId {
            cell.goingButton.isSelected = true
        } else {
            cell.goingButton.isSelected = false
        }
        cell.goingButton.addTarget(self, action: #selector(goingButtonPressed(_:)), for: .touchUpInside)
        cell.bubbleButton.addTarget(self, action: #selector(chatIconPressed(_:)), for: .touchUpInside)
        cell.placeMap.addTarget(self, action: #selector(btnPLaceMapPresses(_:)), for: .touchUpInside)
        cell.btnGuestList.addTarget(self, action: #selector(btnGuestListPressed (_:)), for: .touchUpInside)
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let uniqueId = uid! + "~" + venue!.channelId!

        let foundItems = self.userChatCountList.filter { ($0["uniqueId"] as! String).contains(uniqueId) }
        
        if foundItems.count > 0 {
            let userChatData = foundItems[0]
            let userChatCount = userChatData["chatCount"] as! Int64
            let msgCount = venue!.msgcount
        

            if  userChatCount < msgCount {
                cell.bubbleButton.setImage(UIImage.init(named: "blackRedChat"), for: .normal)
            } else {
                cell.bubbleButton.setImage(UIImage.init(named: "chatRegular"), for: .normal)
            }
        }
        
        if venue?.openingHours?.count ?? 0 > 0 {
            let openingHours = venue?.openingHours
            let weekday = Calendar.current.dateComponents([.weekday], from: Date()).weekday ?? 1
            var dayOfWeek = (weekday + 7 - Calendar.current.firstWeekday) % 7
            //            let dayTime = openingHours?[weekday - 1]
            //            let dayArray = dayTime?.components(separatedBy: ":")
            //            weekday = 8 - weekday
            if dayOfWeek == 0 {
                dayOfWeek = 7
            }
            cell.venueHours.text = "\(openingHours?[dayOfWeek - 1] ?? "")"
        } else {
            cell.venueHours.text = "Hours Not Available"
        }
        
        return cell
    }
    
    func getAttchedChannel(channelId : String){
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isAdminUser
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if isAdminUser {
            if editingStyle == .delete {
                //Delete Action on firebse
                var venue : Venue?
                if (self.searchBar.text?.isEmpty)! {
                    venue = self.venueList[indexPath.row]
                } else {
                    venue = self.filteredVenueList[indexPath.row]
                }
                let placeId = venue?.placeId
                let channelId = venue?.channelId
                let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Removing Venue..."
                
                Firestore.firestore().collection("VenueList")
                    .whereField("placeId", isEqualTo: placeId ?? "")
                    .getDocuments(completion: { (querySnapshot, err) in
                        
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            if let data = querySnapshot!.documents.first{
                                print("PLACE Data:",data.documentID)
                                
                                loadingNotification.label.text = "Venue Removed"
                                if self.selectedPlaceId == placeId {
                                    self.selectedPlaceId = ""
                                }
                                self.removePlaceFromUserFav(placeId!)
                                Firestore.firestore().collection("VenueList").document(data.documentID).delete() { err in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    if let err = err {
                                        print("Error removing document: \(err)")
                                    } else {
                                        print("Place Document successfully removed!")
                                        
                                        tableView.beginUpdates()
                                        self.venueList.remove(at:indexPath.row)
                                        tableView.deleteRows(at: [indexPath], with: .automatic)
                                        tableView.endUpdates()
                                        
                                        //Delete Chat & Place
                                        if channelId?.count ?? 0 > 0 {
                                            Firestore.firestore().collection("VenueChannels").document(channelId ?? "").delete() { err in
                                                if let err = err {
                                                    print("Error removing document: \(err)")
                                                } else {
                                                    print("Channel Document successfully removed!")
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                print("No Data")
                            }
                        }
                    })
            }
        }
    }
    
    @IBAction func chatIconPressed(_ sender: UIButton) {
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        } else {
            venue = self.filteredVenueList[sender.tag]
        }
        
        getOrCreateChannel(place: venue!, index: sender.tag)
    }
    
    @IBAction func btnGuestListPressed(_ sender: UIButton) {
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        } else {
            venue = self.filteredVenueList[sender.tag]
        }
        let guestList = self.storyboard?.instantiateViewController(withIdentifier: "GuestListViewController") as! GuestListViewController
        guestList.modalPresentationStyle = .overCurrentContext
        guestList.place = venue
        self.present(guestList, animated: true, completion: nil)
    }
    
    @IBAction func btnPLaceMapPresses(_ sender: UIButton) {
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        } else {
            venue = self.filteredVenueList[sender.tag]
        }
        let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "PlaceMapViewController") as! PlaceMapViewController
        mapVC.place = venue
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    func addChannelInUserChat(place : Venue, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        Firestore.firestore().collection("UserChatReadCount")
            .whereField("uid", isEqualTo: currentUser.uid)
            .getDocuments(completion: { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard let channel = Channel(document: data) else {
                            return
                        }
                    }else{
                        print("No Data")
                        let channel = Channel(name: place.name!)
                        Firestore.firestore().collection("VenueChannels").addDocument(data: channel.representation){ error in
                            if let e = error {
                                print("Error saving venue channel: \(e.localizedDescription)")
                            }
                        }
                    }
                }
            })
    }
    
    func getOrCreateChannel(place : Venue, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        Firestore.firestore().collection("VenueChannels")
            .whereField("name", isEqualTo: place.name!)
            .getDocuments(completion: { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard let channel = Channel(document: data) else {
                            return
                        }
                        if (self.searchBar.text?.isEmpty)! {
                            self.venueList[index].channelId = channel.id
                        } else {
                            self.filteredVenueList[index].channelId = channel.id
                        }
                        self.updatePlaceWithChannelId(channel.id!, selectedVenue: place)
                        //Add in User Chat Count
                        self.addOrUpdateChatCount(channel.id!, selectedPlace: place)
                        let vc = ChatViewController(user: currentUser, channel: channel, anonymousMode: self.anonymousSwitch.isOn)
                        vc.place = place
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        print("No Data")
                        let channel = Channel(name: place.name!)
                        Firestore.firestore().collection("VenueChannels").addDocument(data: channel.representation){ error in
                            if let e = error {
                                print("Error saving venue channel: \(e.localizedDescription)")
                            }
                        }
                        
                    }
                }
            })
    }
    
    //PRESSING THE BUTTON
    @IBAction func goingButtonPressed(_ sender: UIButton) {
        
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        }else{
            venue = self.filteredVenueList[sender.tag]
        }
        
        let id = venue?.placeId
        
        if selectedPlaceId.count > 0 {
            
            if selectedPlaceId == id {
                deleteFavPlace()
                if (self.searchBar.text?.isEmpty)! {
                    var amount = venueList[sender.tag].amount ?? 0
                    if amount > 0 {
                        amount -= 1
                    }
                    venueList[sender.tag].amount = amount
                    updatePlaceCount(venueList[sender.tag])
                } else {
                    var amount = filteredVenueList[sender.tag].amount ?? 0
                    if amount > 0 {
                        amount -= 1
                    }
                    filteredVenueList[sender.tag].amount = amount
                    updatePlaceCount(filteredVenueList[sender.tag])
                }
                selectedPlaceId = ""
            } else {
                //Update Old One
                if (self.searchBar.text?.isEmpty)! {
                    if let prevIndex = venueList.index(where: {$0.placeId == selectedPlaceId}) {
                        var amount = venueList[prevIndex].amount ?? 0
                        if amount > 0 {
                            amount -= 1
                        }
                        venueList[prevIndex].amount = amount
                        updatePlaceCount(venueList[prevIndex])
                    }
                } else {
                    if let prevIndex = filteredVenueList.index(where: {$0.placeId == selectedPlaceId}) {
                        var amount = filteredVenueList[prevIndex].amount ?? 0
                        if amount > 0 {
                            amount -= 1
                        }
                        filteredVenueList[prevIndex].amount = amount
                        updatePlaceCount(filteredVenueList[prevIndex])
                    }
                }
                
                //Add New One
                selectedPlaceId = id ?? ""
                if (self.searchBar.text?.isEmpty)! {
                    var amount = venueList[sender.tag].amount ?? 0
                    amount += 1
                    venueList[sender.tag].amount = amount
                    updatePlaceCount(venueList[sender.tag])
                } else {
                    var amount = filteredVenueList[sender.tag].amount ?? 0
                    amount += 1
                    filteredVenueList[sender.tag].amount = amount
                    updatePlaceCount(filteredVenueList[sender.tag])
                }
                addUpdateFavoritePlace(id!)
            }
        } else {
            selectedPlaceId = id ?? ""
            if (self.searchBar.text?.isEmpty)! {
                var amount = venueList[sender.tag].amount ?? 0
                amount += 1
                venueList[sender.tag].amount = amount
                updatePlaceCount(venueList[sender.tag])
            } else {
                var amount = filteredVenueList[sender.tag].amount ?? 0
                amount += 1
                filteredVenueList[sender.tag].amount = amount
                updatePlaceCount(filteredVenueList[sender.tag])
            }
            addUpdateFavoritePlace(id!)
        }
        
        tableView.reloadData()
    }
    
    func addUpdateFavoritePlace(_ placeId: String) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        var userName = ""
        
        if self.anonymousSwitch.isOn {
            userName = "Anonymous User"
        } else {
            userName = user?.displayName ?? ""
        }
        
        Firestore.firestore().collection("UserPlaces").document("\(uid ?? "")").setData([
            "UID": uid!,
            "UserName":userName,
            "favPlaceId" : placeId,
        ]) { (err) in
        }
        
    }
    
}

extension NSDate {
    
    func dayOfTheWeek() -> String? {
        let weekdays = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ]
        
        let weekday = Calendar.current.dateComponents([.weekday], from: self as Date).weekday
        return weekdays[weekday ?? 1 - 1]
    }
}

extension ViewController : UISearchBarDelegate {
    
    // This method updates filteredVenueList based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        // When there is no text, filteredVenueList is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        self.filteredVenueList = (searchBar.text?.isEmpty)! ? venueList : venueList.filter({(aObject: Venue) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return (aObject.name?.lowercased().range(of: searchText.lowercased()) != nil)
        })
        tableView.reloadData()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
    }
    
}

//AUTHENTICATION EXTENSION

extension ViewController: FUIAuthDelegate {
    
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        // other URL handling goes here.
        return false
    }
    
    
    func showRestrictionMessage() {
        let controller = UIAlertController(title: "Attention", message: "You must sign in with a valid BC email", preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.signIn()
        }))
        present(controller, animated: true)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        // handle user and error as necessary
        if let user = user {
            let approvedDomains = ["bc.edu"]
            if let userDomain = user.email?.components(separatedBy: "@").last {
                if approvedDomains.contains(userDomain) == false && user.email != "herrdtester@gmail.com" && user.email != "dhaneshgosai@gmail.com" {
                    try? Auth.auth().signOut()
                    showRestrictionMessage()
                    return
                }
            }
            
            if UserDefaults.standard.bool(forKey: "isAccessCodeVerified") {
                tableView.isHidden = false
                print("*** we signed in with the user \(user.email ?? "unknown email") ")
                self.checkAdminUser()
                getFavPlace()
            } else {
                
                let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "TermViewController") as! TermViewController
                welcomeVC.modalPresentationStyle = .overCurrentContext
                self.present(welcomeVC, animated: true, completion: {
                    
                })
            }
        }
    }
    
    func showTermScreen() {
        
        let termVC = self.storyboard?.instantiateViewController(withIdentifier: "TermViewController") as! TermViewController
        termVC.modalPresentationStyle = .overCurrentContext
        self.present(termVC, animated: true, completion: {
            
        })
    }
    
    func showHelpScreen() {
        
        let guestList = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        guestList.modalPresentationStyle = .overCurrentContext
        self.present(guestList, animated: true, completion: {
            self.tableView.isHidden = false
            self.checkAdminUser()
            self.getFavPlace()
        })
    }
    
    func authPickerViewController(forAuthUI authUI: FUIAuth) -> FUIAuthPickerViewController {
        
        let loginViewController = FUIAuthPickerViewController(authUI: authUI)
        
        //loginViewController.view.backgroundColor = UIColor.black
        
        let marginInsets: CGFloat = 16;         let imageHeight: CGFloat = 225
        let imageY = self.view.center.y - imageHeight
        let logoFrame = CGRect(x: self.view.frame.origin.x + marginInsets, y: imageY, width: self.view.frame.width - (marginInsets*2), height: imageHeight)
        let logoImageView = UIImageView(frame: logoFrame)
        logoImageView.image = UIImage(named: "fullLogo")
//        logoImageView.backgroundColor = UIColor.black
        logoImageView.layer.cornerRadius = 15.0
        logoImageView.contentMode = .scaleAspectFit
        
//        let logoBackground = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//        logoBackground.backgroundColor = .black
        
       
        //fix this once Firebase UI releases the dark mode support for sign-in
        
        loginViewController.view.subviews[0].backgroundColor = .black
        loginViewController.view.subviews[0].subviews[0].backgroundColor = .black
        
        loginViewController.view.addSubview(logoImageView)
    
        return loginViewController
    }
    
}

//GOOGLE PLACES EXTENSION

extension ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Adding New Place..."
        
        
        let newPlace : [String : Any] = ["name": place.name,
                                         "address": place.formattedAddress ?? "",
                                         "hours": "Not available",
                                         "amount": 0,
                                         "placeId": place.placeID,
                                         "coordinate": place.coordinate,
                                         "openingHours": place.openingHours]
        addNewPlaceInFireStore(newPlace)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func addNewPlaceInFireStore(_ newPlace: [String:Any]) {
        
        let user = Auth.auth().currentUser
        
        let uid = user?.uid
        let userName = user?.displayName
        let currentBackgroundDate = NSDate()
        
        let cordinate = newPlace["coordinate"] as? CLLocationCoordinate2D
        let latitude = cordinate?.latitude
        let longitude = cordinate?.longitude
        
        let openingHrs = newPlace["openingHours"] as? GMSOpeningHours
        let openingTimeArray = openingHrs?.weekdayText
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: newPlace["placeId"] as! String )
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first?.data(){
                        let placename = data["name"] as! String
                        let alert = UIAlertController(title: "Alert", message: "\(placename) is already added in Venue list. Please use search and find this place from the list via search.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                                //test
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        print("No Data")
                        Firestore.firestore().collection("VenueList").document("\(currentBackgroundDate)").setData([
                            
                            "UID": uid!,
                            "UserName":userName ?? "",
                            "name" : newPlace["name"] as? String ?? "",
                            "address" : newPlace["address"] as? String ?? "",
                            "hours" : newPlace["hours"] as? String ?? "",
                            "amount": 0,
                            "placeId" : newPlace["placeId"] as! String,
                            "latitude" : latitude,
                            "longitude" : longitude,
                            "openingHours": openingTimeArray ?? [],
                            "channelId": "",
                            "msgcount": 0,
                        ]) { (err) in
                            self.getAllVenuesInSingleCall()
                            self.isNewPlaceAdded = true
                        }
                    }
                }
            })
    }
    
    func addOrUpdateChatCount(_ channelId: String, selectedPlace: Venue) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let uniqueId = uid! + "~" + channelId
        
        let currentBackgroundDate = NSDate()
        
        Firestore.firestore().collection("UserChatReadCount")
        .whereField("uniqueId", isEqualTo: uniqueId )
        .getDocuments(completion: { (querySnapshot, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if let data = querySnapshot!.documents.first?.data(){
                    print("User Chat Count already there")
                }else{
                Firestore.firestore().collection("UserChatReadCount").document("\(currentBackgroundDate)").setData([
                        "uniqueId": uniqueId,
                        "chatCount": selectedPlace.msgcount
                    ]) { (err) in
                    }
                }
            }
        })
    }
    
    func scrollTableViewToBottom(){
        DispatchQueue.main.async {
            if (self.searchBar.text?.isEmpty)! {
                let indexPath = IndexPath(row: self.venueList.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }else {
                let indexPath = IndexPath(row: self.filteredVenueList.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
            self.isNewPlaceAdded = false
        }
    }
    
}


