//
//  HelpCell.swift
//  Herrd
//
//  Created by DG on 21/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit

class HelpCell: UITableViewCell {

    @IBOutlet weak var lblDetailText: UILabel!
    
    @IBOutlet weak var helpicon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
