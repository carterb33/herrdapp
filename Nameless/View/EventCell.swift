//
//  EventCell.swift
//  Herrd
//
//  Created by DG on 11/01/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var eventDay: UILabel!
    @IBOutlet weak var cellEvent: UIView!
    @IBOutlet weak var goingButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        eventDay.textColor = AppColor.CommonColor.secondaryColor
        lblEventName.textColor = AppColor.CommonColor.postColor
        cellEvent.backgroundColor = AppColor.CommonColor.cellColor
        goingButton.layer.cornerRadius = 5
//        goingButton.layer.borderColor = UIColor.black.cgColor
//        goingButton.layer.borderWidth = 0.5
        goingButton.backgroundColor = UIColor(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
        goingButton.setTitleColor(UIColor.gray, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        // Configure the view for the selected state
    }

}
