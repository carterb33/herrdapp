//
//  NativeAdCell.swift
//  Herrd
//
//  Created by DG on 02/04/21.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import GoogleMobileAds

class NativeAdCell: UITableViewCell {

    @IBOutlet weak var headlineText: UILabel!
    @IBOutlet weak var adView: GADNativeAdView!
    @IBOutlet weak var adLabel: UILabel!
    @IBOutlet weak var storeView: UILabel!
    @IBOutlet weak var priceView: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    /// The height constraint applied to the ad view, where necessary.
    var heightConstraint: NSLayoutConstraint?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        adView.backgroundColor = AppColor.CommonColor.cellColor
        headlineText.textColor = AppColor.CommonColor.postColor
        adLabel.textColor = AppColor.CommonColor.postColor
        bodyLabel.textColor = AppColor.CommonColor.postColor
        storeView.textColor = AppColor.CommonColor.postColor
        priceView.textColor = AppColor.CommonColor.postColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
