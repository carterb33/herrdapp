//
//  MainScreenCell.swift
//  Herrd
//
//  Created by Carter  Beaulieu on 7/8/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit

class MainScreenCell: UITableViewCell {

    @IBOutlet weak var venueName: UILabel!
    @IBOutlet weak var peopleAmount: UILabel!
    @IBOutlet weak var goingButton: UIButton!
    @IBOutlet weak var bubbleButton: UIButton!
    @IBOutlet weak var placeMap: UIButton!
    @IBOutlet weak var btnGuestList: UIButton!
    @IBOutlet weak var shadowCellView: UIView!
    @IBOutlet weak var guestListText: UIButton!
    @IBOutlet weak var halfView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    //border changes for cell
//
        btnGuestList.setTitleColor(.lightGray, for: .normal)
    venueName.textColor = AppColor.CommonColor.postColor
    shadowCellView.backgroundColor = AppColor.CommonColor.cellColor
        shadowCellView.layer.borderWidth = 0.5
    shadowCellView.layer.borderColor = AppColor.CommonColor.postColor.cgColor
    shadowCellView.layer.cornerRadius = 8
        
        goingButton.layer.borderColor = UIColor.darkGray.cgColor

     //changing shadow of cell
//        shadowCellView.layer.applySketchShadow(
//        color: .white,
//        alpha: 0.4,
//        x: 5,
//        y: 7,
//        blur: 0,
//        spread: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
//extension to better apply sketch shadows
extension CALayer {
  func applySketchShadow(
    color: UIColor = .black,
    alpha: Float = 0.5,
    x: CGFloat = 0,
    y: CGFloat = 2,
    blur: CGFloat = 4,
    spread: CGFloat = 0)
  {
    shadowColor = color.cgColor
    shadowOpacity = alpha
    shadowOffset = CGSize(width: x, height: y)
    shadowRadius = blur / 2.0
    if spread == 0 {
      shadowPath = nil
    } else {
      let dx = -spread
      let rect = bounds.insetBy(dx: dx, dy: dx)
      shadowPath = UIBezierPath(rect: rect).cgPath
    }
  }
}
