//
//  TopCell.swift
//  Herrd
//
//  Created by DG on 22/01/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import AVFoundation
import GiphyUISDK

class TopCell: UITableViewCell, AutoPlayVideoLayerContainer {

    @IBOutlet weak var lblTopicName: UILabel!
    @IBOutlet weak var lblTopicDate: UILabel!
    @IBOutlet weak var lblTopicCount: UILabel!
    @IBOutlet weak var btnUp: UIButton!
    @IBOutlet weak var btnDown: UIButton!
    @IBOutlet weak var bubbleButton: UIButton!
    @IBOutlet weak var upVoteTotal: UILabel!
    @IBOutlet weak var downVoteTotal: UILabel!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var opIcon: UILabel!
    @IBOutlet weak var btnCell: UIButton!
    @IBOutlet weak var imgTopic: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var imgTopicTop: NSLayoutConstraint!
    @IBOutlet weak var imgTopicBottom: NSLayoutConstraint!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var pinnedCup: UIImageView!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var pinnedLink: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var mySubReplyView: UIView!
    
    var topic: Topic?
    var playerController: VideoPlayerController?
    var mediaView = GPHMediaView()
    
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                VideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }
    
    func muteUnmute() {
        let playerVC = VideoPlayerController.sharedVideoPlayer
        playerVC.muteUnMuteVideo()
        //Save value in defaults
        if UserDefaults.standard.bool(forKey: "isVideoMute") {
            UserDefaults.standard.set(false, forKey: "isVideoMute")
            UserDefaults.standard.synchronize()
        } else {
            UserDefaults.standard.set(true, forKey: "isVideoMute")
            UserDefaults.standard.synchronize()
        }
    }
    
    func getMuteUnmuteState() -> Bool? {
        let playerVC = VideoPlayerController.sharedVideoPlayer
        return playerVC.getPlayerMuteUnMuteState()
    }
    
    func configureCell(videoUrl: String?) {
        self.videoURL = videoUrl
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let imgtopic = imgTopic {
            let horizontalMargin: CGFloat = 0
            let width: CGFloat = imgtopic.frame.size.width - horizontalMargin * 2
            videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: width)
        }
    }
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(imgTopic.frame, from: imgTopic)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
            return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        imgTopic?.layer.addSublayer(videoLayer)
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        mediaView.media = nil
    
    }

}
