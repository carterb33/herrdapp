//
//  PrivateUsersCell.swift
//  Herrd
//
//  Created by DG on 01/19/21.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit

class PrivateUsersCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLastMsg: UILabel!
    @IBOutlet weak var lblLastMsgDate: UILabel!
    @IBOutlet weak var lblChatCount: UILabel!
    @IBOutlet weak var dmListView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        lblUserName.textColor = AppColor.CommonColor.postColor
        lblLastMsgDate.textColor = AppColor.CommonColor.secondaryColor
        lblLastMsg.textColor = AppColor.CommonColor.postColor
        dmListView.backgroundColor = AppColor.CommonColor.cellColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
