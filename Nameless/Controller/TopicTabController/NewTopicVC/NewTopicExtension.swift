//
//  NewTopicExtension.swift
//  Herrd
//
//  Created by DG on 17/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit
import GiphyUISDK

extension NewTopicViewController: GiphyDelegate {
    func didSearch(for term: String) {
        print("your user made a search! ", term)
    }
    
    func didSelectMedia(giphyViewController: GiphyViewController, media: GPHMedia) {
        
        giphyViewController.dismiss(animated: true, completion: { [weak self] in
            self?.mediaType = .GIF
            self?.selectedImage = nil
            self?.imgTopic.image = nil
            self?.mediaView.media = media
            self?.mediaView.frame = self?.imgTopic.frame ?? CGRect(x: 0, y: 0, width: 125, height: 125)
            self?.mediaView.layer.cornerRadius = 10
            self?.mediaView.clipsToBounds = true
            self?.mainView.addSubview(self!.mediaView)
            self?.mainView.bringSubviewToFront((self?.btnChoosePhoto)!)
            self?.mainView.bringSubviewToFront((self?.btnChooseLogo)!)
        })
//        GPHCache.shared.clear()
    }
    
    func didDismiss(controller: GiphyViewController?) {
//        GPHCache.shared.clear()
    }
}
