//
//  NewTopicViewController.swift
//  Herrd
//
//  Created by DG on 31/01/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import AVFoundation
import ImageViewer
import Photos
import AssetsLibrary
import Firebase
import IQKeyboardManagerSwift
import FirebaseAuth
import GiphyUISDK
import JVFloatLabeledTextField

class NewTopicViewController: UIViewController {

    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    
    @IBOutlet weak var slideIdicator: UIView!
    @IBOutlet weak var txtPost: JVFloatLabeledTextView!
    @IBOutlet weak var lblTextCount: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTopic: UIImageView!
    var mediaView = GPHMediaView()
    @IBOutlet weak var btnChoosePhoto: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnChooseLogo: UIButton!
    var isSubTopic = false
    var isReplySubTopic = false
    var selectedTopic : Topic?
    var selectedSubTopic : SubTopic?
    var maxLength = 60
    var selectedImage : UIImage?
    var selectedLogoImage : UIImage?
    var mediaUrl : URL?
    var representedAssetIdentifier: String!
    var mediaType : TopicMediaType?
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var imgCenterX: NSLayoutConstraint!
    @IBOutlet weak var btnChooseImgCenterX: NSLayoutConstraint!
    @IBOutlet weak var imgLogoWidth: NSLayoutConstraint!
    @IBOutlet weak var imgLogoHeight: NSLayoutConstraint!
    @IBOutlet weak var postOutlet: UIButton!
    var selectedButton : UIButton?
    var items: [DataItem] = []
    @IBOutlet weak var mainViewCenterY: NSLayoutConstraint!
    var adminList : [String] = []
    @IBOutlet weak var txtButtonTitle: JVFloatLabeledTextField!
    @IBOutlet weak var txtButtonLink: JVFloatLabeledTextField!
    @IBOutlet weak var buttonViewHeight: NSLayoutConstraint!
    var postButtonLastOrigin: CGFloat = 0
    var isAdmin = false
    var keyBoardHeight: CGFloat = 0
    let gradiantImage = UIImage(named: "gradientRectangle")!
    let gradiantImage1 = UIImage(named: "gradientRec6")!

    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        
        postOutlet.backgroundColor = UIColor.init(patternImage: gradiantImage)

        postButtonLastOrigin = self.postOutlet.frame.origin.y
        addKeyBoardObserver()
        slideIdicator.roundCorners(.allCorners, radius: 10)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
        
        mainView.backgroundColor = AppColor.CommonColor.cellColor
        
        lblTitle.textColor = AppColor.CommonColor.postColor
        txtPost.textColor = AppColor.CommonColor.postColor
        btnChoosePhoto.tintColor = AppColor.CommonColor.postColor
//        btnChoosePhoto.titleLabel?.textColor = AppColor.CommonColor.postColor
        btnChooseLogo.tintColor = AppColor.CommonColor.postColor
//        btnChooseLogo.titleLabel?.textColor = AppColor.CommonColor.postColor

        self.navigationItem.hidesBackButton = true
        
        postOutlet.isEnabled = true
        postOutlet.isHidden = false

        maxLength = isSubTopic ? 100 : 200
        lblTextCount.text =  "\(0)/\(maxLength)"
        //txtPost.layer.borderColor =  UIColor(red:130/255, green:130/255, blue:130/255, alpha:1.0).cgColor
        //txtPost.layer.borderWidth = 1.0
        //txtPost.layer.cornerRadius = 5.0
        if isReplySubTopic && isSubTopic {
            imgHeight.constant = 0
            imgLogoHeight.constant = 0
            btnChoosePhoto.isHidden = true
            btnChooseLogo.isHidden = true
            imgPlay.isHidden = true
            lblTitle.text = "Reply to Reply"
            txtPost.placeholder = "Your amazing reply"
            postOutlet.setTitle("Reply", for: .normal)
        }else if isSubTopic {
            imgHeight.constant = 0
            imgLogoHeight.constant = 0
            btnChoosePhoto.isHidden = true
            btnChooseLogo.isHidden = true
            imgPlay.isHidden = true
            lblTitle.text = "Reply to Post"
            txtPost.placeholder = "Your amazing reply"
            postOutlet.setTitle("Reply", for: .normal)
        } else {
            imgHeight.constant = 125
            imgLogoHeight.constant = 125
            btnChoosePhoto.isHidden = false
            btnChooseLogo.isHidden = false
            
            imgPlay.isHidden = true
            lblTitle.text = "New Post"
            txtPost.placeholder = "What's Happening?"
            lblTextCount.text = "200"
            postOutlet.setTitle("Post", for: .normal)
        }
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if  self.adminList.contains(uid ?? ""){
            self.isAdmin = true
            self.imgLogoHeight.constant = 125
            self.imgLogoWidth.constant = 125
            self.imgCenterX.constant = -70
            self.btnChooseImgCenterX.constant = -70
            self.buttonViewHeight.constant = 110
            self.btnChooseLogo.isHidden = false
        } else {
            self.isAdmin = false
            self.imgLogoHeight.constant = 0
            self.imgLogoWidth.constant = 0
            self.imgCenterX.constant = 0
            self.btnChooseImgCenterX.constant = 0
            self.buttonViewHeight.constant = 0
            self.btnChooseLogo.isHidden = true
        }
    
        txtPost.heightAnchor.constraint(greaterThanOrEqualToConstant: 22)
        txtPost.isScrollEnabled = false
//        self.updateViewHeight(0)
        
        let group = DispatchGroup()
//
//        // Group our requests:
        group.enter()
//        firstAsyncRequest {
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
////            self.txtPost.becomeFirstResponder()
//        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.025) {
            self.updateViewHeight(0)
            group.leave()
        }
        
        group.notify(queue: .main) {
          // Update UI
            self.txtPost.becomeFirstResponder()
        }
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
        if (self.postOutlet.frame.origin.y > (postButtonLastOrigin + 17)) {
            self.view.frame.origin = CGPoint(x: 0, y: (isAdmin ? (self.view.frame.origin.y - 20): (self.view.frame.origin.y - 20)))
        } else if (self.postOutlet.frame.origin.y < (postButtonLastOrigin - 17)) {
            self.view.frame.origin = CGPoint(x: 0, y: (isAdmin ? (self.view.frame.origin.y + 20): (self.view.frame.origin.y + 20)))
        }
        postButtonLastOrigin = self.postOutlet.frame.origin.y
    }
    
    deinit {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        removeKeyBoardObserver()
    }
    
    func addKeyBoardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(NewTopicViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewTopicViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyBoardObserver() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if self.txtPost.isFirstResponder && keyBoardHeight == 0 {
            if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                    let keyboardRectangle = keyboardFrame.cgRectValue
                    let keyboardHeight = keyboardRectangle.height
                    UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveLinear) {
                        self.view.frame.origin = CGPoint(x: 0,y: self.view.frame.origin.y - keyboardHeight - 5)
                    } completion: { finished in
                        print("Animation Finished",finished)
                    }
                    self.keyBoardHeight = keyboardHeight
                }
        }
    }

    @objc func keyboardWillHide(notification: Notification) {
        keyBoardHeight = 0
        self.updateViewHeight(0)
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        let presentedVC = self.presentationController as? PresentationController
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                if txtPost.text.count > 0 {
                    showUnsavedChangesAlert(presentedVC!)
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: presentedVC!.viewHeight)
                }
            }
        }
    }
    
    func showUnsavedChangesAlert(_ presentedVC: PresentationController) {
        self.view.endEditing(true)
        CommonUtility.showAlert(title: "Unsaved Changes",
                                message: "You have some unsaved changes. Are you sure you want to dismiss this page?",
                                controller: self,
                                alertStyle: .alert,
                                actionTitles: ["Cancel","Dismiss"],
                                actionStyles: [.cancel,.destructive],
                                actions: [
                                    { action in
                                        // Set back to original position of the view controller
                                        UIView.animate(withDuration: 0.3) {
                                            self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: presentedVC.viewHeight)
                                        }
                                    },
                                    { action in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                ])
    }
    
    func updateViewHeight(_ extraHeight: CGFloat) {
        let presentedVC = self.presentationController as? PresentationController
        presentedVC?.viewHeight = self.postOutlet.frame.origin.y + 64 + extraHeight
        presentedVC?.containerViewDidLayoutSubviews()
        self.pointOrigin = self.view.frame.origin
    }
    

    @IBAction func btnCancelClicked(_ sender: Any) {
        if txtPost.text.count > 0 {
            if let presentedVC = self.presentationController as? PresentationController {
                self.showUnsavedChangesAlert(presentedVC)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(IQKeyboardManager.shared.keyboardDistanceFromTextField)
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 60
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print(IQKeyboardManager.shared.keyboardDistanceFromTextField)
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 10
    }
    
    @IBAction func btnPostClicked(_ sender: Any) {
        self.view.endEditing(true)
        if txtPost.text!.count > 0 {
            postOutlet.isEnabled = false
            postOutlet.isHidden = true
            if isSubTopic {
                //Add sub topic
                addSubTopic()
            } else {
                let topicId = Date().timeIntervalSince1970
                let user = Auth.auth().currentUser
                let uid = user?.uid
                if  self.adminList.contains(uid ?? "") && (self.selectedLogoImage != nil) {
                    addLogoImage(topicId, self.selectedLogoImage!)
                } else {
                    uploadImageAndAddNewTopic(topicId,nil)
                }
                
            }
            self.dismiss(animated: true, completion: {})
        }
    }
    
    @IBAction func btnImgClicked(_ sender: Any) {
        items.removeAll()
        // Show the ImageViewer with the first item
        var galleryItem: GalleryItem!
        if self.mediaType == .Video {
            galleryItem = GalleryItem.video(fetchPreviewImageBlock: { $0(self.imgTopic.image) }, videoURL: self.mediaUrl!)
        } else {
            galleryItem = GalleryItem.image { $0(self.imgTopic.image) }
        }
        
        items.append(DataItem(imageView: self.imgTopic, galleryItem: galleryItem))
        self.presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self, configuration: CommonUtility.galleryConfiguration()))
    }
    
    func presentImageGalleryNew(_ gallery: GalleryViewController, completion: (() -> Void)? = {}) {
        let presentedVC = self.presentationController as? PresentationController
        presentedVC?.presentedViewController.present(gallery, animated: false, completion: completion)
    }
    
    @IBAction func btnLogoClicked(_ sender: Any) {
        items.removeAll()
        // Show the ImageViewer with the first item
        let galleryItem = GalleryItem.image { $0(self.imgLogo.image) }
        items.append(DataItem(imageView: self.imgTopic, galleryItem: galleryItem))
        self.presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self, configuration: CommonUtility.galleryConfiguration()))
    }
    
    func addLogoImage(_ topicId : Double,_ logoImage: UIImage) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        guard let logoImgData = logoImage.pngData() else {
            return
        }
        TopicFirebaseUtility()
            .uploadImageToTopicFolder(imageName: String(topicId) + "_logo" + ".png",
                                      imageData: logoImgData) { (uploadUrl, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = err {
                print("Got error",error)
            } else {
                self.uploadImageAndAddNewTopic(topicId,uploadUrl)
            }
        }
    }
    
    func uploadImageAndAddNewTopic(_ topicId : Double,_ logoImageUrl: String?) {
        
        switch self.mediaType {
        case .Image, .Video:
            if let image = self.selectedImage {
                if self.mediaType == .Video {
                    addNewTopicWithVideoAndThumbnail(topicId, thumbnail: image,logoImageUrl)
                } else {
                    addNewTopicWithImage(topicId,image,logoImageUrl)
                }
            } else {
                //Add new topic without any media
                addNewTopicWithoutMedia(topicId,logoImageUrl)
            }
        case .GIF:
            if let media = self.mediaView.media {
                addNewTopicWithGIF(topicId,logoImageUrl,media)
            } else {
                //Add new topic without any media
                addNewTopicWithoutMedia(topicId,logoImageUrl)
            }
        default:
            addNewTopicWithoutMedia(topicId,logoImageUrl)
        }
    }
    
    func addNewTopicWithoutMedia(_ topicId : Double,_ logoImageUrl: String?) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        TopicFirebaseUtility().addNewTopic(topicId: topicId,
                                           title: txtPost.text!,
                                           buttonTitle: self.txtButtonTitle.text ?? nil,
                                           buttonUrl: self.txtButtonLink.text ?? nil,
                                           imageUrl:"",
                                           videoUrl: "",
                                           gifUrl: "",
                                           gifId: "",
                                           logoUrl: logoImageUrl,
                                           mediaType: "none",
                                           completion: { (newTopic, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Got error",error)
            } else {
                self.popToParentWithDataRefresh(newTopic)
            }
        })
    }
    
    func addNewTopicWithGIF(_ topicId : Double,_ logoImageUrl: String?,_ media: GPHMedia?) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        TopicFirebaseUtility().addNewTopic(topicId: topicId,
                                           title: txtPost.text!,
                                           buttonTitle: self.txtButtonTitle.text ?? nil,
                                           buttonUrl: self.txtButtonLink.text ?? nil,
                                           imageUrl:"",
                                           videoUrl: "",
                                           gifUrl: media?.url,
                                           gifId: media?.id,
                                           logoUrl: logoImageUrl,
                                           mediaType: "gif",
                                           completion: { (newTopic, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Got error",error)
            } else {
                self.popToParentWithDataRefresh(newTopic)
            }
        })
    }
    
    func addNewTopicWithImage(_ topicId : Double,_ image: UIImage,_ logoImageUrl: String?) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        TopicFirebaseUtility().uploadImageToTopicFolder(imageName: String(topicId) + ".jpg", imageData: image.jpegData(compressionQuality: 0.3)!) { (uploadUrl, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = err {
                print("Got error",error)
            } else {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                TopicFirebaseUtility().addNewTopic(topicId: topicId,
                                                   title: self.txtPost.text!,
                                                   buttonTitle: self.txtButtonTitle.text ?? nil,
                                                   buttonUrl: self.txtButtonLink.text ?? nil,
                                                   imageUrl:uploadUrl ?? "",
                                                   videoUrl: "",
                                                   gifUrl: "",
                                                   gifId: "",
                                                   logoUrl: logoImageUrl,
                                                   mediaType: "image",
                                                   completion: { (newTopic, error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let error = error {
                        print("Got error",error)
                    } else {
                        self.popToParentWithDataRefresh(newTopic)
                    }
                })
            }
        }
    }
    
    func addNewTopicWithVideoAndThumbnail(_ topicId: Double, thumbnail: UIImage,_ logoImageUrl: String?) {
        do {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let videoData = try Data(contentsOf: self.mediaUrl!)
            TopicFirebaseUtility().uploadVideoToTopicFolder(videoName: String(topicId) + ".mp4", videoData: videoData) { (videouploadUrl, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = err {
                    print("Got error",error)
                } else {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    TopicFirebaseUtility().uploadImageToTopicFolder(imageName: String(topicId) + ".jpg", imageData: thumbnail.jpegData(compressionQuality: 0.3)!) { (imguploadUrl, err) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let error = err {
                            print("Got error",error)
                        }
                        else {
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            TopicFirebaseUtility().addNewTopic(topicId: topicId,
                                                               title: self.txtPost.text!,
                                                               buttonTitle: self.txtButtonTitle.text ?? nil,
                                                               buttonUrl: self.txtButtonLink.text ?? nil,
                                                               imageUrl:imguploadUrl ?? "",
                                                               videoUrl:videouploadUrl ?? "",
                                                               gifUrl: "",
                                                               gifId: "",
                                                               logoUrl: logoImageUrl,
                                                               mediaType: "movie",
                                                               completion:
                                { (newTopic, error) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    if let error = error {
                                        print("Got error",error)
                                    } else {
                                        self.popToParentWithDataRefresh(newTopic)
                                    }
                                })
                        }
                    }
                }
            }
        } catch {
            MBProgressHUD.hide(for: self.view, animated: true)
            print("Video data issue")
        }
    }
    
    func addSubTopic() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let subTopicId = isReplySubTopic ? (self.selectedSubTopic?.subtopic_id ?? 0) : (self.selectedTopic?.topic_id ?? 0)
        
        SubTopicFirebaseUtility().addNewSubTopic(title: self.txtPost.text!,
                                                 topic_id: subTopicId,
                                                 isReplySubTopic: (isReplySubTopic ? true : false),
                                                 completion: { (newTopic, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Got error",error)
            } else {
                let applicationDelegate = UIApplication.shared.delegate as? AppDelegate
                let window = applicationDelegate?.window
                if let mainTabBar = window?.rootViewController as? MainTabBarController {
                    if let navigationCont = mainTabBar.selectedViewController as? UINavigationController {
                        if self.isReplySubTopic {
                            let parent =  navigationCont.viewControllers.last as? ReplySubTopicViewController
    //                        parent?.getSubTopics()
    //                        self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: false) {
                                parent?.getSubTopics()
                            }
                        } else {
                            let parent =  navigationCont.viewControllers.last as? SubTopicViewController
    //                        parent?.getSubTopics()
    //                        self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: false) {
                                parent?.getSubTopics()
                            }
                        }
                    }
                }
                
//                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                
                let user = Auth.auth().currentUser
                let uid = user?.uid ?? ""
                
                // Fetch created by user and compare with current user
                if let subTopicCreatedBy = self.isReplySubTopic ? self.selectedSubTopic?.created_by : self.selectedTopic?.created_by, subTopicCreatedBy != uid
                {
                    // Fetch device id of sub topic or topic created user
                    FirebaseUtility().getDeviceTokenUsingUserId(userId: subTopicCreatedBy) { (deviceId, error) in
                        if let error = error {
                            print("Error getting documents: \(error)")
                        } else {
                            if let device_Id = deviceId, device_Id.count > 0
                            {
                                var tmpNewTopic = newTopic
                                tmpNewTopic?.removeValue(forKey: "subtopic_date")   // Remove this because of crash, crash occur because it's date type value
                                tmpNewTopic?["isReplySubTopic"] = self.isReplySubTopic
                                let title = "New Reply to: \"" + (self.isReplySubTopic ? (self.selectedSubTopic?.subtopic_title ?? "") : (self.selectedTopic?.topic_title ?? "")) + "\""
                                let parameters = ["to": device_Id,
                                                  "notification": ["body":tmpNewTopic?["subtopic_title"],
                                                                   "title":title,
                                                                   "sound":"default",
                                                                   "badge":"1"],
                                                           "data":tmpNewTopic ?? []] as [String : Any]
                                
                                FirebaseUtility().sendNotificationAPI(param: parameters)    // Send notification
                            }
                        }
                    }
                }
            }
        })
    }
    
    func popToParentWithDataRefresh(_ newTopic: [String: Any]?) {
        let applicationDelegate = UIApplication.shared.delegate as? AppDelegate
        let window = applicationDelegate?.window
        if let mainTabBar = window?.rootViewController as? MainTabBarController {
            if let navigationCont = mainTabBar.selectedViewController as? UINavigationController {
                if let parent =  navigationCont.viewControllers.last as? TopicNewsViewController {
                    self.dismiss(animated: false) {
                        if parent.topicSegment.selectedSegmentIndex == 1 {
                            do {
                                if let topicData = newTopic {
                                    let topic = try Topic(data: topicData)
                                    parent.topicList.insert(topic, at: 0)
                                    parent.topicListCopy.insert(topic, at: 0)
                                    parent.topicTableView.reloadData()
                                }
                            } catch { print(error) }
                        }
    //                    parent.getAllTopics()
                    }
                } else if let parent =  navigationCont.viewControllers.last as? MineViewController {
                    self.dismiss(animated: false) {
                        if parent.topicSegment.selectedSegmentIndex == 0 {
                            do {
                                if let topicData = newTopic {
                                    let topic = try Topic(data: topicData)
                                    parent.topicList.insert(topic, at: 0)
                                    parent.topicListCopy.insert(topic, at: 0)
                                    parent.topicTableView.isHidden = false
                                    parent.messageView.isHidden = true
                                    parent.topicTableView.reloadData()
                                }
                            } catch { print(error) }
                        }
                    }
                }
            }
        }
        
    }
    
    @IBAction func btnChoosePhotoTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.selectedButton = sender
        showActionSheet(sender)
    }
    
    func showActionSheet(_ sender: UIButton) {
        
        let actionSheet = UIAlertController(title: nil,
                                            message: self.selectedButton == self.btnChoosePhoto ? "Choose Photo / Video / GIF" : "Choose Logo",
                                            preferredStyle: UIAlertController.Style.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))

        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        if self.selectedButton == self.btnChoosePhoto {
            let gifAction = UIAlertAction(title: "GIF", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                self.gifLibrary()
            })
            let flagImage = UIImage(named: "Poweredby_100px-Black_VertLogo")
            gifAction.setValue(flagImage, forKey: "image")
            actionSheet.addAction(gifAction)
        }

        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: UIAlertAction.Style.cancel,
                                            handler: nil))

        //if iPhone
        if UIDevice.isPhone {
            self.present(actionSheet, animated: true, completion: nil)
        }
        else {
            //In iPad Change Rect to position Popover
            actionSheet.popoverPresentationController?.sourceRect = sender.frame
            actionSheet.popoverPresentationController?.sourceView = self.mainView
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            if self.isCamAccessDenied() == false {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = UIImagePickerController.SourceType.camera
                myPickerController.mediaTypes = self.selectedButton == self.btnChoosePhoto ? [TopicMediaType.Image.rawValue, TopicMediaType.Video.rawValue] : [TopicMediaType.Image.rawValue]
                myPickerController.allowsEditing = true
                myPickerController.videoMaximumDuration = 40
                myPickerController.videoQuality = .typeMedium
                self.present(myPickerController, animated: true, completion: nil)
            }
        } else {
            CommonUtility.showAlert(title: "Camera Not Available",
                                    message: "Camera not found on this device.",
                                    controller: self)
        }
    }

    func photoLibrary() {

        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.mediaTypes = self.selectedButton == self.btnChoosePhoto ? ["public.image", "public.movie"] : ["public.image"]
        myPickerController.allowsEditing = true
        myPickerController.videoMaximumDuration = 40
        myPickerController.videoQuality = .typeMedium
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)

    }
    
    func gifLibrary() {
        let giphy = GiphyViewController()
        giphy.theme = GPHTheme(type: GPHThemeType.light)
        giphy.mediaTypeConfig = GPHContentType.defaultSetting
        GiphyViewController.trayHeightMultiplier = 0.9
        giphy.shouldLocalizeSearch = true
        giphy.delegate = self
        giphy.dimBackground = true
        giphy.modalPresentationStyle = .overCurrentContext
        giphy.selectedContentType = .gifs
        present(giphy, animated: true, completion: nil)
    }
    
    func isCamAccessDenied()-> Bool
    {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if status == .restricted || status == .denied {
        DispatchQueue.main.async
            {
            var alertText = Messages.Info.CameraPrivacy

                var alertButton = "Ok"
                var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)

                if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
                {
                    alertText = Messages.Info.CameraPrivacyWithSetting

                    alertButton = "Settings"

                    goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                    })
                }

                let alert = UIAlertController(title: "Privacy error", message: alertText, preferredStyle: .alert)
                if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
                               {
                                let cancleAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                                alert.addAction(cancleAction)
                }
                
                alert.addAction(goAction)
                self.present(alert, animated: true, completion: nil)
            }
            return true
        }
        return false
    }
}

extension AVAsset{
    var videoThumbnail:UIImage?{

        let assetImageGenerator = AVAssetImageGenerator(asset: self)
        assetImageGenerator.appliesPreferredTrackTransform = true

        var time = self.duration
        time.value = min(time.value, 2)

        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbNail = UIImage.init(cgImage: imageRef)
            print("Video Thumbnail genertated successfuly")
            return thumbNail
        } catch {
            print("error getting thumbnail video",error.localizedDescription)
            return nil
        }

    }
}

extension NewTopicViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //Remove GIF View
        self.mediaView.removeFromSuperview()
        self.mediaView.media = nil
        self.mediaType = TopicMediaType(rawValue: info[.mediaType] as? String ?? "")
        if self.mediaType == .Video {
            self.mediaUrl = info[.mediaURL] as? URL
//            let video = NSData(contentsOfURL: videoURL)

            //Create AVAsset from url
            if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
           
                let ass = AVAsset(url:videoURL)

                if let videoThumbnail = ass.videoThumbnail{
                  print("Success")
                    self.selectedImage = videoThumbnail
                    self.imgTopic.image = videoThumbnail
                    self.imgPlay.isHidden = false
                }
            }
//            var asset = PHPhotoLibrary.shared()
//            var req = PHAsset.
//            asset.
//            asset.assetForURL(info[.phAsset], resultBlock: { asset in
//                if let ast = asset {
//                    let assetRep = ast.defaultRepresentation()
//                    let iref = assetRep.fullResolutionImage().takeUnretainedValue()
//                    let image = UIImage(CGImage: iref)
//                    dispatch_async(dispatch_get_main_queue(), {
//                        // ...Update UI with image here
//                    })
//                }
//            }, failureBlock: { error in
//                print("Error: \(error)")
//            })
        }
        else if self.mediaType == .Image {
            if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                if self.selectedButton == self.btnChoosePhoto {
                    self.selectedImage = pickedImage
                    self.imgTopic.image = pickedImage
                    self.imgPlay.isHidden = true
                } else {
                    self.selectedLogoImage = pickedImage
                    self.imgLogo.image = pickedImage
                }
            }
        }
        
        picker.dismiss(animated: true, completion: nil)

    }

    func getImageFrom(asset: PHAsset) {
        representedAssetIdentifier = asset.localIdentifier
        let imageManager = PHCachingImageManager()

        imageManager.requestImage(for: asset, targetSize: CGSize(width: 350, height: 350), contentMode: .default, options: nil) { (image, _) in

            if(image != nil) {
                 self.imgTopic.image = image
            }
        }
    }
}

extension NewTopicViewController : UITextViewDelegate {
    
//    func textViews(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
//        print("textViewDidBeginEditing")
        
//        UIView.animate(withDuration: 5) {
//            self.mainViewCenterY.constant = -125
//        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
//        print("textViewDidEndEditing")
//        UIView.animate(withDuration: 5) {
//            self.mainViewCenterY.constant = 0
//        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newLength = (textView.text?.count ?? 0) + text.count - range.length
        
        if newLength <= maxLength {
            lblTextCount.text =  "\(maxLength - newLength)"
            return true
        } else {
            return false
        }
    }
    
}

// The GalleryItemsDataSource provides the items to show
extension NewTopicViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        return items.count
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return items[index].galleryItem
    }
}
