//
//  TopicNewsTableView.swift
//  Herrd
//
//  Created by DG on 17/04/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import FirebaseAuth
import ImageViewer
import AVFoundation
import AVKit
import GiphyUISDK
import GoogleMobileAds
import SafariServices

//TABLEVIEW EXTENSION
extension TopicNewsViewController: UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.topicSegment.selectedSegmentIndex == 2 {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        if self.topicSegment.selectedSegmentIndex == 2 {
           switch(section) {
                case 0:return "Reported Topic(s)"
                case 1:return "Reported Reply(s)"
                default :return ""
            }
        } else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let titleView = view as! UITableViewHeaderFooterView
        switch(section) {
             case 0:
                titleView.textLabel?.text = "Reported Topic(s)"
             case 1:
                titleView.textLabel?.text = "Reported Reply(s)"
             default :
                titleView.textLabel?.text = ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(section) {
            case 0:return topicList.count
            case 1:return subTopicList.count
            default :return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else {
            return 100
        }
        
//        let topic = self.topicList[indexPath.row]
//        if topic.imageUrl?.count ?? 0 > 0 {
//            if topic.topic_title?.count ?? 0 > 30 {
//                return 245
//            } else {
//                return 225
//            }
//        } else {
//            return 100
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Trigger pagination when scrolled to last cell
        // Feel free to adjust when you want pagination to be triggered
        if self.topicSegment.selectedSegmentIndex == 2 {
            if indexPath.section == 0 {
                if (indexPath.row == topicList.count - 1 && !isFetchingData && !isEndOfDocs) {
                    self.getReportedTopic()
                }
            } else if indexPath.section == 1 {
                if (indexPath.row == subTopicList.count - 1 && !isFetchingDataSub && !isEndOfDocsSub) {
                    self.getReportedSubTopics()
                }
            }
        } else {
            if (indexPath.row == topicList.count - 1 && !isFetchingData && !isEndOfDocs) {
                self.getAllTopics()
            }
        }

        switch indexPath.section {
        case 0:
            if self.topicList[indexPath.row] is GADNativeAd {
                return self.loadAdCell(tableView,indexPath)
            } else {
                return self.loadTopicCell(tableView,indexPath)
            }
        case 1:
            return self.loadSubTopicCell(tableView,indexPath)
        default:
            return UITableViewCell()
        }
        
    }
    
    func loadTopicCell(_ tableView: UITableView,_ indexPath: IndexPath) -> TopCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.topicCellId,
                                                 for: indexPath) as! TopCell
        
        guard let topic = self.topicList[indexPath.row] as? Topic else{
            return cell
        }
        
        cell.totalView.backgroundColor = AppColor.CommonColor.cellColor
        cell.lblTopicName.textColor = AppColor.CommonColor.postColor
        cell.upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        cell.downVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        cell.bubbleButton.titleLabel?.textColor = AppColor.CommonColor.secondaryColor
        chatButton.tintColor = AppColor.CommonColor.postColor
        cell.lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
        
        //Remove Old Media view
        cell.mediaView.removeFromSuperview()
        cell.mediaView.media = nil
        //Admin user ids
        var promoText = ""
        if  self.adminList.contains(topic.created_by ?? "") {

            cell.imgPin.isHidden = false
            cell.btnUp.isHidden = true
            cell.btnDown.isHidden = true
            cell.btnReport.isHidden = true
            cell.lblTopicCount.isHidden = true
            cell.pinnedCup.isHidden = false
            if let buttonTitle = topic.buttonTitle, buttonTitle.count > 0 {
                let underLineStyle = NSUnderlineStyle.single.rawValue
                let attributedStringColor = [
                    NSAttributedString.Key.foregroundColor : UIColor(named: "ThemeColor"),
                    NSAttributedString.Key.underlineColor: UIColor(named: "ThemeColor"),
                    NSAttributedString.Key.underlineStyle: underLineStyle,
                    NSAttributedString.Key.font: UIFont(name: "Arial Rounded MT Bold", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: .bold)
                ] as [NSAttributedString.Key : Any];
                // create the attributed string
                let attributedString = NSAttributedString(string: buttonTitle, attributes: attributedStringColor)
                // Set the label
                cell.pinnedLink.setAttributedTitle(attributedString, for: .normal)
                cell.pinnedLink.isHidden = false
            } else {
                cell.pinnedLink.isHidden = true
            }
            
            cell.downVoteTotal.isHidden = true
            cell.upVoteTotal.isHidden = true
            promoText = " ⏤ pinned post"
            
            if topic.logoUrl?.count ?? 0 > 0 {
                // Get a reference to the storage service using the default Firebase App
                let storage = Storage.storage()
                
                // Reference to an image file in Firebase Storage
                let reference = storage.reference().child("Topic Images/\(topic.topic_id ?? 0)_logo.png")
                
                // UIImageView in your ViewController
                let imageView: UIImageView = cell.pinnedCup
                
                // Placeholder image
                let placeholderImage = UIImage(named: "placeholder")
                
                // Load the image using SDWebImage
                imageView.sd_setImage(with: reference, placeholderImage: placeholderImage)
            }
            
        }
        else {
            cell.pinnedLink.isHidden = true
            cell.imgPin.isHidden = true
            cell.btnUp.isHidden = false
            cell.btnDown.isHidden = false
            cell.btnReport.isHidden = false
            cell.lblTopicCount.isHidden = false
            cell.downVoteTotal.isHidden = false
            cell.upVoteTotal.isHidden = false
            cell.pinnedCup.isHidden = true
            promoText = ""
            
            if self.topicSegment.selectedSegmentIndex == 2 {
                cell.btnUp.isHidden = true
                cell.btnDown.isHidden = true
                cell.btnReport.isHidden = true
                cell.bubbleButton.isHidden = true
                cell.btnCell.isHidden = true
            } else {
                cell.btnUp.isHidden = false
                cell.btnDown.isHidden = false
                cell.btnReport.isHidden = false
                cell.bubbleButton.isHidden = false
                cell.btnCell.isHidden = false

            }
        }
        
        cell.topic = topic
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if (topic.upVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = true
            cell.btnDown.isSelected = false
        } else if (topic.downVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = true
        } else {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = false
        }
        if topic.upVoteIds!.count > 0 {
            let upvoteIds = topic.upVoteIds?.components(separatedBy: ",")
            cell.upVoteTotal?.text = "\(upvoteIds?.count ?? 0)"
        } else {
            cell.upVoteTotal?.text = "0"
        }
        
        if topic.downVoteIds!.count > 0  {
            let downvoteIds = topic.downVoteIds?.components(separatedBy: ",")
            cell.downVoteTotal?.text = "\(downvoteIds?.count ?? 0)"
        } else {
            cell.downVoteTotal?.text = "0"
        }
        
        cell.lblTopicName?.text = topic.topic_title
        cell.lblTopicCount.text = self.topicSegment.selectedSegmentIndex == 2 ?
            "\(topic.reportedIds?.components(separatedBy: ",").count ?? 0)" :
            "\(topic.difference ?? 0)"
        cell.btnUp.tag = indexPath.row
        cell.btnDown.tag = indexPath.row
        cell.bubbleButton.tag = indexPath.row
        cell.btnCell.tag = indexPath.row
        cell.btnImg.tag = indexPath.row
        cell.btnMute.tag = indexPath.row
        cell.btnReport.tag = indexPath.row
        cell.pinnedLink.tag = indexPath.row
        cell.btnUp.addTarget(self, action: #selector(TopicNewsViewController.btnUpClicked(_:)),
                             for: .touchUpInside)
        cell.btnDown.addTarget(self, action: #selector(TopicNewsViewController.btnDownClicked(_:)),
                               for: .touchUpInside)
        cell.btnImg.addTarget(self, action: #selector(TopicNewsViewController.btnViewImageClicked(_:)),
                              for: .touchUpInside)
        cell.btnReport.addTarget(self, action:              #selector(TopicNewsViewController.btnReportClicked(_:)),
                                 for: .touchUpInside)
        cell.btnMute.addTarget(self, action: #selector(TopicNewsViewController.btnMuteClicked(_:)),
                               for: .touchUpInside)
        if let timestamp = topic.topic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            cell.lblTopicDate?.text = timeAgo + promoText
        }
        
        if topic.imageUrl?.count ?? 0 > 0 {
            // Get a reference to the storage service using the default Firebase App
            let storage = Storage.storage()
            
            // Reference to an image file in Firebase Storage
            let reference = storage.reference().child("Topic Images/\(topic.topic_id ?? 0).jpg")
            
            // UIImageView in your ViewController
            let imageView: UIImageView = cell.imgTopic
            
            // Placeholder image
            let placeholderImage = UIImage(named: "placeholder")
            
            // Load the image using SDWebImage
            imageView.sd_setImage(with: reference, placeholderImage: placeholderImage)
            cell.imgTopic.isHidden = false
            cell.imgTopicTop.constant = 10
            cell.imgTopicBottom.constant = 10
            cell.imgHeight.constant = cell.imgTopic.frame.size.width
            if topic.mediaType == "movie" {
                cell.btnMute.isHidden = true
                //                if cell.videoURL == nil {
                cell.configureCell(videoUrl: topic.videoUrl)
                //                }
                
                //cell.videoLayer.frame = CGRect(x: 0, y: 0, width: cell.imgTopic.frame.size.width, height: cell.imgTopic.frame.size.height)
                if UserDefaults.standard.bool(forKey: "isVideoMute") {
                    cell.btnMute.isSelected = true
                } else {
                    cell.btnMute.isSelected = false
                }
            } else {
                cell.btnMute.isHidden = true
                cell.configureCell(videoUrl: nil)
            }
        } else if topic.gifId?.count ?? 0 > 0 {
            cell.btnMute.isHidden = true
            cell.configureCell(videoUrl: nil)
            //Set Image Frame
            cell.imgTopic.isHidden = false
            cell.mediaView.isHidden = true
            cell.imgTopic.image = UIImage(named: "placeholder-gif")
            cell.imgTopicTop.constant = 10
            cell.imgTopicBottom.constant = 10
            cell.imgHeight.constant = cell.imgTopic.frame.size.width
            //SetUp GIF
            cell.mediaView.frame = CGRect(x: cell.imgTopic.frame.origin.x, y: cell.imgTopic.frame.origin.y, width: cell.imgTopic.frame.size.width, height: cell.imgTopic.frame.width)
            cell.totalView.addSubview(cell.mediaView)
            cell.totalView.bringSubviewToFront(cell.btnImg)
            GiphyCore.shared.gifByID(topic.gifId ?? "") { (response, error) in
                if let media = response?.data {
                    DispatchQueue.main.sync {
                        cell.mediaView.media = media
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            cell.imgTopic.isHidden = true
                            cell.mediaView.isHidden = false
                            cell.mediaView.frame = CGRect(x: cell.imgTopic.frame.origin.x, y: cell.imgTopic.frame.origin.y, width: cell.imgTopic.frame.size.width, height: cell.imgTopic.frame.width)
                        }
                        
                        cell.mediaView.layer.cornerRadius = 10
                        cell.mediaView.clipsToBounds = true
                    }
                }
            }
        } else {
            cell.btnMute.isHidden = true
            cell.imgTopicTop.constant = 7
            cell.imgTopicBottom.constant = 7
            cell.imgHeight.constant = 0
            cell.configureCell(videoUrl: nil)
            cell.imgTopic.isHidden = true
        }
        
        cell.bubbleButton.addTarget(self, action: #selector(chatIconPressed(_:)),
                                    for: .touchUpInside)
        
        cell.btnCell.addTarget(self, action: #selector(chatIconPressed(_:)),
                               for: .touchUpInside)
        cell.pinnedLink.addTarget(self, action: #selector(pinnedLinkPressed(_:)),
                               for: .touchUpInside)
        cell.bubbleButton.titleLabel?.font = UIFont(name: "ArialRoundedMTBold", size: 12)
        cell.bubbleButton.titleLabel?.textColor = topic.subTopicsCount > 0 ?
            AppColor.CommonColor.secondaryColor :
            AppColor.CommonColor.cellColor

        cell.bubbleButton.setAttributedTitle(
            NSAttributedString(string: topic.subTopicsCount > 1 ?
                                "\(topic.subTopicsCount) replies" :
                                "\(topic.subTopicsCount) reply"), for: .normal)
        
        return cell
    }
    
    func loadSubTopicCell(_ tableView: UITableView,_ indexPath: IndexPath) -> TopCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.subTopicCellId) as! TopCell
        
        let subtopic = self.subTopicList[indexPath.row]
        cell.btnUp.isHidden = true
        cell.btnDown.isHidden = true
        cell.btnReport.isHidden = true
        cell.bubbleButton.isHidden = true
        cell.opIcon.isHidden = true
        cell.lblTopicName?.text = subtopic.subtopic_title
        cell.lblTopicCount.text = self.topicSegment.selectedSegmentIndex == 2 ?
        "\(subtopic.reportedIds?.components(separatedBy: ",").count ?? 0)" : "\(subtopic.difference ?? 0)"
        
        if let timestamp = subtopic.subtopic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            cell.lblTopicDate?.text = timeAgo
        }
        
        return cell
    }
    
    func loadAdCell(_ tableView: UITableView,_ indexPath: IndexPath) -> NativeAdCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.nativeAdCellId,
                                                 for: indexPath) as! NativeAdCell
        guard let nativeAd = self.topicList[indexPath.row] as? GADNativeAd else {
            return cell
        }
        cell.heightConstraint?.isActive = false

        (cell.adView.headlineView as? UILabel)?.text = nativeAd.headline
        
        cell.adView.mediaView?.mediaContent = nativeAd.mediaContent

        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
          // By acting as the delegate to the GADVideoController, this ViewController receives messages
          // about events in the video lifecycle.
//          mediaContent.videoController.delegate = self
//          videoStatusLabel.text = "Ad contains a video asset."
        } else {
//          videoStatusLabel.text = "Ad does not contain a video."
        }

        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = cell.adView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
            cell.heightConstraint = NSLayoutConstraint(
            item: mediaView,
            attribute: .height,
            relatedBy: .equal,
            toItem: mediaView,
            attribute: .width,
            multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
            constant: 0)
            cell.heightConstraint?.isActive = true
        }

        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (cell.adView.bodyView as? UILabel)?.text = nativeAd.body
        cell.adView.bodyView?.isHidden = nativeAd.body == nil

        (cell.adView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        cell.adView.callToActionView?.isHidden = nativeAd.callToAction == nil

        (cell.adView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        cell.adView.iconView?.isHidden = nativeAd.icon == nil

        //(cell.adView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        cell.adView.starRatingView?.isHidden = nativeAd.starRating == nil

        (cell.adView.storeView as? UILabel)?.text = nativeAd.store
        cell.adView.storeView?.isHidden = nativeAd.store == nil

        (cell.adView.priceView as? UILabel)?.text = nativeAd.price
        cell.adView.priceView?.isHidden = nativeAd.price == nil

        (cell.adView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        cell.adView.advertiserView?.isHidden = nativeAd.advertiser == nil

        // In order for the SDK to process touch events properly, user interaction should be disabled.
        cell.adView.callToActionView?.isUserInteractionEnabled = false

        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        cell.adView.nativeAd = nativeAd
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? AutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            VideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
        }
    }
    
    @IBAction func chatIconPressed(_ sender: UIButton) {
        guard let topic = self.topicList[sender.tag] as? Topic else {
            return
        }
        if let subTopicVC = self.storyboard?.instantiateViewController(withIdentifier: "SubTopicViewController") as? SubTopicViewController {
            subTopicVC.selectedTopic = topic
            subTopicVC.adminList = self.adminList
            self.navigationController?.pushViewController(subTopicVC, animated: true)
        }
    }
    
    @IBAction func pinnedLinkPressed(_ sender: UIButton) {
        guard let topic = self.topicList[sender.tag] as? Topic else { return }
        if let buttonUrl = topic.buttonUrl, buttonUrl.count > 0 {
            guard let url = URL(string: buttonUrl) else { return }
            let safariVC = SFSafariViewController(url: url)
            present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        }
    }

    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnReportClicked(_ sender: UIButton) {
        guard let topic = self.topicList[sender.tag] as? Topic else { return }
        //Check if user's self post or other post
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(sender,topic.created_by == uid ? .DeletePost : .ReportPost,topic,sender.tag,.Topic,nil)
        
    }
    
    @IBAction func btnReportSubTopicClicked(_ sender: UIButton) {
        var subtopic : SubTopic?
        subtopic = self.subTopicList[sender.tag]
        //Check if user's self post or other post
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(sender,subtopic?.created_by == uid ?
                                            .DeletePost : .ReportPost,
                                             nil,
                                             sender.tag,
                                             .SubTopic, subtopic)
    }
    
    func showActionSheetForReportPostOrDelete(_ sender: UIButton,_ actionType: ActionSheetType,_ topic: Topic?,_ index: Int,_ type: TopicType, _ subtopic: SubTopic?) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if #available(iOS 13.0, *) {
            alertController.view.tintColor = UIColor { traitCollection in
                switch traitCollection.userInterfaceStyle {
                    case .dark: return .white
                    default: return .black
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        if actionType == .ReportPost {
            let chatAction = UIAlertAction(title: "Direct Message", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Report post")
                self.getOrCreateTopicPrivateChannel(topic: topic!, index: index)
            })
            let chatIocn = UIImage(named: "chatSmall")
            chatAction.setValue(chatIocn, forKey: "image")
            
            let reportAction = UIAlertAction(title: "Report post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Report post")
                self.showReportPostAlert(topic, index)
            })
            let flagImage = UIImage(named: "flagIcon")
            reportAction.setValue(flagImage, forKey: "image")
            //for original image use this flagImage?.withRenderingMode(.alwaysOriginal)
            
            let hideAction = UIAlertAction(title: "Hide post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Hide post")
                self.showHidePostAlert(topic, index)
            })
            let blockIconImage = UIImage(named: "blockIcon")
            hideAction.setValue(blockIconImage, forKey: "image")
            
            let blockAction = UIAlertAction(title: "Block user", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Block user")
                self.showBlockUserAlert(topic, index)
            })
            let hideIconImage = UIImage(named: "hideIcon")
            blockAction.setValue(hideIconImage, forKey: "image")
            
            alertController.addAction(chatAction)
            alertController.addAction(reportAction)
            alertController.addAction(hideAction)
            alertController.addAction(blockAction)
        } else {
            let deleteAction = UIAlertAction(title: "Delete Post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Delete post")
                self.showDelelePostAlert(topic,index,type,subtopic)
            })
            let deleteIconImage = UIImage(named: "deleteIcon")
            deleteAction.setValue(deleteIconImage, forKey: "image")
            
            alertController.addAction(deleteAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        alertController.addAction(cancelAction)
        
        //if iPhone
        if UIDevice.isPhone {
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            //In iPad Change Rect to position Popover
            if let cell = sender.superview?.superview?.superview {
                /* Get the souce rect frame */
                let buttonFrame = sender.frame
                var showRect    = cell.convert(buttonFrame, to: self.topicTableView)
                showRect        = self.topicTableView.convert(showRect, to: view)
                showRect.origin.y += 10
                showRect.origin.x += sender.frame.size.width + 5
                
                alertController.popoverPresentationController?.sourceView = self.view
                alertController.popoverPresentationController?.sourceRect = showRect
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    func showDelelePostAlert(_ topic: Topic?,_ index: Int,_ type: TopicType, _ subtopic: SubTopic?) {
        let alert = UIAlertController(title: type == .Topic ? "Delete Post?" : "Delete Reply?",
                                      message: type == .Topic ? Messages.Warning.DeletePost : Messages.Warning.DeleteReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            type == .Topic ? self.deletePost(topic, index) :
                            self.deleteSubTopic(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showReportPostAlert(_ topic: Topic?,_ index: Int) {
        let alert = UIAlertController(title: "Report Post?", message: Messages.Info.ReviewPost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Report", style: .default, handler: { action in
            self.reportPost(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHidePostAlert(_ topic: Topic?,_ index: Int) {
        let alert = UIAlertController(title: "Hide Post?", message: Messages.Info.HidePost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Hide", style: .default, handler: { action in
            self.hidePost(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showBlockUserAlert(_ topic: Topic?,_ index: Int) {
        let alert = UIAlertController(title: "Block User?", message: Messages.Info.BlockPost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Block", style: .default, handler: { action in
            self.blockUser(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getOrCreateChannel(topic : Topic, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else { return }
        
        Firestore.firestore().collection(FirebaseConst.Collections.Venues)
            .whereField("name", isEqualTo: "\(topic.topic_id!)")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard let channel = Channel(document: data) else { return }
                        
                        (self.topicList[index] as? Topic)?.channelId = channel.id
                        
                        self.updateTopicWithChannelId(channel.id!, selectedTopic: topic)
                        //Add in User Chat Count
                        self.addOrUpdateChatCount(channel.id!, selectedTopic: topic)
                        let vc = ChatViewController(user: currentUser, channel: channel, anonymousMode: false, topic: topic)
                        vc.topic = topic
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        print("Create New Channel Data")
                        let channel = Channel(name: "\(topic.topic_id!)",channelRef: "")
                        Firestore.firestore().collection(FirebaseConst.Collections.Venues).addDocument(data: channel.representation){ error in
                            if let e = error {
                                print("Error saving venue channel: \(e.localizedDescription)")
                            }
                        }
                    }
                }
            })
    }
    
    func getOrCreateTopicPrivateChannel(topic : Topic, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else { return }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .whereField("channelRef", isEqualTo: "\(topic.topic_id!)+\(topic.created_by!)+\(currentUser.uid)")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    print("Error getting documents: \(err)")
                } else {
                    if let document = querySnapshot!.documents.first{
                        var channel = document.data()
                        channel["id"] = document.documentID
                        self.updateTopicWithChannelIdAndRef(document.documentID, channelRef: channel["channelRef"] as! String, selectedTopic: topic, currentUser: currentUser)
                        if (self.topicList[index] as? Topic)?.privateChannelRef != nil {
                            (self.topicList[index] as? Topic)?.privateChannelRef?.append(channel["channelRef"] as! String)
                        } else {
                            (self.topicList[index] as? Topic)?.privateChannelRef = [(channel["channelRef"] as! String)]
                        }
                        
                        self.pushToDirectPrivateChatVC(channel: channel, index)
                        self.refreshTopicChatList(topic.topic_id!)
                    }else{
                        print("Create New Channel Data")
                        let channel = Channel(name: "\(topic.topic_id!)",channelRef: "\(topic.topic_id!)+\(topic.created_by!)+\(currentUser.uid)")
                        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels).addDocument(data: channel.representation){ error in
                            if let e = error {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                print("Error saving venue channel: \(e.localizedDescription)")
                                return
                            }
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.getOrCreateTopicPrivateChannel(topic: topic, index: index)
                        }
                    }
                }
            })
    }
    
    func refreshTopicChatList(_ topic_id: Double) {
        if let navController = self.tabBarController?.viewControllers?[1] as? UINavigationController {
            if let badgeValue = navController.tabBarItem.badgeValue {
                var badgeCount = Int(badgeValue)
                badgeCount = (badgeCount ?? 0) + 1
                navController.tabBarItem.badgeValue = badgeCount ?? 0 > 0 ? "\(badgeCount ?? 0)" : nil
            } else {
                navController.tabBarItem.badgeValue = "1"
            }
            
            if let topicChatListViewController =  navController.viewControllers.first as? TopicChatListViewController {
                //Convert Data in specific types
                topicChatListViewController.updateTableMsgCount(topic_id)
            }
        }
    }
    
    func pushToDirectPrivateChatVC(channel: [String:Any],_ selectedIndex: Int) {
        guard let selectedTopic = self.topicList[selectedIndex] as? Topic else {
            return
        }
        
        FirebaseUtility().getUserDetails(userId: selectedTopic.created_by!) { (user, error) in
            var senderNickName = ""
            MBProgressHUD.hide(for: self.view, animated: true)
            if user != nil, user?.nickName != nil{
                senderNickName = user?.nickName ?? ""
                self.pushToChatContainer(channel, selectedTopic, senderNickName)
            } else {
                //Fetch Nick Name
                FirebaseUtility().getNames { (names, error) in
                    if (error != nil) {
                        print("Got error while fetching names")
                    } else {
                        // Add or update device id on firebase
                        if names?.count ?? 0 > 0, let name = names?[0] {
                            FirebaseUtility().addOrUpdateNickNameWithUserId(userId: selectedTopic.created_by!, nickName:(name["name"] as? String)!) { (isAdded, error) in
                                if let error = error {
                                    print("Error getting documents: \(error)")
                                } else {
                                    if let name = name["name"] as? String {
                                        self.pushToChatContainer(channel, selectedTopic, name)
                                    }
                                    self.updateNameObjectAssigned(name)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func pushToChatContainer(_ channel: [String:Any],_ selectedTopic: Topic,_ senderNickName: String) {
        //Push to container view
        if let containerVC = self.storyboard?.instantiateViewController(withIdentifier: "ContainerViewController") as? ContainerViewController {
            guard let currentUser = Auth.auth().currentUser else { return }
            guard let selectedChannel = Channel(data: channel) else { return }
            containerVC.selectedChannel = selectedChannel
            containerVC.currentUser = currentUser
            containerVC.topic = selectedTopic
            if let topic_title = selectedTopic.topic_title {
                containerVC.title = topic_title.count > Constants.chatTitleMaxLength ? String(topic_title.prefix(Constants.chatTitleMaxLength)) + "..." : topic_title
            }
            containerVC.senderDisplayName = senderNickName
            containerVC.selectedFirebaseCollection = FirebaseConst.Collections.TopicChannels
            //User Unread counts
            containerVC.userUnreadCounts = channel[currentUser.uid] as? Int ?? 0
            self.navigationController?.show(containerVC, sender: nil)
            self.setNavigationBarColor()
        }
    }
    
    func setNavigationBarColor() {
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = .black
    }
    
    func updateTopicWithChannelId(_ channelId : String, selectedTopic : Topic) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "channelId": channelId
                    ])
                }
        }
    }
    
    func updateTopicWithChannelIdAndRef(_ channelId : String,
                                        channelRef: String,
                                        selectedTopic : Topic,
                                        currentUser: User) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    var newPrivateChannelRef: [String] = []
                    if let privateChannelRef = document?.data()["privateChannelRef"] as? [String] {
                        if !privateChannelRef.contains(channelRef) {
                            newPrivateChannelRef = privateChannelRef + [channelRef]
                        } else {
                            newPrivateChannelRef = [channelRef]
                        }
                    } else {
                        newPrivateChannelRef = [channelRef]
                    }
                    var newconnected_users:  [String] = []
                    if let connected_users = document?.data()["connected_users"] as? [String] {
                        if !connected_users.contains(currentUser.uid) {
                            newconnected_users = connected_users + [currentUser.uid]
                        } else {
                            newconnected_users = [currentUser.uid]
                        }
                    } else {
                        newconnected_users = [currentUser.uid]
                    }
                    document?.reference.updateData([
                        "channelId": channelId,
                        "privateChannelRef": newPrivateChannelRef,
                        "connected_users": newconnected_users
                    ])
                }
        }
    }
    
    func addOrUpdateChatCount(_ channelId: String, selectedTopic: Topic) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let uniqueId = uid! + "~" + channelId
        
        let currentBackgroundDate = NSDate()
        
        Firestore.firestore().collection(FirebaseConst.Collections.UserChatCount)
        .whereField("uniqueId", isEqualTo: uniqueId )
        .getDocuments(completion: { (querySnapshot, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if (querySnapshot!.documents.first?.data()) != nil{
                    print("User Chat Count already there")
                }else{
                    Firestore.firestore().collection(FirebaseConst.Collections.UserChatCount).document("\(currentBackgroundDate)").setData([
                        "uniqueId": uniqueId,
                        "chatCount": selectedTopic.msgcount
                    ]) { (err) in
                    }
                }
            }
        })
    }
    
    func deletePost(_ topic: Topic?,_ index: Int) {
        
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Removing Topic..."
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: topic?.topic_id ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        
                        loadingNotification.label.text = "Topic Removed"
                        Firestore.firestore().collection("Topics").document(data.documentID).delete() { err in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let err = err {
                                print("Error removing document: \(err)")
                            } else {
                                print("Topic Document successfully removed!")
                                
                                // Create a reference to the file to delete
                                if topic?.mediaType == "movie" {
                                    let desertRef = Storage.storage().reference().child("Topic Images/\(topic?.topic_id ?? 0).jpg")

                                    // Delete the file
                                    desertRef.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Images/\(topic?.topic_id ?? 0).jpg deleted successfully")
                                      }
                                    }
                                    
                                    let desertRefVideo = Storage.storage().reference().child("Topic Videos/\(topic?.topic_id ?? 0).mp4")

                                    // Delete the video file
                                    desertRefVideo.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Videos/\(topic?.topic_id ?? 0).mp4 deleted successfully")
                                      }
                                    }
                                } else if topic?.mediaType == "image" {
                                    let desertRef = Storage.storage().reference().child("Topic Images/\(topic?.topic_id ?? 0).jpg")

                                    // Delete the file
                                    desertRef.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Images/\(topic?.topic_id ?? 0).jpg deleted successfully")
                                      }
                                    }
                                }
                                
//                                let indexPath = IndexPath.init(row: index, section: 0)
//                                self.topicTableView.beginUpdates()
                                self.topicList.remove(at:index)
                                //Remove topic from copied array
                                if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                                    return objTopic.topic_id == topic?.topic_id
                                }) {
                                    self.topicListCopy.remove(at:foundIndex)
                                }
                                self.topicTableView.reloadData()
//                                self.topicTableView.deleteRows(at: [indexPath], with: .automatic)
//                                self.topicTableView.endUpdates()
                                
                                //Delete Sub Topics & Place
                                SubTopicFirebaseUtility().getSubTopics(topic?.topic_id, completion: { (subTopicList, error) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    if let error = error {
                                        print("Error getting documents: \(error)")
                                    } else {
                                        subTopicList?.forEach {
                                            Firestore.firestore()
                                                .collection(FirebaseConst.Collections.SubTopics)
                                                .document($0.documentId!).delete() { err in
                                                if let err = err {
                                                    print("Error removing document: \(err)")
                                                } else {
                                                    print("Sub Topic Document successfully removed!")
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }else{
                        print("No Topic to delete Data")
                    }
                }
            })
        
    }
    
    func deleteSubTopic(_ subtopic: SubTopic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Removing Sub Topic..."
        
        Firestore.firestore()
            .collection(FirebaseConst.Collections.SubTopics)
            .document(subtopic?.documentId ?? "").delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                print("Sub Topic Document successfully removed!")
                loadingNotification.label.text = "Topic Removed"
                MBProgressHUD.hide(for: self.view, animated: true)
                self.subTopicList.remove(at:index)
                self.topicTableView.reloadData()
            }
        }
    }
    
    func reportPost(_ topic: Topic?,_ index: Int) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if !(topic?.reportedIds?.contains(uid!) ?? true) ||  topic?.reportedIds == nil{
            let copyTopic = topic?.copy(with: nil) as? Topic
            var reportedIdsStringArr : [String] = []
            if (copyTopic?.reportedIds?.count ?? 0) > 0 {
                reportedIdsStringArr = copyTopic!.reportedIds?.components(separatedBy: ",") ?? []
            }
               
            //New record added
            reportedIdsStringArr.append(uid!)
               
            let reportedIdsString = (reportedIdsStringArr.count > 1 ? (reportedIdsStringArr.joined(separator: ",") ) : reportedIdsStringArr.first ?? "")
           
            copyTopic!.reportedIds = reportedIdsString
               
            self.topicList.remove(at: index)
            self.topicList.insert(copyTopic!, at: index)
//          self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
            self.hapticImpact.impactOccurred()
                               
            print("Local Topic Report Done$$")
            print(reportedIdsStringArr.count)

            //Firebase Updation
            TopicFirebaseUtility().addReportedIdstoTopic(copyTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Report Failed", message: Messages.Errors.ReportFailure, controller: self)
                    self.topicList.remove(at: index)
                    self.topicList.insert(updatedTopic!, at: index)
//                  self.topicTableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                } else {
                    print("Firebase Topic Report Done%%")
                    print(reportedIdsStringArr.count)
                }
            })
        }
    }
    
    func hidePost(_ topic: Topic?,_ index: Int) {
    
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let topic = topic else { return }
    
        //Check and add new record
        if !topic.hidePostsUserIdsArr.contains(uid) {
            topic.hidePostsUserIdsArr.append(uid)
            self.hapticImpact.impactOccurred()
            //Firebase Updation
            TopicFirebaseUtility().addHidePostUserIdstoTopic(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Hide post Failed", message: Messages.Errors.HideFailure, controller: self)
                } else {
                    print("Firebase Topic Hide Done%%")
                    self.topicList.remove(at:index)
                    self.topicTableView.reloadData()
                }
            })
        }
    }
    
    func blockUser(_ topic: Topic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Blocking User..."
        FirebaseUtility()
            .addOrUpdateBlockedUserIds(blockeduserId: topic?.created_by ?? "") { (isAdded, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Block User Failed",
                                        message: Messages.Errors.BlockUserFailure,
                                        controller: self)
            } else {
                print("Firebase Block User Done%%")
                loadingNotification.label.text = "Removing Blocked User Posts..."
                self.topicList.removeAll { (topicLocal) -> Bool in
                    return (topicLocal as? Topic)?.created_by == topic?.created_by
                }
                self.topicTableView.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
    }
    
    @IBAction func btnUpClicked(_ sender: UIButton) {
        
        guard let topic = self.topicList[sender.tag] as? Topic else { return }
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = topic.copy(with: nil) as? Topic
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
    
        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic!.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (topic.upVoteIds?.contains(uid!))! {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            let upvoteString = (upvoteStringArr.count > 1 ?
                                    upvoteStringArr.joined(separator: ",") :
                                    upvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.difference = difference
            
            self.topicTableView.beginUpdates()
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            
            //Update topic in copy array
            if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                return objTopic.topic_id == topic.topic_id
            }) {
                self.topicListCopy.remove(at: foundIndex)
                self.topicListCopy.insert(copyTopic!, at: foundIndex)
            }
            
            if topic.mediaType == "movie" {
                DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
                    self.pausePlayeVideos()
                }
            }

            self.hapticImpact.impactOccurred()
            
            //Firebase Updation
            TopicFirebaseUtility().upVoteTopicRemove(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                    //Update topic in copy array
                    if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                        return objTopic.topic_id == topic.topic_id
                    }) {
                        self.topicListCopy.remove(at: foundIndex)
                        self.topicListCopy.insert(updatedTopic!, at: foundIndex)
                    }
                } else {
//                    print("Firebase Topic Removed UpVote Done")
//                    print(upvoteStringArr.count)
                }
            })
            self.topicTableView.endUpdates()
        } else {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
//                print("uid is in downvote")
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            upvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ?
                                    upvoteStringArr.joined(separator: ",") :
                                    upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ?
                                    downvoteStringArr.joined(separator: ",") :
                                    downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            
            self.topicTableView.beginUpdates()
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            
            //Update topic in copy array
            if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                return objTopic.topic_id == topic.topic_id
            }) {
                self.topicListCopy.remove(at: foundIndex)
                self.topicListCopy.insert(copyTopic!, at: foundIndex)
            }
            
            if topic.mediaType == "movie" {
                DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
                    self.pausePlayeVideos()
                }
            }
            
            self.hapticImpact.impactOccurred()

            //Firebase Updation
            TopicFirebaseUtility().upVoteTopicNew(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                    //Update topic in copy array
                    if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                        return objTopic.topic_id == topic.topic_id
                    }) {
                        self.topicListCopy.remove(at: foundIndex)
                        self.topicListCopy.insert(updatedTopic!, at: foundIndex)
                    }
                } else {
//                    print("Firebase Topic Upvote Done%%")
//                    print(upvoteStringArr.count)
                }
            })
            self.topicTableView.endUpdates()
        }
    }
    
    @IBAction func btnDownClicked(_ sender: UIButton) {
        
        guard let topic = self.topicList[sender.tag] as? Topic else { return }
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = topic.copy(with: nil) as? Topic
        
        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic?.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (topic.downVoteIds?.contains(uid!))! {
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            let downvoteString = (downvoteStringArr.count > 1 ?
                                    downvoteStringArr.joined(separator: ",") :
                                    downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic?.downVoteIds = downvoteString
            copyTopic?.difference = difference
            
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            
            //Update topic in copy array
            if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                return objTopic.topic_id == topic.topic_id
            }) {
                self.topicListCopy.remove(at: foundIndex)
                self.topicListCopy.insert(copyTopic!, at: foundIndex)
            }
            
            if topic.mediaType == "movie" {
                DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
                    self.pausePlayeVideos()
                }
            }
            
            self.hapticImpact.impactOccurred()

            //Firebase Updation
            TopicFirebaseUtility().downVoteTopicRemove(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Downvote Failed",
                                            message: Messages.Errors.DownVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                    //Update topic in copy array
                    if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                        return objTopic.topic_id == topic.topic_id
                    }) {
                        self.topicListCopy.remove(at: foundIndex)
                        self.topicListCopy.insert(updatedTopic!, at: foundIndex)
                    }
                } else {
//                    print("Firebase Topic Remove Downvote Done")
                }
            })
        } else {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            downvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ?
                                    upvoteStringArr.joined(separator: ",") :
                                    upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ?
                                    downvoteStringArr.joined(separator: ",") :
                                    downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            
            //Update topic in copy array
            if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                return objTopic.topic_id == topic.topic_id
            }) {
                self.topicListCopy.remove(at: foundIndex)
                self.topicListCopy.insert(copyTopic!, at: foundIndex)
            }
            
            if topic.mediaType == "movie" {
                DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
                    self.pausePlayeVideos()
                }
            }
            
            self.hapticImpact.impactOccurred()

            TopicFirebaseUtility().downVoteTopicNew(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Downvote Failed",
                                            message: Messages.Errors.DownVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                    //Update topic in copy array
                    if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                        return objTopic.topic_id == topic.topic_id
                    }) {
                        self.topicListCopy.remove(at: foundIndex)
                        self.topicListCopy.insert(updatedTopic!, at: foundIndex)
                    }
                } else {
//                    print("Firebase Topic Downvote Done")
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        if indexPath.section == 0 {
            let user = Auth.auth().currentUser
            let uid = user?.uid
            let topic = self.topicList[indexPath.row]
            if appdelegate!.isAdminUser { return .delete }
            else if (topic as? Topic)?.created_by == uid { return .delete }
            else { return .none }
        } else {
            if appdelegate!.isAdminUser { return .delete }
            else { return .none }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        guard let topic = self.topicList[indexPath.row] as? Topic else { return }
        if indexPath.section == 0 {
            let user = Auth.auth().currentUser
            let uid = user?.uid
            if appdelegate!.isAdminUser || topic.created_by == uid {
                if editingStyle == .delete {
                    //Delete Action on firebse
                    self.showDelelePostAlert(topic, indexPath.row,.Topic,nil)
                }
            }
        } else {
            if appdelegate!.isAdminUser {
                if editingStyle == .delete {
                    //Delete Action on firebse
                    if indexPath.section == 0 {
                        self.showDelelePostAlert(topic, indexPath.row,.Topic,nil)
                    } else {
                        let subtopic = self.subTopicList[indexPath.row]
                        self.showDelelePostAlert(nil, indexPath.row,.SubTopic,subtopic)
                    }
                }
            }
        }
    }
    
    @IBAction func btnMuteClicked(_ sender: UIButton) {
//        let topic = self.topicList[sender.tag]
//        if topic.mediaType == "movie" {
//            if UserDefaults.standard.bool(forKey: "isVideoMute") {
//                UserDefaults.standard.set(false, forKey: "isVideoMute")
//                UserDefaults.standard.synchronize()
//                sender.isSelected = false
//            } else {
//                UserDefaults.standard.set(true, forKey: "isVideoMute")
//                UserDefaults.standard.synchronize()
//                sender.isSelected = true
//            }
//
//            if let cell = tableView(self.topicTableView, cellForRowAt: IndexPath.init(row: sender.tag, section: 0)) as? TopCell {
//                cell.muteUnmute()
//            }
//            return
//        }
    }
    
    @IBAction func btnViewImageClicked(_ sender: UIButton) {
        guard let topic = self.topicList[sender.tag] as? Topic else {
            return
        }
        if topic.mediaType == "gif" {
            guard let fullScreenGif = self.storyboard?.instantiateViewController(withIdentifier: "FullScreenGIFViewController") as? FullScreenGIFViewController else { return }
            fullScreenGif.topic = topic
            self.present(fullScreenGif, animated: true) {}
        } else if topic.mediaType == "movie" {
            
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } catch let error {
                print("Error in AVAudio Session\(error.localizedDescription)")
            }
            
            if let cell = tableView(self.topicTableView, cellForRowAt: IndexPath.init(row: sender.tag, section: 0)) as? TopCell {
                cell.muteUnmute()
                //Show in full screen
                let playerVC = VideoPlayerController.sharedVideoPlayer
                if let player = playerVC.getCurrentPlayer() {
                    let playerController = AVPlayerViewController()
                    playerController.player = player
                    present(playerController, animated: true) {
//                        player.play()
                        player.isMuted = false
                    }
                }
            }
        } else {
            //Remove old items
            items.removeAll()
            // Show the ImageViewer with the first item
            var galleryItem: GalleryItem!
            let cell = tableView(self.topicTableView, cellForRowAt: IndexPath.init(row: sender.tag, section: 0)) as? TopCell
            if topic.mediaType == "movie" {
                galleryItem = GalleryItem.video(fetchPreviewImageBlock: { $0(cell?.imgTopic.image) }, videoURL: URL.init(string: topic.videoUrl!)!)
            } else {
                galleryItem = GalleryItem.image { $0(cell?.imgTopic.image) }
            }
            
            items.append(DataItem(imageView: cell!.imgTopic,
                                  galleryItem: galleryItem))
            self.presentImageGallery(GalleryViewController(startIndex: 0,
                                                           itemsDataSource: self,
                                                           configuration: CommonUtility.galleryConfiguration()))
        }
    }
    
    func  pausePlayeVideos(){
        VideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: self.topicTableView)
    }
    
    @objc func appEnteredFromBackground() {
        VideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: self.topicTableView, appEnteredFromBackground: true)
    }
}

// The GalleryItemsDataSource provides the items to show
extension TopicNewsViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        return items.count
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return items[index].galleryItem
    }
}

