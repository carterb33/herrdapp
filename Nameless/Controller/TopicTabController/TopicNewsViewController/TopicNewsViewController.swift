//
//  TopicNewsViewController.swift
//  Herrd
//
//  Created by DG on 21/01/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import FirebaseAuth
import FirebaseUI
import GoogleMobileAds
import SafariServices
import WMSegmentControl


class TopicNewsViewController: UIViewController {
    
    @IBOutlet weak var lblCityTitle: UILabel!
    @IBOutlet weak var topicTableView: UITableView!
    @IBOutlet weak var topicSegment: WMSegment!
    @IBOutlet weak var btnNewTopic: UIButton!
    @IBOutlet weak var adminIcon: UIImageView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblSmilyIcon: UILabel!
    @IBOutlet weak var lblMessageTitle: UILabel!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet var chatButton: UIButton!
    @IBOutlet weak var venueButton: UIButton!
    @IBOutlet weak var schoolButton: UIButton!
    var lastSelectedSegment = 0
    var lastDocOfPage: DocumentSnapshot?
    var lastDocOfPageSub: DocumentSnapshot?
    var isFetchingData = false
    var isFetchingDataSub = false
    var isEndOfDocs = false
    var isEndOfDocsSub = false
    var pageNumber = 0
    let badgeSize: CGFloat = 25
    let badgeTag = 9830384
    
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var lblBadgeCount: UILabel!
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self, action: #selector(refreshAndSortListData(sender:)), for: .valueChanged)
        return control
    }()
    var topicList : [AnyObject] = []
    var topicListCopy : [Topic] = []
    var privateChatTopicList : [Topic] = []
    var subTopicList : [SubTopic] = []
    var adminList : [String] = []
    var authUI: FUIAuth!
    var items: [DataItem] = []
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    let reloadAfterTime = 0.0001
    
    var isScrollToTop = false
    /// The ad unit ID from the AdMob UI.
    let adUnitID = "ca-app-pub-8946459992553219/1646164482"
    var adLoader: GADAdLoader!
    /// The number of native ads to load (must be less than 5).
    let numAdsToLoad = 0
    var nativeAdsReceivedCount = 0
    /// The native ads.
    var nativeAds = [GADNativeAd]()
    
    let gradiantImage = UIImage(named: "gradientRec6")!
//    let hideVenue = UserDefaults.standard.value(forKey: "venueOn") as? Int

    
    override func viewDidLoad() {
        print("$$$")
        
        super.viewDidLoad()
        venueButton.tintColor = AppColor.CommonColor.postColor
        calendarButton.tintColor = AppColor.CommonColor.postColor
        chatButton.tintColor = AppColor.CommonColor.postColor
        
        self.topicTableView.separatorStyle = .none
        calendarButton.layer.cornerRadius = 20
        calendarButton.layer.borderWidth = 0.70
        calendarButton.layer.borderColor = UIColor.darkGray.cgColor
        
        //Segment with type Bottom Bar
        topicSegment.selectorType = .bottomBar
        topicSegment.textColor = AppColor.CommonColor.secondaryColor
        topicSegment.selectorTextColor = AppColor.CommonColor.postColor
        topicSegment.selectorColor = UIColor.init(patternImage: gradiantImage)
        
        topicSegment.SelectedFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .bold)
        topicSegment.normalFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .regular)
        //Add Logo in Navigation bar
        CommonUtility.addLogoInNavigationBar(self.navigationController, self.navigationItem)
        
        topicTableView.dataSource = self
        topicTableView.delegate = self
        topicTableView.estimatedRowHeight = 100
        
        addPullToRefresh()
        
        authUI = FUIAuth.defaultAuthUI()
        authUI?.delegate = self
        
        if authUI.auth?.currentUser != nil {
            setupforAds()
            self.checkAdminUser()
            self.checkNickNameAndAssign()
            self.getAllPrivateChatTopics()
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.delegate = self
        //Setting height width again
        NSLayoutConstraint.activate([
            chatButton.widthAnchor.constraint(equalToConstant: 60),
            chatButton.heightAnchor.constraint(equalToConstant: 44),
        ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        signIn()
        pausePlayeVideos()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    func setupforAds() {
        
        //Reset received ads count to 0
        self.nativeAdsReceivedCount = 0
        let options = GADMultipleAdsAdLoaderOptions()
        if let numberOfAdsFirebse = UserDefaults.standard.value(forKey: "numberOfAds") as? Int {
            options.numberOfAds = numberOfAdsFirebse
        } else {
            options.numberOfAds = numAdsToLoad
        }
        
        let mediaOption = GADNativeAdMediaAdLoaderOptions()
        mediaOption.mediaAspectRatio = .square
        // Prepare the ad loader and start loading ads.
        adLoader = GADAdLoader(adUnitID: adUnitID,
                               rootViewController: self,
                               adTypes: [.native],
                               options: [options,mediaOption])
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }
    
    func checkNickNameAndAssign() {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        FirebaseUtility().getUserDetails(userId: currentUid) { (user, error) in
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.currentUserDetails = user
            if user != nil, user?.nickName != nil{
            } else { self.assignNickName(currentUid) }
        }
    }
    
    func assignNickName(_ uid: String) {
        //Fetch Nick Name
        FirebaseUtility().getNames { (names, error) in
            if (error != nil) {
                print("Got error while fetching names")
            } else {
                // Add or update device id on firebase
                if names?.count ?? 0 > 0, let name = names?[0] {
                    FirebaseUtility().addOrUpdateNickNameWithUserId(userId: uid, nickName:(name["name"] as? String)!) { (isAdded, error) in
                        if let error = error {
                            print("Error getting documents: \(error)")
                        } else {
                            self.updateNameObjectAssigned(name)
                        }
                    }
                }
            }
        }
    }
    
    func updateNameObjectAssigned(_ name: [String:Any]) {
        FirebaseUtility().updateNickNameAssignedWithId(Id: name["id"] as! String) { (isAssigned, error) in
            if (error != nil) {
                print("Got error while fetching names")
            } else {
                print("Name Object assigned successfully.")
            }
        }
    }
    
    //SIGN IN FUNCTION
    func signIn() {
        if authUI.auth?.currentUser == nil {
            self.authUI?.providers = [FUIGoogleAuth(authUI: self.authUI)]
            if let presentedViewController = self.presentedViewController, presentedViewController.isKind(of: UIAlertController.self) {
                presentedViewController.dismiss(animated: true) {
                    self.present(self.authUI.authViewController(), animated: true, completion: {})
                }
            } else {
                self.present(authUI.authViewController(), animated: true, completion: {})
            }
        } else {
            self.topicTableView.isHidden = false
        }
    }
    
    //LOGGING OUT A USER
    @IBAction func logOut(_ sender: UIBarButtonItem) {
        do {
            try authUI!.signOut()
            print("^^ Successfully signed out")
            topicTableView.isHidden = true
            let appdelegate = UIApplication.shared.delegate as? AppDelegate
            appdelegate?.isAdminUser = false
            self.topicList = []
            self.topicListCopy = []
            signIn()
        } catch {
            topicTableView.isHidden = true
            print("** ERROR: Couldn't sign out")
        }
    }
    
    func checkAdminUser() {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }

        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        
        if let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String] {
            self.adminList = adminlist
            if self.adminList.contains(uid) {
                appdelegate?.isAdminUser = true
                //Add extra segment in topic seg
                if self.topicSegment.buttonTitles.components(separatedBy: ",").count < 3 {
                    self.topicSegment.buttonTitles = "Top,New,Review"
                }
            } else {
                if self.topicSegment.buttonTitles.components(separatedBy: ",").count > 2 {
                    self.topicSegment.buttonTitles = "Top,New,Review"
                }
                appdelegate?.isAdminUser = false

            }
            //First time call to get topics
            self.isEndOfDocs = false
            self.lastDocOfPage = nil
            self.topicListCopy = []
            self.pageNumber = 0
            if self.topicSegment.selectedSegmentIndex == 2 {
                self.lastDocOfPage = nil
                self.isEndOfDocs = false
                self.topicList = []
                self.lastDocOfPageSub = nil
                self.isEndOfDocsSub = false
                self.subTopicList = []
            }
            self.getAllTopics()
        } else {
            FirebaseUtility().getAppSettings { (error, doc) in
                if let err = error {
                    print("Got error while reading app settings",err)
                }
                //Call Again to get admin list
                self.checkAdminUser()
            }
        }
    }
    
    @IBAction func btnActionPress(_ sender: UIButton) {
        switch sender.titleLabel?.text {
        case "New Post":
            opneNewTopicPopUp()
        default:
            print("Defualt action")
        }
    }
    
    @IBAction func settingPress(_ sender: UIButton) {
        if let eventViewController = self.storyboard?.instantiateViewController(withIdentifier: "EventViewController") as? EventViewController {
            self.navigationController?.pushViewController(eventViewController, animated: true)
        }
    }
    
    @IBAction func chatPress(_ sender: UIButton) {
        if let topicChatListViewController = self.storyboard?.instantiateViewController(withIdentifier: "TopicChatListViewController") as? TopicChatListViewController {
            self.navigationController?.pushViewController(topicChatListViewController, animated: true)
        }
    }
    
    
    @IBAction func infoPress(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Important",
                                                message: Messages.Info.AnonymousPost,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func topicSegmentValueChange(_ sender: WMSegment) {
        self.lastDocOfPage = nil
        self.isEndOfDocs = false
        self.topicListCopy = []
        self.pageNumber = 0
        
        if self.topicSegment.selectedSegmentIndex == 2 {
            self.lastDocOfPage = nil
            self.isEndOfDocs = false
            self.topicList = []
            self.lastDocOfPageSub = nil
            self.isEndOfDocsSub = false
            self.subTopicList = []
        }
        self.getAllTopics()
    }
    
    @IBAction func btnNewTopicClicked(_ sender: Any) {
        opneNewTopicPopUp()
    }
    
    func opneNewTopicPopUp() {
        let newTopicVC = self.storyboard?.instantiateViewController(withIdentifier: "NewTopicViewController") as! NewTopicViewController
        newTopicVC.modalPresentationStyle = .custom
        newTopicVC.transitioningDelegate = self
        newTopicVC.adminList = self.adminList
        
        //        self.navigationController?.pushViewController(newTopicVC, animated: true)
        self.present(newTopicVC, animated: true, completion: {})
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            topicTableView.refreshControl = refreshControl
        } else {
            topicTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.isEndOfDocs = false
        self.lastDocOfPage = nil
        self.topicListCopy = []
        self.pageNumber = 0
        if self.topicSegment.selectedSegmentIndex == 2 {
            self.lastDocOfPage = nil
            self.isEndOfDocs = false
            self.topicList = []
            self.lastDocOfPageSub = nil
            self.isEndOfDocsSub = false
            self.subTopicList = []
        }
        self.getAllTopics()
        self.getAllPrivateChatTopics()
    }
    
    func getAllTopics(){
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        switch self.topicSegment.selectedSegmentIndex {
        case 0:
            if self.lastDocOfPage != nil {
                self.getTopTopics()
            } else {
                self.getPinnedTopics()
            }
        case 1:
            self.getNewTopics()
        case 2:
            self.getReportedTopic()
            self.getReportedSubTopics()
        default:
            print("Default Segment called")
        }
        
        self.lastSelectedSegment = self.topicSegment.selectedSegmentIndex
    }
    
    func getReportedTopic() {
        isFetchingData = true
        if let lastDoc = self.lastDocOfPage {
            TopicFirebaseUtility().getReportedTopicsNextPage(lastDoc, { (topicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if topicList?.count ?? 0 > 0 {
                        self.filterTopicsByReportCount(topicList ?? [])
                        self.lastDocOfPage = lastDoc
                    } else {
                        self.isEndOfDocs = true
                    }
                }
            })
        } else {
            TopicFirebaseUtility().getReportedTopicsFirstPage { (topicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if topicList?.count ?? 0 > 0 {
                        self.topicTableView.isHidden = false
                        self.messageView.isHidden = true
                        self.filterTopicsByReportCount(topicList ?? [])
                        self.lastDocOfPage = lastDoc
                    } else {
                        self.topicTableView.isHidden = true
                        self.lblMessageTitle.text = Messages.Info.NoRecordInReview
                        self.btnAction.isHidden = true
                        self.messageView.isHidden = false
                    }
                }
            }
        }
    }
    
    func getReportedSubTopics() {
        isFetchingDataSub = true
        if let lastDoc = self.lastDocOfPageSub {
            SubTopicFirebaseUtility().getReportedSubTopicsNextPage(lastDoc, { (subtopicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if subtopicList?.count ?? 0 > 0 {
                        self.filterSubTopicsByReportCount(subtopicList ?? [])
                        self.lastDocOfPageSub = lastDoc
                    } else {
                        self.isEndOfDocsSub = true
                    }
                }
            })
        } else {
            
            SubTopicFirebaseUtility().getReportedSubTopicsFirstPage { (subtopicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if subtopicList?.count ?? 0 > 0 {
                        self.topicTableView.isHidden = false
                        self.messageView.isHidden = true
                        self.filterSubTopicsByReportCount(subtopicList ?? [])
                        self.lastDocOfPageSub = lastDoc
                    } else {
                        if self.topicList.count == 0 {
                            self.topicTableView.isHidden = true
                            self.lblMessageTitle.text = Messages.Info.NoRecordInSubTopicReview
                            self.btnAction.isHidden = true
                            self.messageView.isHidden = false
                        }
                    }
                }
            }
        }
    }
    
    func getNewTopics() {
        isFetchingData = true
        if let lastDoc = self.lastDocOfPage {
            TopicFirebaseUtility().getNewTopicsNextPage(lastDoc, { (topicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if topicList?.count ?? 0 > 0 {
                        self.pageNumber += 1
                        self.filterTopicsByNewOnly(topicList ?? [])
                        self.lastDocOfPage = lastDoc
                    } else {
                        self.isEndOfDocs = true
                    }
                }
            })
        } else {
            TopicFirebaseUtility().getNewTopicsFirstPage({ (topicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.topicTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    }
                    //self.topicTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .middle, animated: false)
//                    self.topicTableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.topicTableView.frame.size.width, height: self.topicTableView.frame.size.height), animated: true)
                    if topicList?.count ?? 0 > 0 {
                        self.topicTableView.isHidden = false
                        self.messageView.isHidden = true
                        self.filterTopicsByNewOnly(topicList ?? [])
                        self.lastDocOfPage = lastDoc
                    } else {
                        self.topicTableView.isHidden = true
                        self.lblMessageTitle.text = Messages.Info.NoRecordInTopic
                        self.btnAction.isHidden = true
                        self.messageView.isHidden = false
                    }
                }
            })
        }
    }
    
    func getPinnedTopics() {
        TopicFirebaseUtility().getAllPinnedTopics( { (topicList, error) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                if let topicList = topicList, topicList.count > 0 {
                    self.topicListCopy.append(contentsOf: topicList)
                }
                DispatchQueue.main.async {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                }
                self.getTopTopics()
            }
        })
    }
    
    func getTopTopics() {
        isFetchingData = true
        if let lastDoc = self.lastDocOfPage {
            TopicFirebaseUtility().getTopTopicsNextPage(lastDoc, { (topicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if let topicList = topicList, topicList.count > 0 {
                        self.pageNumber += 1
                        self.filterTopicsByVote(topicList)
                        self.lastDocOfPage = lastDoc
                    } else {
                        self.isEndOfDocs = true
                    }
                }
            })
        } else {
            TopicFirebaseUtility().getTopTopicsFirstPage({ (topicList, error, lastDoc) in
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.topicTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    }
                    if let topicList = topicList, topicList.count > 0 {
                        self.topicTableView.isHidden = false
                        self.messageView.isHidden = true
                        self.filterTopicsByVote(topicList)
                        self.lastDocOfPage = lastDoc
                    } else {
                        self.topicTableView.isHidden = true
                        self.lblMessageTitle.text = Messages.Info.NoRecordInTopic
                        self.btnAction.isHidden = true
                        self.messageView.isHidden = false
                    }
                }
            })
        }
    }
    
    func filterTopicsByVote(_ topicList: [Topic]) {
        
        self.topicListCopy.append(contentsOf: topicList)
        self.topicList = []
        //Admin user ids
        let filteredAdminTopics = self.topicListCopy.filter() {
            self.adminList.contains($0.created_by ?? "")
        }
        var filteredTopics = self.topicListCopy.filter() {
            !self.adminList.contains($0.created_by ?? "")
        }
          
        //Sort Array by Vote count
        filteredTopics.sort(by: {
            $0.difference! > $1.difference!
        })
        
        self.topicList = filteredAdminTopics + filteredTopics
        //Add Ads object
        self.addAdsObjectInTopicList()
        let playerVC = VideoPlayerController.sharedVideoPlayer
        playerVC.resetAndClearCache(tableView: self.topicTableView)
        self.topicTableView.reloadData()
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        self.isFetchingData = false
    }
    
    func filterTopicsByNewOnly(_ topicList: [Topic]) {
        //Backup topics
        self.topicListCopy.append(contentsOf: topicList)
        self.topicList = []
        
        var filteredTopics = self.topicListCopy.filter() {
            !self.adminList.contains($0.created_by ?? "")
        }
        
        //Sort Array by Date
        filteredTopics.sort(by: {
            $0.topic_date!.dateValue() > $1.topic_date!.dateValue()
        })
        
        self.topicList = filteredTopics
        //Add Ads object
        self.addAdsObjectInTopicList()
        
        let playerVC = VideoPlayerController.sharedVideoPlayer
        playerVC.resetAndClearCache(tableView: self.topicTableView)
        self.topicTableView.reloadData()
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        self.isFetchingData = false
    }
    
    func filterTopicsByReportCount(_ reftopicList: [Topic]) {
        var topicReportedList: [Topic] = reftopicList
        //Sort Array by Date
        topicReportedList.sort(by: {
            return ($0.reportedIds?.components(separatedBy: ",").count ?? 0) > ($1.reportedIds?.components(separatedBy: ",").count ?? 0)
        })
        self.topicList.append(contentsOf: topicReportedList)
        
        let playerVC = VideoPlayerController.sharedVideoPlayer
        playerVC.resetAndClearCache(tableView: self.topicTableView)
        self.topicTableView.reloadData()
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        self.isFetchingData = false
    }
    
    func filterSubTopicsByReportCount(_ reftopicList: [SubTopic]) {
        self.subTopicList.append(contentsOf: reftopicList)
        //Sort Array by Date
        self.subTopicList.sort(by: {
            return ($0.reportedIds?.components(separatedBy: ",").count ?? 0) > ($1.reportedIds?.components(separatedBy: ",").count ?? 0)
        })
        
        let playerVC = VideoPlayerController.sharedVideoPlayer
        playerVC.resetAndClearCache(tableView: self.topicTableView)
        self.topicTableView.reloadData()
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        self.isFetchingDataSub = false
    }
    
    func getAllPrivateChatTopics(){
        
        //Get My Topic with Private chat
        TopicFirebaseUtility().getMyTopicsWithPrivateChannels { (mytopicList, error) in
            if let error = error {
                print("Error getting documents: \(error)")
            }
            //Get Other Topics with Private chat
            TopicFirebaseUtility().getTopicsByPrivateChannel { (topicList, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if let mytopics = mytopicList, mytopics.count > 0 {
                        self.privateChatTopicList = mytopics
                        if let topicList = topicList, topicList.count > 0 {
                            self.setTabBarBadgeCount(mytopics + topicList)
                        } else {
                            self.setTabBarBadgeCount(mytopics)
                        }
                    } else {
                        if let topicList = topicList, topicList.count > 0 {
                            self.setTabBarBadgeCount(topicList)
                        }
                    }
                }
            }
        }
    }
    
    func setTabBarBadgeCount(_ topicList: [Topic]) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let totalCount = topicList.reduce(0) { (total, topic: Topic) -> Int in
            if let privateMsgCount = topic.privateMsgCount {
                if let msgCount = privateMsgCount[uid] {
                    return total + msgCount
                } else { return total }
            } else { return total }
        }
        
        DispatchQueue.main.async {
            if totalCount > 0 {
                self.lblBadgeCount.isHidden = false
                self.lblBadgeCount.text = "\(totalCount)"
            } else {
                self.lblBadgeCount.isHidden = true
            }
        }
    }
    
    func addAdsObjectInTopicList() {
        if nativeAds.count <= 0 { return }
        let adsCountToBeAdded = Int(self.topicList.count / 20)
        for index in 1...adsCountToBeAdded {
            let adIndex = (index - 1) % nativeAds.count
            if adIndex == nativeAds.count - 2 {
                //Adding more ads
                setupforAds()
            }
            if adIndex < nativeAds.count {
                let nativeAd = self.nativeAds[adIndex]
                let insertAdAtIndex = index * (Constants.pageSize - 5)
                self.topicList.insert(nativeAd, at: insertAdAtIndex)
            }
        }
    }
}

// MARK: - GADUnifiedNativeAdDelegate implementation
extension TopicNewsViewController: GADNativeAdDelegate {

  func nativeAdDidRecordClick(_ nativeAd: GADNativeAd) {
    print("\(#function) called")
  }

  func nativeAdDidRecordImpression(_ nativeAd: GADNativeAd) {
    print("\(#function) called")
  }

  func nativeAdWillPresentScreen(_ nativeAd: GADNativeAd) {
    print("\(#function) called")
  }

  func nativeAdWillDismissScreen(_ nativeAd: GADNativeAd) {
    print("\(#function) called")
  }

  func nativeAdDidDismissScreen(_ nativeAd: GADNativeAd) {
    print("\(#function) called")
  }

  func nativeAdWillLeaveApplication(_ nativeAd: GADNativeAd) {
    print("\(#function) called")
  }
}

extension TopicNewsViewController : GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAds.append(nativeAd)
        nativeAdsReceivedCount+=1
    }
}

extension TopicNewsViewController : GADAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print("\(adLoader) failed with error: \(error.localizedDescription)")
        nativeAdsReceivedCount+=1
    }
}

extension TopicNewsViewController : UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UILabel {

    func addImageWith(name: String, behindText: Bool) {

        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: name)
        let attachmentString = NSAttributedString(attachment: attachment)

        guard let txt = self.text else {
            return
        }

        if behindText {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSAttributedString(string: txt)
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }

    func removeImage() {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
}

extension TopicNewsViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}
