//
//  FullScreenGIFViewController.swift
//  Herrd
//
//  Created by DG on 21/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit
import GiphyUISDK

class FullScreenGIFViewController: UIViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    var topic: Topic?
    var mediaView = GPHMediaView()
    override func viewDidLoad() {
        super.viewDidLoad()
        //SetUp GIF
        self.mediaView.frame = CGRect(x: 5,
                                      y: self.view.frame.height/2 - self.view.frame.size.width/2 - 5,
                                      width: self.view.frame.size.width - 10,
                                      height: self.view.frame.width)
        self.view.addSubview(mediaView)
        GiphyCore.shared.gifByID(self.topic?.gifId ?? "") { (response, error) in
            if let media = response?.data {
                DispatchQueue.main.sync {
                    self.mediaView.media = media
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    }
                    self.mediaView.layer.cornerRadius = 10
                    self.mediaView.clipsToBounds = true
                    print("Media Loaded for: ",self.topic?.gifId ?? "",self.mediaView.frame)
                }
            }
        }
        //Set Button Image
        self.btnClose.setImage(FullScreenGIFViewController.closeShape(edgeLength: 15).toImage(), for: .normal)
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    static func closeShape(edgeLength: CGFloat) -> CAShapeLayer {

        let container = CAShapeLayer()
        container.bounds.size = CGSize(width: edgeLength + 4, height: edgeLength + 4)
        container.frame.origin = CGPoint.zero

        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: 0, y: 0))
        linePath.addLine(to: CGPoint(x: edgeLength, y: edgeLength))
        linePath.move(to: CGPoint(x: 0, y: edgeLength))
        linePath.addLine(to: CGPoint(x: edgeLength, y: 0))

        let elementBorder = CAShapeLayer()
        elementBorder.bounds.size = CGSize(width: edgeLength, height: edgeLength)
        elementBorder.position = CGPoint(x: container.bounds.midX, y: container.bounds.midY)
        elementBorder.lineCap = CAShapeLayerLineCap.round
        elementBorder.path = linePath.cgPath
        elementBorder.strokeColor = UIColor.darkGray.cgColor
        elementBorder.lineWidth = 2.5

        let elementFill = CAShapeLayer()
        elementFill.bounds.size = CGSize(width: edgeLength, height: edgeLength)
        elementFill.position = CGPoint(x: container.bounds.midX, y: container.bounds.midY)
        elementFill.lineCap = CAShapeLayerLineCap.round
        elementFill.path = linePath.cgPath
        elementFill.strokeColor = UIColor.white.cgColor
        elementFill.lineWidth = 2

        container.addSublayer(elementBorder)
        container.addSublayer(elementFill)

        return container
    }

}

extension CALayer {

    func toImage() -> UIImage {

        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        self.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
}
