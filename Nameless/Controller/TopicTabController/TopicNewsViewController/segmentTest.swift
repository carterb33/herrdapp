//
//  segmentTest.swift
//  Herrd
//
//  Created by Carter Beaulieu on 6/2/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit

extension UISegmentedControl{
    func removeBorder(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.black.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.black.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        let postColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)

        let attributesSelected = [
            NSAttributedString.Key.foregroundColor : postColor,
            NSAttributedString.Key.font : UIFont(name: "ArialRoundedMTBold", size: 17)
        ]
        
        let attributesNormal = [
            NSAttributedString.Key.foregroundColor : UIColor.darkGray,
            NSAttributedString.Key.font : UIFont(name: "ArialRoundedMTBold", size: 17)
        ]
        
        self.setTitleTextAttributes(attributesSelected as [NSAttributedString.Key : Any], for: .selected)

        self.setTitleTextAttributes(attributesNormal as [NSAttributedString.Key : Any], for: .normal)

    }

    func addUnderlineForSelectedSegment(){
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth - 1 ))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        let postColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
        underline.backgroundColor = AppColor.CommonColor.postColor
        underline.tag = 1
        self.addSubview(underline)
    }
    
    func addUnderlineForSelectedSegmentMain(){
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments+1)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth - 1 ))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        let postColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
        underline.backgroundColor = AppColor.CommonColor.postColor
        underline.tag = 1
        self.addSubview(underline)
    }
    


    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments )) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            if self.selectedSegmentIndex == 1 {
                underline.frame.origin.x = underlineFinalXPosition + 6
            }
            if self.selectedSegmentIndex == 0{
                underline.frame.origin.x = underlineFinalXPosition
            }
            if self.selectedSegmentIndex == 2 {
                underline.frame.origin.x = underlineFinalXPosition + 12
            }
        })
    }
    
   
}

extension UIImage{

    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}
