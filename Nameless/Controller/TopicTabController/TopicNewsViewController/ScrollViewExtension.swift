//
//  ScrollViewExtension.swift
//  Herrd
//
//  Created by DG on 16/05/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit

extension TopicNewsViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pausePlayeVideos()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            pausePlayeVideos()
        }
    }
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if !decelerate {
//            self.playVideoOnVisiableCell()
//        }
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if !scrollView.isDecelerating && !scrollView.isDragging {
//            self.playVideoOnVisiableCell()
//        }
//    }
//
//    func playVideoOnVisiableCell() {
////        let appDelegate = UIApplication.shared.delegate
//        if self.topicList.count > 0 {
//
////            if let cell = self.topicTableView.visibleCells.first as? TopCell {
////
////            }
//
//            for (index, cell) in self.topicTableView.visibleCells.enumerated()
//            {
////                print("\(index) = \(cell)")
//                let ccellRect = self.view.convert(cell.bounds, to: cell)
//                print(index," ", ccellRect.origin.y)
//                let topCell = cell as? TopCell
//                if index == 0 {
//                    topCell?.avPlayer?.play()
//                } else {
//                    topCell?.avPlayer?.pause()
//                }
//            }
//
////            for cell in self.topicTableView.visibleCells {
////                let topCell = cell as? TopCell
////                let ccellRect = self.view.convert(topCell!.bounds, to: topCell)
////                print(ccellRect.origin.y, topCell)
////                // NSLog(@"--Cell frame %f",ccellRect.origin.y);
////                let limit = CGFloat(-260)
////                //Set Condition of cell visible within some range
////                if(ccellRect.origin.y > limit)
////                {
////                    topCell?.avPlayer?.play()
////                } else {
////                    topCell?.avPlayer?.pause()
////                }
////            }
//        }
//    }
}
