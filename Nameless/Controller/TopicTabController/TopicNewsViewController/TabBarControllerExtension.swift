//
//  TabBarControllerDelegateExtension.swift
//  Herrd
//
//  Created by DG on 12/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit

extension TopicNewsViewController : UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let oldNav = tabBarController.selectedViewController as? UINavigationController
        if let lastVC = oldNav?.viewControllers.last, lastVC is TopicNewsViewController  {
            self.isScrollToTop = true
        }
        
        (tabBarController as! MainTabBarController).lastTabSelection = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let lastSelectedIndex = (tabBarController as! MainTabBarController).lastTabSelection
        if self.isScrollToTop && lastSelectedIndex == tabBarController.selectedIndex {
            self.scrollToTop()
        }
    }
    
    func scrollToTop()
    {
        self.topicTableView.scrollRectToVisible(CGRect(x: 0,y: 0,width: self.topicTableView.frame.width, height: self.topicTableView.frame.height), animated: true)
        let indexPath = IndexPath(row: 0, section: 0)
        self.topicTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        self.isScrollToTop = false
    }
}

extension TopicChatListViewController : UITabBarControllerDelegate {
 
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let oldNav = tabBarController.selectedViewController as? UINavigationController
        if let lastVC = oldNav?.viewControllers.last, lastVC is TopicNewsViewController  {
            self.isScrollToTop = true
        }
        
        (tabBarController as! MainTabBarController).lastTabSelection = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let lastSelectedIndex = (tabBarController as! MainTabBarController).lastTabSelection
        if self.isScrollToTop && lastSelectedIndex == tabBarController.selectedIndex {
            self.scrollToTop()
        }
    }
    
    func scrollToTop()
    {
        self.topicTableView.scrollRectToVisible(CGRect(x: 0,y: 0,width: self.topicTableView.frame.width, height: self.topicTableView.frame.height), animated: true)
        self.isScrollToTop = false
    }
}

extension MineViewController : UITabBarControllerDelegate {
 
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let oldNav = tabBarController.selectedViewController as? UINavigationController
        if let lastVC = oldNav?.viewControllers.last, lastVC is TopicNewsViewController  {
            self.isScrollToTop = true
        }
        (tabBarController as! MainTabBarController).lastTabSelection = tabBarController.selectedIndex
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let lastSelectedIndex = (tabBarController as! MainTabBarController).lastTabSelection
        if self.isScrollToTop && lastSelectedIndex == tabBarController.selectedIndex {
            self.scrollToTop()
        }
    }
    
    func scrollToTop()
    {
        self.topicTableView.scrollRectToVisible(CGRect(x: 0,y: 0,width: self.topicTableView.frame.width, height: self.topicTableView.frame.height), animated: true)
        self.isScrollToTop = false
    }
}
