//
//  ReplySubTopicViewController.swift
//  Herrd
//
//  Created by DG on 05/08/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import ImageViewer
import WMSegmentControl

class ReplySubTopicViewController: UIViewController {

    @IBOutlet weak var subTopicTableView: UITableView!
    @IBOutlet weak var subTopicSegment: WMSegment!
    @IBOutlet weak var btnNewSubTopic: UIButton!
    //Topic Detail View
    @IBOutlet weak var lblTopicName: UILabel!
    @IBOutlet weak var lblTopicDate: UILabel!
    @IBOutlet weak var lblTopicCount: UILabel!
    @IBOutlet weak var btnUp: UIButton!
    @IBOutlet weak var btnDown: UIButton!
    @IBOutlet weak var lblSubTopicCount: UILabel!
    @IBOutlet weak var imgTopic: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var upVoteTotal: UILabel!
    @IBOutlet weak var downVoteTotal: UILabel!
    @IBOutlet weak var topview: UIView!
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self, action: #selector(refreshAndSortListData(sender:)), for: .valueChanged)
        return control
    }()
    
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    var subTopicList : [SubTopic] = []
    var selectedSubTopic : SubTopic?
    var items: [DataItem] = []
    var actionFrom: ActionFromType?
    var subTopicId : Double = 0
    
    let gradiantImage = UIImage(named: "gradientRec6")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subTopicTableView.separatorStyle = .none
        
        //Segment with type Bottom Bar
        subTopicSegment.selectorType = .bottomBar
        subTopicSegment.textColor = AppColor.CommonColor.secondaryColor
        subTopicSegment.selectorTextColor = AppColor.CommonColor.postColor
        subTopicSegment.selectorColor = UIColor.init(patternImage: gradiantImage)
        
        subTopicSegment.SelectedFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .bold)
        subTopicSegment.normalFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .regular)
        
        topview.backgroundColor = AppColor.CommonColor.cellColor
        lblTopicName.textColor = AppColor.CommonColor.postColor
        upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        downVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
        lblSubTopicCount.textColor = AppColor.CommonColor.postColor
        
        if subTopicId == 0  // Execute as normal flow
        {
            setupNavView()
            addPullToRefresh()
            getSubTopics()
        }
        else    // Execute when come from notification click
        {
            view.subviews.forEach { $0.isHidden = true }
            MBProgressHUD.showAdded(to: self.view, animated: true)
            // Fetch particular sub topic using sub topic id
            SubTopicFirebaseUtility().getSubTopicsBySubTopicId(self.subTopicId) { (subTopic, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    self.view.subviews.forEach { $0.isHidden = false }
                    if (subTopic != nil) {
                        self.selectedSubTopic = subTopic
                        self.setupNavView()
                        self.addPullToRefresh()
                        self.getSubTopics()
                    }
                }
            }
        }
        
//        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
//              subTopicSegment.setTitleTextAttributes(titleTextAttributes, for: .normal)
//              subTopicSegment.setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
    
    func setupNavView() {
        //Add Logo in Navigation bar
        CommonUtility.addLogoInNavigationBar(self.navigationController, self.navigationItem)
        setDataForTopic()
    }
    
    func setDataForTopic() {
        self.lblTopicName.text = self.selectedSubTopic?.subtopic_title
        self.lblTopicCount.text = "\(self.selectedSubTopic?.difference ?? 0)"
        
        if self.selectedSubTopic!.upVoteIds!.count > 0 {
            let upvoteIds = self.selectedSubTopic!.upVoteIds?.components(separatedBy: ",")
            self.upVoteTotal?.text = "\(upvoteIds?.count ?? 0)"
        } else {
            self.upVoteTotal?.text = "0"
        }
        if self.selectedSubTopic!.downVoteIds!.count > 0  {
            let downvoteIds = self.selectedSubTopic!.downVoteIds?.components(separatedBy: ",")
            self.downVoteTotal?.text = "\(downvoteIds?.count ?? 0)"
        } else {
            self.downVoteTotal?.text = "0"
        }
        
        if let timestamp = self.selectedSubTopic?.subtopic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            self.lblTopicDate?.text = timeAgo
        }
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        if (self.selectedSubTopic?.upVoteIds?.contains(uid!))! {
            self.btnUp.isSelected = true
            self.btnDown.isSelected = false
        } else if (self.selectedSubTopic?.downVoteIds?.contains(uid!))! {
            self.btnUp.isSelected = false
            self.btnDown.isSelected = true
        } else {
            self.btnUp.isSelected = false
            self.btnDown.isSelected = false
        }
        
        self.imgHeight.constant = 0
        self.imgPlay.isHidden = true
    }
    
    @IBAction func btnViewImageClicked(_ sender: UIButton) {
//        items.removeAll()
//        // Show the ImageViewer with the first item
//        var galleryItem: GalleryItem!
//        if selectedTopic?.mediaType == "movie" {
//            galleryItem = GalleryItem.video(fetchPreviewImageBlock: { $0(self.imgTopic.image) }, videoURL: URL.init(string: selectedTopic!.videoUrl!)!)
//        } else {
//            galleryItem = GalleryItem.image { $0(self.imgTopic.image) }
//        }
//
//        items.append(DataItem(imageView: self.imgTopic, galleryItem: galleryItem))
//        self.presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self, configuration: CommonUtility.galleryConfiguration()))
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            subTopicTableView.refreshControl = refreshControl
        } else {
            subTopicTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.getSubTopics()
    }
    
    @IBAction func btnTopicUpClicked(_ sender: UIButton) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if (self.selectedSubTopic!.upVoteIds?.contains(uid!))! {

            //Local Updation
            let copyTopic = self.selectedSubTopic!.copy(with: nil) as? SubTopic

            var upvoteStringArr : [String] = []
            if copyTopic!.upVoteIds!.count > 0 {
                upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
            }

            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }

            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let difference = (copyTopic!.difference ?? 0) - 1
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.difference = difference
            self.selectedSubTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Sub Topic Removed Upvote Done")
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().upVoteSubTopicRemove(self.selectedSubTopic!, completion: { (updatedSubTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedSubTopic = updatedSubTopic
                    self.setDataForTopic()
                } else {
                    print("Firebase Sub Topic Removed Upvote Done")
                }
            })
        } else {

            //Local Updation
            let copyTopic = self.selectedSubTopic!.copy(with: nil) as? SubTopic

            var upvoteStringArr : [String] = []
            var downvoteStringArr : [String] = []
            if copyTopic!.upVoteIds!.count > 0 {
                upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
            }
            if copyTopic!.downVoteIds!.count > 0 {
                downvoteStringArr = copyTopic!.downVoteIds?.components(separatedBy: ",") ?? []
            }

            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }

            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }

            //New record added
            upvoteStringArr.append(uid!)

            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference

            self.selectedSubTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Sub Topic Upvote Done")
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().upVoteSubTopicNew(self.selectedSubTopic!, completion: { (updatedSubTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedSubTopic = updatedSubTopic
                    self.setDataForTopic()
                } else {
                    print("Firebase Sub Topic Upvote Done")
                }
            })
        }
    }
    
    @IBAction func btnTopicDownClicked(_ sender: UIButton) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = self.selectedSubTopic!.copy(with: nil) as? SubTopic

        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic!.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (self.selectedSubTopic!.downVoteIds?.contains(uid!))! {

            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }

            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            self.selectedSubTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Sub Topic Removed Downvote Done")
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().downVoteSubTopicRemove(self.selectedSubTopic!, completion: { (updatedSubTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedSubTopic = updatedSubTopic
                    self.setDataForTopic()
                } else {
                    print("Firebse Sub Topic Removed Downvote Done")
                }
            })
        } else {

            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }

            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }

            //New record added
            downvoteStringArr.append(uid!)

            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference

            self.selectedSubTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Sub Topic Downvote Done")
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().downVoteSubTopicNew(self.selectedSubTopic!, completion: { (updatedSubTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedSubTopic = updatedSubTopic
                    self.setDataForTopic()
                } else {
                    print("Firebase Sub Topic Downvote Done")
                }
            })
        }
    }
    
    @IBAction func subTopicSegmentValueChange(_ sender: WMSegment) {
        self.sortSubTopics()
        self.subTopicTableView.reloadData()
    }
    
    @IBAction func btnNewTopicClicked(_ sender: Any) {
        let newTopicVC = NewTopicViewController()
        newTopicVC.modalPresentationStyle = .custom
        newTopicVC.transitioningDelegate = self
        newTopicVC.isSubTopic = true
        newTopicVC.isReplySubTopic = true
        newTopicVC.selectedSubTopic = self.selectedSubTopic
        self.present(newTopicVC, animated: true, completion: {})
    }
    
    func getSubTopics(){
        //fetch user chat count list
        MBProgressHUD.showAdded(to: self.view, animated: true)
        SubTopicFirebaseUtility().getSubTopics(self.selectedSubTopic?.subtopic_id, completion: { (subTopicList, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                self.subTopicList = subTopicList ?? []
                self.sortSubTopics()
                self.lblSubTopicCount.text = self.subTopicList.count != 1 ?
                    "\(self.subTopicList.count) subreplies" :
                    "\(self.subTopicList.count) subreply"
                self.subTopicTableView.reloadData()
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                //Update Sub Topic Count
                self.updateSubTopicCount()
            }
        })
    }
    
    func updateSubTopicCount() {
        SubTopicFirebaseUtility().updateReplySubTopicCount(self.selectedSubTopic!, subReplyCount: self.subTopicList.count) { (topic, error) in
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                //check if count updated update topic list data
                let oldCount = Int(self.selectedSubTopic?.subReplyCount ?? 0)
                if oldCount < self.subTopicList.count {
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? SubTopicViewController {
                        if let index = prevVC.subTopicList.firstIndex(where: { (item) -> Bool in
                            item.subtopic_id == self.selectedSubTopic?.subtopic_id
                        }) {
                            prevVC.subTopicList[index].subReplyCount = Int64(self.subTopicList.count)
                            //Reload Cell
                            prevVC.subTopicTableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                        }
                    }
                }
            }
        }
    }
    
    func sortSubTopics() {
        self.subTopicList.sort(by: {
            self.subTopicSegment.selectedSegmentIndex == 0 ?
                //Sort Array by Vote count
                $0.subtopic_date!.dateValue() < $1.subtopic_date!.dateValue() :
                //Sort Array by Date
                $0.difference! > $1.difference!
        })
    }

}

// The GalleryItemsDataSource provides the items to show
extension ReplySubTopicViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        return items.count
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return items[index].galleryItem
    }
}

extension ReplySubTopicViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}
