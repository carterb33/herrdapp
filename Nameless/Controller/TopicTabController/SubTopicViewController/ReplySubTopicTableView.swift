//
//  ReplySubTopicTableView.swift
//  Herrd
//
//  Created by DG on 22/09/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD

enum ActionFromType {
    case TableView
    case HeaderView
}

//TABLEVIEW EXTENSION
extension ReplySubTopicViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return subTopicList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.topicCellId) as! TopCell
        
        let subtopic = self.subTopicList[indexPath.row]
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if (subtopic.upVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = true
            cell.btnDown.isSelected = false
        } else if (subtopic.downVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = true
        } else {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = false
        }
        var originalPoster = ""
        if subtopic.created_by == self.selectedSubTopic?.created_by {
            cell.opIcon.isHidden = true
            originalPoster = " –– original poster"
        } else {
            cell.opIcon.isHidden = true
        }
        
        if subtopic.upVoteIds!.count > 0 {
            let upvoteIds = subtopic.upVoteIds?.components(separatedBy: ",")
            cell.upVoteTotal?.text = "\(upvoteIds?.count ?? 0)"
        } else {
            cell.upVoteTotal?.text = "0"
        }
        
        if subtopic.downVoteIds!.count > 0  {
            let downvoteIds = subtopic.downVoteIds?.components(separatedBy: ",")
            cell.downVoteTotal?.text = "\(downvoteIds?.count ?? 0)"
        } else {
            cell.downVoteTotal?.text = "0"
        }
        
        cell.lblTopicName?.text = subtopic.subtopic_title
        cell.lblTopicCount.text = "\(subtopic.difference ?? 0)"
        cell.lblTopicName.textColor = AppColor.CommonColor.postColor
        cell.lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
        cell.upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        cell.downVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        cell.bottomView.backgroundColor = AppColor.CommonColor.cellColor
        cell.btnUp.tag = indexPath.row
        cell.btnDown.tag = indexPath.row
        cell.btnReport.tag = indexPath.row
        cell.btnUp.addTarget(self, action: #selector(SubTopicViewController.btnUpClicked(_:)), for: .touchUpInside)
        cell.btnDown.addTarget(self, action: #selector(SubTopicViewController.btnDownClicked(_:)), for: .touchUpInside)
        cell.btnReport.addTarget(self, action:                         #selector(ReplySubTopicViewController.btnReportSubTopicClicked(_:)),
        for: .touchUpInside)
        if let timestamp = subtopic.subtopic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            cell.lblTopicDate?.text = timeAgo + originalPoster
        }
        
        return cell
    }
    
    @IBAction func btnUpClicked(_ sender: UIButton) {
        
        let subtopic = self.subTopicList[sender.tag]
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if (subtopic.upVoteIds?.contains(uid!))! {
            
            //Local Updation
            let copySubTopic = subtopic.copy(with: nil) as? SubTopic
            
            var upvoteStringArr : [String] = []
            if copySubTopic!.upVoteIds!.count > 0 {
                upvoteStringArr = copySubTopic!.upVoteIds?.components(separatedBy: ",") ?? []
            }
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let difference = (copySubTopic!.difference ?? 0) - 1
            copySubTopic!.upVoteIds = upvoteString
            copySubTopic!.difference = difference
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            self.hapticImpact.impactOccurred()
            print("Local SubTopic Removed UpVote Done")
            
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().upVoteSubTopicRemove(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.subTopicList.remove(at: sender.tag)
                    self.subTopicList.insert(updatedTopic!, at: sender.tag)
                    self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
                    print("Firebase SubTopic Removed UpVote Done")

                }
            })
        } else {
            
            //Local Updation
                let copySubTopic = subtopic.copy(with: nil) as? SubTopic
                
                var upvoteStringArr : [String] = []
                var downvoteStringArr : [String] = []
                if copySubTopic!.upVoteIds!.count > 0 {
                    upvoteStringArr = copySubTopic!.upVoteIds?.components(separatedBy: ",") ?? []
                }
                if copySubTopic!.downVoteIds!.count > 0 {
                    downvoteStringArr = copySubTopic!.downVoteIds?.components(separatedBy: ",") ?? []
                }
                
                
                if (upvoteStringArr.contains(uid!)) {
                    if let index = upvoteStringArr.firstIndex(of: uid!) {
                        upvoteStringArr.remove(at: index)
                    }
                }
                
                if (downvoteStringArr.contains(uid!)) {
                    if let index = downvoteStringArr.firstIndex(of: uid!) {
                        downvoteStringArr.remove(at: index)
                    }
                }
                
                //New record added
                upvoteStringArr.append(uid!)
                
                let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
                let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
                let difference = upvoteStringArr.count - downvoteStringArr.count
            
                copySubTopic!.upVoteIds = upvoteString
                copySubTopic!.downVoteIds = downvoteString
                copySubTopic!.difference = difference
                
                self.subTopicList.remove(at: sender.tag)
                self.subTopicList.insert(copySubTopic!, at: sender.tag)
                self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                self.hapticImpact.impactOccurred()
                print("Local SubTopic Upvote Done")
            
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().upVoteSubTopicNew(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.subTopicList.remove(at: sender.tag)
                    self.subTopicList.insert(updatedTopic!, at: sender.tag)
                    self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
                    print("Firebase SubTopic Upvote Done")
                }
            })
        }
    }
    
    @IBAction func btnDownClicked(_ sender: UIButton) {
        
        let subtopic = self.subTopicList[sender.tag]
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copySubTopic = subtopic.copy(with: nil) as? SubTopic
        
        var downvoteStringArr : [String] = []
        if copySubTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copySubTopic?.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var upvoteStringArr : [String] = []
        if copySubTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copySubTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (subtopic.downVoteIds?.contains(uid!))! {

            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copySubTopic!.downVoteIds = downvoteString
            copySubTopic!.difference = difference
            
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            self.hapticImpact.impactOccurred()
            print("Local SubTopic Removed Downvote Done")
            
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().downVoteSubTopicRemove(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Downvote Failed",
                                            message: Messages.Errors.DownVoteFailure,
                                            controller: self)
                    self.subTopicList.remove(at: sender.tag)
                    self.subTopicList.insert(updatedTopic!, at: sender.tag)
                    self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
                    print("Firebase SubTopic Removed Downvote Done")
                }
            })
        } else {
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            downvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copySubTopic!.upVoteIds = upvoteString
            copySubTopic!.downVoteIds = downvoteString
            copySubTopic!.difference = difference
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            self.hapticImpact.impactOccurred()
            print("Local SubTopic Downvote Done")
            
            SubTopicFirebaseUtility().downVoteSubTopicNew(subtopic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                     CommonUtility.showAlert(title: "Downvote Failed",
                                             message: Messages.Errors.DownVoteFailure,
                                             controller: self)
                    self.subTopicList.remove(at: sender.tag)
                    self.subTopicList.insert(updatedTopic!, at: sender.tag)
                    self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
                    print("Firebase SubTopic Downvote Done")
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        if appdelegate!.isAdminUser {
            if editingStyle == .delete {
                //Delete Action on firebse
                let subtopic = self.subTopicList[indexPath.row]
                self.showDelelePostAlert(subtopic,indexPath.row)
            }
        }
    }
    
    @IBAction func btnReportClicked(_ sender: UIButton) {
        //Check if user's self post or other post
        actionFrom = .HeaderView
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(.HeaderView,sender,selectedSubTopic?.created_by == uid ?
                                            .DeletePost : .ReportPost,
                                             selectedSubTopic,
                                             sender.tag)
    }
    
    @IBAction func btnReportSubTopicClicked(_ sender: UIButton) {
        actionFrom = .TableView
        var subtopic : SubTopic?
        subtopic = self.subTopicList[sender.tag]
        //Check if user's self post or other post
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(.TableView,sender,subtopic?.created_by == uid ?
                                            .DeletePost : .ReportPost,
                                             subtopic,
                                             sender.tag)
    }
    
    func showActionSheetForReportPostOrDelete(_ actionFrom: ActionFromType,_ sender: UIButton,_ actionType: ActionSheetType,_ subtopic: SubTopic?,_ index: Int) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if #available(iOS 13.0, *) {
            alertController.view.tintColor = UIColor { traitCollection in
                switch traitCollection.userInterfaceStyle {
                    case .dark: return .white
                    default: return .black
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        if actionType == .ReportPost {
            let reportAction = UIAlertAction(title:"Report reply",style: .default,handler: { (alert: UIAlertAction!) -> Void in
                print("Report post")
                self.showReportPostAlert(subtopic, index)
            })
            let flagImage = UIImage(named: "flagIcon")
            reportAction.setValue(flagImage, forKey: "image")
            //for original image use this flagImage?.withRenderingMode(.alwaysOriginal)
            
            let hideAction = UIAlertAction(title:"Hide reply", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Hide post")
                self.showHidePostAlert(subtopic, index)
            })
            let blockIconImage = UIImage(named: "blockIcon")
            hideAction.setValue(blockIconImage, forKey: "image")
            
            let blockAction = UIAlertAction(title:"Block user", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Block user")
                self.showBlockUserAlert(subtopic, index)
            })
            let hideIconImage = UIImage(named: "hideIcon")
            blockAction.setValue(hideIconImage, forKey: "image")
            
            alertController.addAction(reportAction)
            alertController.addAction(hideAction)
            alertController.addAction(blockAction)
        } else {
            let deleteAction = UIAlertAction(title: "Delete Post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Delete post")
                self.showDelelePostAlert(subtopic,index)
            })
            let deleteIconImage = UIImage(named: "deleteIcon")
            deleteAction.setValue(deleteIconImage, forKey: "image")
            
            alertController.addAction(deleteAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        alertController.addAction(cancelAction)
        
        if UIDevice.isPhone {
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            if actionFrom == .HeaderView {
                if let superView = sender.superview {
                    let buttonFrame = sender.frame
                    var showRect    = superView.convert(buttonFrame, to: self.view)
                    showRect        = self.view.convert(showRect, to: view)
                    showRect.origin.y += 10
                    showRect.origin.x += sender.frame.size.width
                    alertController.popoverPresentationController?.sourceView = self.view
                    alertController.popoverPresentationController?.sourceRect = showRect
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                //In iPad Change Rect to position Popover
                if let cell = sender.superview?.superview?.superview {
                    /* Get the souce rect frame */
                    let buttonFrame = sender.frame
                    var showRect    = cell.convert(buttonFrame, to: self.subTopicTableView)
                    showRect        = self.subTopicTableView.convert(showRect, to: view)
                    showRect.origin.y += 10
                    showRect.origin.x += sender.frame.size.width + 5
                    
                    alertController.popoverPresentationController?.sourceView = self.view
                    alertController.popoverPresentationController?.sourceRect = showRect
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func showDelelePostAlert(_ subtopic: SubTopic?,_ index: Int) {
        let alert = UIAlertController(title:"Delete Reply?",
                                      message:Messages.Warning.DeleteReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            print("Delete Action")
            self.deleteSubTopic(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showReportPostAlert(_ subtopic: SubTopic?,_ index: Int) {
        let alert = UIAlertController(title:"Report Reply?", message:Messages.Info.ReviewReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Report", style: .default, handler: { action in
            print("Report Action")
            self.reportSubPost(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHidePostAlert(_ subtopic: SubTopic?,_ index: Int) {
        let alert = UIAlertController(title:"Hide Reply?", message: Messages.Info.HideReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Hide", style: .default, handler: { action in
            print("Hide Action")
            self.hideSubPost(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showBlockUserAlert(_ subtopic: SubTopic?,_ index: Int) {
        let alert = UIAlertController(title: "Block User?", message: Messages.Info.BlockPost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Block", style: .default, handler: { action in
            print("Block Action")
            self.blockUserInSubTopic(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteSubTopic(_ subtopic: SubTopic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Removing Sub Topic..."
        
        Firestore.firestore().collection("SubTopics").document(subtopic?.documentId ?? "").delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                print("Sub Topic Document successfully removed!")
                loadingNotification.label.text = "Topic Removed"
                MBProgressHUD.hide(for: self.view, animated: true)
                if self.actionFrom == .TableView {
                    self.subTopicList.remove(at:index)
                    self.subTopicTableView.reloadData()
                    self.lblSubTopicCount.text = self.subTopicList.count > 1 ? "\(self.subTopicList.count) Replies" : "\(self.subTopicList.count) Replies"
                } else {
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? SubTopicViewController {
                        prevVC.getSubTopics()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        }
    }
    
    func reportSubPost(_ subtopic: SubTopic?,_ index: Int) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if !(subtopic?.reportedIds?.contains(uid!) ?? true) ||  subtopic?.reportedIds == nil{
            let copySubTopic = subtopic?.copy(with: nil) as? SubTopic
            var reportedIdsStringArr : [String] = []
            if (copySubTopic?.reportedIds?.count ?? 0) > 0 {
                reportedIdsStringArr = copySubTopic!.reportedIds?.components(separatedBy: ",") ?? []
            }
               
            //New record added
            reportedIdsStringArr.append(uid!)
               
            let reportedIdsString = (reportedIdsStringArr.count > 1 ? (reportedIdsStringArr.joined(separator: ",") ) : reportedIdsStringArr.first ?? "")
           
            copySubTopic!.reportedIds = reportedIdsString
               
            self.subTopicList.remove(at: index)
            self.subTopicList.insert(copySubTopic!, at: index)
            
            self.hapticImpact.impactOccurred()
                               
            print("Local Topic Report Done$$")
            print(reportedIdsStringArr.count)

            //Firebase Updation
            SubTopicFirebaseUtility().addReportedIdstoSubTopic(copySubTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Report Failed",
                                            message: Messages.Errors.ReportFailure,
                                            controller: self)
                    self.subTopicList.remove(at: index)
                    self.subTopicList.insert(copySubTopic!, at: index)
                } else {
                    print("Firebase Topic Report Done%%")
                    print(reportedIdsStringArr.count)
                }
            })
        }
    }
    
    func hideSubPost(_ subtopic: SubTopic?,_ index: Int) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
                
        let copySubTopic = subtopic?.copy(with: nil) as? SubTopic
        var hideUserIdsStringArr : [String] = []
        if (copySubTopic?.hidePostsUserIds?.count ?? 0) > 0 {
            hideUserIdsStringArr = copySubTopic!.hidePostsUserIds?.components(separatedBy: ",") ?? []
        }
           
        //New record added
        hideUserIdsStringArr.append(uid!)
           
        let hideUserIdsString = (hideUserIdsStringArr.count > 1 ? (hideUserIdsStringArr.joined(separator: ",") ) : hideUserIdsStringArr.first ?? "")
       
        copySubTopic!.hidePostsUserIds = hideUserIdsString

        self.hapticImpact.impactOccurred()
                           
        print("Local Hide Sub Topic Done$$")
        print(hideUserIdsStringArr.count)

        //Firebase Updation
        SubTopicFirebaseUtility().addHidePostUserIdstoSubTopic(copySubTopic!, completion: { (updatedTopic, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Hide sub post Failed",
                                        message: Messages.Errors.HideSubTopicFailure,
                                        controller: self)
            } else {
                print("Firebase Sub Topic Hide Done%%")
                print(hideUserIdsStringArr.count)
                if self.actionFrom == .TableView {
                    self.subTopicList.remove(at:index)
                    self.subTopicTableView.reloadData()
                } else {
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? SubTopicViewController {
                        prevVC.getSubTopics()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
    
    func blockUserInSubTopic(_ subtopic: SubTopic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Blocking User..."
        FirebaseUtility().addOrUpdateSubTopicBlockedUserIds(blockeduserId: subtopic?.created_by ?? "") { (isAdded, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Block User Failed",
                                        message: Messages.Errors.BlockUserFailure,
                                        controller: self)
            } else {
                print("Firebase Block User Done%%")
                loadingNotification.label.text = "Removing Blocked User Posts..."
                if self.actionFrom == .TableView {
                    //Remove all sub reply
                    self.subTopicList.removeAll { (subTopicLocal) -> Bool in
                        return subTopicLocal.created_by == subtopic?.created_by
                    }
                    self.subTopicTableView.reloadData()
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                } else {
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? SubTopicViewController {
                        prevVC.getSubTopics()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
    }

}
