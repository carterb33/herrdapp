//
//  SubTopicViewController.swift
//  Herrd
//
//  Created by DG on 05/04/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import ImageViewer
import AVFoundation
import AVKit
import GiphyUISDK
import WMSegmentControl

class SubTopicViewController: UIViewController {

    @IBOutlet weak var subTopicTableView: UITableView!
    @IBOutlet weak var subTopicSegment: WMSegment!
    @IBOutlet weak var btnNewSubTopic: UIButton!
    //Topic Detail View
    @IBOutlet weak var lblTopicName: UILabel!
    @IBOutlet weak var lblTopicDate: UILabel!
    @IBOutlet weak var lblTopicCount: UILabel!
    @IBOutlet weak var btnUp: UIButton!
    @IBOutlet weak var btnDown: UIButton!
    @IBOutlet weak var lblSubTopicCount: UILabel!
    @IBOutlet weak var imgTopic: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var upVoteTotal: UILabel!
    @IBOutlet weak var downVoteTotal: UILabel!
    var mediaView = GPHMediaView()
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self,
                          action:#selector(SubTopicViewController.refreshAndSortListData(sender:)),
                          for: .valueChanged)
        return control
    }()
    
    var subTopicList : [SubTopic] = []
    var selectedTopic : Topic?
    var items: [DataItem] = []
    var adminList : [String] = []
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    var topicId : Double = 0
    var selectedChannel: Channel?
    var isFromPrivateChatList = false
    
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    @IBOutlet weak var pinnedCup: UIImageView!
    @IBOutlet weak var topicChatContainer: UIView!
    
    lazy var pricateChatListVC: TopicPrivateChatListViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "TopicPrivateChatListViewController") as! TopicPrivateChatListViewController
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    let gradiantImage = UIImage(named: "gradientRec6")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Segment with type Bottom Bar
        subTopicSegment.selectorType = .bottomBar
        subTopicSegment.textColor = AppColor.CommonColor.secondaryColor
        subTopicSegment.selectorTextColor = AppColor.CommonColor.postColor
        subTopicSegment.selectorColor = UIColor.init(patternImage: gradiantImage)
        
        subTopicSegment.SelectedFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .bold)
        subTopicSegment.normalFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .regular)
        
        lblSubTopicCount.textColor = AppColor.CommonColor.postColor
        self.subTopicTableView.separatorStyle = .none
        
        totalView.backgroundColor = AppColor.CommonColor.cellColor
        lblTopicName.textColor = AppColor.CommonColor.postColor
        lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
        upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        downVoteTotal.textColor = AppColor.CommonColor.secondaryColor

        if topicId == 0 // Execute as normal flow
        {
            setupNavView()
            if isFromPrivateChatList {
                self.topicChatContainer.isHidden = false
            } else {
                self.topicChatContainer.isHidden = true
                addPullToRefresh()
                getSubTopics()
            }
        }
        else    // Execute when come from notification click
        {
            if !isFromPrivateChatList {
                view.subviews.forEach { $0.isHidden = true }
                
                MBProgressHUD.showAdded(to: self.view, animated: true)
                UserFirebaseUtility().getAdminUsers { (adminUsers, error) in
                    if let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String] {
                        self.adminList = adminlist
                    }
                    
                    // Fetch particular topic using topic id
                    TopicFirebaseUtility().getTopicsByTopicId(self.topicId) { (topic, error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if let error = error {
                            print("Error getting documents: \(error)")
                        } else {
                            self.view.subviews.forEach { $0.isHidden = false }
                            self.topicChatContainer.isHidden = !self.isFromPrivateChatList
                            if (topic != nil) {
                                self.selectedTopic = topic
                                self.setupNavView()
                                self.addPullToRefresh()
                                self.getSubTopics()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func addPrivateChatListView() {
        add(asChildViewController: pricateChatListVC)
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        topicChatContainer.addSubview(viewController.view)
        viewController.view.frame = topicChatContainer.bounds
//        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "topicPrivateUserListSegue" {
            if let navController = segue.destination as? UINavigationController, let chatUserList = navController.viewControllers.first as? TopicPrivateChatListViewController {
                chatUserList.topic = selectedTopic
            }
        }
    }
    
    func setupNavView() {
        //Add Logo in Navigation bar
        CommonUtility.addLogoInNavigationBar(self.navigationController, self.navigationItem)
        self.navigationController?.navigationBar.tintColor = UIColor.white // to change the all text color in navigation bar or navigation
        setDataForTopic()
    }
    
    func setupVideoPlayer() {
        guard let url = URL(string: (selectedTopic?.videoUrl)!) else { return }
        
        let avPlayer = AVPlayer(playerItem: AVPlayerItem(url: url))
        videoLayer = AVPlayerLayer(player: avPlayer)
        videoLayer.frame = imgTopic.bounds
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        imgTopic.layer.addSublayer(videoLayer)
        avPlayer.play()
        avPlayer.isMuted = true
    }
    
    func setDataForTopic() {
        self.lblTopicName.text = self.selectedTopic?.topic_title
        self.lblTopicCount.text = "\(self.selectedTopic?.difference ?? 0)"
        
        if self.selectedTopic!.upVoteIds!.count > 0 {
            let upvoteIds = self.selectedTopic!.upVoteIds?.components(separatedBy: ",")
            self.upVoteTotal?.text = "\(upvoteIds?.count ?? 0)"
        } else {
            self.upVoteTotal?.text = "0"
        }
        if self.selectedTopic!.downVoteIds!.count > 0  {
            let downvoteIds = self.selectedTopic!.downVoteIds?.components(separatedBy: ",")
            self.downVoteTotal?.text = "\(downvoteIds?.count ?? 0)"
        } else {
            self.downVoteTotal?.text = "0"
        }
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        var promoText = ""
        if  self.adminList.contains(self.selectedTopic?.created_by ?? ""){
            self.upVoteTotal.isHidden = true
            self.downVoteTotal.isHidden = true
            self.imgPin.isHidden = false
            self.btnUp.isHidden = true
            self.btnDown.isHidden = true
            self.btnReport.isHidden = true
            self.lblTopicCount.isHidden = true
            self.pinnedCup.isHidden = false

            promoText = " ⏤ pinned post"
            
            if selectedTopic?.logoUrl?.count ?? 0 > 0 {
                // Get a reference to the storage service using the default Firebase App
                let storage = Storage.storage()
                // Reference to an image file in Firebase Storage
                let reference = storage.reference().child("Topic Images/\(selectedTopic?.topic_id ?? 0)_logo.png")
                // UIImageView in your ViewController
                let imageView: UIImageView = self.pinnedCup
                // Placeholder image
                let placeholderImage = UIImage(named: "placeholder")
                // Load the image using SDWebImage
                imageView.sd_setImage(with: reference, placeholderImage: placeholderImage)
            }
        } else {
            self.imgPin.isHidden = true
            self.btnUp.isHidden = false
            self.btnDown.isHidden = false
            self.btnReport.isHidden = false
            self.lblTopicCount.isHidden = false
            
            promoText = ""
        }
        
        if let timestamp = self.selectedTopic?.topic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            self.lblTopicDate?.text = timeAgo + promoText
        }
        
        if (self.selectedTopic?.upVoteIds?.contains(uid!))! {
            self.btnUp.isSelected = true
            self.btnDown.isSelected = false
        } else if (self.selectedTopic?.downVoteIds?.contains(uid!))! {
            self.btnUp.isSelected = false
            self.btnDown.isSelected = true
        } else {
            self.btnUp.isSelected = false
            self.btnDown.isSelected = false
        }
        
        if selectedTopic!.imageUrl?.count ?? 0 > 0 {
            // Get a reference to the storage service using the default Firebase App
            let storage = Storage.storage()
            // Reference to an image file in Firebase Storage
            let reference = storage.reference().child("Topic Images/\(selectedTopic!.topic_id ?? 0).jpg")
            // UIImageView in your ViewController
            let imageView: UIImageView = self.imgTopic
            // Placeholder image
            let placeholderImage = UIImage(named: "placeholder")
            // Load the image using SDWebImage
            imageView.sd_setImage(with: reference, placeholderImage: placeholderImage)
            
            self.imgHeight.constant = 150
            if selectedTopic?.mediaType == "movie" {
                self.imgPlay.isHidden = true
                self.setupVideoPlayer()
            } else {
                self.imgPlay.isHidden = true
            }
        } else if selectedTopic!.gifId?.count ?? 0 > 0 {
            //Set Image Frame
            self.imgPlay.isHidden = true
            self.imgTopic.isHidden = true
            self.imgHeight.constant = 150
            //SetUp GIF
            GiphyCore.shared.gifByID(self.selectedTopic?.gifId ?? "") { (response, error) in
                if let media = response?.data {
                    DispatchQueue.main.sync {
                        self.mediaView.media = media
                        self.mediaView.frame = CGRect(x: self.imgTopic.frame.origin.x, y: self.imgTopic.frame.origin.y, width: self.imgTopic.frame.size.width, height: self.imgTopic.frame.height)
                        self.mediaView.layer.cornerRadius = 10
                        self.mediaView.clipsToBounds = true
                        self.totalView.addSubview(self.mediaView)
                    }
                }
            }
        } else {
            self.imgHeight.constant = 0
            self.imgPlay.isHidden = true
        }
    }
    
    @IBAction func btnViewImageClicked(_ sender: UIButton) {
        items.removeAll()
        
        if selectedTopic?.mediaType == "movie" {
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } catch let error {
                print("Error in AVAudio Session\(error.localizedDescription)")
            }
            
            //Show in full screen
            if let player = videoLayer.player {
                let playerController = AVPlayerViewController()
                playerController.player = player
                present(playerController, animated: true) {
                    player.isMuted = false
                }
            }
        } else {
            // Show the ImageViewer with the first item
            var galleryItem: GalleryItem!
            galleryItem = GalleryItem.image { $0(self.imgTopic.image) }
            items.append(DataItem(imageView: self.imgTopic, galleryItem: galleryItem))
            self.presentImageGallery(GalleryViewController(startIndex: 0,
                                                           itemsDataSource: self,
                                                           configuration:CommonUtility.galleryConfiguration()))
        }
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            subTopicTableView.refreshControl = refreshControl
        } else {
            subTopicTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.getSubTopics()
    }
    
    @IBAction func btnTopicUpClicked(_ sender: UIButton) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = self.selectedTopic!.copy(with: nil) as? Topic
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }

        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic!.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (self.selectedTopic!.upVoteIds?.contains(uid!))! {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            let upvoteString = (upvoteStringArr.count > 1 ?
                                    upvoteStringArr.joined(separator: ",") :
                                    upvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.difference = difference
            self.selectedTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Topic Removed Upvote Done")
            TopicFirebaseUtility().upVoteTopicRemove(self.selectedTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedTopic = updatedTopic
                    self.setDataForTopic()
                } else {
                    self.updateTopicUpDownChanges()
                    print("Firebase Topic Removed Upvote Done")
                }
            })
        } else {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            upvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ?
                                    upvoteStringArr.joined(separator: ",") :
                                    upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ?
                                    downvoteStringArr.joined(separator: ",") :
                                    downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference

            self.selectedTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Topic Upvote Done")
            TopicFirebaseUtility().upVoteTopicNew(self.selectedTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedTopic = updatedTopic
                    self.setDataForTopic()
                } else {
                    self.updateTopicUpDownChanges()
                    print("Firebase Topic Upvote Done")
                }
            })
        }
    }
    
    @IBAction func btnTopicDownClicked(_ sender: UIButton) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = self.selectedTopic!.copy(with: nil) as? Topic
        
        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic!.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (self.selectedTopic!.downVoteIds?.contains(uid!))! {
        
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            let downvoteString = (downvoteStringArr.count > 1 ?
                                    downvoteStringArr.joined(separator: ",") :
                                    downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            
            self.selectedTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Topic Removed Downvote Done")

            TopicFirebaseUtility().downVoteTopicRemove(self.selectedTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedTopic = updatedTopic
                    self.setDataForTopic()
                } else {
                    self.updateTopicUpDownChanges()
                    print("Firebse Topic Removed Downvote Done")
                }
            })
        } else {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            downvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ?
                                    upvoteStringArr.joined(separator: ",") :
                                    upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ?
                                    downvoteStringArr.joined(separator: ",") :
                                    downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            
            self.selectedTopic = copyTopic
            self.setDataForTopic()
            self.hapticImpact.impactOccurred()
            print("Local Topic Downvote Done")

            TopicFirebaseUtility().downVoteTopicNew(self.selectedTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    self.selectedTopic = updatedTopic
                    self.setDataForTopic()
                } else {
                    self.updateTopicUpDownChanges()
                    print("Firebase Topic Downvote Done")
                }
            })
        }
    }
    
    @IBAction func subTopicSegmentValueChange(_ sender: WMSegment) {
        self.sortSubTopics()
        self.subTopicTableView.reloadData()
    }
    
    @IBAction func btnNewTopicClicked(_ sender: Any) {
        let newTopicVC = NewTopicViewController()
        newTopicVC.modalPresentationStyle = .custom
        newTopicVC.transitioningDelegate = self
        newTopicVC.isSubTopic = true
        newTopicVC.selectedTopic = self.selectedTopic
        self.present(newTopicVC, animated: true, completion: {})
    }
    
    func getSubTopics(){
        //fetch user chat count list
        MBProgressHUD.showAdded(to: self.view, animated: true)
        SubTopicFirebaseUtility().getSubTopics(self.selectedTopic?.topic_id, completion: { (subTopicList, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                self.subTopicList = subTopicList ?? []
                self.sortSubTopics()
                self.lblSubTopicCount.text = self.subTopicList.count != 1 ?
                    "\(self.subTopicList.count) replies" :
                    "\(self.subTopicList.count) reply"
                self.subTopicTableView.reloadData()
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                //Update Sub Topic Count
                self.updateSubTopicCount()
            }
        })
    }
    
    func updateSubTopicCount() {
        SubTopicFirebaseUtility().updateSubTopicCount(self.selectedTopic!, subTopicCount: self.subTopicList.count) { (topic, error) in
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                //check if count updated update topic list data
                let oldCount = Int(self.selectedTopic?.subTopicsCount ?? 0)
                if oldCount < self.subTopicList.count {
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                        if let index = prevVC.topicList.firstIndex(where: { (item) -> Bool in
                            (item as? Topic)?.topic_id == self.selectedTopic?.topic_id
                        }) {
                            (prevVC.topicList[index] as? Topic)?.subTopicsCount = Int64(self.subTopicList.count)
                            //Reload Cell
                            prevVC.topicTableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                        }
                    }
                }
            }
        }
    }
    
    func updateTopicUpDownChanges() {
        if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
            if let index = prevVC.topicList.firstIndex(where: { (item) -> Bool in
                (item as? Topic)?.topic_id == self.selectedTopic?.topic_id
            }) {
                (prevVC.topicList[index] as? Topic)?.upVoteIds = self.selectedTopic?.upVoteIds
                (prevVC.topicList[index] as? Topic)?.downVoteIds = self.selectedTopic?.downVoteIds
                (prevVC.topicList[index] as? Topic)?.difference = self.selectedTopic?.difference
                //Reload Cell
                prevVC.topicTableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
            }
            
            if let index = prevVC.topicListCopy.firstIndex(where: { (item) -> Bool in
                (item as? Topic)?.topic_id == self.selectedTopic?.topic_id
            }) {
                prevVC.topicListCopy[index].upVoteIds = self.selectedTopic?.upVoteIds
                prevVC.topicListCopy[index].downVoteIds = self.selectedTopic?.downVoteIds
                prevVC.topicListCopy[index].difference = self.selectedTopic?.difference
            }
        }
    }
    
    func sortSubTopics() {
        self.subTopicList.sort(by: {
            self.subTopicSegment.selectedSegmentIndex == 0 ?
                //Sort Array by Vote count
                $0.difference! > $1.difference! :
                //Sort Array by Date
                $0.subtopic_date!.dateValue() > $1.subtopic_date!.dateValue()
        })
    }
}

// The GalleryItemsDataSource provides the items to show
extension SubTopicViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        return items.count
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return items[index].galleryItem
    }
}

extension SubTopicViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}
