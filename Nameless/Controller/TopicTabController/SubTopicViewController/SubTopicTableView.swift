//
//  SubTopicTableView.swift
//  Herrd
//
//  Created by DG on 22/09/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD

//TABLEVIEW EXTENSION
extension SubTopicViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return subTopicList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.topicCellId) as! TopCell
        
        let subtopic = self.subTopicList[indexPath.row]
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
       
        
        if (subtopic.upVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = true
            cell.btnDown.isSelected = false
        } else if (subtopic.downVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = true
        } else {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = false
        }
        var originalPoster = ""
        if subtopic.created_by == self.selectedTopic?.created_by {
            cell.opIcon.isHidden = true
            originalPoster = " –– original poster"
        } else {
            cell.opIcon.isHidden = true
        }
        
        if subtopic.upVoteIds!.count > 0 {
            let upvoteIds = subtopic.upVoteIds?.components(separatedBy: ",")
            cell.upVoteTotal?.text = "\(upvoteIds?.count ?? 0)"
        } else {
            cell.upVoteTotal?.text = "0"
        }
        
        if subtopic.downVoteIds!.count > 0  {
            let downvoteIds = subtopic.downVoteIds?.components(separatedBy: ",")
            cell.downVoteTotal?.text = "\(downvoteIds?.count ?? 0)"
        } else {
            cell.downVoteTotal?.text = "0"
        }
        
        cell.lblTopicName.textColor = AppColor.CommonColor.postColor
        cell.lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
        cell.replyView.backgroundColor = AppColor.CommonColor.cellColor
        cell.upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        cell.downVoteTotal.textColor = AppColor.CommonColor.secondaryColor
        cell.bubbleButton.titleLabel?.textColor = AppColor.CommonColor.secondaryColor

        cell.lblTopicName?.text = subtopic.subtopic_title
        cell.lblTopicCount.text = "\(subtopic.difference ?? 0)"
        cell.btnUp.tag = indexPath.row
        cell.btnDown.tag = indexPath.row
        cell.bubbleButton.tag = indexPath.row
        cell.btnCell.tag = indexPath.row
        cell.btnReport.tag = indexPath.row
        cell.btnUp.addTarget(self, action: #selector(SubTopicViewController.btnUpClicked(_:)), for: .touchUpInside)
        cell.btnDown.addTarget(self, action: #selector(SubTopicViewController.btnDownClicked(_:)),  for: .touchUpInside)
        cell.bubbleButton.addTarget(self, action: #selector(chatIconPressed(_:)),
                                    for: .touchUpInside)
        cell.btnCell.addTarget(self, action: #selector(chatIconPressed(_:)),
                               for: .touchUpInside)
        cell.btnReport.addTarget(self, action:                         #selector(SubTopicViewController.btnReportSubTopicClicked(_:)),
                                 for: .touchUpInside)
        
    
        let attributeText = NSAttributedString(string: subtopic.subReplyCount > 1 ? "\(subtopic.subReplyCount) replies" : "\(subtopic.subReplyCount) reply")
        cell.bubbleButton.setAttributedTitle(attributeText, for: .normal)
        
        if subtopic.subReplyCount == 0{
            cell.bubbleButton.titleLabel?.textColor = AppColor.CommonColor.cellColor
        }
       
        
        if let timestamp = subtopic.subtopic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            cell.lblTopicDate?.text = timeAgo + originalPoster
        }
        
        
        
        return cell
    }
    
    @IBAction func chatIconPressed(_ sender: UIButton) {
        var subtopic : SubTopic?
        subtopic = self.subTopicList[sender.tag]
        if let replySubTopicVC = self.storyboard?.instantiateViewController(withIdentifier: "ReplySubTopicViewController") as? ReplySubTopicViewController {
            replySubTopicVC.selectedSubTopic = subtopic
            self.navigationController?.pushViewController(replySubTopicVC, animated: true)
        }
    }
    
    @IBAction func btnUpClicked(_ sender: UIButton) {
        
        let subtopic = self.subTopicList[sender.tag]
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copySubTopic = subtopic.copy(with: nil) as? SubTopic
        
        var upvoteStringArr : [String] = []
        if copySubTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copySubTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
    
        var downvoteStringArr : [String] = []
        if copySubTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copySubTopic!.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (subtopic.upVoteIds?.contains(uid!))! {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copySubTopic!.upVoteIds = upvoteString
            copySubTopic!.difference = difference
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            self.hapticImpact.impactOccurred()
            print("Local SubTopic Removed UpVote Done")
            
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().upVoteSubTopicRemove(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.subTopicList.remove(at: sender.tag)
                    self.subTopicList.insert(updatedTopic!, at: sender.tag)
                    self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
                    print("Firebase SubTopic Removed UpVote Done")

                }
            })
        } else {
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            upvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copySubTopic!.upVoteIds = upvoteString
            copySubTopic!.downVoteIds = downvoteString
            copySubTopic!.difference = difference
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.hapticImpact.impactOccurred()
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            print("Local SubTopic Upvote Done")
        
//            MBProgressHUD.showAdded(to: self.view, animated: true)
        SubTopicFirebaseUtility().upVoteSubTopicNew(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Upvote Failed",
                                        message: Messages.Errors.UpVoteFailure,
                                        controller: self)
                self.subTopicList.remove(at: sender.tag)
                self.subTopicList.insert(updatedTopic!, at: sender.tag)
                self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            } else {
                print("Firebase SubTopic Upvote Done")
            }
        })
        }
    }
    
    @IBAction func btnDownClicked(_ sender: UIButton) {
        
        let subtopic = self.subTopicList[sender.tag]
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copySubTopic = subtopic.copy(with: nil) as? SubTopic
        
        var downvoteStringArr : [String] = []
        if copySubTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copySubTopic?.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var upvoteStringArr : [String] = []
        if copySubTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copySubTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (subtopic.downVoteIds?.contains(uid!))! {
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copySubTopic!.downVoteIds = downvoteString
            copySubTopic!.difference = difference
            
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            print("Local SubTopic Removed Downvote Done")
            
//            MBProgressHUD.showAdded(to: self.view, animated: true)
            SubTopicFirebaseUtility().downVoteSubTopicRemove(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Downvote Failed",
                                            message: Messages.Errors.DownVoteFailure,
                                            controller: self)
                    self.subTopicList.remove(at: sender.tag)
                    self.subTopicList.insert(updatedTopic!, at: sender.tag)
                    self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
                    self.hapticImpact.impactOccurred()
                    print("Firebase SubTopic Removed Downvote Done")
                }
            })
        } else {
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            downvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copySubTopic!.upVoteIds = upvoteString
            copySubTopic!.downVoteIds = downvoteString
            copySubTopic!.difference = difference
            
            self.subTopicList.remove(at: sender.tag)
            self.subTopicList.insert(copySubTopic!, at: sender.tag)
            self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            self.hapticImpact.impactOccurred()
            print("Local SubTopic Downvote Done")
        
//            MBProgressHUD.showAdded(to: self.view, animated: true)
        SubTopicFirebaseUtility().downVoteSubTopicNew(subtopic, completion: { (updatedTopic, error) in
//                MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Downvote Failed",
                                        message: Messages.Errors.DownVoteFailure,
                                        controller: self)
                self.subTopicList.remove(at: sender.tag)
                self.subTopicList.insert(updatedTopic!, at: sender.tag)
                self.subTopicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            } else {
                print("Firebase SubTopic Downvote Done")
            }
        })
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        if appdelegate!.isAdminUser {
            if editingStyle == .delete {
                //Delete Action on firebse
                let subtopic = self.subTopicList[indexPath.row]
                self.showDelelePostAlert(nil,indexPath.row, .SubTopic, subtopic)
            }
        }
    }
    
    @IBAction func btnReportClicked(_ sender: UIButton) {
        //Check if user's self post or other post
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(sender,selectedTopic?.created_by == uid ?
                                            .DeletePost : .ReportPost,
                                             selectedTopic,
                                             sender.tag,
                                             .Topic, nil)
    }
    
    @IBAction func btnReportSubTopicClicked(_ sender: UIButton) {
        var subtopic : SubTopic?
        subtopic = self.subTopicList[sender.tag]
        //Check if user's self post or other post
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(sender,subtopic?.created_by == uid ?
                                            .DeletePost : .ReportPost,
                                             nil,
                                             sender.tag,
                                             .SubTopic, subtopic)
    }
    
    func showActionSheetForReportPostOrDelete(_ sender: UIButton,_ actionType: ActionSheetType,_ topic: Topic?,_ index: Int,_ type: TopicType, _ subtopic: SubTopic?) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if #available(iOS 13.0, *) {
            alertController.view.tintColor = UIColor { traitCollection in
                switch traitCollection.userInterfaceStyle {
                    case .dark: return .white
                    default: return .black
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        if actionType == .ReportPost {
            let reportAction = UIAlertAction(title: type == .Topic ? "Report post" : "Report reply",style: .default,handler: { (alert: UIAlertAction!) -> Void in
                print("Report post")
                self.showReportPostAlert(topic, index, type, subtopic)
            })
            let flagImage = UIImage(named: "flagIcon")
            reportAction.setValue(flagImage, forKey: "image")
            //for original image use this flagImage?.withRenderingMode(.alwaysOriginal)
            
            let hideAction = UIAlertAction(title: type == .Topic ? "Hide post" : "Hide reply", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Hide post")
                self.showHidePostAlert(topic, index, type, subtopic)
            })
            let blockIconImage = UIImage(named: "blockIcon")
            hideAction.setValue(blockIconImage, forKey: "image")
            
            let blockAction = UIAlertAction(title:"Block user", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Block user")
                self.showBlockUserAlert(topic, index, type, subtopic)
            })
            let hideIconImage = UIImage(named: "hideIcon")
            blockAction.setValue(hideIconImage, forKey: "image")
            
            alertController.addAction(reportAction)
            alertController.addAction(hideAction)
            alertController.addAction(blockAction)
        } else {
            let deleteAction = UIAlertAction(title: "Delete Post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Delete post")
                self.showDelelePostAlert(topic,index, type, subtopic)
            })
            let deleteIconImage = UIImage(named: "deleteIcon")
            deleteAction.setValue(deleteIconImage, forKey: "image")
            
            alertController.addAction(deleteAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        alertController.addAction(cancelAction)
        
        //if iPhone
        if UIDevice.isPhone {
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            if type == .Topic {
                if let superView = sender.superview {
                    let buttonFrame = sender.frame
                    var showRect    = superView.convert(buttonFrame, to: self.view)
                    showRect        = self.view.convert(showRect, to: view)
                    showRect.origin.x += sender.frame.size.width
                    alertController.popoverPresentationController?.sourceView = self.view
                    alertController.popoverPresentationController?.sourceRect = showRect
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                //In iPad Change Rect to position Popover
                if let cell = sender.superview?.superview?.superview {
                    /* Get the souce rect frame */
                    let buttonFrame = sender.frame
                    var showRect    = cell.convert(buttonFrame, to: self.subTopicTableView)
                    showRect        = self.subTopicTableView.convert(showRect, to: view)
                    showRect.origin.y += 10
                    showRect.origin.x += sender.frame.size.width + 5
                    
                    alertController.popoverPresentationController?.sourceView = self.view
                    alertController.popoverPresentationController?.sourceRect = showRect
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    func showDelelePostAlert(_ topic: Topic?,_ index: Int,_ type: TopicType, _ subtopic: SubTopic?) {
        let alert = UIAlertController(title: type == .Topic ? "Delete Post?" : "Delete Reply?",
                                      message: type == .Topic ? Messages.Warning.DeletePost : Messages.Warning.DeleteReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            print("Delete Action")
            type == .Topic ? self.deletePost(topic, index) :
                            self.deleteSubTopic(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showReportPostAlert(_ topic: Topic?,_ index: Int,_ type: TopicType,_ subtopic: SubTopic?) {
        let alert = UIAlertController(title: type == .Topic ? "Report Post?" : "Report Reply?", message: type == .Topic ? Messages.Info.ReviewPost : Messages.Info.ReviewReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Report", style: .default, handler: { action in
            print("Report Action")
            type == .Topic ? self.reportPost(topic, index) :
                            self.reportSubPost(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHidePostAlert(_ topic: Topic?,_ index: Int,_ type: TopicType,_ subtopic: SubTopic?) {
        let alert = UIAlertController(title: type == .Topic ? "Hide Post?" : "Hide Reply?", message: type == .Topic ? Messages.Info.HidePost : Messages.Info.HideReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Hide", style: .default, handler: { action in
            print("Hide Action")
            type == .Topic ? self.hidePost(topic, index) :
                            self.hideSubPost(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showBlockUserAlert(_ topic: Topic?,_ index: Int,_ type: TopicType,_ subtopic: SubTopic?) {
        let alert = UIAlertController(title: "Block User?", message: Messages.Info.BlockPost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Block", style: .default, handler: { action in
            print("Block Action")
            type == .Topic ?  self.blockUser(topic, index) :
                            self.blockUserInSubTopic(subtopic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePost(_ topic: Topic?,_ index: Int) {
        
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Removing Topic..."
        
        Firestore.firestore().collection("Topics")
            .whereField("topic_id", isEqualTo: topic?.topic_id ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        
                        loadingNotification.label.text = "Topic Removed"
                        Firestore.firestore().collection("Topics").document(data.documentID).delete() { err in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let err = err {
                                print("Error removing document: \(err)")
                            } else {
                                print("Topic Document successfully removed!")
                                
                                // Create a reference to the file to delete
                                if topic?.mediaType == "movie" {
                                    let desertRef = Storage.storage().reference().child("Topic Images/\(topic?.topic_id ?? 0).jpg")

                                    // Delete the file
                                    desertRef.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Images/\(topic?.topic_id ?? 0).jpg deleted successfully")
                                      }
                                    }
                                    
                                    let desertRefVideo = Storage.storage().reference().child("Topic Videos/\(topic?.topic_id ?? 0).mp4")

                                    // Delete the video file
                                    desertRefVideo.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Videos/\(topic?.topic_id ?? 0).mp4 deleted successfully")
                                      }
                                    }
                                } else if topic?.mediaType == "image" {
                                    let desertRef = Storage.storage().reference().child("Topic Images/\(topic?.topic_id ?? 0).jpg")

                                    // Delete the file
                                    desertRef.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Images/\(topic?.topic_id ?? 0).jpg deleted successfully")
                                      }
                                    }
                                }
                                
                                //Delete Sub Topics & Place
                                SubTopicFirebaseUtility().getSubTopics(topic?.topic_id, completion: { (subTopicList, error) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    if let error = error {
                                        print("Error getting documents: \(error)")
                                    } else {
                                        subTopicList?.forEach {
                                            Firestore.firestore().collection("SubTopics").document($0.documentId!).delete() { err in
                                                if let err = err {
                                                    print("Error removing document: \(err)")
                                                } else {
                                                    print("Sub Topic Document successfully removed!")
                                                }
                                            }
                                        }
                                        if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                                            if let foundIndex = prevVC.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
                                                return objTopic.topic_id == topic?.topic_id
                                            }) {
                                                prevVC.topicListCopy.remove(at:foundIndex)
                                            }
                                            prevVC.getAllTopics()
                                        }
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                })
                            }
                        }
                    }else{
                        print("No Topic to delete Data")
                    }
                }
            })
        
    }
    
    func deleteSubTopic(_ subtopic: SubTopic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Removing Sub Topic..."
        
        Firestore.firestore().collection("SubTopics").document(subtopic?.documentId ?? "").delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                print("Sub Topic Document successfully removed!")
                loadingNotification.label.text = "Topic Removed"
                MBProgressHUD.hide(for: self.view, animated: true)
                self.subTopicList.remove(at:index)
                self.subTopicTableView.reloadData()
                self.lblSubTopicCount.text = self.subTopicList.count > 1 ? "\(self.subTopicList.count) Replies" : "\(self.subTopicList.count) Replies"
            }
        }
    }
        
    func reportPost(_ topic: Topic?,_ index: Int) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if !(topic?.reportedIds?.contains(uid!) ?? true) ||  topic?.reportedIds == nil{
            let copyTopic = topic?.copy(with: nil) as? Topic
            var reportedIdsStringArr : [String] = []
            if (copyTopic?.reportedIds?.count ?? 0) > 0 {
                reportedIdsStringArr = copyTopic!.reportedIds?.components(separatedBy: ",") ?? []
            }
               
            //New record added
            reportedIdsStringArr.append(uid!)
               
            let reportedIdsString = (reportedIdsStringArr.count > 1 ? (reportedIdsStringArr.joined(separator: ",") ) : reportedIdsStringArr.first ?? "")
           
            copyTopic!.reportedIds = reportedIdsString
               
            if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                prevVC.topicList.remove(at: index)
                prevVC.topicList.insert(copyTopic!, at: index)
            }
            
            self.hapticImpact.impactOccurred()
                               
            print("Local Topic Report Done$$")
            print(reportedIdsStringArr.count)

            //Firebase Updation
            TopicFirebaseUtility().addReportedIdstoTopic(copyTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Report Failed",
                                            message: Messages.Errors.ReportFailure,
                                            controller: self)
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                        prevVC.topicList.remove(at: index)
                        prevVC.topicList.insert(updatedTopic!, at: index)
                    }
                } else {
                    print("Firebase Topic Report Done%%")
                    print(reportedIdsStringArr.count)
                }
            })
        }
    }
    
    func reportSubPost(_ subtopic: SubTopic?,_ index: Int) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if !(subtopic?.reportedIds?.contains(uid!) ?? true) ||  subtopic?.reportedIds == nil{
            let copySubTopic = subtopic?.copy(with: nil) as? SubTopic
            var reportedIdsStringArr : [String] = []
            if (copySubTopic?.reportedIds?.count ?? 0) > 0 {
                reportedIdsStringArr = copySubTopic!.reportedIds?.components(separatedBy: ",") ?? []
            }
               
            //New record added
            reportedIdsStringArr.append(uid!)
               
            let reportedIdsString = (reportedIdsStringArr.count > 1 ? (reportedIdsStringArr.joined(separator: ",") ) : reportedIdsStringArr.first ?? "")
           
            copySubTopic!.reportedIds = reportedIdsString
               
            self.subTopicList.remove(at: index)
            self.subTopicList.insert(copySubTopic!, at: index)
            
            self.hapticImpact.impactOccurred()
                               
            print("Local Topic Report Done$$")
            print(reportedIdsStringArr.count)

            //Firebase Updation
            SubTopicFirebaseUtility().addReportedIdstoSubTopic(copySubTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Report Failed",
                                            message: Messages.Errors.ReportFailure,
                                            controller: self)
                    self.subTopicList.remove(at: index)
                    self.subTopicList.insert(copySubTopic!, at: index)
                } else {
                    print("Firebase Topic Report Done%%")
                    print(reportedIdsStringArr.count)
                }
            })
        }
    }
        
    func hidePost(_ topic: Topic?,_ index: Int) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let topic = topic else { return }
    
        //Check and add new record
        if !topic.hidePostsUserIdsArr.contains(uid) {
            topic.hidePostsUserIdsArr.append(uid)
            self.hapticImpact.impactOccurred()
            //Firebase Updation
            TopicFirebaseUtility().addHidePostUserIdstoTopic(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Hide post Failed", message: Messages.Errors.HideFailure, controller: self)
                } else {
                    print("Firebase Topic Hide Done%%")
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                        prevVC.topicList.remove(at:index)
                        prevVC.topicTableView.reloadData()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }
    }
    
    func hideSubPost(_ subtopic: SubTopic?,_ index: Int) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
                
        let copySubTopic = subtopic?.copy(with: nil) as? SubTopic
        var hideUserIdsStringArr : [String] = []
        if (copySubTopic?.hidePostsUserIds?.count ?? 0) > 0 {
            hideUserIdsStringArr = copySubTopic!.hidePostsUserIds?.components(separatedBy: ",") ?? []
        }
           
        //New record added
        hideUserIdsStringArr.append(uid!)
           
        let hideUserIdsString = (hideUserIdsStringArr.count > 1 ? (hideUserIdsStringArr.joined(separator: ",") ) : hideUserIdsStringArr.first ?? "")
       
        copySubTopic!.hidePostsUserIds = hideUserIdsString

        self.hapticImpact.impactOccurred()
                           
        print("Local Hide Sub Topic Done$$")
        print(hideUserIdsStringArr.count)

        //Firebase Updation
        SubTopicFirebaseUtility().addHidePostUserIdstoSubTopic(copySubTopic!, completion: { (updatedTopic, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Hide sub post Failed",
                                        message: Messages.Errors.HideSubTopicFailure,
                                        controller: self)
            } else {
                print("Firebase Sub Topic Hide Done%%")
                print(hideUserIdsStringArr.count)
                self.subTopicList.remove(at:index)
                self.subTopicTableView.reloadData()
            }
        })
    }
        
    func blockUser(_ topic: Topic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Blocking User..."
        FirebaseUtility().addOrUpdateBlockedUserIds(blockeduserId: topic?.created_by ?? "") { (isAdded, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Block User Failed",
                                        message: Messages.Errors.BlockUserFailure,
                                        controller: self)
            } else {
                print("Firebase Block User Done%%")
                loadingNotification.label.text = "Removing Blocked User Posts..."
                if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                    prevVC.topicList.removeAll { (topicLocal) -> Bool in
                        return (topicLocal as? Topic)?.created_by == topic?.created_by
                    }
                    prevVC.topicTableView.reloadData()
                }

                MBProgressHUD.hide(for: self.view, animated: true)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    func blockUserInSubTopic(_ subtopic: SubTopic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Blocking User..."
        FirebaseUtility().addOrUpdateSubTopicBlockedUserIds(blockeduserId: subtopic?.created_by ?? "") { (isAdded, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                MBProgressHUD.hide(for: self.view, animated: true)
                CommonUtility.showAlert(title: "Block User Failed",
                                        message: Messages.Errors.BlockUserFailure,
                                        controller: self)
            } else {
                print("Firebase Block User Done%%")
                loadingNotification.label.text = "Removing Blocked User Posts..."
                
                //Remove all sub reply
                self.subTopicList.removeAll { (subTopicLocal) -> Bool in
                    return subTopicLocal.created_by == subtopic?.created_by
                }
                self.subTopicTableView.reloadData()
                
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
    }
}
