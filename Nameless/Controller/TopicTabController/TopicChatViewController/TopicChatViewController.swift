//
//  TopicChatViewController.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 15/01/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import MessageKit
import FirebaseFirestore
import InputBarAccessoryView
import IQKeyboardManagerSwift

class TopicChatViewController: MessagesViewController {
    
    private let db = Firestore.firestore()
    private var reference: CollectionReference?
    private let storage = Storage.storage().reference()
    
    private var messages: [Message] = []
    private var messageListener: ListenerRegistration?
    
    private var currentUser: User
    private var selectedChannel: Channel
    private var selectedFirebaseCollection = ""
    private var senderDisplayName = ""
    private var receiverID = ""
    private var userUnreadCounts : Int = 0
    
    let newChatColor = UIColor(red: 188/255.0, green: 65/255.0, blue: 132/255.0, alpha: 1.0)
    
    let gradiantImage = UIImage(named: "chatGrad")!
    
    var place : Venue?
    var topic : Topic?
    var isAnonymous : Bool?
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageListener?.remove()
    }
    
//    init(user: User, channel: Channel, anonymousMode:Bool) {
//        self.currentUser = user
//        self.selectedChannel = channel
//        self.isAnonymous = anonymousMode
//        super.init(nibName: nil, bundle: nil)
//        if let topic = self.topic {
//            title = topic.topic_title
//        } else {
//            title = channel.name
//        }
//    }
//
    init(user: User, channel: Channel, anonymousMode:Bool, topic: Topic) {
        self.currentUser = user
        self.selectedChannel = channel
        self.isAnonymous = anonymousMode
        super.init(nibName: nil, bundle: nil)
        title = topic.topic_title
    }
    
    init(user: User,
         channel: Channel,
         anonymousMode:Bool,
         topic: Topic,
         firebseCollection: String,
         senderDisaplyName: String,
         userUnreadCounts: Int) {
        self.currentUser = user
        self.selectedChannel = channel
        self.isAnonymous = anonymousMode
        self.topic = topic
        self.selectedFirebaseCollection = firebseCollection
        self.senderDisplayName = senderDisaplyName
        self.userUnreadCounts = userUnreadCounts
        super.init(nibName: nil, bundle: nil)
        title = topic.topic_title
    }
    
    func setData(user: User, channel: Channel, anonymousMode:Bool, topic: Topic) {
        self.currentUser = user
        self.selectedChannel = channel
        self.isAnonymous = anonymousMode
        title = topic.topic_title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setReceiverID()
        
        self.view.backgroundColor = .black
        self.messagesCollectionView.backgroundColor = .black
        if #available(iOS 11, *) {
//            self.automaticallyAdjustsScrollViewInsets = true
            self.messagesCollectionView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
        }
        
        firebaseSetup()
        messageUISetup()
        
    }
    
    func setReceiverID() {
        //Set Receiver ID
        var idsArray = self.selectedChannel.channelRef.components(separatedBy: "+")
        idsArray.remove(at: 0)
        if let findIndex = idsArray.firstIndex(where: { (value) -> Bool in
            return value == currentUser.uid
        }) {
            idsArray.remove(at: findIndex)
        }
        self.receiverID = idsArray.first ?? ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enable = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        if messages.count == 0 {
            //delete channel
            self.deleteCurrentChannel()
        } else {
            //Update Last message in channel
            updateLastMessage()
            if let prewVC = self.navigationController?.viewControllers[0] as? ViewController {
                prewVC.getUserChatCounts()
            }
        }
    }
    
    func deleteCurrentChannel() {
        guard let docId = self.selectedChannel.id  else { return }
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels).document(docId).delete { (error) in
            if let error = error {
                print("Got error while deleteing channel.",error)
                return
            }
            //Delete ref from topic
            guard let topiDocId = self.topic?.documentId else { return }
            if var privateChannelRef = self.topic?.privateChannelRef, privateChannelRef.contains(self.selectedChannel.channelRef) {
                if let index = privateChannelRef.firstIndex(of: self.selectedChannel.channelRef) {
                    privateChannelRef.remove(at: index)
                }
                Firestore.firestore().collection(FirebaseConst.Collections.Topics).document(topiDocId).updateData([
                    "privateChannelRef": privateChannelRef
                ]) { err in
                    if let err = err {
                       print("Got error while remove chaneel ref from topic.",err)
                    } else {
                        print("Channel ref deleted from topic.")
                    }
                }
            }
        }
    }
    
    func updateLastMessage() {
        
        guard let channelId = self.selectedChannel.id else {
            return
        }
        
        guard let lastMsg = self.messages.last else {
            return
        }
        
        FirebaseUtility().updateLastMessageWithChannelId(channelId: channelId,
                                                         lastMsg: lastMsg.content, msgTime: lastMsg.sentDate) { (isUpdated, error) in
            if error != nil {
                print("Got error while adding last mesg in channel")
            }
        }
    }
    
    func firebaseSetup() {
        
        guard let id = selectedChannel.id else {
            navigationController?.popViewController(animated: true)
            return
        }
        let firebaseChannelCollection = self.selectedFirebaseCollection.count > 0 ?
            self.selectedFirebaseCollection : FirebaseConst.Collections.Venues
        let path = [firebaseChannelCollection, id, "thread"].joined(separator: "/")
        reference = db.collection(path)
        
        messageListener = reference?.addSnapshotListener { querySnapshot, error in
            guard let snapshot = querySnapshot else {
                print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                return
            }
            
            //Mark All Message Read
            guard let selectedTopic = self.topic else {
                return
            }
            var firstRead = false
            if self.messages.count == 0 {
                self.markAllMessageRead(selectedTopic,readCount: self.userUnreadCounts)
                firstRead = true
            }
            
            snapshot.documentChanges.forEach { change in
                self.handleDocumentChange(change)
            }
            let uids = snapshot.documentChanges.filter { (change: DocumentChange) -> Bool in
                let data = change.document.data()
                return data["senderID"] as! String != self.currentUser.uid
            }
            if !firstRead && uids.count > 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.markAllMessageRead(selectedTopic,readCount: uids.count)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        self.becomeFirstResponder()
    }
    
    func messageUISetup() {
        
        maintainPositionOnKeyboardFrameChanged = true
        if #available(iOS 13.0, *) {
            messageInputBar.inputTextView.textColor = UIColor { traitCollection in
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return .white
                default:
                    return .black
                }
            }
        } else {
            // Fallback on earlier versions
            messageInputBar.inputTextView.textColor = .black
        }
        
        messageInputBar.inputTextView.tintColor = UIColor.init(patternImage: gradiantImage)
    
        messageInputBar.inputTextView.placeholder = "Your message"
        messageInputBar.sendButton.setTitleColor(UIColor.init(patternImage: gradiantImage), for: .normal)
        
        messageInputBar.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.textMessageSizeCalculator.incomingAvatarSize = .zero
        }
        
//        messageInputBar.leftStackView.alignment = .center
//        messageInputBar.setLeftStackViewWidthConstant(to: 10, animated: false)
//        messageInputBar.setStackViewItems([cameraItem], forStack: .left, animated: false) // 3
        
    }
    
    // MARK: - Firebse Actions
    
    private func save(_ message: Message) {
        reference?.addDocument(data: message.representation) { error in
            if let e = error {
                print("Error sending message: \(e.localizedDescription)")
                return
            }
            guard let selectedTopic = self.topic else {
                return
            }
            self.sendPushNotification(message: message)
            self.updateSenderReceiverMsgCount(selectedTopic)
            self.messagesCollectionView.scrollToLastItem()
        }
    }
    
    func sendPushNotification(message: Message) {
        // Fetch device id of sub topic or topic created user
        FirebaseUtility().getDeviceTokenUsingUserId(userId: self.receiverID) { (deviceId, error) in
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                if let device_Id = deviceId, device_Id.count > 0
                {
                    guard var selectedTopic = self.topic?.representation else {
                        return
                    }
                    selectedTopic["isFromPrivateMsg"] = true
                    selectedTopic.removeValue(forKey: "topic_date")
                    var title = ""
                    if let topic_title = selectedTopic["topic_title"] {
                        title = "New Message on \(topic_title)"
                    } else {
                        title = "New Message"
                    }
                    
                    let parameters = ["to": device_Id,
                                      "notification": ["body":message.content,
                                                       "title":title,
                                                       "sound":"default",
                                                       "badge":"1"],
                                      "data": selectedTopic] as [String : Any]
                    
                    FirebaseUtility().sendNotificationAPI(param: parameters)    // Send notification
                }
            }
        }
    }
    
    private func insertNewMessage(_ message: Message) {
        guard !messages.contains(message) else {
            return
        }
        
        messages.append(message)
        messages.sort()
        
//        updateTopicMsgCount(topic!,senderId: message.sender.senderId)
//
//        updateUserMsgCount()
        
        let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
        let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
            DispatchQueue.main.async {
                self.messagesCollectionView.scrollToLastItem()
            }
        }
    }
    
    func updateTopicMsgCount(_ selectedTopic : Topic, senderId: String) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    if let msgCount = document?.data()["msgcount"] as? Int {
                        document?.reference.updateData([
                            "msgcount": senderId == self.currentUser.uid ? msgCount + 1 : msgCount - 1
                        ])
                    }
                }
        }
    }
    
    func updateSenderReceiverMsgCount(_ selectedTopic : Topic) {
        let topicID = "\(selectedTopic.topic_id!)"
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .whereField("name", isEqualTo: topicID)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    guard ((querySnapshot?.documents.isEmpty) != nil) else {
                        return
                    }
                    let document = querySnapshot?.documents.first
                    
                    if let msgCount = document?.data()[self.receiverID] as? Int {
                        document?.reference.updateData([
                            self.receiverID : msgCount + 1
                        ])
                    } else {
                        document?.reference.updateData([
                            self.receiverID : 1
                        ])
                    }
                }
        }
        
        //Update msg count in main topic
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    guard ((querySnapshot?.documents.isEmpty) != nil) else {
                        return
                    }
                    let document = querySnapshot?.documents.first
                    
                    if var privateMsgCount = document?.data()["privateMsgCount"] as? [String:Int] {
                        if let msgcount = privateMsgCount[self.receiverID] {
                            privateMsgCount[self.receiverID] = msgcount + 1
                        } else {
                            privateMsgCount[self.receiverID] = 1
                        }
                        document?.reference.updateData([
                            "privateMsgCount" : privateMsgCount
                        ])
                    } else {
                        document?.reference.updateData([
                            "privateMsgCount" : [self.receiverID: 1]
                        ])
                    }
                    
//                    if let msgcount = document?.data()[self.receiverID + "_msgcount"] as? Int {
//                        document?.reference.updateData([
//                            self.receiverID + "_msgcount" : msgcount + 1
//                        ])
//                    } else {
//                        document?.reference.updateData([
//                            self.receiverID + "_msgcount" : 1
//                        ])
//                    }
                }
        }
    }
    
    func markAllMessageRead(_ selectedTopic : Topic, readCount: Int) {
        let topicID = "\(selectedTopic.topic_id!)"
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .whereField("name", isEqualTo: topicID)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    guard ((querySnapshot?.documents.isEmpty) != nil) else {
                        return
                    }
                    let document = querySnapshot?.documents.first
                    document?.reference.updateData([
                        self.currentUser.uid : 0
                    ])
                }
        }
        
        //Mark read msg in topic
        //Update msg count in main topic
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    guard ((querySnapshot?.documents.isEmpty) != nil) else {
                        return
                    }
                    let document = querySnapshot?.documents.first
                    
                    if var privateMsgCount = document?.data()["privateMsgCount"] as? [String:Int] {
                        if let msgcount = privateMsgCount[self.currentUser.uid], msgcount > 0 {
                            privateMsgCount[self.currentUser.uid] = msgcount - readCount
                        } else {
                            privateMsgCount[self.currentUser.uid] = 0
                        }
                        document?.reference.updateData([
                            "privateMsgCount" : privateMsgCount
                        ])
                        //Update Topic list with read count
                        if let navCount = self.navigationController?.viewControllers.count, let topicChatListViewController =  self.navigationController?.viewControllers[navCount - 2] as? TopicChatListViewController {
                            topicChatListViewController.updateChatCount(selectedTopic.topic_id!, privateMsgCount, privateMsgCount[self.currentUser.uid]!)
                        } else {
                            if let navCont = self.tabBarController?.selectedViewController as? UINavigationController {
                                if let topicChatListViewController =  navCont.viewControllers[1] as? TopicChatListViewController {
                                    topicChatListViewController.updateChatCount(selectedTopic.topic_id!, privateMsgCount, privateMsgCount[self.currentUser.uid]!)
                                }
                            }
                            else if let navController = self.tabBarController?.viewControllers?[1] as? UINavigationController {
                                if let topicChatListViewController =  navController.viewControllers.first as? TopicChatListViewController {
                                    topicChatListViewController.updateChatCount(selectedTopic.topic_id!, privateMsgCount, privateMsgCount[self.currentUser.uid]!)
                                }
                            }
                        }
                    } else {
                        document?.reference.updateData([
                            "privateMsgCount" : [self.currentUser.uid: 0]
                        ])
                    }
                    
//                    if let msgcount = document?.data()[self.currentUser.uid + "_msgcount"] as? Int {
//                        document?.reference.updateData([
//                            self.currentUser.uid + "_msgcount" : msgcount - self.userUnreadCounts
//                        ])
//                    } else {
//                        document?.reference.updateData([
//                            self.currentUser.uid + "_msgcount" : 0
//                        ])
//                    }
                }
        }
    }
    
    func updateUserMsgCount() {
        
        let uniqueId = self.selectedChannel.channelRef
        
        Firestore.firestore().collection("UserChatReadCount")
            .whereField("uniqueId", isEqualTo: uniqueId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "chatCount": self.messages.count
                        ])
                }
        }
        
    }
    
    private func handleDocumentChange(_ change: DocumentChange) {
        guard let message = Message(document: change.document) else {
            return
        }
        
        switch change.type {
        case .added:
                insertNewMessage(message)
        default:
            break
        }
    }

}

// MARK: - MessagesDisplayDelegate

extension TopicChatViewController: MessagesDisplayDelegate {
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? UIColor.init(patternImage: gradiantImage) : .incomingMessage
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
        return false
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
    }
    
}

// MARK: - MessagesLayoutDelegate

extension TopicChatViewController: MessagesLayoutDelegate {
    
    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 25, height: 25)
    }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
//    func cellBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
//        return 30
//    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 0, height: 8)
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
//    func configureAvatarView(
//        _ avatarView: AvatarView,
//        for message: MessageType,
//        at indexPath: IndexPath,
//        in messagesCollectionView: MessagesCollectionView) {
//
//        var initials = "?"
//        if isFromCurrentSender(message: message) && self.isAnonymous! {
//            initials = "?"
//        } else {
////            let name = message.sender.displayName
//            let name = self.senderDisplayName
//            let nameArray = name.components(separatedBy: " ")
//            let firstname = nameArray.first
//            let lastname = nameArray.last
//            initials = String(firstname?.first! ?? " ").uppercased() + String(lastname?.first! ?? " ").uppercased()
//        }
//
//        avatarView.initials = initials
////        let message = messages[indexPath.section]
////        let color = message.member.color
////        avatarView.backgroundColor = color
//    }
    
}

// MARK: - MessagesDataSource

extension TopicChatViewController: MessagesDataSource {
    
    func currentSender() -> SenderType {
        var userName = ""
        if self.isAnonymous! {
            userName = "Anonymous User"
        } else {
            userName = currentUser.displayName ?? currentUser.uid
        }
        return Sender(id: currentUser.uid, displayName: userName)
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        var name = ""
        if isFromCurrentSender(message: message) && self.isAnonymous! {
            name = "me"
        } else {
//            name = message.sender.displayName
//            name = self.senderDisplayName
            name = "anonymous"
        }
        
        let paragraph = NSMutableParagraphStyle()
        isFromCurrentSender(message: message) ? paragraph.alignment = .right : (paragraph.alignment = .left)
        
        return NSAttributedString(
            string: name,
            attributes: [
                .font: UIFont.preferredFont(forTextStyle: .caption1),
                .foregroundColor: UIColor(white: 0.5, alpha: 1),
                .paragraphStyle: paragraph
            ]
        )
    }
    
//    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//        if indexPath.section % 3 == 0 {
//            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//        }
//        return nil
//    }
    
    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        return NSAttributedString(string: "Read", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        var name = ""
        if isFromCurrentSender(message: message) && self.isAnonymous! {
            name = "anonymous"
        } else {
            name = message.sender.displayName
        }
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
}

// MARK: - MessageInputBarDelegate

extension TopicChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        let message = Message(user: currentUser, content: text)
        save(message)
        inputBar.inputTextView.text = ""
    }
}
