//
//  TopicPrivateChatListViewController.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 19/01/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import UIKit
import Foundation
import Firebase

class TopicPrivateChatListViewController: UIViewController {
    
    @IBOutlet weak var channelTableView: UITableView!
    @IBOutlet weak var chatListView: UIView!

    var topic : Topic?
    var selectedIndex: Int = 0
    var animalNames: [String] = []
    var channelSenderList: [UserInfo] = []
    var channelList: [[String:Any]] = []
    var currentUID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Direct Message List"
        //Assign Current UID
        self.currentUID = Auth.auth().currentUser?.uid ?? ""
        generateRandListOfUsers()
    }
    
    func generateRandListOfUsers() {
        for _ in 1...100 {
            if let firstName = StaticData.randomName.randomElement(), let lastName = StaticData.animalNames.randomElement() {
                self.animalNames.append(firstName.capitalizingFirstLetter() + " " + lastName.capitalizingFirstLetter())
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.channelTableView.tableFooterView = UIView()
        setNavigationBarColor()
        getAllChannels()
    }
    
    func setNavigationBarColor() {
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = .black
    }
    
    func getAllChannels() {
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    guard ((querySnapshot?.documents.isEmpty) != nil) else {
                        return
                    }
                    
                    var channelList : [[String:Any]]  = []
                    querySnapshot?.documents.forEach {
                        var documentValue = $0.data()
                        documentValue["id"] = $0.documentID
                        channelList.append(documentValue)
                    }
                    self.channelList = channelList.filter({ (channel) -> Bool in
                        return (self.topic?.privateChannelRef?.contains(channel["channelRef"] as! String) ?? false)
                    })
                    
                    if self.channelList.count > 0 {
                        let privateChannelRefList = self.channelList.map { $0["channelRef"] }
                        var uids: [String] = []
                        for privateChannelRef in privateChannelRefList {
                            uids.append(self.getSenderUid(privateChannelRef as! String))
                        }
                        FirebaseUtility().getUserDetailsList(userIds: uids) { (users, error) in
                            if (error != nil) {
                               print("Error while fetching user list")
                            } else {
                                self.channelSenderList = users ?? []
                                //Update Channel Array with Sender Name
                                var updatedChannelList: [[String:Any]] = []
                                self.channelList = self.channelList.map { (channel) in
                                    let users = self.channelSenderList.filter { (user) -> Bool in
                                        return user.id == self.getSenderUid(channel["channelRef"] as! String)
                                    }
                                    var copyChannel = channel
                                    if (users.count > 0) {
                                        if let user = users.first {
                                            copyChannel["senderNickName"] = user.nickName
                                        }
                                    }
                                    updatedChannelList.append(copyChannel)
                                    return channel
                                  }
                                self.channelList = updatedChannelList
                                //Sort Array by Date
                                self.channelList.sort(by: {
                                    guard let firstDate = $0["lastMsgDate"] as? Timestamp else {
                                        return false
                                    }
                                    guard let nextDate = $1["lastMsgDate"] as? Timestamp else {
                                        return false
                                    }
                                    return firstDate.dateValue() > nextDate.dateValue()
                                })
                                self.channelTableView.reloadData()
                            }
                        }
                    }
                    
                    
                }
        }
    }
    
    func getSenderUid(_ channelRef: String) -> String {
        var ids = channelRef.components(separatedBy: "+")
        ids.remove(at: 0)
        if let findIndex = ids.firstIndex(where: { (value) -> Bool in
            return value == self.currentUID
        }) {
            ids.remove(at: findIndex)
        }
        
        return ids.first ?? ""
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "topicPrivateChatSegue" {
            if let containerVC = segue.destination as? ContainerViewController {
                let channel = self.channelList[self.selectedIndex]
                guard let currentUser = Auth.auth().currentUser else {
                    return
                }
                guard let selectedChannel = Channel(data: channel) else {
                    return
                }
                guard let selectedTopic = self.topic else {
                    return
                }
                containerVC.selectedChannel = selectedChannel
                containerVC.currentUser = currentUser
                containerVC.topic = selectedTopic
                if let topic_title = selectedTopic.topic_title {
                    containerVC.title = topic_title.count > Constants.chatTitleMaxLength ? String(topic_title.prefix(Constants.chatTitleMaxLength)) + "..." : topic_title
                }
                if let senderNickName = channel["senderNickName"] as? String {
                    containerVC.senderDisplayName = senderNickName
                } else {
                    containerVC.senderDisplayName = self.animalNames[selectedIndex]
                }
                
                containerVC.selectedFirebaseCollection = FirebaseConst.Collections.TopicChannels
                //User Unread counts
                containerVC.userUnreadCounts = channel[currentUser.uid] as? Int ?? 0
            }
        }
    }

}

extension TopicPrivateChatListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.PrivateUsersCell) as! PrivateUsersCell
        let channel = self.channelList[indexPath.row]
        if let senderName = channel["senderNickName"] as? String {
            cell.lblUserName.text = senderName
        } else {
            cell.lblUserName.text = self.animalNames[indexPath.row]
        }
        
        if let lastMsg = channel["lastMsg"] as? String {
            cell.lblLastMsg.text = lastMsg
        }
        
        if let lastMsgDate = channel["lastMsgDate"] as? Timestamp {
            let timeAgo = lastMsgDate.dateValue().timeAgo(numericDates:true)
            cell.lblLastMsgDate?.text = timeAgo
        }
    
        if let msgCount = channel[self.currentUID] as? Int {
            if msgCount > 0 {
                cell.lblChatCount.text = "\(channel[self.currentUID] ?? 0)"
                cell.lblChatCount.layer.cornerRadius = 12.5
                cell.lblChatCount.layer.masksToBounds = true
                cell.lblChatCount.isHidden = false
            } else {
                cell.lblChatCount.isHidden = true
            }
        } else {
            cell.lblChatCount.isHidden = true
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.getOrCreateTopicPrivateChannel(indexPath.row)
        self.selectedIndex = indexPath.row
        
        //Push to container view
        self.performSegue(withIdentifier: "topicPrivateChatSegue", sender: self.channelList[indexPath.row])
    }

    
    func getOrCreateTopicPrivateChannel(_ index:Int) {
        self.selectedIndex = index
        guard let channelRef = self.topic?.privateChannelRef?[index] else {
            return
        }
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .whereField("channelRef", isEqualTo: channelRef)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard let channel = Channel(document: data) else {
                            return
                        }
                        //Push to container view
                        self.performSegue(withIdentifier: "topicPrivateChatSegue", sender: channel)
                    }
                }
            })
    }
}

class ContainerViewController: UIViewController {
    
    var currentUser: User?
    var selectedChannel: Channel?
    var selectedFirebaseCollection = ""
    var senderDisplayName = ""
    var topic : Topic?
    var userUnreadCounts : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let chatViewController = TopicChatViewController(user: currentUser!,
                                                         channel: selectedChannel!,
                                                         anonymousMode: true,
                                                         topic: topic!,
                                                         firebseCollection: selectedFirebaseCollection,
                                                         senderDisaplyName: senderDisplayName,
                                                         userUnreadCounts:userUnreadCounts)
        self.addChild(chatViewController)
        chatViewController.view.frame = view.bounds
        view.addSubview(chatViewController.view)
        chatViewController.didMove(toParent: self)
    }
    

}
