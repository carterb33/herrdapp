//
//  EventViewController.swift
//  Herrd
//
//  Created by DG on 09/01/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import SDWebImage
//import FirebaseStorage
import MBProgressHUD
//import FirebaseAuth
import Firebase

class EventViewController: UIViewController {
    
    @IBOutlet weak var lblCityTitle: UILabel!
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var eventNowButton: UIImageView!
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self, action: #selector(refreshAndSortListData(sender:)), for: .valueChanged)
        return control
    }()
    var eventList : [[String:Any]] = []
    var currentUid: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.eventTableView.separatorStyle = .none
        lblCityTitle.textColor = AppColor.CommonColor.postColor
//        let appdelegate = UIApplication.shared.delegate as? AppDelegate
//        if appdelegate?.currentCity?.count ?? 0 > 0 {
//            self.lblCityTitle.text = appdelegate?.currentCity
//        }
        self.lblCityTitle.text = "Events"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 72, height: 36))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 72, height: 36))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "new-nav-logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
        
        eventTableView.dataSource = self
        eventTableView.delegate = self
        
        eventTableView.rowHeight = 300
        
        //Get Current Uid
        self.currentUid = Auth.auth().currentUser?.uid
        
        self.getTodaysEvent()
         addPullToRefresh()
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            eventTableView.refreshControl = refreshControl
        } else {
            eventTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.getTodaysEvent()
    }
    
    func getTodaysEvent(){
            //fetch user chat count list
            MBProgressHUD.showAdded(to: self.view, animated: true)
            FirebaseUtility().getEvents { (eventList, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
//                    print("Event List",eventList)
                    self.eventList = eventList ?? []
                    
                    self.eventTableView.reloadData()
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                }
            }
        }

}

//TABLEVIEW EXTENSION
extension EventViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as! EventCell
        
        let event = self.eventList[indexPath.row]
        
        cell.goingButton.tag = indexPath.row
        cell.goingButton.addTarget(self, action: #selector(goingButtonPressed(_:)), for: .touchUpInside)
        if let goingList = event["goingUserList"] as? [String] {
            let goingNumberString = String(describing: goingList.count) + " 👤"
            cell.goingButton.setTitle(goingNumberString, for: .normal)
            if let currUid = self.currentUid {
                if goingList.contains(currUid) {
                    cell.goingButton.isSelected = true
                    cell.goingButton.backgroundColor = UIColor(red: 152/255, green: 211/255, blue: 81/255, alpha: 1.0)
                    cell.goingButton.setTitleColor(UIColor.black, for: .selected)
                } else {
                    cell.goingButton.isSelected = false
                    cell.goingButton.backgroundColor = UIColor(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
                    cell.goingButton.setTitleColor(UIColor.gray, for: .normal)
                }
            }
        } else {
            let goingNumberString = String(describing: 0) + " 👤"
            cell.goingButton.setTitle(goingNumberString, for: .normal)
        }
        
        cell.lblEventName?.text = event["event_title"] as? String
        
        print(event["going_button"]! )
        print("$$$$")
        
        if event["going_button"]! as! String == "no" {
            cell.goingButton.isHidden = true
        }

//        cell.eventDay?.text = event["event_day"] as? String
        
        //CHANGING text of cell.eventDay if the event is Today
                let todayDate = Date()
                let components = Calendar.current.dateComponents([.year, .month, .day], from: todayDate)
                guard
                    let start = Calendar.current.date(from: components),
                    let end = Calendar.current.date(byAdding: .day, value: 1, to: start)
                else {
                    fatalError("Could not find start date or calculate end date.")
                }
                
                let timestamp: Timestamp = event["event_date"]! as! Timestamp
                let RequestedDate = timestamp.dateValue()


                if RequestedDate > start && RequestedDate < end {
                    cell.eventDay?.text = "Today 🎉"
                    cell.eventDay?.textColor = AppColor.CommonColor.postColor
                } else {
                    cell.eventDay?.text = event["event_day"] as? String
                    cell.eventDay?.textColor = AppColor.CommonColor.secondaryColor
                }
        ///
        
        if let eventMainURL = event["event_main_img"] as? String {
            let storageRef = Storage.storage().reference(forURL: eventMainURL)
            cell.imgMain.sd_setImage(with: storageRef, placeholderImage: UIImage(named: "placeholder.png"))
        }
        else {
            print("Main Image is nil")
        }
        
        if let eventthumbURL = event["event_thumb"] as? String {
            let storageRef = Storage.storage().reference(forURL: eventthumbURL)
            cell.imgThumb.sd_setImage(with: storageRef, placeholderImage: UIImage(named: "placeholder.png"))
        }
        else {
            print("Thumb Image is nil")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventDetail = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailViewController") as? EventDetailViewController
        eventDetail?.eventDetail = self.eventList[indexPath.row]
        present(eventDetail!, animated: true, completion: nil)
    }
    
    @IBAction func goingButtonPressed(_ sender: UIButton) {
        let event = self.eventList[sender.tag]
        if sender.isSelected == true {
            sender.backgroundColor = UIColor(red: 55/255, green: 55/255, blue: 55/255, alpha: 1.0)
            sender.setTitleColor(UIColor.gray, for: .normal)
            sender.isSelected = false
            if let goingList = event["goingUserList"] as? [String] {
                let goingNumberString = String(describing: goingList.count - 1) + " 👤"
                sender.setTitle(goingNumberString, for: .normal)
            }
            //subtract 1 going_amount in Firebase event document
            //show this same isSelected state when user comes back to visit this View Controller
        } else {
            sender.backgroundColor = UIColor(red: 152/255, green: 211/255, blue: 81/255, alpha: 1.0)
            sender.isSelected = true
            sender.setTitleColor(UIColor.black, for: .selected)
            //add 1 going_amount in Firebase event document
            //show this same isSelected state when user comes back to visit this View Controller
            if let goingList = event["goingUserList"] as? [String] {
                let goingNumberString = String(describing: goingList.count + 1) + " 👤"
                sender.setTitle(goingNumberString, for: .normal)
            } else {
                let goingNumberString = String(describing: 1) + " 👤"
                sender.setTitle(goingNumberString, for: .normal)
            }
        }
        self.updateEventGoingUserList(self.eventList[sender.tag],sender.tag)
    }
    
    func updateEventGoingUserList(_ event: [String:Any],_ index: Int ) {
        
        //Get Current Uid
        guard let currentUid = Auth.auth().currentUser?.uid else {
            return
        }
        
        guard let eventId = event["event_id"] else {
            return
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.Events)
            .whereField("event_id", isEqualTo: eventId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    if let document = querySnapshot!.documents.first{
                        var documentValue = document.data()
                        documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                            print(test,test1)
                        }
                        
                        if var goingUserList = documentValue["goingUserList"] as? [String] {
                            if goingUserList.contains(currentUid) {
                                if let findIndex = goingUserList.firstIndex(of: currentUid) {
                                    goingUserList.remove(at: findIndex)
                                }
                            } else {
                                goingUserList.append(currentUid)
                            }
                            document.reference.updateData([
                                "goingUserList": goingUserList
                            ]) { err in
                                if let err = err {
                                   print("Got error while going list.",err)
                                } else {
                                    print("Going list updated.")
                                    self.eventList[index]["goingUserList"] = goingUserList
                                    self.eventTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                                }
                            }
                        } else {
                            document.reference.updateData([
                                "goingUserList": [currentUid]
                            ]) { err in
                                if let err = err {
                                   print("Got error while going list.",err)
                                } else {
                                    print("Going list updated.")
                                    self.eventList[index]["goingUserList"] = [currentUid]
                                    self.eventTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                                }
                            }
                        }
                    }
                }
        }
    }
    
}
