//
//  EventDetailViewController.swift
//  Herrd
//
//  Created by DG on 11/01/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import FirebaseStorage
import SafariServices

class EventDetailViewController: UIViewController, SFSafariViewControllerDelegate {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblEventDesc: UILabel!
    @IBOutlet weak var lblEventDescHeader: UILabel!
    @IBOutlet weak var lblEventTime: UILabel!
    @IBOutlet weak var eventButton: UIButton!
    var eventDetail : [String:Any] = [:]
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        mainView.backgroundColor = AppColor.CommonColor.cellColor
        eventButton.layer.cornerRadius = 3
//        eventButton.layer.borderWidth = 1
//        eventButton.layer.borderColor = UIColor.black.cgColor
        if self.eventDetail["event_btn_label"] as? String == "" {
            eventButton.isHidden = true
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(EventDetailViewController.tapFunction))
        lblAddressTitle.isUserInteractionEnabled = true
        lblAddressTitle.addGestureRecognizer(tap)
        

//
////        guard let url = URL(string: buttonUrl)
//        let safariVC = SFSafariViewController(url: url)
//        present(safariVC, animated: true, completion: nil)
//        safariVC.delegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.setEventData()
            
        }
        
        setLableColor()
    }

    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let site = (self.eventDetail["event_url1"] as? String)!
        print(site)
        let siteURL = URL(string: site)
        let safariVC = SFSafariViewController(url: siteURL!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }
    
    func setLableColor() {
        self.lblEventTitle.textColor = AppColor.CommonColor.postColor
        self.lblAddressTitle.textColor = AppColor.CommonColor.postColor
        self.lblAddress.textColor = AppColor.CommonColor.postColor
        self.lblEventDesc.textColor = AppColor.CommonColor.postColor
        self.lblEventDescHeader.textColor = AppColor.CommonColor.postColor
        self.lblEventTime.textColor = AppColor.CommonColor.postColor
    }


    func setEventData() {
//
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "📍 " + (self.eventDetail["event_venue"] as? String)! )
        attributeString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(3, attributeString.length - 3 ))
      

        self.eventButton.setTitle(self.eventDetail["event_btn_label"] as? String, for: .normal)
        self.lblEventTitle.text = self.eventDetail["event_title"] as? String
        self.lblEventTime.text = "⏰ " + ((self.eventDetail["event_time"] as? String)!)
        self.lblAddress.text = "🗓 " + ((self.eventDetail["event_address"] as? String)!)
        self.lblEventDesc.text = self.eventDetail["event_desc"] as? String
        self.lblAddressTitle.attributedText = attributeString
        if let eventMainURL = self.eventDetail["event_main_img"] as? String {
            let storageRef = Storage.storage().reference(forURL: eventMainURL)
            self.imgMain.sd_setImage(with: storageRef, placeholderImage: UIImage(named: "placeholder.png"))
        }
        else {
            print("Main Image is nil")
        }
        
        if let eventthumbURL = self.eventDetail["event_thumb"] as? String {
            let storageRef = Storage.storage().reference(forURL: eventthumbURL)
            self.imgThumb.sd_setImage(with: storageRef, placeholderImage: UIImage(named: "placeholder.png"))
        } else {
            print("Thumb Image is nil")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func eventBtnPress(_ sender: UIButton) {
        let site = (self.eventDetail["event_btn_url"] as? String)!
        print(site)
        let siteURL = URL(string: site)
        let safariVC = SFSafariViewController(url: siteURL!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
        
    }
    
}
