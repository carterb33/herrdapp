//
//  MineViewController.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 09/04/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import FirebaseAuth
import FirebaseUI
import GoogleMobileAds
import WMSegmentControl

class MineViewController: UIViewController {
    
    @IBOutlet weak var lblCityTitle: UILabel!
    @IBOutlet weak var topicTableView: UITableView!
    @IBOutlet weak var topicSegment: WMSegment!
    @IBOutlet weak var btnNewTopic: UIButton!
    @IBOutlet weak var adminIcon: UIImageView!
    @IBOutlet weak var logoButton: UIButton!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var lblSmilyIcon: UILabel!
    @IBOutlet weak var lblMessageTitle: UILabel!
    @IBOutlet weak var venueButton: UIButton!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var lblBadgeCount: UILabel!
    @IBOutlet var chatButton: UIButton!
    @IBOutlet weak var schoolButton2: UIButton!
    var lastSelectedSegment = 0
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self, action: #selector(refreshAndSortListData(sender:)), for: .valueChanged)
        return control
    }()
    var topicList : [AnyObject] = []
    var topicListCopy : [Topic] = []
    var subTopicList : [SubTopic] = []
    var adminList : [String] = []
    var items: [DataItem] = []
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    let reloadAfterTime = 0.0001
    var isScrollToTop = false
    
    let gradiantImage = UIImage(named: "gradientRec6")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topicTableView.separatorStyle = .none
        //Segment with type Bottom Bar
        topicSegment.selectorType = .bottomBar
        venueButton.tintColor = AppColor.CommonColor.postColor
        topicSegment.textColor = AppColor.CommonColor.secondaryColor
        topicSegment.selectorTextColor = AppColor.CommonColor.postColor
        topicSegment.selectorColor = UIColor.init(patternImage: gradiantImage)
        
        topicSegment.SelectedFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .bold)
        topicSegment.normalFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .regular)
        //Add Logo in Navigation bar
        CommonUtility.addLogoInNavigationBar(self.navigationController, self.navigationItem)
        
        logoButton.layer.cornerRadius = 20
        logoButton.layer.borderWidth = 0.7
        logoButton.layer.borderColor = UIColor.darkGray.cgColor
        
        topicTableView.dataSource = self
        topicTableView.delegate = self
        topicTableView.estimatedRowHeight = 100
        
        addPullToRefresh()
        
        if Auth.auth().currentUser != nil {
            self.checkAdminUser()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pausePlayeVideos()
    }
    
    func checkAdminUser() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        
        if let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String] {
            self.adminList = adminlist
            if self.adminList.contains(uid!) {
                appdelegate?.isAdminUser = true
            } else {
                appdelegate?.isAdminUser = false
            }
            //First time call to get topics
            self.getAllTopics()
        } else {
            FirebaseUtility().getAppSettings { (error, doc) in
                if let err = error {
                    print("Got error while reading app settings",err)
                }
                //Call Again to get admin list
                self.checkAdminUser()
            }
        }
    }
    
    @IBAction func chatPress(_ sender: UIButton) {
        if let topicChatListViewController = self.storyboard?.instantiateViewController(withIdentifier: "TopicChatListViewController") as? TopicChatListViewController {
            self.navigationController?.pushViewController(topicChatListViewController, animated: true)
        }
    }
    
    @IBAction func btnActionPress(_ sender: UIButton) {
        switch sender.titleLabel?.text {
        case "New Post":
            opneNewTopicPopUp()
        default:
            print("Defualt action")
        }
    }
    
    @IBAction func infoPress(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Important",
                                                message: Messages.Info.AnonymousPost,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func topicSegmentValueChange(_ sender: WMSegment) {
        self.getAllTopics()
        
    }
    
    @IBAction func btnNewTopicClicked(_ sender: Any) {
        opneNewTopicPopUp()
    }
    
    func opneNewTopicPopUp() {
        let newTopicVC = self.storyboard?.instantiateViewController(withIdentifier: "NewTopicViewController") as! NewTopicViewController
        newTopicVC.modalPresentationStyle = .overCurrentContext
        newTopicVC.adminList = self.adminList
        //        self.navigationController?.pushViewController(newTopicVC, animated: true)
        self.present(newTopicVC, animated: true, completion: {})
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            topicTableView.refreshControl = refreshControl
        } else {
            topicTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.topicListCopy = []
        self.getAllTopics()
    }
    
    func getAllTopics(){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if self.topicSegment.selectedSegmentIndex == 0 {
            TopicFirebaseUtility().getMyTopics { (topicList, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if topicList?.count ?? 0 > 0 {
                        self.topicTableView.isHidden = false
                        self.messageView.isHidden = true
                        var topiclist = topicList
                        topiclist?.sort(by: {
                            $0.difference! > $1.difference!
                        })
                        self.topicList = topiclist ?? []
                        let playerVC = VideoPlayerController.sharedVideoPlayer
                        playerVC.resetAndClearCache(tableView: self.topicTableView)
                        self.topicTableView.reloadData()
                    } else {
                        self.topicTableView.isHidden = true
                        self.lblMessageTitle.text = Messages.Info.NoRecordInMyTopic
                        self.btnAction.setTitle("New Post", for: .normal)
                        self.btnAction.isHidden = true
                        self.messageView.isHidden = false
                    }
                    
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                }
            }
        }
        else if self.topicSegment.selectedSegmentIndex == 1 {
            SubTopicFirebaseUtility().getMySubTopics { (subtopicList, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    if subtopicList?.count ?? 0 > 0 {
                        self.topicTableView.isHidden = false
                        self.messageView.isHidden = true
                        self.subTopicList = subtopicList ?? []
                        //Sort Array by Date
                        self.subTopicList.sort(by: {
                            $0.subtopic_date!.dateValue() > $1.subtopic_date!.dateValue()
                        })
                        self.topicTableView.reloadData()
                        if self.refreshControl.isRefreshing {
                            self.refreshControl.endRefreshing()
                        }
                    } else {
                        self.topicTableView.isHidden = true
                        self.lblMessageTitle.text = Messages.Info.NoRecordInMySubTopic
                        self.btnAction.isHidden = true
                        self.messageView.isHidden = false
                    }
                }
            }
        }
        
        self.lastSelectedSegment = self.topicSegment.selectedSegmentIndex
    }
    
    func setTabBarBadgeCount(_ topicList: [Topic]) {
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else { return }
        
        let totalCount = topicList.reduce(0) { (total, topic: Topic) -> Int in
            if let privateMsgCount = topic.privateMsgCount {
                if let msgCount = privateMsgCount[uid] {
                    return total + msgCount
                } else {
                    return total
                }
            } else {
                return total
            }
        }
        
        if let navController = self.tabBarController?.viewControllers?[1] as? UINavigationController {
            navController.tabBarItem.badgeValue = totalCount > 0 ? "\(totalCount)" : nil
        }
        
    }
}
