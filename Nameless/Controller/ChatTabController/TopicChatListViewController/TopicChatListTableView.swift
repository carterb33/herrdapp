//
//  TopicChatListTableView.swift
//  Herrd
//
//  Created by DG on 15/01/21.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import FirebaseAuth
import ImageViewer
import AVFoundation
import AVKit
import GiphyUISDK

//TABLEVIEW EXTENSION
extension TopicChatListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.loadTopicCell(tableView,indexPath)
    }
    
    func loadTopicCell(_ tableView: UITableView,_ indexPath: IndexPath) -> TopCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DataIdentifires.topicCellId,
                                                 for: indexPath) as! TopCell
        
        let topic = self.topicList[indexPath.row]
        //Remove Old Media view
        cell.mediaView.removeFromSuperview()
        cell.mediaView.media = nil
        
        var promoText = ""
        if  self.adminList.contains(topic.created_by ?? ""){
            cell.totalView.backgroundColor = AppColor.CommonColor.cellColor
            cell.lblTopicName.textColor = AppColor.CommonColor.postColor
            cell.upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
            cell.downVoteTotal.textColor = AppColor.CommonColor.secondaryColor
            cell.lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
            cell.imgPin.isHidden = false
            cell.btnUp.isHidden = true
            cell.btnDown.isHidden = true
            cell.btnReport.isHidden = true
            cell.lblTopicCount.isHidden = true
            cell.pinnedCup.isHidden = false
            
            cell.downVoteTotal.isHidden = true
            cell.upVoteTotal.isHidden = true
            promoText = " ⏤ pinned post"
            
            if topic.logoUrl?.count ?? 0 > 0 {
                // Get a reference to the storage service using the default Firebase App
                let storage = Storage.storage()
                
                // Reference to an image file in Firebase Storage
                let reference = storage.reference().child("Topic Images/\(topic.topic_id ?? 0)_logo.jpg")
                
                // UIImageView in your ViewController
                let imageView: UIImageView = cell.pinnedCup
                
                // Placeholder image
                let placeholderImage = UIImage(named: "placeholder")
                
                // Load the image using SDWebImage
                imageView.sd_setImage(with: reference, placeholderImage: placeholderImage)
            }
            
        }
        else {
            cell.totalView.backgroundColor = AppColor.CommonColor.cellColor
            cell.lblTopicName.textColor = AppColor.CommonColor.postColor
            cell.upVoteTotal.textColor = AppColor.CommonColor.secondaryColor
            cell.downVoteTotal.textColor = AppColor.CommonColor.secondaryColor
            cell.lblTopicDate.textColor = AppColor.CommonColor.secondaryColor
            cell.imgPin.isHidden = true
            cell.btnUp.isHidden = false
            cell.btnDown.isHidden = false
            cell.btnReport.isHidden = false
            cell.lblTopicCount.isHidden = false
            cell.downVoteTotal.isHidden = false
            cell.upVoteTotal.isHidden = false
            cell.pinnedCup.isHidden = true
            promoText = ""
            
            cell.btnUp.isHidden = false
            cell.btnDown.isHidden = false
            cell.btnReport.isHidden = false
            cell.bubbleButton.isHidden = false
            cell.btnCell.isHidden = false
            
        }
        
        cell.topic = topic
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if (topic.upVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = true
            cell.btnDown.isSelected = false
        } else if (topic.downVoteIds?.contains(uid!))! {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = true
        } else {
            cell.btnUp.isSelected = false
            cell.btnDown.isSelected = false
        }
        if topic.upVoteIds!.count > 0 {
            let upvoteIds = topic.upVoteIds?.components(separatedBy: ",")
            cell.upVoteTotal?.text = "\(upvoteIds?.count ?? 0)"
        } else {
            cell.upVoteTotal?.text = "0"
        }
        
        if topic.downVoteIds!.count > 0  {
            let downvoteIds = topic.downVoteIds?.components(separatedBy: ",")
            cell.downVoteTotal?.text = "\(downvoteIds?.count ?? 0)"
        } else {
            cell.downVoteTotal?.text = "0"
        }
        
        cell.lblTopicName?.text = topic.topic_title
        cell.lblTopicCount.text = "\(topic.difference ?? 0)"
        cell.btnUp.tag = indexPath.row
        cell.btnDown.tag = indexPath.row
        cell.bubbleButton.tag = indexPath.row
        cell.btnCell.tag = indexPath.row
        cell.btnImg.tag = indexPath.row
        cell.btnMute.tag = indexPath.row
        cell.btnReport.tag = indexPath.row
        cell.btnUp.addTarget(self, action: #selector(TopicNewsViewController.btnUpClicked(_:)),
                             for: .touchUpInside)
        cell.btnDown.addTarget(self, action: #selector(TopicNewsViewController.btnDownClicked(_:)),
                               for: .touchUpInside)
        cell.btnImg.addTarget(self, action: #selector(TopicNewsViewController.btnViewImageClicked(_:)),
                              for: .touchUpInside)
        cell.btnMute.addTarget(self, action: #selector(TopicNewsViewController.btnMuteClicked(_:)),
                               for: .touchUpInside)
        cell.btnReport.addTarget(self, action:              #selector(TopicNewsViewController.btnReportClicked(_:)),
                                 for: .touchUpInside)
        if let timestamp = topic.topic_date {
            let timeAgo = timestamp.dateValue().timeAgo(numericDates:true)
            cell.lblTopicDate?.text = timeAgo + promoText
        }
        
        if topic.imageUrl?.count ?? 0 > 0 {
            // Get a reference to the storage service using the default Firebase App
            let storage = Storage.storage()
            
            // Reference to an image file in Firebase Storage
            let reference = storage.reference().child("Topic Images/\(topic.topic_id ?? 0).jpg")
            
            // UIImageView in your ViewController
            let imageView: UIImageView = cell.imgTopic
            
            // Placeholder image
            let placeholderImage = UIImage(named: "placeholder")
            
            // Load the image using SDWebImage
            imageView.sd_setImage(with: reference, placeholderImage: placeholderImage)
            cell.imgTopic.isHidden = false
            cell.imgTopicTop.constant = 10
            cell.imgTopicBottom.constant = 10
            cell.imgHeight.constant = cell.imgTopic.frame.size.width
            if topic.mediaType == "movie" {
                cell.btnMute.isHidden = true
                //                if cell.videoURL == nil {
                cell.configureCell(videoUrl: topic.videoUrl)
                //                }
                
                //cell.videoLayer.frame = CGRect(x: 0, y: 0, width: cell.imgTopic.frame.size.width, height: cell.imgTopic.frame.size.height)
                if UserDefaults.standard.bool(forKey: "isVideoMute") {
                    cell.btnMute.isSelected = true
                } else {
                    cell.btnMute.isSelected = false
                }
            } else {
                cell.btnMute.isHidden = true
                cell.configureCell(videoUrl: nil)
            }
        } else if topic.gifId?.count ?? 0 > 0 {
            cell.btnMute.isHidden = true
            cell.configureCell(videoUrl: nil)
            //Set Image Frame
            cell.imgTopic.isHidden = false
            cell.mediaView.isHidden = true
            cell.imgTopic.image = UIImage(named: "placeholder-gif")
            cell.imgTopicTop.constant = 10
            cell.imgTopicBottom.constant = 10
            cell.imgHeight.constant = cell.imgTopic.frame.size.width
            //SetUp GIF
            cell.mediaView.frame = CGRect(x: cell.imgTopic.frame.origin.x, y: cell.imgTopic.frame.origin.y, width: cell.imgTopic.frame.size.width, height: cell.imgTopic.frame.width)
            cell.totalView.addSubview(cell.mediaView)
            cell.totalView.bringSubviewToFront(cell.btnImg)
            GiphyCore.shared.gifByID(topic.gifId ?? "") { (response, error) in
                if let media = response?.data {
                    DispatchQueue.main.sync {
                        cell.mediaView.media = media
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            cell.imgTopic.isHidden = true
                            cell.mediaView.isHidden = false
                            cell.mediaView.frame = CGRect(x: cell.imgTopic.frame.origin.x, y: cell.imgTopic.frame.origin.y, width: cell.imgTopic.frame.size.width, height: cell.imgTopic.frame.width)
                        }
                        
                        cell.mediaView.layer.cornerRadius = 10
                        cell.mediaView.clipsToBounds = true
                    }
                }
            }
        } else {
            cell.btnMute.isHidden = true
            cell.imgTopicTop.constant = 7
            cell.imgTopicBottom.constant = 7
            cell.imgHeight.constant = 0
            cell.configureCell(videoUrl: nil)
            cell.imgTopic.isHidden = true
        }
        
        cell.bubbleButton.addTarget(self, action: #selector(chatIconPressed(_:)),
                                    for: .touchUpInside)
        
        cell.btnCell.addTarget(self, action: #selector(chatIconPressed(_:)),
                               for: .touchUpInside)
        let privateMsgCount = topic.privateMsgCount?[uid!] ?? 0
        if privateMsgCount > 0 {
            cell.bubbleButton.titleLabel?.textColor = .lightGray
            cell.bubbleButton.setImage(UIImage(named: "small-chatUnread"), for: .normal)
            cell.bubbleButton.setTitle("\(privateMsgCount)", for: .normal)
        } else {
            cell.bubbleButton.titleLabel?.textColor = .lightGray
            cell.bubbleButton.setImage(UIImage(named: "small-chatRegular"), for: .normal)
            cell.bubbleButton.setTitle("", for: .normal)
        }
        
//        #imageLiteral(resourceName: "blackChatIcon")
//        let attributeText = NSAttributedString(string: privateMsgCount > 0 ? "\(privateMsgCount)" : "")
//        cell.bubbleButton.setAttributedTitle(attributeText, for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? AutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            VideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
        }
    }
    
    @IBAction func chatIconPressed(_ sender: UIButton) {
        self.selectedIndex = sender.tag
        if topicSegment.selectedSegmentIndex == 0 {
            if let subTopicVC = self.storyboard?.instantiateViewController(withIdentifier: "SubTopicViewController") as? SubTopicViewController {
                subTopicVC.selectedTopic = self.topicList[sender.tag]
                subTopicVC.adminList = self.adminList
                subTopicVC.isFromPrivateChatList = true
                self.navigationController?.pushViewController(subTopicVC, animated: true)
            }
        } else {
            //Push to container view
            self.getTopicPrivateChannel(sender.tag)
        }
        
    }
    
    @IBAction func btnReportClicked(_ sender: UIButton) {
        var topic : Topic?
        topic = self.topicList[sender.tag]
        //Check if user's self post or other post
        let user = Auth.auth().currentUser
        let uid = user?.uid
        showActionSheetForReportPostOrDelete(sender,topic?.created_by == uid ? .DeletePost : .ReportPost,topic,sender.tag,.Topic,nil)
        
    }
    
    func showActionSheetForReportPostOrDelete(_ sender: UIButton,_ actionType: ActionSheetType,_ topic: Topic?,_ index: Int,_ type: TopicType, _ subtopic: SubTopic?) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if #available(iOS 13.0, *) {
            alertController.view.tintColor = UIColor { traitCollection in
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return .white
                default:
                    return .black
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        if actionType == .ReportPost {
            
            let reportAction = UIAlertAction(title: "Report post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Report post")
                self.showReportPostAlert(topic, index)
            })
            let flagImage = UIImage(named: "flagIcon")
            reportAction.setValue(flagImage, forKey: "image")
            //for original image use this flagImage?.withRenderingMode(.alwaysOriginal)
            
            let hideAction = UIAlertAction(title: "Hide post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Hide post")
                self.showHidePostAlert(topic, index)
            })
            let blockIconImage = UIImage(named: "blockIcon")
            hideAction.setValue(blockIconImage, forKey: "image")
            
            let blockAction = UIAlertAction(title: "Block user", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Block user")
                self.showBlockUserAlert(topic, index)
            })
            let hideIconImage = UIImage(named: "hideIcon")
            blockAction.setValue(hideIconImage, forKey: "image")
            
            alertController.addAction(reportAction)
            alertController.addAction(hideAction)
            alertController.addAction(blockAction)
        } else {
            let deleteAction = UIAlertAction(title: "Delete Post", style: .default, handler: { (alert: UIAlertAction!) -> Void in
                print("Delete post")
                self.showDelelePostAlert(topic,index,type,subtopic)
            })
            let deleteIconImage = UIImage(named: "deleteIcon")
            deleteAction.setValue(deleteIconImage, forKey: "image")
            
            alertController.addAction(deleteAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        alertController.addAction(cancelAction)
        
        //if iPhone
        if UIDevice.isPhone {
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            //In iPad Change Rect to position Popover
            if let cell = sender.superview?.superview?.superview {
                /* Get the souce rect frame */
                let buttonFrame = sender.frame
                var showRect    = cell.convert(buttonFrame, to: self.topicTableView)
                showRect        = self.topicTableView.convert(showRect, to: view)
                showRect.origin.y += 10
                showRect.origin.x += sender.frame.size.width + 5
                
                alertController.popoverPresentationController?.sourceView = self.view
                alertController.popoverPresentationController?.sourceRect = showRect
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    func showDelelePostAlert(_ topic: Topic?,_ index: Int,_ type: TopicType, _ subtopic: SubTopic?) {
        let alert = UIAlertController(title: type == .Topic ? "Delete Post?" : "Delete Reply?",
                                      message: type == .Topic ? Messages.Warning.DeletePost : Messages.Warning.DeleteReply, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            self.deletePost(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showReportPostAlert(_ topic: Topic?,_ index: Int) {
        let alert = UIAlertController(title: "Report Post?", message: Messages.Info.ReviewPost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Report", style: .default, handler: { action in
            self.reportPost(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHidePostAlert(_ topic: Topic?,_ index: Int) {
        let alert = UIAlertController(title: "Hide Post?", message: Messages.Info.HidePost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Hide", style: .default, handler: { action in
            self.hidePost(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showBlockUserAlert(_ topic: Topic?,_ index: Int) {
        let alert = UIAlertController(title: "Block User?", message: Messages.Info.BlockPost, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Block", style: .default, handler: { action in
            self.blockUser(topic, index)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Cancel Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePost(_ topic: Topic?,_ index: Int) {
        
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Removing Topic..."
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: topic?.topic_id ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        
                        loadingNotification.label.text = "Topic Removed"
                        Firestore.firestore().collection("Topics").document(data.documentID).delete() { err in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            if let err = err {
                                print("Error removing document: \(err)")
                            } else {
                                print("Topic Document successfully removed!")
                                
                                // Create a reference to the file to delete
                                if topic?.mediaType == "movie" {
                                    let desertRef = Storage.storage().reference().child("Topic Images/\(topic?.topic_id ?? 0).jpg")

                                    // Delete the file
                                    desertRef.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Images/\(topic?.topic_id ?? 0).jpg deleted successfully")
                                      }
                                    }
                                    
                                    let desertRefVideo = Storage.storage().reference().child("Topic Videos/\(topic?.topic_id ?? 0).mp4")

                                    // Delete the video file
                                    desertRefVideo.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Videos/\(topic?.topic_id ?? 0).mp4 deleted successfully")
                                      }
                                    }
                                } else if topic?.mediaType == "image" {
                                    let desertRef = Storage.storage().reference().child("Topic Images/\(topic?.topic_id ?? 0).jpg")

                                    // Delete the file
                                    desertRef.delete { error in
                                      if let error = error {
                                        // Uh-oh, an error occurred!
                                        print(error)
                                      } else {
                                        // File deleted successfully
                                        print("Topic Images/\(topic?.topic_id ?? 0).jpg deleted successfully")
                                      }
                                    }
                                }
                                
//                                let indexPath = IndexPath.init(row: index, section: 0)
//                                self.topicTableView.beginUpdates()
                                self.topicList.remove(at:index)
                                self.topicTableView.reloadData()
//                                self.topicTableView.deleteRows(at: [indexPath], with: .automatic)
//                                self.topicTableView.endUpdates()
                                
                                //Delete Sub Topics & Place
                                SubTopicFirebaseUtility().getSubTopics(topic?.topic_id, completion: { (subTopicList, error) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    if let error = error {
                                        print("Error getting documents: \(error)")
                                    } else {
                                        subTopicList?.forEach {
                                            Firestore.firestore()
                                                .collection(FirebaseConst.Collections.SubTopics)
                                                .document($0.documentId!).delete() { err in
                                                if let err = err {
                                                    print("Error removing document: \(err)")
                                                } else {
                                                    print("Sub Topic Document successfully removed!")
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }else{
                        print("No Topic to delete Data")
                    }
                }
            })
        
    }
    
    func reportPost(_ topic: Topic?,_ index: Int) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        if !(topic?.reportedIds?.contains(uid!) ?? true) ||  topic?.reportedIds == nil{
            let copyTopic = topic?.copy(with: nil) as? Topic
            var reportedIdsStringArr : [String] = []
            if (copyTopic?.reportedIds?.count ?? 0) > 0 {
                reportedIdsStringArr = copyTopic!.reportedIds?.components(separatedBy: ",") ?? []
            }
               
            //New record added
            reportedIdsStringArr.append(uid!)
               
            let reportedIdsString = (reportedIdsStringArr.count > 1 ? (reportedIdsStringArr.joined(separator: ",") ) : reportedIdsStringArr.first ?? "")
           
            copyTopic!.reportedIds = reportedIdsString
               
            self.topicList.remove(at: index)
            self.topicList.insert(copyTopic!, at: index)
//          self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
            self.hapticImpact.impactOccurred()
                               
            print("Local Topic Report Done$$")
            print(reportedIdsStringArr.count)

            //Firebase Updation
            TopicFirebaseUtility().addReportedIdstoTopic(copyTopic!, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Report Failed", message: Messages.Errors.ReportFailure, controller: self)
                    self.topicList.remove(at: index)
                    self.topicList.insert(updatedTopic!, at: index)
//                  self.topicTableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                } else {
                    print("Firebase Topic Report Done%%")
                    print(reportedIdsStringArr.count)
                }
            })
        }
    }
    
    func hidePost(_ topic: Topic?,_ index: Int) {
    
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let topic = topic else { return }
    
        //Check and add new record
        if !topic.hidePostsUserIdsArr.contains(uid) {
            topic.hidePostsUserIdsArr.append(uid)
            self.hapticImpact.impactOccurred()
            //Firebase Updation
            TopicFirebaseUtility().addHidePostUserIdstoTopic(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Hide post Failed", message: Messages.Errors.HideFailure, controller: self)
                } else {
                    print("Firebase Topic Hide Done%%")
                    self.topicList.remove(at:index)
                    self.topicTableView.reloadData()
                }
            })
        }
    }
    
    func blockUser(_ topic: Topic?,_ index: Int) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Blocking User..."
        FirebaseUtility()
            .addOrUpdateBlockedUserIds(blockeduserId: topic?.created_by ?? "") { (isAdded, error) in
            if let error = error {
                print("Error getting documents: \(error)")
                CommonUtility.showAlert(title: "Block User Failed",
                                        message: Messages.Errors.BlockUserFailure,
                                        controller: self)
            } else {
                print("Firebase Block User Done%%")
                loadingNotification.label.text = "Removing Blocked User Posts..."
                self.topicList.removeAll { (topicLocal) -> Bool in
                    return topicLocal.created_by == topic?.created_by
                }
                self.topicTableView.reloadData()
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
        
    }
    
    func getTopicPrivateChannel(_ index:Int) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let selectedTopic = self.topicList[index]
        guard let currentId = Auth.auth().currentUser?.uid else {
            return
        }
        
        //get DisplayName
        FirebaseUtility().getUserDetails(userId: selectedTopic.created_by!) { (user, error) in
            var senderNickName = ""
            MBProgressHUD.hide(for: self.view, animated: true)
            if user != nil, user?.nickName != nil{
                senderNickName = user?.nickName ?? ""
            } else {
//                self.assignNickName(selectedTopic.created_by!)
//                senderNickName = self.animalNames[selectedIndex]
            }
            
            let channelRef = CommonUtility.getChannelRefFromTopicRefList(selectedTopic.privateChannelRef ?? [],
                                                                         currentUid: currentId)
            Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
                .whereField("channelRef", isEqualTo: channelRef)
                .getDocuments(completion: { (querySnapshot, err) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        if let document = querySnapshot!.documents.first{
                            var channel = document.data()
                            channel["id"] = document.documentID
                            //Push to container view
                            if let containerVC = self.storyboard?.instantiateViewController(withIdentifier: "ContainerViewController") as? ContainerViewController {
                                guard let currentUser = Auth.auth().currentUser else {
                                    return
                                }
                                guard let selectedChannel = Channel(data: channel) else {
                                    return
                                }
                                guard let selectedIndex = self.selectedIndex else {
                                    return
                                }
                                let selectedTopic = self.topicList[selectedIndex]
                                containerVC.selectedChannel = selectedChannel
                                containerVC.currentUser = currentUser
                                containerVC.topic = selectedTopic
                                if let topic_title = selectedTopic.topic_title {
                                    containerVC.title = topic_title.count > Constants.chatTitleMaxLength ? String(topic_title.prefix(Constants.chatTitleMaxLength)) + "..." : topic_title
                                }
                                containerVC.senderDisplayName = senderNickName
                                containerVC.selectedFirebaseCollection = FirebaseConst.Collections.TopicChannels
                                //User Unread counts
                                containerVC.userUnreadCounts = channel[currentUser.uid] as? Int ?? 0
                                self.navigationController?.show(containerVC, sender: nil)
                                self.setNavigationBarColor()
                            }
                        }
                    }
                })
        }
    }
    
    func setNavigationBarColor() {
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = .black
    }
    
    func updateTopicWithChannelId(_ channelId : String, selectedTopic : Topic) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "channelId": channelId
                    ])
                }
        }
    }
    
    func addOrUpdateChatCount(_ channelRef: String, selectedTopic: Topic) {
        
        let currentBackgroundDate = NSDate()
        
        Firestore.firestore().collection(FirebaseConst.Collections.UserChatCount)
        .whereField("uniqueId", isEqualTo: channelRef )
        .getDocuments(completion: { (querySnapshot, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if (querySnapshot!.documents.first?.data()) != nil{
                    print("User Chat Count already there")
                }else{
                    Firestore.firestore().collection(FirebaseConst.Collections.UserChatCount).document("\(currentBackgroundDate)").setData([
                        "uniqueId": channelRef,
                        "chatCount": selectedTopic.msgcount
                    ]) { (err) in
                    }
                }
            }
        })
    }
    
    @IBAction func btnUpClicked(_ sender: UIButton) {
        
        let topic = self.topicList[sender.tag]
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = topic.copy(with: nil) as? Topic
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic!.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (topic.upVoteIds?.contains(uid!))! {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.difference = difference
            
            self.topicTableView.beginUpdates()
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            
            if topic.mediaType == "movie" {
                DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
//                    var rect = self.topicTableView.frame
////                    rect.origin.y = rect.origin.y + 1
//                    self.topicTableView.scrollRectToVisible(rect, animated: false)
                    self.pausePlayeVideos()
                    
                }
            }
//            print("Local Topic Removed Upvote Done")
            self.hapticImpact.impactOccurred()
//            print(upvoteStringArr.count)
            
            //Firebase Updation
            TopicFirebaseUtility().upVoteTopicRemove(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
//                    print("Firebase Topic Removed UpVote Done")
//                    print(upvoteStringArr.count)
                }
            })
            self.topicTableView.endUpdates()
        } else {
            
            if (upvoteStringArr.contains(uid!)) {
//                print("uid is in upvote")
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
//                print("uid is in downvote")
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            upvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            
            self.topicTableView.beginUpdates()
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            
            if topic.mediaType == "movie" {
                            DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
            //                    var rect = self.topicTableView.frame
            ////                    rect.origin.y = rect.origin.y + 1
            //                    self.topicTableView.scrollRectToVisible(rect, animated: false)
                                self.pausePlayeVideos()
                                
                            }
                        }
            self.hapticImpact.impactOccurred()
            
//            print("Local Topic Upvote Done$$")
//            print(upvoteStringArr.count)

            //Firebase Updation
            TopicFirebaseUtility().upVoteTopicNew(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Upvote Failed",
                                            message: Messages.Errors.UpVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
//                    print("Firebase Topic Upvote Done%%")
//                    print(upvoteStringArr.count)
                }
            })
            self.topicTableView.endUpdates()
        }
    }
    
    @IBAction func btnDownClicked(_ sender: UIButton) {
        
        let topic = self.topicList[sender.tag]
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        //Local Updation
        let copyTopic = topic.copy(with: nil) as? Topic
        
        var downvoteStringArr : [String] = []
        if copyTopic!.downVoteIds!.count > 0 {
            downvoteStringArr = copyTopic?.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var upvoteStringArr : [String] = []
        if copyTopic!.upVoteIds!.count > 0 {
            upvoteStringArr = copyTopic!.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (topic.downVoteIds?.contains(uid!))! {

            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
            copyTopic?.downVoteIds = downvoteString
            copyTopic?.difference = difference
            
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            if topic.mediaType == "movie" {
                            DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
            //                    var rect = self.topicTableView.frame
            ////                    rect.origin.y = rect.origin.y + 1
            //                    self.topicTableView.scrollRectToVisible(rect, animated: false)
//                                self.pausePlayeVideos()
                                
                            }
                        }
//            print("Local Topic Removed Downvote Done")
            self.hapticImpact.impactOccurred()

            //Firebase Updation
            TopicFirebaseUtility().downVoteTopicRemove(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Downvote Failed",
                                            message: Messages.Errors.DownVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
//                    print("Firebase Topic Remove Downvote Done")
                }
            })
        } else {
            
            if (upvoteStringArr.contains(uid!)) {
                if let index = upvoteStringArr.firstIndex(of: uid!) {
                    upvoteStringArr.remove(at: index)
                }
            }
            
            if (downvoteStringArr.contains(uid!)) {
                if let index = downvoteStringArr.firstIndex(of: uid!) {
                    downvoteStringArr.remove(at: index)
                }
            }
            
            //New record added
            downvoteStringArr.append(uid!)
            
            let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
            let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
            let difference = upvoteStringArr.count - downvoteStringArr.count
        
            copyTopic!.upVoteIds = upvoteString
            copyTopic!.downVoteIds = downvoteString
            copyTopic!.difference = difference
            
            self.topicList.remove(at: sender.tag)
            self.topicList.insert(copyTopic!, at: sender.tag)
            self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
            if topic.mediaType == "movie" {
                            DispatchQueue.main.asyncAfter(deadline: .now() + reloadAfterTime) {
            //                    var rect = self.topicTableView.frame
            ////                    rect.origin.y = rect.origin.y + 1
            //                    self.topicTableView.scrollRectToVisible(rect, animated: false)
                                self.pausePlayeVideos()
                                
                            }
                        }
            self.hapticImpact.impactOccurred()
//            print("Local Topic Downvote Done")
            TopicFirebaseUtility().downVoteTopicNew(topic, completion: { (updatedTopic, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                    CommonUtility.showAlert(title: "Downvote Failed",
                                            message: Messages.Errors.DownVoteFailure,
                                            controller: self)
                    self.topicList.remove(at: sender.tag)
                    self.topicList.insert(updatedTopic!, at: sender.tag)
                    self.topicTableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .automatic)
                } else {
//                    print("Firebase Topic Downvote Done")
                }
            })
        }
    }
    
    @IBAction func btnMuteClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnViewImageClicked(_ sender: UIButton) {
        let topic = self.topicList[sender.tag]
        if topic.mediaType == "gif" {
            guard let fullScreenGif = self.storyboard?.instantiateViewController(withIdentifier: "FullScreenGIFViewController") as? FullScreenGIFViewController else { return }
            fullScreenGif.topic = topic
            self.present(fullScreenGif, animated: true) {
                
            }
        } else if topic.mediaType == "movie" {
            
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } catch let error {
                    print("Error in AVAudio Session\(error.localizedDescription)")
            }
            
            if let cell = tableView(self.topicTableView, cellForRowAt: IndexPath.init(row: sender.tag, section: 0)) as? TopCell {
                cell.muteUnmute()
                //Show in full screen
                let playerVC = VideoPlayerController.sharedVideoPlayer
                if let player = playerVC.getCurrentPlayer() {
                    let playerController = AVPlayerViewController()
                    playerController.player = player
                    present(playerController, animated: true) {
//                        player.play()
                        player.isMuted = false
                    }
                }
            }
        } else {
            //Remove old items
            items.removeAll()
            // Show the ImageViewer with the first item
            var galleryItem: GalleryItem!
            let cell = tableView(self.topicTableView, cellForRowAt: IndexPath.init(row: sender.tag, section: 0)) as? TopCell
            if topic.mediaType == "movie" {
                galleryItem = GalleryItem.video(fetchPreviewImageBlock: { $0(cell?.imgTopic.image) }, videoURL: URL.init(string: topic.videoUrl!)!)
            } else {
                galleryItem = GalleryItem.image { $0(cell?.imgTopic.image) }
            }
            
            items.append(DataItem(imageView: cell!.imgTopic,
                                  galleryItem: galleryItem))
            self.presentImageGallery(GalleryViewController(startIndex: 0,
                                                           itemsDataSource: self,
                                                           configuration: CommonUtility.galleryConfiguration()))
        }
    }
    
    func  pausePlayeVideos(){
        VideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: self.topicTableView)
    }
    
    @objc func appEnteredFromBackground() {
        VideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: self.topicTableView, appEnteredFromBackground: true)
    }
    
    func getOrCreateTopicPrivateChannel(topic : Topic, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        var channelRef = ""
        if topic.created_by == currentUser.uid {
            channelRef = "\(currentUser.uid)+\(topic.created_by ?? "")"
        } else {
            channelRef = "\(topic.created_by ?? "")+\(currentUser.uid)"
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
            .whereField("channelRef", isEqualTo: channelRef)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard let channel = Channel(document: data) else {
                            return
                        }
                        if let subTopicVC = self.storyboard?.instantiateViewController(withIdentifier: "SubTopicViewController") as? SubTopicViewController {
                            subTopicVC.selectedTopic = topic
                            subTopicVC.adminList = self.adminList
                            subTopicVC.selectedChannel = channel
                            self.navigationController?.pushViewController(subTopicVC, animated: true)
                        }
                    }
                }
            })
    }
}


// The GalleryItemsDataSource provides the items to show
extension TopicChatListViewController: GalleryItemsDataSource {
    func itemCount() -> Int {
        return items.count
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return items[index].galleryItem
    }
}
