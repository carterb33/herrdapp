//
//  TopicChatListViewController.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 15/01/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase
import FirebaseAuth
import FirebaseUI
import WMSegmentControl

class TopicChatListViewController: UIViewController {
    
    @IBOutlet weak var lblCityTitle: UILabel!
    @IBOutlet weak var topicTableView: UITableView!
    @IBOutlet weak var adminIcon: UIImageView!
    @IBOutlet weak var lblSmilyIcon: UILabel!
    @IBOutlet weak var lblMessageTitle: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var topicSegment: WMSegment!
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self, action: #selector(refreshAndSortListData(sender:)), for: .valueChanged)
        return control
    }()
    
    var topicList : [Topic] = []
    var topicListCopy : [Topic] = []
    var adminList : [String] = []
    var items: [DataItem] = []
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    let reloadAfterTime = 0.0001
    var selectedIndex: Int?
    var isScrollToTop = false
    
    let gradiantImage = UIImage(named: "gradientRec6")!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TableView Setup
        topicTableView.backgroundColor = .black
        self.topicTableView.separatorStyle = .none
        
        //Segment with type Bottom Bar
        topicSegment.selectorType = .bottomBar
        topicSegment.textColor = AppColor.CommonColor.secondaryColor
        topicSegment.selectorTextColor = AppColor.CommonColor.postColor
        topicSegment.selectorColor = UIColor.init(patternImage: gradiantImage)
        
        topicSegment.SelectedFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .bold)
        topicSegment.normalFont = UIFont(name: "Arial Rounded MT Bold", size: 17) ?? UIFont.systemFont(ofSize: 17, weight: .regular)
        
        //Add Logo in Navigation bar
        CommonUtility.addLogoInNavigationBar(self.navigationController, self.navigationItem)
        
        topicTableView.dataSource = self
        topicTableView.delegate = self
        topicTableView.estimatedRowHeight = 100
        
        addPullToRefresh()
        
        self.checkAdminUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.delegate = self
        self.getAllTopics()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pausePlayeVideos()
    }
    
    func checkAdminUser() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        if let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String] {
            self.adminList = adminlist
            if self.adminList.contains(uid ?? "") {
                appdelegate?.isAdminUser = true
            } else {
                appdelegate?.isAdminUser = false
            }
        }
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            topicTableView.refreshControl = refreshControl
        } else {
            topicTableView.addSubview(refreshControl)
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.topicListCopy = []
        self.getAllTopics()
    }
    
    @IBAction func topicSegmentValueChange(_ sender: WMSegment) {
        self.getAllTopics()
    }
    
    func getAllTopics(){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if self.topicSegment.selectedSegmentIndex == 0 {
            //Get My Topic with Private chat
            TopicFirebaseUtility().getMyTopicsWithPrivateChannels { (mytopicList, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    self.filterTopicsByChatCount(mytopicList ?? [])
                    self.setTabBarBadgeCount()
                }
            }
        } else {
            TopicFirebaseUtility().getTopicsByPrivateChannel { (topicList, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    self.filterTopicsByChatCount(topicList ?? [])
                    self.setTabBarBadgeCount()
                }
            }
        }
    }
    
    func setTabBarBadgeCount() {
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else { return }
        
        let totalCount = self.topicList.reduce(0) { (total, topic: Topic) -> Int in
            if let privateMsgCount = topic.privateMsgCount {
                if let msgCount = privateMsgCount[uid] {
                    return total + msgCount
                } else {
                    return total
                }
            } else {
                return total
            }
        }
        
        //Update both count TopicVC & MineVC
        self.tabBarController?.viewControllers?.forEach({ vc in
            if let navBar = vc as? UINavigationController {
                if let topicVC = navBar.viewControllers.first as? TopicNewsViewController {
                    if totalCount > 0 {
                        topicVC.lblBadgeCount.isHidden = false
                        topicVC.lblBadgeCount.text = "\(totalCount)"
                    } else {
                        topicVC.lblBadgeCount.isHidden = true
                    }
                } else if let mineVC = navBar.viewControllers.first as? MineViewController {
                    if totalCount > 0 {
                        mineVC.lblBadgeCount.isHidden = false
                        mineVC.lblBadgeCount.text = "\(totalCount)"
                    } else {
                        mineVC.lblBadgeCount.isHidden = true
                    }
                }
            }
        })
            
        
        
        
        
        
    }
    
    func updateChatCount(_ topicId: Double,_ privateMsgCount: [String:Int],_ readCount: Int) {
        
        if let navVC = self.tabBarController?.viewControllers?[0] as? UINavigationController {
            if let topicVC = navVC.viewControllers.first as? TopicNewsViewController {
                //update read count on cache data of private chat count
                if let foundIndex = topicVC.privateChatTopicList.firstIndex(where: { (objTopic) -> Bool in
                    return objTopic.topic_id == topicId
                }) {
                    topicVC.privateChatTopicList[foundIndex].privateMsgCount = privateMsgCount
                }
                topicVC.setTabBarBadgeCount(topicVC.privateChatTopicList)
                if let badgeCountText  = topicVC.lblBadgeCount.text {
                    if var badgeCountInt = Int(badgeCountText) {
                        badgeCountInt = badgeCountInt - readCount
                        if badgeCountInt > 0 {
                            topicVC.lblBadgeCount.text = "\(badgeCountInt)"
                            topicVC.lblBadgeCount.isHidden = false
                        } else {
                            topicVC.lblBadgeCount.isHidden = true
                        }
                    }
                }
            }
        }
        
        //Update read count on tab badge
        if let badgeValue = self.navigationController?.tabBarItem.badgeValue {
            var badgeCount = Int(badgeValue)
            badgeCount = (badgeCount ?? 0) - readCount
            self.navigationController?.tabBarItem.badgeValue = badgeCount ?? 0 > 0 ? "\(badgeCount ?? 0)" : nil
            
        } else {
            self.navigationController?.tabBarItem.badgeValue = nil
        }
        
        //update read count on cache data
        if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
            return objTopic.topic_id == topicId
        }) {
            self.topicListCopy[foundIndex].privateMsgCount = privateMsgCount
        }
        //update read count on tableView Cell
        if let foundIndex = self.topicList.firstIndex(where: { (objTopic) -> Bool in
            return objTopic.topic_id == topicId
        }) {
            self.topicList[foundIndex].privateMsgCount = privateMsgCount
            let cell = self.topicTableView.cellForRow(at: IndexPath(row: foundIndex, section: 0))
            print(cell as Any)
        }
        
    }
    
    func updateTableMsgCount(_ topicId: Double) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        //update read count on cache data
        if let foundIndex = self.topicListCopy.firstIndex(where: { (objTopic) -> Bool in
            return objTopic.topic_id == topicId
        }) {
            let topic = self.topicListCopy[foundIndex]
            if topic.created_by == uid {
                if let msgCount = self.topicListCopy[foundIndex].privateMsgCount?[uid] {
                    self.topicListCopy[foundIndex].privateMsgCount?[uid] = msgCount + 1
                }
            } else {
                if let msgCount = self.topicListCopy[foundIndex].privateMsgCount?[topic.created_by!] {
                    self.topicListCopy[foundIndex].privateMsgCount?[topic.created_by!] = msgCount + 1
                }
            }
            
        }
        
        //update read count on tableView Cell
        if let foundIndex = self.topicList.firstIndex(where: { (objTopic) -> Bool in
            return objTopic.topic_id == topicId
        }) {
            let topic = self.topicList[foundIndex]
            if topic.created_by == uid {
                if let msgCount = self.topicList[foundIndex].privateMsgCount?[topic.created_by!] {
                    self.topicList[foundIndex].privateMsgCount?[topic.created_by!] = msgCount + 1
                }
            } else {
                if let msgCount = self.topicList[foundIndex].privateMsgCount?[uid] {
                    self.topicList[foundIndex].privateMsgCount?[uid] = msgCount + 1
                }
            }
//            let cell = self.topicTableView.cellForRow(at: IndexPath(row: foundIndex, section: 0)) as? TopCell
            self.topicTableView.reloadRows(at: [IndexPath(row: foundIndex, section: 0)], with: .none)
//            print(cell as Any)
        } else {
            self.topicListCopy = []
            self.getAllTopics()
        }
    }
    
    func filterTopicsByChatCount(_ topicList: [Topic]) {
        self.topicList = []
        
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else { return }
        
        if self.topicSegment.selectedSegmentIndex == 0 {
            self.topicList = topicList.filter({ (topic) -> Bool in
                return topic.created_by == uid
            })
        } else {
            self.topicList = topicList.filter({ (topic) -> Bool in
                return topic.created_by != uid
            })
        }
        
        self.topicList.sort(by: {
            guard let firstCount = $0.privateMsgCount?[uid] else {
                return false
            }
            
            guard let secondCount = $1.privateMsgCount?[uid] else {
                return false
            }
            return firstCount > secondCount
        })
        
        if self.topicList.count > 0 {
            self.topicTableView.isHidden = false
            self.messageView.isHidden = true
        } else {
            self.topicTableView.isHidden = true
            self.lblMessageTitle.text = Messages.Info.NoRecordInChatTopic
            self.messageView.isHidden = false
        }
        
        let playerVC = VideoPlayerController.sharedVideoPlayer
        playerVC.resetAndClearCache(tableView: self.topicTableView)
        self.topicTableView.reloadData()
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
    }
}
