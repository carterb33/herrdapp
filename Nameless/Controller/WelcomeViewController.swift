//
//  WelcomeViewController.swift
//  Herrd
//
//  Created by DG on 21/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtAccessCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtAccessCode.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        self.view.endEditing(true)
        guard let accessCode = txtAccessCode.text,  accessCode.count > 0  else {
            // Present error to user...
            self.displayError(message: Messages.Warning.AccessCodeBlankValidationMsg)
            txtAccessCode.becomeFirstResponder()
            return
        }
        
        if accessCode.count != 4 {
            self.displayError(message: Messages.Warning.AccessCodeLengthValidationMsg)
            txtAccessCode.becomeFirstResponder()
            return
        }
        
        if accessCode == "mods" {
            if let navigationCont = self.presentingViewController as? UINavigationController {
                let parent =  navigationCont.viewControllers.last as? ViewController
                self.dismiss(animated: false) {
                    parent?.showTermScreen()
                }
            }
        } else {
            self.displayError(message: Messages.Warning.AccessCodeValidationMsg)
            txtAccessCode.becomeFirstResponder()
        }
        
    }
    
    func displayError(message: String) {
        let alertViewController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
        self.present(alertViewController, animated: true, completion: nil)
    }

}
