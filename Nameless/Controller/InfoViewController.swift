//
//  InfoViewController.swift
//  Herrd
//
//  Created by Carter  Beaulieu on 8/22/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseUI
import GoogleSignIn
import MBProgressHUD

class InfoViewController: UIViewController {
    
    @IBOutlet weak var confirmPress: UIButton!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var btnAdminPortal: UIButton!
    @IBOutlet weak var nameChangeNew: UILabel!
    var authUI: FUIAuth!
    var handle: AuthStateDidChangeListenerHandle!
    var uid : String?
    
    override func viewDidLoad() {
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        authUI = FUIAuth.defaultAuthUI()
        nameField.isHidden = true
        confirmPress.isHidden = true
    
        guard let user = Auth.auth().currentUser else {
            return
        }
        
        // The user's ID, unique to the Firebase project.
        // Do NOT use this value to authenticate with your backend server,
        // if you have one. Use getTokenWithCompletion:completion: instead.
        nameChangeNew.text = user.displayName
        
        //Check admin user
        if let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String], adminlist.contains(user.uid) {
            self.btnAdminPortal.isHidden = false
        } else {
            self.btnAdminPortal.isHidden = true
        }
        
        uid = user.uid
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Add state change listener for detect user state
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil, let uid = self.uid {
                // Remove device id of user
                FirebaseUtility().addOrUpdateDeviceTokenWithUserId(userId: uid, deviceToken: "") { (isAdded, error) in
                    if let error = error {
                        print("Error getting documents: \(error)")
                    } else {
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)   // Remove state change listener
    }
    
    @IBAction func unblockAllPress(_ sender: UIButton) {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Unblocking Users..."
        
        FirebaseUtility().deleteBlockedUserIds { (isDeleted, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                
                let alertController = UIAlertController(title: "User Unblocked",
                                                        message: Messages.Info.AllUserUnlocked,
                                                        preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
                    print("Ok Action")
                    if let prevVC = self.navigationController?.viewControllers[ (self.navigationController?.viewControllers.count)! - 2] as? TopicNewsViewController {
                        prevVC.isEndOfDocs = false
                        prevVC.lastDocOfPage = nil
                        prevVC.topicListCopy = []
                        if prevVC.topicSegment.selectedSegmentIndex == 2 {
                            prevVC.lastDocOfPage = nil
                            prevVC.isEndOfDocs = false
                            prevVC.topicList = []
                            prevVC.lastDocOfPageSub = nil
                            prevVC.isEndOfDocsSub = false
                            prevVC.subTopicList = []
                        }
                        prevVC.getAllTopics()
                    }
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func logOutPress(_ sender: UIButton) {
        do {
            try authUI!.signOut()
            if let prewView = self.navigationController?.viewControllers.first as? ViewController {
                prewView.tableView.isHidden = true
                prewView.selectedPlaceId = ""
                let appdelegate = UIApplication.shared.delegate as? AppDelegate
                appdelegate?.isAdminUser = false
                prewView.venueList = []
            }
            
            self.navigationController?.popViewController(animated: true)
            print("^^ Successfully signed out")
        } catch {
            print("** ERROR: Couldn't sign out")
        }
    }
    
    @IBAction func privPolicyPress(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: APPURL.PrivacyUrl)! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func tosPress(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: APPURL.PolicyUrl)! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func adminPortalPress(_ sender: UIButton) {
        if let adminPortal = self.storyboard?.instantiateViewController(withIdentifier: "AdminPortalViewController") {
            self.navigationController?.pushViewController(adminPortal, animated: true)
        }
    }
    
    @IBAction func changeNamePress(_ sender: UIButton) {
        nameField.isHidden = false
        confirmPress.isHidden = false
        let user = Auth.auth().currentUser
        nameChangeNew.text = user?.displayName
    }
    
    @IBAction func confirmPress(_ sender: UIButton) {
        if (nameField.text?.last == " ") ||
            nameField.text!.isEmpty ||
            (nameField.text?.first == " ")
        {
            CommonUtility.showAlert(title: "Invalid", message: Messages.Warning.EntryValidationMsg, controller: self)
        } else {
            nameField.resignFirstResponder()

            let user = Auth.auth().currentUser
            if let user = user {
                // The user's ID, unique to the Firebase project.
                // Do NOT use this value to authenticate with your backend server,
                // if you have one. Use getTokenWithCompletion:completion: instead.
//                print(user.displayName)
                // ...
                confirmPress.isHidden = true
                nameField.isHidden = true
                self.nameChangeNew.text = user.displayName
            }
            
            let fullName = user?.displayName
            var components = fullName!.components(separatedBy: " ")
            if(components.count > 0)
            {
                let firstName = components.removeFirst()
                let lastName = components.removeLast()
                print(firstName)
                print(lastName)
                let nameChange = nameField.text! + " " + lastName
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = nameChange
                changeRequest?.commitChanges { (error) in
                    // ...
                    self.nameChangeNew.text = user?.displayName
                }
            }
        }
    }
    
}

