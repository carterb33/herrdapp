//
//  GuestListViewController.swift
//  Herrd
//
//  Created by Sheshnath on 19/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase

class GuestListViewController: UIViewController {

    @IBOutlet weak var guestTableView: UITableView!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    var guestList : [UserPlace] = [UserPlace]()
    var place : Venue?
    override func viewDidLoad() {
        super.viewDidLoad()
        guestTableView.backgroundColor = .lightGray
        self.lblSubTitle.text = place?.name
        getGuestListFromPlace()
    }
    
    func getGuestListFromPlace() {

        Firestore.firestore().collection("UserPlaces")
            .whereField("favPlaceId", isEqualTo: place?.placeId ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    querySnapshot!.documents.forEach {
                        do {
                            let documentValue = $0.data()
                            let places = try UserPlace(data: documentValue)
                            self.guestList.append(places)
                        } catch {
                            print(error)
                        }
                    }
                    self.guestTableView.reloadData()
                }
            })
        
    }
    
    @IBAction func btnDismissClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDismissClickedLeft(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDismissClickedRight(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}

//TABLEVIEW EXTENSION
extension GuestListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserPlaceCell")!
        let userPlace = guestList[indexPath.row]
        cell.textLabel?.text = userPlace.UserName
        return cell
    }
        
}
