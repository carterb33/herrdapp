//
//  MainTabBarController.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 05/05/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseUI

class MainTabBarController: UITabBarController {
    var lastTabSelection = 0
    let btnNewTopic = UIButton.init(type: .custom)
    let image = UIImage(named: "finalPlus")
    var adminList : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.barTintColor = .black
        btnNewTopic.setImage(image, for: .normal)
        btnNewTopic.addTarget(self, action: #selector(buttonTapped(sender:)), for: UIControl.Event.touchUpInside)
//        btnNewTopic.layer.cornerRadius = 25
//        btnNewTopic.layer.borderWidth = 5
//        btnNewTopic.layer.borderColor = UIColor.black.cgColor
        self.tabBar.insertSubview(btnNewTopic, aboveSubview: self.tabBar)
        self.checkAdminUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func opneNewTopicPopUp() {
        let newTopicVC = NewTopicViewController()
        newTopicVC.modalPresentationStyle = .custom
        newTopicVC.transitioningDelegate = self
        newTopicVC.adminList = self.adminList
        //        self.navigationController?.pushViewController(newTopicVC, animated: true)
        guard let lastNav = self.selectedViewController as? UINavigationController else {
            return
        }
        
        guard let lastVC = lastNav.viewControllers.last else {
            return
        }
        lastVC.present(newTopicVC, animated: true, completion: {})
    }
    
    func checkAdminUser() {
        if let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String] {
            self.adminList = adminlist
        } else {
            FirebaseUtility().getAppSettings { (error, doc) in
                if let err = error {
                    print("Got error while reading app settings",err)
                }
                //Call Again to get admin list
                self.checkAdminUser()
            }
        }
    }
    
    
    @objc func buttonTapped(sender: UIButton) {
        opneNewTopicPopUp()
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // safe place to set the frame of button manually
        btnNewTopic.frame = CGRect.init(x: self.tabBar.center.x - 30 , y: 0, width: 58, height: 62)
        self.tabBar.bringSubviewToFront(btnNewTopic)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func btnNewTopicHideShow() {
        guard let lastNav = self.selectedViewController as? UINavigationController else {
            return
        }
        
        guard let lastVC = lastNav.viewControllers.last else {
            return
        }
        
        if lastVC.isKind(of:TopicNewsViewController.self) ||
            lastVC.isKind(of: MineViewController.self)  {
            btnNewTopic.isHidden = false
        } else {
            btnNewTopic.isHidden = true
        }
    }
}

extension MainTabBarController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

