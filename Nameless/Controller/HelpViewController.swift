//
//  HelpViewController.swift
//  Herrd
//
//  Created by DG on 21/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    @IBOutlet weak var tblHelp: UITableView!
    @IBOutlet weak var btnGetStarted: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func btnGetStartedClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            UserDefaults.standard.set(true, forKey: "isAccessCodeVerified")
        }
    }
    
}

//TABLEVIEW EXTENSION
extension HelpViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StaticData.helpArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpCell") as! HelpCell
        let helpDetails = StaticData.helpArray[indexPath.row]
        cell.lblDetailText.text = helpDetails["detailText"]
        cell.helpicon.image = UIImage.init(named: helpDetails["icon"] ?? "")
        return cell
    }
    
}

