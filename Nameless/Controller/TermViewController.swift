//
//  TermViewController.swift
//  Herrd
//
//  Created by Sheshnath on 23/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit

class TermViewController: UIViewController {

    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var txtTerms: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func termsLink(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: APPURL.PolicyUrl)! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func privacyLink(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: APPURL.PrivacyUrl)! as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func btnAgreeClick(_ sender: Any) {
        if let navigationCont = self.presentingViewController as? UINavigationController {
            let parent =  navigationCont.viewControllers.last
            
            self.dismiss(animated: false) {
                if let viewcontroller = parent as? ViewController {
                    viewcontroller.showHelpScreen()
                } else if let topicVC = parent as? TopicNewsViewController {
                    topicVC.showHelpScreen()
                }
            }
        }
    }

}
