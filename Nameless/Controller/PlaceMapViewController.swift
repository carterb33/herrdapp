//
//  PlaceMapViewController.swift
//  Herrd
//
//  Created by DG on 10/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PlaceMapViewController: UIViewController {

    var place : Venue?
    var locationManager: CLLocationManager?
    let newPin = MKPointAnnotation()
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager?.requestWhenInUseAuthorization()
        
        self.title = place?.name
        self.navigationItem.setTitle(place?.name ?? "", subtitle:"")
        
        let center = CLLocationCoordinate2D(latitude: place?.latitude ?? 0.0 , longitude:place?.longitude ?? 0.0)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        //set region on the map
        mapView.setRegion(region, animated: true)
        
        newPin.coordinate = center
        mapView.addAnnotation(newPin)
        mapView.showsUserLocation = true
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if let userLocation = self.mapView.userLocation.location?.coordinate {
//
//                let region = MKCoordinateRegion(
//                    center: userLocation, latitudinalMeters: 5000, longitudinalMeters: 5000)
//
//                self.mapView.setRegion(region, animated: true)
//            }
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setDirectionRequest()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = .black
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func setDirectionRequest() {
        
        if let userLocation = self.mapView.userLocation.location?.coordinate {
            
            let request = MKDirections.Request()
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: userLocation.latitude, longitude: userLocation.longitude), addressDictionary: nil))
            request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: place?.latitude ?? 0.0, longitude: place?.longitude ?? 0.0), addressDictionary: nil))
            request.requestsAlternateRoutes = true
            request.transportType = .automobile
            
            let directions = MKDirections(request: request)
            directions.calculate { [unowned self] response, error in
                guard let unwrappedResponse = response else { return }
                let route = unwrappedResponse.routes[0] as MKRoute
                let distance = route.distance/1609.344 //Converting from meter to miles
                let etaTime = route.expectedTravelTime.stringFromTimeInterval()
                
                self.navigationItem.setTitle(self.place?.name ?? "", subtitle:"\(String(format:"%.2f",distance)) Miles | ETA : \(etaTime)")
                for route in unwrappedResponse.routes {
                    self.mapView.addOverlay(route.polyline)
                    self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
                }
            }
        }
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let minutes = (interval / 60) % 60
        return String(format: "%02d", minutes)
    }

}

extension PlaceMapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.gray
        renderer.lineWidth = 3
        return renderer
    }
}
