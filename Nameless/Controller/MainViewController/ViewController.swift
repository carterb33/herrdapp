//
//  ViewController.swift
//  Herrd
//
//  Created by Carter  Beaulieu on 7/6/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseUI
import GooglePlaces
import GoogleSignIn
import MBProgressHUD
import CoreLocation
import MapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var anonymousSwitch: UISwitch!
    @IBOutlet weak var overallButtonPressed: UIButton!
    @IBOutlet weak var lblCityTitle: UILabel!
    
    @IBOutlet weak var venueName: UIButton!
    @IBOutlet weak var plusVenueButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.attributedTitle = NSAttributedString(string: Messages.Actions.PullToRefresh)
        control.backgroundColor = UIColor.black
        control.tintColor = UIColor.white
        control.addTarget(self, action: #selector(refreshAndSortListData(sender:)), for: .valueChanged)
        return control
    }()
    
    var isNewPlaceAdded = false
    
    //Filter array for search data
    var filteredVenueList : [Venue] = []
    var userChatCountList : [[String:Any]] = []
    var venueList : [Venue] = [Venue]()
    var authUI: FUIAuth!
    var selectedPlaceId : String = ""
    
    //For current location
    var locationManager: CLLocationManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCityTitle.textColor = AppColor.CommonColor.postColor
        lblCityTitle.text = "Where to?"

        //Basic UI setup
        self.basicUISetUp()
        
        //get location
        self.getCurrentLocation()
        
        authUI = FUIAuth.defaultAuthUI()
        authUI?.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        
        if authUI.auth?.currentUser != nil {
            self.checkAdminUser()
        }
        
        addPullToRefresh()
        
        checkOldDataAndRemove()
        
    }
    
    func dateDayDifference(firstDate: Date, secondDate: Date) -> Int? {
        let calendar = Calendar.current

        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: firstDate)
        let date2 = calendar.startOfDay(for: secondDate)

        let components = calendar.dateComponents([.day], from: date1, to: date2)
        return components.day
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        signIn()
    }
    
    func basicUISetUp(){
        plusVenueButton.layer.cornerRadius = 10
        plusVenueButton.backgroundColor = AppColor.CommonColor.cellColor
        plusVenueButton.titleLabel?.textColor = AppColor.CommonColor.postColor
        
        searchBar.barTintColor = UIColor.clear
        searchBar.backgroundColor = UIColor.clear
        searchBar.isTranslucent = true
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        tableView.rowHeight = 135
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: -21, width: 230, height: 80))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: -21, width: 230, height: 80))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "fullLogo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
        
    }
    
    func getCurrentLocation(){
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func checkOldDataAndRemove() {
        
        FirebaseUtility().getAppSettings { (error, doc) in
            if let err = error {
                print("Got error while reading app settings",err)
            } else {
                guard let document = doc else {
                    self.getFavPlace()
                    return
                }
                if let lastVenueDeletedDate = document["lastVenueDeletedDate"] as? Timestamp{
                    var calendar = Calendar.current
                    calendar.timeZone = (NSTimeZone(abbreviation: "UTC") as TimeZone?)!
                    let order = calendar.compare(Date(), to: lastVenueDeletedDate.dateValue(), toGranularity: .day)
                    
                    let hour = calendar.component(.hour, from: Date())
                    if order == .orderedDescending && hour >= Constants.utcPlusHours  {
                        //Remove all the venues
                        self.removeOldVenues()
                    } else {
                        //No venue available for delete
                        self.getFavPlace()
                    }
                } else {
                    //No Dates stored so its first call for delete all the venues
                    self.removeOldVenues()
                }
            }
        }
    }
    
    func removeOldVenues() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        //create batch object to delete all the data
        let batch = Firestore.firestore().batch()
        //Get All Venues
        Firestore.firestore().collection("VenueList")
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    guard let querySnaps = querySnapshot else {
                        self.getFavPlace()
                        return
                    }
                    querySnaps.documents.forEach {
                        batch.updateData(["amount": 0], forDocument: $0.reference)
                    }
                    
                    batch.commit { (error) in
                        if let err = error {
                            print("update all old venues batch failed",err)
                        }
                        print("update all old venue batch success")
                        //Update App setting for future delete
                        FirebaseUtility().updateAppSettings(data: ["lastVenueDeletedDate" : Date()]) { error in
                            if let err = error {
                                print("Date not update in app settings.",err)
                            }
                            UserDefaults.standard.setValue(Date(), forKey: "lastVenueDeletedDate")
                        }
                        //Old venue deleted now lets start fresh flow of fetch venues.
                        self.removeAllFavPlace()
                    }
                }
        })
    }
    
    func removeAllFavPlace() {
        //create batch object to delete all the data
        let batch = Firestore.firestore().batch()
        Firestore.firestore().collection("UserPlaces")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    guard let querySnap = querySnapshot else {
                        self.getFavPlace()
                        return
                    }
                    querySnap.documents.forEach {
                        batch.deleteDocument($0.reference)
                    }
                    
                    batch.commit { (error) in
                        if let err = error {
                            print("delete all old venues likes batch failed",err)
                        }
                        print("delete all old venue likes batch success")
                    }
                    self.getFavPlace()
                }
            })
    }

    func getFavPlace() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection("UserPlaces")
            .whereField("UID", isEqualTo: uid ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first?.data(){
                        self.selectedPlaceId = data["favPlaceId"] as! String
                    }else{
                        print("No Fav Venue Data")
                    }
                    self.getAllVenuesInSingleCall()
                }
            })
    }
    
    func checkAdminUser() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection(FirebaseConst.Collections.Admin)
            .whereField("UID", isEqualTo: uid ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if (querySnapshot!.documents.first?.data()) != nil{
                        let appdelegate = UIApplication.shared.delegate as? AppDelegate
                        appdelegate?.isAdminUser = true
                    }
                }
        })
    }


    func getAllVenuesInSingleCall() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        Firestore.firestore().collection("VenueList")
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.venueList = []
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    querySnapshot!.documents.forEach {
                        do {
                            let documentValue = $0.data()
                            let venue = try Venue(data: documentValue)
                            self.venueList.append(venue)
                        } catch {
                            print(error)
                        }
                    }
                    self.venueList.sort(by: {
                        $0.amount! > $1.amount!
                    })
//                    self.getUserChatCounts()
                    self.tableView.reloadData()
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                    if self.isNewPlaceAdded {
                        self.scrollTableViewToBottom()
                    }
                }
            })
    }
    
    func checkDistanceAndAddToVenueList(venues:[Venue]) {
        
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        self.venueList = []
        self.tableView.reloadData()
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        for i in 0..<venues.count {
            let venue = venues[i]
            let request = MKDirections.Request()
            request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: venue.latitude!, longitude: venue.longitude!), addressDictionary: nil))
            request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: appdelegate?.currentLocation?.coordinate.latitude ?? 0.0, longitude: appdelegate?.currentLocation?.coordinate.longitude ?? 0.0), addressDictionary: nil))
            request.requestsAlternateRoutes = true
            request.transportType = .automobile
            
            let directions = MKDirections(request: request)
            directions.calculate { [unowned self]  response, error in
                guard let unwrappedResponse = response else {
                    return
                }
                let route = unwrappedResponse.routes[0] as MKRoute
                let distance = route.distance/1609.344 //Converting from meter to miles
                if distance <= 25 {
                    self.tableView.beginUpdates()
                    self.venueList.append(venue)
                    let indexPath = IndexPath.init(row: self.venueList.count - 1, section: 0)
                    self.tableView.insertRows(at: [indexPath], with: .automatic)
                    self.tableView.endUpdates()
                }
            }
        }
    }
    
    func addNewAddedVenueInVenuList(newPlaceId : String) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        Firestore.firestore().collection("VenueList")
        .whereField("placeId", isEqualTo: newPlaceId )
        .getDocuments(completion: { (querySnapshot, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if let data = querySnapshot!.documents.first?.data(){
                    print(data)
                    do {
                        let venue = try Venue(data: data)
                        
                        let request = MKDirections.Request()
                        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: venue.latitude!, longitude: venue.longitude!), addressDictionary: nil))
                        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: appdelegate?.currentLocation?.coordinate.latitude ?? 0.0, longitude: appdelegate?.currentLocation?.coordinate.longitude ?? 0.0), addressDictionary: nil))
                        request.requestsAlternateRoutes = true
                        request.transportType = .automobile
                        
                        let directions = MKDirections(request: request)
                        directions.calculate { [unowned self]  response, error in
                            guard let unwrappedResponse = response else {
                                return
                            }
                            let route = unwrappedResponse.routes[0] as MKRoute
                            let distance = route.distance/1609.344 //Converting from meter to miles
                            if distance <= 25 {
                                self.tableView.beginUpdates()
                                self.venueList.insert(venue, at: 0)
                                let indexPath = IndexPath.init(row: 0, section: 0)
                                self.tableView.insertRows(at: [indexPath], with: .automatic)
                                self.tableView.endUpdates()
                            }
                        }
                    } catch {
                        print(error)
                    }
                }
            }
        })
    }
    
    func getUserChatCounts(){
        //fetch user chat count list
        FirebaseUtility().getUserChatReadCount { (userChatList, error) in
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                self.userChatCountList = userChatList ?? []
                
                self.tableView.reloadData()
                if self.refreshControl.isRefreshing {
                    self.refreshControl.endRefreshing()
                }
                if self.isNewPlaceAdded {
                    self.scrollTableViewToBottom()
                }
            }
        }
    }
    
    func updatePlaceCount(_ selectedVenue : Venue) {
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: selectedVenue.placeId ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "amount": selectedVenue.amount ?? 0
                    ])
                }
        }
    }
    
    func removePlaceFromUserFav(_ selectedPlaceId : String) {
        
        Firestore.firestore().collection("UserPlaces")
            .whereField("favPlaceId", isEqualTo: selectedPlaceId )
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else {
                    querySnapshot!.documents.forEach {
                        $0.reference.updateData([
                            "favPlaceId": ""
                        ])
                    }
                }
        }
    }
    
    func updatePlaceWithChannelId(_ channelId : String, selectedVenue : Venue) {
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: selectedVenue.placeId ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "channelId": channelId
                    ])
                }
        }
    }
    
    func deleteFavPlace() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection("UserPlaces").document(uid!).delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                print("Document successfully removed!")
            }
        }
    }
    
    func addPullToRefresh() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        if selectedPlaceId.count > 0 {
            if sender.isOn {
                //Send Anonymous name
                addUpdateFavoritePlace(selectedPlaceId)
                UserDefaults.standard.set(true, forKey: "AnonymousMode")
                UserDefaults.standard.synchronize()
            } else {
                //Send Actual Name
                addUpdateFavoritePlace(selectedPlaceId)
                UserDefaults.standard.set(false, forKey: "AnonymousMode")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    @objc func refreshAndSortListData(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.getAllVenuesInSingleCall()
    }
    
    //SIGN IN FUNCTION
    func signIn() {
        let providers: [FUIAuthProvider] = [
            FUIGoogleAuth(authUI: authUI)
        ]
        if authUI.auth?.currentUser == nil {
            self.authUI?.providers = providers
            present(authUI.authViewController(), animated: true, completion: nil)
        } else {
            tableView.isHidden = false
        }
    }
    
    //LOGGING OUT A USER
    @IBAction func logOut(_ sender: UIBarButtonItem) {
        do {
            try authUI!.signOut()
            print("^^ Successfully signed out")
            tableView.isHidden = true
            self.selectedPlaceId = ""
            let appdelegate = UIApplication.shared.delegate as? AppDelegate
            appdelegate?.isAdminUser = false
            self.venueList = []
            signIn()
        } catch {
            tableView.isHidden = true
            print("** ERROR: Couldn't sign out")
        }
    }
    
    //INFO BUTTON FOR ANONYMOUS MODE
    @IBAction func infoButton(_ sender: UIButton) {
        CommonUtility.showAlert(title: "Anonymous Mode",
                                message: Messages.Info.AnonymousMode,
                                controller: self)
    }
    
    //ADDING A NEW VENUE LOCATION WITH GOOGLE PLACES API
    @IBAction func addVenuePressed(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.secondaryTextColor = UIColor (white: 10, alpha: 0.8)
        autocompleteController.primaryTextColor = .white;
        autocompleteController.primaryTextHighlightColor = .gray;
        autocompleteController.tableCellBackgroundColor = .black;
        autocompleteController.tableCellSeparatorColor = .lightGray;
        autocompleteController.tintColor = .lightGray;
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
}

extension ViewController : UISearchBarDelegate {
    
    // This method updates filteredVenueList based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        // When there is no text, filteredVenueList is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        self.filteredVenueList = (searchBar.text?.isEmpty)! ? venueList : venueList.filter({(aObject: Venue) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return (aObject.name?.lowercased().range(of: searchText.lowercased()) != nil)
        })
        tableView.reloadData()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
    }
    
}

extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        appdelegate?.currentLocation = locations.last! as CLLocation
        self.locationManager.stopUpdatingLocation()
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(appdelegate!.currentLocation!) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if let placemark = placemarks {
                if placemark.count>0{
                    let placemark = placemarks![0]
                    if self.lblCityTitle.text != placemark.locality {
                        print(placemark.locality!)
                        print(placemark.administrativeArea!)
                        print(placemark.country!)
                        appdelegate?.currentCity = "\(placemark.locality!)"
//                        self.lblCityTitle.text = "\(placemark.locality!)"
                        self.lblCityTitle.text = "Where to?"

                    }
                }
            }
        }
    }
}
