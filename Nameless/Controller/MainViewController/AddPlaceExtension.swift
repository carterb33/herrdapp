//
//  AddPlaceExtension.swift
//  Herrd
//
//  Created by Sheshnath on 25/09/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import GooglePlaces
import MBProgressHUD
import Firebase
import FirebaseAuth

//GOOGLE PLACES EXTENSION
extension ViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Adding New Place..."
        
        
        let newPlace : [String : Any] = ["name": place.name ?? "",
                                         "address": place.formattedAddress ?? "",
                                         "hours": "Not available",
                                         "amount": 0,
                                         "placeId": place.placeID ?? "",
                                         "coordinate": place.coordinate,
                                         "openingHours": place.openingHours ?? ""]
        addNewPlaceInFireStore(newPlace)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func addNewPlaceInFireStore(_ newPlace: [String:Any]) {
        
        let user = Auth.auth().currentUser
        
        let uid = user?.uid
        let userName = user?.displayName
        let currentBackgroundDate = NSDate()
        
        let cordinate = newPlace["coordinate"] as? CLLocationCoordinate2D
        let latitude = cordinate?.latitude
        let longitude = cordinate?.longitude
        
        let openingHrs = newPlace["openingHours"] as? GMSOpeningHours
        let openingTimeArray = openingHrs?.weekdayText
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: newPlace["placeId"] as! String )
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first?.data(){
                        let placename = data["name"] as! String
                        let alert = UIAlertController(title: "Alert", message: "\(placename) is already added in Venue list. Please use search and find this place from the list via search.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        print("Add New venue")
                        Firestore.firestore().collection("VenueList").document("\(currentBackgroundDate)").setData([
                            
                            "UID": uid!,
                            "UserName":userName ?? "",
                            "name" : newPlace["name"] as? String ?? "",
                            "address" : newPlace["address"] as? String ?? "",
                            "hours" : newPlace["hours"] as? String ?? "",
                            "amount": 0,
                            "placeId" : newPlace["placeId"] as! String,
                            "latitude" : latitude as Any,
                            "longitude" : longitude as Any,
                            "openingHours": openingTimeArray ?? [],
                            "channelId": "",
                            "msgcount": 0,
                        ]) { (err) in
//                            self.addNewAddedVenueInVenuList(newPlaceId: newPlace["placeId"] as! String)
                             self.getAllVenuesInSingleCall()
                            self.isNewPlaceAdded = true
                        }
                    }
                }
            })
    }
    
    func addOrUpdateChatCount(_ channelId: String, selectedPlace: Venue) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let uniqueId = uid! + "~" + channelId
        
        let currentBackgroundDate = NSDate()
        
        Firestore.firestore().collection("UserChatReadCount")
        .whereField("uniqueId", isEqualTo: uniqueId )
        .getDocuments(completion: { (querySnapshot, err) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if (querySnapshot!.documents.first?.data()) != nil{
                    print("User Chat Count already there")
                }else{
                Firestore.firestore().collection("UserChatReadCount").document("\(currentBackgroundDate)").setData([
                        "uniqueId": uniqueId,
                        "chatCount": selectedPlace.msgcount
                    ]) { (err) in
                    }
                }
            }
        })
    }
    
    func scrollTableViewToBottom(){
        DispatchQueue.main.async {
            if (self.searchBar.text?.isEmpty)! {
                let indexPath = IndexPath(row: self.venueList.count-1, section: 0)
                print(indexPath)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }else {
                let indexPath = IndexPath(row: self.filteredVenueList.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
            self.isNewPlaceAdded = false
        }
    }
    
}
