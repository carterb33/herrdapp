//
//  TopicFUIAuthExtension.swift
//  Herrd
//
//  Created by DG on 25/09/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseUI
import GoogleSignIn

//AUTHENTICATION EXTENSION

extension TopicNewsViewController: FUIAuthDelegate {
    
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        // other URL handling goes here.
        return false
    }
    
    
    func showRestrictionMessage() {
        let controller = UIAlertController(title: "Attention",
                                           message: Messages.Warning.ValidMailSignIn,
                                           preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.signIn()
        }))
        present(controller, animated: true)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        // handle user and error as necessary
        if let user = user {
            let approvedDomains = ["bc.edu"]
            if let userDomain = user.email?.components(separatedBy: "@").last {
                if approvedDomains.contains(userDomain) == false &&
                    !StaticData.RequiredEmails.contains(user.email ?? "")
                {
                    try? Auth.auth().signOut()
                    showRestrictionMessage()
                    return
                }
            }
            
            // Check device token exist
            if let deviceToken = UserDefaults.standard.object(forKey: "deviceId") as? String
            {
                // Add or update device id on firebase
                FirebaseUtility().addOrUpdateDeviceTokenWithUserId(userId: user.uid, deviceToken: deviceToken) { (isAdded, error) in
                    if let error = error {
                        print("Error getting documents: \(error)")
                    } else {
                    }
                }
            }
            //Check nick name and assign
            self.checkNickNameAndAssign()
            
            if UserDefaults.standard.bool(forKey: "isAccessCodeVerified") {
                topicTableView.isHidden = false
                print("*** we signed in with the user \(user.email ?? "unknown email") ")
                self.checkAdminUser()
            } else {
                let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "TermViewController") as! TermViewController
                welcomeVC.modalPresentationStyle = .overCurrentContext
                self.present(welcomeVC, animated: true, completion: {})
            }
        } else {
            self.signIn()
        }
    }
    
    func showTermScreen() {
        
        let termVC = self.storyboard?.instantiateViewController(withIdentifier: "TermViewController") as! TermViewController
        termVC.modalPresentationStyle = .overCurrentContext
        self.present(termVC, animated: true, completion: {
            
        })
    }
    
    func showHelpScreen() {
        self.topicTableView.isHidden = false
        self.setupforAds()
        self.getAllPrivateChatTopics()
        self.checkAdminUser()
    }
    
    func authPickerViewController(forAuthUI authUI: FUIAuth) -> FUIAuthPickerViewController {
        
        let loginViewController = FUIAuthPickerViewController(authUI: authUI)
        
        //loginViewController.view.backgroundColor = UIColor.black
        
        let marginInsets: CGFloat = 16;         let imageHeight: CGFloat = 225
        let imageY = self.view.center.y - imageHeight
        let logoFrame = CGRect(x: self.view.frame.origin.x + marginInsets, y: imageY, width: self.view.frame.width - (marginInsets*2), height: imageHeight)
        let logoImageView = UIImageView(frame: logoFrame)
        logoImageView.image = UIImage(named: "loginLogo")
//        logoImageView.backgroundColor = UIColor.black
        logoImageView.layer.cornerRadius = 15.0
        logoImageView.contentMode = .scaleAspectFit
        
//        let logoBackground = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//        logoBackground.backgroundColor = .black
        
       
        //fix this once Firebase UI releases the dark mode support for sign-in
        
        loginViewController.view.subviews[0].backgroundColor = .black
        loginViewController.view.subviews[0].subviews[0].backgroundColor = .black
        
        loginViewController.view.addSubview(logoImageView)
    
        return loginViewController
    }
    
}
