//
//  TableViewExtension.swift
//  Herrd
//
//  Created by Sheshnath on 25/09/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import MBProgressHUD

//TABLEVIEW EXTENSION
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.searchBar.text?.isEmpty)! {
            return venueList.count
        }else{
            return filteredVenueList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MainScreenCell
        
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[indexPath.row]
        } else {
            venue = self.filteredVenueList[indexPath.row]
        }
        
        cell.venueName?.attributedText = NSAttributedString(string: venue?.name ?? "",
                                                            attributes:[.underlineStyle: NSUnderlineStyle.single.rawValue])
        let amount = venue?.amount
        cell.peopleAmount?.text = "\(amount ?? 0)"
        cell.goingButton.tag = indexPath.row
        cell.bubbleButton.tag = indexPath.row
        cell.placeMap.tag = indexPath.row
//        cell.placeMap.setTitleColor(AppColor.CommonColor.postColor, for: .normal)
        cell.btnGuestList.tag = indexPath.row
//        cell.btnGuestList.titleLabel?.textColor = .systemPink

//        let gradiantImage = UIImage(named: "gradientRec6")!

        if selectedPlaceId == venue?.placeId {
            cell.goingButton.isSelected = true
            cell.goingButton.backgroundColor = UIColor(red: 255/255, green: 157/255, blue: 120/255, alpha: 1.0)
        } else {
            cell.goingButton.isSelected = false
            cell.goingButton.backgroundColor = .lightGray
        }
        cell.goingButton.addTarget(self, action: #selector(goingButtonPressed(_:)), for: .touchUpInside)
        cell.bubbleButton.addTarget(self, action: #selector(chatIconPressed(_:)), for: .touchUpInside)
        cell.placeMap.addTarget(self, action: #selector(btnPLaceMapPresses(_:)), for: .touchUpInside)
        cell.btnGuestList.addTarget(self, action: #selector(btnGuestListPressed (_:)), for: .touchUpInside)
//        cell.guestListText.addTarget(self, action: #selector(btnGuestListPressed (_:)), for: .touchUpInside)
        
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let uniqueId = uid! + "~" + venue!.channelId!

        let foundItems = self.userChatCountList.filter { ($0["uniqueId"] as! String).contains(uniqueId) }
        let msgCount = venue!.msgcount
        if foundItems.count > 0 {
            let userChatData = foundItems[0]
            let userChatCount = userChatData["chatCount"] as! Int64
            if  userChatCount < msgCount {
                cell.bubbleButton.setImage(UIImage.init(named: "greyUnread"), for: .normal)
            } else {
                cell.bubbleButton.setImage(UIImage.init(named: "greyChat"), for: .normal)
            }
        } else {
            if msgCount > 0 {
                cell.bubbleButton.setImage(UIImage.init(named: "greyUnread"), for: .normal)
            } else {
                cell.bubbleButton.setImage(UIImage.init(named: "greyChat"), for: .normal)
            }
        }
        
        return cell
    }
    
    func getAttchedChannel(channelId : String){
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        return appdelegate!.isAdminUser
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        if appdelegate!.isAdminUser {
            if editingStyle == .delete {
                //Delete Action on firebse
                var venue : Venue?
                if (self.searchBar.text?.isEmpty)! {
                    venue = self.venueList[indexPath.row]
                } else {
                    venue = self.filteredVenueList[indexPath.row]
                }
                let placeId = venue?.placeId
                let channelId = venue?.channelId
                let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Removing Venue..."
                
                Firestore.firestore().collection("VenueList")
                    .whereField("placeId", isEqualTo: placeId ?? "")
                    .getDocuments(completion: { (querySnapshot, err) in
                        
                        if let err = err {
                            print("Error getting documents: \(err)")
                        } else {
                            if let data = querySnapshot!.documents.first{
                                
                                loadingNotification.label.text = "Venue Removed"
                                if self.selectedPlaceId == placeId {
                                    self.selectedPlaceId = ""
                                }
                                self.removePlaceFromUserFav(placeId!)
                                Firestore.firestore().collection("VenueList").document(data.documentID).delete() { err in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    if let err = err {
                                        print("Error removing document: \(err)")
                                    } else {
                                        print("Place Document successfully removed!")
                                        
                                        tableView.beginUpdates()
                                        self.venueList.remove(at:indexPath.row)
                                        tableView.deleteRows(at: [indexPath], with: .automatic)
                                        tableView.endUpdates()
                                        
                                        //Delete Chat & Place
                                        if channelId?.count ?? 0 > 0 {
                                            Firestore.firestore().collection("VenueChannels").document(channelId ?? "").delete() { err in
                                                if let err = err {
                                                    print("Error removing document: \(err)")
                                                } else {
                                                    print("Channel Document successfully removed!")
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                print("No Venue to delete Data")
                            }
                        }
                    })
            }
        }
    }
    
    @IBAction func chatIconPressed(_ sender: UIButton) {
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        } else {
            venue = self.filteredVenueList[sender.tag]
        }
        getOrCreateChannel(place: venue!, index: sender.tag)
    }
    
    @IBAction func btnGuestListPressed(_ sender: UIButton) {
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        } else {
            venue = self.filteredVenueList[sender.tag]
        }
        let guestList = self.storyboard?.instantiateViewController(withIdentifier: "GuestListViewController") as! GuestListViewController
        guestList.modalPresentationStyle = .overCurrentContext
        guestList.place = venue
        self.present(guestList, animated: true, completion: nil)
    }
    
    @IBAction func btnPLaceMapPresses(_ sender: UIButton) {
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        } else {
            venue = self.filteredVenueList[sender.tag]
        }
        let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "PlaceMapViewController") as! PlaceMapViewController
        mapVC.place = venue
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    func addChannelInUserChat(place : Venue, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        Firestore.firestore().collection("UserChatReadCount")
            .whereField("uid", isEqualTo: currentUser.uid)
            .getDocuments(completion: { (querySnapshot, err) in
                
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard Channel(document: data) != nil else {
                            return
                        }
                    }else{
                        print("Add New Channel")
                        let channel = Channel(name: place.name!,channelRef: "")
                        Firestore.firestore().collection("VenueChannels").addDocument(data: channel.representation){ error in
                            if let e = error {
                                print("Error saving venue channel: \(e.localizedDescription)")
                            }
                        }
                    }
                }
            })
    }
    
    func getOrCreateChannel(place : Venue, index:Int) {
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        Firestore.firestore().collection("VenueChannels")
            .whereField("name", isEqualTo: place.name!)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first{
                        guard let channel = Channel(document: data) else {
                            return
                        }
                        if (self.searchBar.text?.isEmpty)! {
                            self.venueList[index].channelId = channel.id
                        } else {
                            self.filteredVenueList[index].channelId = channel.id
                        }
                        self.updatePlaceWithChannelId(channel.id!, selectedVenue: place)
                        //Add in User Chat Count
                        self.addOrUpdateChatCount(channel.id!, selectedPlace: place)
                        let vc = ChatViewController(user: currentUser, channel: channel, anonymousMode: self.anonymousSwitch.isOn)
                        vc.place = place
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        print("Create New Channel Data")
                        let channel = Channel(name: place.name!, channelRef: "")
                        Firestore.firestore().collection("VenueChannels").addDocument(data: channel.representation){ error in
                            if let e = error {
                                print("Error saving venue channel: \(e.localizedDescription)")
                            }
                        }
                    }
                }
            })
    }
    
    //PRESSING THE BUTTON
    @IBAction func goingButtonPressed(_ sender: UIButton) {
        
        var venue : Venue?
        if (self.searchBar.text?.isEmpty)! {
            venue = self.venueList[sender.tag]
        }else{
            venue = self.filteredVenueList[sender.tag]
        }
        
        let id = venue?.placeId
        
        if selectedPlaceId.count > 0 {
            if selectedPlaceId == id {
                deleteFavPlace()
                if (self.searchBar.text?.isEmpty)! {
                    var amount = venueList[sender.tag].amount ?? 0
                    if amount > 0 {
                        amount -= 1
                    }
                    venueList[sender.tag].amount = amount
                    updatePlaceCount(venueList[sender.tag])
                } else {
                    var amount = filteredVenueList[sender.tag].amount ?? 0
                    if amount > 0 {
                        amount -= 1
                    }
                    filteredVenueList[sender.tag].amount = amount
                    updatePlaceCount(filteredVenueList[sender.tag])
                }
                selectedPlaceId = ""
            } else {
                //Update Old One
                if (self.searchBar.text?.isEmpty)! {
                    if let prevIndex = venueList.firstIndex(where: {$0.placeId == selectedPlaceId}) {
                        var amount = venueList[prevIndex].amount ?? 0
                        if amount > 0 {
                            amount -= 1
                        }
                        venueList[prevIndex].amount = amount
                        updatePlaceCount(venueList[prevIndex])
                    }
                } else {
                    if let prevIndex = filteredVenueList.firstIndex(where: {$0.placeId == selectedPlaceId}) {
                        var amount = filteredVenueList[prevIndex].amount ?? 0
                        if amount > 0 {
                            amount -= 1
                        }
                        filteredVenueList[prevIndex].amount = amount
                        updatePlaceCount(filteredVenueList[prevIndex])
                    }
                }
                
                //Add New One
                selectedPlaceId = id ?? ""
                if (self.searchBar.text?.isEmpty)! {
                    var amount = venueList[sender.tag].amount ?? 0
                    amount += 1
                    venueList[sender.tag].amount = amount
                    updatePlaceCount(venueList[sender.tag])
                } else {
                    var amount = filteredVenueList[sender.tag].amount ?? 0
                    amount += 1
                    filteredVenueList[sender.tag].amount = amount
                    updatePlaceCount(filteredVenueList[sender.tag])
                }
                addUpdateFavoritePlace(id!)
            }
        } else {
            selectedPlaceId = id ?? ""
            if (self.searchBar.text?.isEmpty)! {
                var amount = venueList[sender.tag].amount ?? 0
                amount += 1
                venueList[sender.tag].amount = amount
                updatePlaceCount(venueList[sender.tag])
            } else {
                var amount = filteredVenueList[sender.tag].amount ?? 0
                amount += 1
                filteredVenueList[sender.tag].amount = amount
                updatePlaceCount(filteredVenueList[sender.tag])
            }
            addUpdateFavoritePlace(id!)
        }
        
        tableView.reloadData()
    }
    
    func addUpdateFavoritePlace(_ placeId: String) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        var userName = ""
        
        if self.anonymousSwitch.isOn {
            userName = "Anonymous User"
        } else {
            userName = user?.displayName ?? ""
        }
        
        Firestore.firestore().collection("UserPlaces").document("\(uid ?? "")").setData([
            "UID": uid!,
            "UserName":userName,
            "favPlaceId" : placeId,
        ]) { (err) in
        }
        
    }
    
}
