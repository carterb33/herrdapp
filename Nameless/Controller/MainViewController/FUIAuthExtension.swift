//
//  FUIAuthExtension.swift
//  Herrd
//
//  Created by DG on 25/09/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseUI
import GoogleSignIn

//AUTHENTICATION EXTENSION

extension ViewController: FUIAuthDelegate {
    
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        // other URL handling goes here.
        return false
    }
    
    
    func showRestrictionMessage() {
        let controller = UIAlertController(title: "Attention",
                                           message: Messages.Warning.ValidMailSignIn,
                                           preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.signIn()
        }))
        present(controller, animated: true)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        // handle user and error as necessary
        if let user = user {
            let approvedDomains = ["bc.edu"]
            if let userDomain = user.email?.components(separatedBy: "@").last {
                if approvedDomains.contains(userDomain) == false &&
                    !StaticData.RequiredEmails.contains(user.email ?? "")
                {
                    try? Auth.auth().signOut()
                    showRestrictionMessage()
                    return
                }
            }
            
            if UserDefaults.standard.bool(forKey: "isAccessCodeVerified") {
                tableView.isHidden = false
                print("*** we signed in with the user \(user.email ?? "unknown email") ")
                self.checkAdminUser()
                getFavPlace()
            } else {
                
                let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "TermViewController") as! TermViewController
                welcomeVC.modalPresentationStyle = .overCurrentContext
                self.present(welcomeVC, animated: true, completion: {
                    
                })
            }
        }
    }
    
    func showTermScreen() {
        
        let termVC = self.storyboard?.instantiateViewController(withIdentifier: "TermViewController") as! TermViewController
        termVC.modalPresentationStyle = .overCurrentContext
        self.present(termVC, animated: true, completion: {
            
        })
    }
    
    func showHelpScreen() {
        
//        let guestList = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
//        guestList.modalPresentationStyle = .overCurrentContext
//        self.present(guestList, animated: true, completion: {
            self.tableView.isHidden = false
            self.checkAdminUser()
            self.getFavPlace()
//        })
    }
    
    func authPickerViewController(forAuthUI authUI: FUIAuth) -> FUIAuthPickerViewController {
        
        let loginViewController = FUIAuthPickerViewController(authUI: authUI)
        
        //loginViewController.view.backgroundColor = UIColor.black
        
        let marginInsets: CGFloat = 16;         let imageHeight: CGFloat = 225
        let imageY = self.view.center.y - imageHeight
        let logoFrame = CGRect(x: self.view.frame.origin.x + marginInsets, y: imageY, width: self.view.frame.width - (marginInsets*2), height: imageHeight)
        let logoImageView = UIImageView(frame: logoFrame)
        logoImageView.image = UIImage(named: "loginLogo")
//        logoImageView.backgroundColor = UIColor.black
        logoImageView.layer.cornerRadius = 15.0
        logoImageView.contentMode = .scaleAspectFit
        
//        let logoBackground = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//        logoBackground.backgroundColor = .black
        
       
        //fix this once Firebase UI releases the dark mode support for sign-in
        
        loginViewController.view.subviews[0].backgroundColor = .black
        loginViewController.view.subviews[0].subviews[0].backgroundColor = .black
        
        loginViewController.view.addSubview(logoImageView)
    
        return loginViewController
    }
    
}
