//
//  ChatViewController.swift
//  Herrd
//
//  Created by DG on 06/08/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import MessageKit
import FirebaseFirestore
import InputBarAccessoryView

class ChatViewController: MessagesViewController {
    
    private let db = Firestore.firestore()
    private var reference: CollectionReference?
    private let storage = Storage.storage().reference()
    
    private var messages: [Message] = []
    private var messageListener: ListenerRegistration?
    
    private let currentUser: User
    private let selectedChannel: Channel
    
    var place : Venue?
    var topic : Topic?
    var isAnonymous : Bool?
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        messageListener?.remove()
    }
    
    init(user: User, channel: Channel, anonymousMode:Bool) {
        self.currentUser = user
        self.selectedChannel = channel
        self.isAnonymous = anonymousMode
        super.init(nibName: nil, bundle: nil)
        if let topic = self.topic {
            title = topic.topic_title
        } else {
            title = channel.name
        }
    }
    
    init(user: User, channel: Channel, anonymousMode:Bool, topic: Topic) {
        self.currentUser = user
        self.selectedChannel = channel
        self.isAnonymous = anonymousMode
        super.init(nibName: nil, bundle: nil)
        title = topic.topic_title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.messagesCollectionView.backgroundColor = .black
        if #available(iOS 11, *) {
//            self.automaticallyAdjustsScrollViewInsets = true
            self.messagesCollectionView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
        }
        
        firebaseSetup()
        messageUISetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = .black
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
//        let navVCCount = self.navigationController?.viewControllers.count
        if let prewVC = self.navigationController?.viewControllers[0] as? ViewController {
            prewVC.getUserChatCounts()
        }
    }
    
    func firebaseSetup() {
        
        guard let id = selectedChannel.id else {
            navigationController?.popViewController(animated: true)
            return
        }
        let path = ["VenueChannels", id, "thread"].joined(separator: "/")
        reference = db.collection(path)
        
        messageListener = reference?.addSnapshotListener { querySnapshot, error in
            guard let snapshot = querySnapshot else {
                print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                return
            }
            
            snapshot.documentChanges.forEach { change in
                self.handleDocumentChange(change)
            }
        }
        
    }
    
    func messageUISetup() {
        
        navigationItem.largeTitleDisplayMode = .never
        
        maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.inputTextView.tintColor = .primary
        messageInputBar.inputTextView.textColor = .black
        messageInputBar.inputTextView.placeholder = "Your message"
        messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
        
        messageInputBar.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
//        messageInputBar.leftStackView.alignment = .center
//        messageInputBar.setLeftStackViewWidthConstant(to: 10, animated: false)
//        messageInputBar.setStackViewItems([cameraItem], forStack: .left, animated: false) // 3
        
        
        let button = UIButton()
        button.frame = CGRect(x:0, y:0, width:10, height:10) //won't work if you don't set frame
        button.setImage(UIImage(named: "threeHeadsGrey"), for: .normal)
        button.widthAnchor.constraint(equalToConstant: 25.0).isActive = true
        button.heightAnchor.constraint(equalToConstant: 23.0).isActive = true
        button.addTarget(self, action: #selector(btnGuestListPressed (_:)), for: .touchUpInside)
        
        let barButton = UIBarButtonItem()
        barButton.customView = button
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func btnGuestListPressed(_ sender: UIButton) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let guestList = main.instantiateViewController(withIdentifier: "GuestListViewController") as! GuestListViewController
        guestList.modalPresentationStyle = .overCurrentContext
        guestList.place = place
        self.present(guestList, animated: true, completion: nil)
    }
    
    // MARK: - Firebse Actions
    
    private func save(_ message: Message) {
        reference?.addDocument(data: message.representation) { error in
            if let e = error {
                print("Error sending message: \(e.localizedDescription)")
                return
            }
            
            self.messagesCollectionView.scrollToLastItem()
        }
    }
    
    private func insertNewMessage(_ message: Message) {
        guard !messages.contains(message) else {
            return
        }
        
        messages.append(message)
        messages.sort()
        if let place = self.place {
            updatePlaceMsgCount(place)
        } else {
            updateTopicMsgCount(topic!)
        }
        
        updateUserMsgCount()
        
        let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
        let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
            DispatchQueue.main.async {
                self.messagesCollectionView.scrollToLastItem(animated: true)
            }
        }
    }
    
    func updatePlaceMsgCount(_ selectedVenue : Venue) {
        
        Firestore.firestore().collection("VenueList")
            .whereField("placeId", isEqualTo: selectedVenue.placeId ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "msgcount": self.messages.count
                        ])
                }
        }
    }
    
    func updateTopicMsgCount(_ selectedTopic : Topic) {
        
        Firestore.firestore().collection("Topics")
            .whereField("topic_id", isEqualTo: selectedTopic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "msgcount": self.messages.count
                        ])
                }
        }
    }
    
    func updateUserMsgCount() {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let uniqueId = uid! + "~" + self.selectedChannel.id!
        
        Firestore.firestore().collection("UserChatReadCount")
            .whereField("uniqueId", isEqualTo: uniqueId)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    let document = querySnapshot!.documents.first
                    document?.reference.updateData([
                        "chatCount": self.messages.count
                        ])
                }
        }
        
    }
    
    private func handleDocumentChange(_ change: DocumentChange) {
        guard let message = Message(document: change.document) else {
            return
        }
        
        switch change.type {
        case .added:
                insertNewMessage(message)
        default:
            break
        }
    }

}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .primary : .incomingMessage
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
        return false
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
    }
    
}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
    
    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 25, height: 25)
    }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
//    func cellBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
//        return 30
//    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 0, height: 8)
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
    func configureAvatarView(
        _ avatarView: AvatarView,
        for message: MessageType,
        at indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView) {
        
        var initials = "?"
        if isFromCurrentSender(message: message) && self.isAnonymous! {
            initials = "?"
        } else {
            let name = message.sender.displayName
            let nameArray = name.components(separatedBy: " ")
            let firstname = nameArray.first
            let lastname = nameArray.last
            initials = String(firstname?.first! ?? " ").uppercased() + String(lastname?.first! ?? " ").uppercased()
        }
        
        avatarView.initials = initials
//        let message = messages[indexPath.section]
//        let color = message.member.color
//        avatarView.backgroundColor = color
    }
    
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
    
    func currentSender() -> SenderType {
        var userName = ""
        if self.isAnonymous! {
            userName = "Anonymous User"
        } else {
            userName = currentUser.displayName ?? currentUser.uid
        }
        return Sender(id: currentUser.uid, displayName: userName)
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        var name = ""
        if isFromCurrentSender(message: message) && self.isAnonymous! {
            name = "anonymous"
        } else {
            name = message.sender.displayName
        }
        
        let paragraph = NSMutableParagraphStyle()
        isFromCurrentSender(message: message) ? paragraph.alignment = .right : (paragraph.alignment = .left)
        
        return NSAttributedString(
            string: name,
            attributes: [
                .font: UIFont.preferredFont(forTextStyle: .caption1),
                .foregroundColor: UIColor(white: 0.5, alpha: 1),
                .paragraphStyle: paragraph
            ]
        )
    }
    
//    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
//        if indexPath.section % 3 == 0 {
//            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
//        }
//        return nil
//    }
    
    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        return NSAttributedString(string: "Read", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10), NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        var name = ""
        if isFromCurrentSender(message: message) && self.isAnonymous! {
            name = "anonymous"
        } else {
            name = message.sender.displayName
        }
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
    
}

// MARK: - MessageInputBarDelegate

extension ChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        let message = Message(user: currentUser, content: text)
        save(message)
        inputBar.inputTextView.text = ""
    }
}
