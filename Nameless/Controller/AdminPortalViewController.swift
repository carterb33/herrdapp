//
//  AdminPortalViewController.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 05/05/21.
//  Copyright © 2021 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MBProgressHUD

class BatchCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblPendingCounts: UILabel!
    @IBOutlet weak var btnFetchData: UIButton!
    @IBOutlet weak var btnRunBatch: UIButton!
}

class AdminPortalViewController: UIViewController {
    @IBOutlet weak var batchTableView: UITableView!
    var lastDocOfPage: DocumentSnapshot?
    var pageNumber = 0
    var randomNames: Set<String> = []
    var firebseNames: [[String:Any]] = []
    var batches: [[String:Any]] = [
        [
            "title": "Move Old Topics",
            "subTitle": "This batch move topic older then 7 days to 'OldTopics' collection"
        ],
        [
            "title": "Update all new topics with string date",
            "subTitle": "This batch will update all new topics with topic string date."
        ],
        [
            "title": "Add Random Names",
            "subTitle": "This batch will add random generated names on firebase."
        ],
        [
            "title": "Move Old Sub Topics",
            "subTitle": "This batch move sub topic older then 7 days to 'OldSubTopics' collection"
        ],
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Admin Portal"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        generateRandListOfUsers()
    }
}

extension AdminPortalViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return batches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BatchCell", for: indexPath) as! BatchCell
        let batch = batches[indexPath.row]
        cell.lblTitle.text = batch["title"] as? String
        cell.lblSubTitle.text = batch["subTitle"] as? String
        if let status = batch["status"] as? String {
            cell.lblPendingCounts.text = status
        } else {
            cell.lblPendingCounts.text = "to get data click on 'Fetch Data'"
        }
        
        cell.btnRunBatch.tag = indexPath.row
        cell.btnRunBatch.addTarget(self, action: #selector(btnRunBatchPress(_:)), for: .touchUpInside)
        if let data = batch["data"] as? [DocumentSnapshot], data.count == 0 && indexPath.row == 2 {
            cell.btnRunBatch.isEnabled = true
            cell.btnRunBatch.backgroundColor = .systemBlue
            cell.btnFetchData.isEnabled = false
            cell.btnFetchData.backgroundColor = .lightGray
        } else if let data = batch["data"] as? [DocumentSnapshot], data.count > 0 {
            cell.btnRunBatch.isEnabled = true
            cell.btnRunBatch.backgroundColor = .systemBlue
            cell.btnFetchData.isEnabled = false
            cell.btnFetchData.backgroundColor = .lightGray
        } else {
            cell.btnRunBatch.isEnabled = false
            cell.btnRunBatch.backgroundColor = .lightGray
            cell.btnFetchData.isEnabled = true
            cell.btnFetchData.backgroundColor = .systemBlue
        }
        
        cell.btnFetchData.tag = indexPath.row
        cell.btnFetchData.addTarget(self, action: #selector(btnFetchDataPress(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    @IBAction func btnRunBatchPress(_ sender: UIButton) {
        let batch = batches[sender.tag]
        switch sender.tag {
        case 0:
            if let data = batch["data"] as? [DocumentSnapshot], data.count > 0 {
                self.runBatchForMoveTopic(data,sender.tag)
            }
        case 1:
            if let data = batch["data"] as? [DocumentSnapshot], data.count > 0 {
                self.runBatchToInsertStringDate(data,sender.tag)
            }
        case 2:
            self.runBatchForAddNames(sender.tag)
        case 3:
            if let data = batch["data"] as? [DocumentSnapshot], data.count > 0 {
                self.runBatchForMoveSubTopic(data,sender.tag)
            }
        default:
            return
        }
    }
    
    @IBAction func btnFetchDataPress(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.fetchOldTopics(sender.tag)
        case 1:
            self.fetchLatestTopics(sender.tag)
        case 2:
            self.fetchAllNames(sender.tag)
        case 3:
            self.fetchOldSubTopics(sender.tag)
        default:
            return
        }
    }
    
    func fetchOldTopics(_ index: Int) {
        
        let todayDate = Date()
        var components = Calendar.current.dateComponents([.year, .month, .day], from: todayDate)
        components.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        guard
            let start = Calendar.current.date(from: components),
            let end = Calendar.current.date(byAdding: .day, value: -3, to: start)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        print("Fetch Data till: ",end)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_date", isLessThan: end)
            .order(by: "topic_date", descending: false)
            .limit(to: 250)
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                } else {
                    print("Init Topic count: ",querySnapshot!.documents.count)
                    if let documents = querySnapshot?.documents, documents.count > 0 {
                        self.batches[index]["data"] = documents
                        self.batches[index]["status"] = "Total Fetched Data: \(documents.count)"
                        self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    } else {
                        self.batches[index]["data"] = []
                        self.batches[index]["status"] = "All Topics older than 1 month moved to old topic collection."
                        self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    }
                    
                }
            })
                    
    }
    
    func fetchOldSubTopics(_ index: Int) {
        
        let todayDate = Date()
        var components = Calendar.current.dateComponents([.year, .month, .day], from: todayDate)
        components.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        guard
            let start = Calendar.current.date(from: components),
            let end = Calendar.current.date(byAdding: .day, value: -2, to: start)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        print("Fetch Data till: ",end)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .whereField("subtopic_date", isLessThan: end)
            .order(by: "subtopic_date", descending: false)
            .limit(to: 250)
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting Sub Topics documents: \(err)")
                } else {
                    print("Init Sub Topic count: ",querySnapshot!.documents.count)
                    if let documents = querySnapshot?.documents, documents.count > 0 {
                        self.batches[index]["data"] = documents
                        self.batches[index]["status"] = "Total Fetched Sub Topics: \(documents.count)"
                        self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    } else {
                        self.batches[index]["data"] = []
                        self.batches[index]["status"] = "All Sub Topics older than 1 month moved to old sub topic collection."
                        self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    }
                    
                }
            })
                    
    }
    
    func fetchLatestTopics(_ index: Int) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
            
        if let lastDoc = self.lastDocOfPage {
            Firestore.firestore().collection(FirebaseConst.Collections.Topics)
                .order(by: "topic_date", descending: true)
                .limit(to: 500)
                .start(afterDocument: lastDoc)
                .getDocuments(completion: { (querySnapshot, err) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let err = err {
                        print("Error getting Topics documents: \(err)")
                    } else {
                        print("Init Topic count: ",querySnapshot!.documents.count)
                        if let documents = querySnapshot?.documents, documents.count > 0 {
                            self.pageNumber += 1
                            self.batches[index]["data"] = documents
                            self.batches[index]["status"] = "Total Fetched topics: \(documents.count)(\(self.pageNumber + 1))"
                            self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                            if let lastDoc = querySnapshot?.documents.last {
                                self.lastDocOfPage = lastDoc
                            }
                        }
                        
                    }
                })
        } else {
            Firestore.firestore().collection(FirebaseConst.Collections.Topics)
                .order(by: "topic_date", descending: true)
                .limit(to: 500)
                .getDocuments(completion: { (querySnapshot, err) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let err = err {
                        print("Error getting Topics documents: \(err)")
                    } else {
                        print("Init Topic count: ",querySnapshot!.documents.count)
                        if let documents = querySnapshot?.documents, documents.count > 0 {
                            self.batches[index]["data"] = documents
                            self.batches[index]["status"] = "Total Fetched topics: \(documents.count)(\(self.pageNumber + 1))"
                            self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                            if let lastDoc = querySnapshot?.documents.last {
                                self.lastDocOfPage = lastDoc
                            }
                        }
                        
                    }
                })
        }
        
                    
    }
    
    func fetchAllNames(_ index: Int) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Firestore.firestore().collection(FirebaseConst.Collections.Names)
//            .whereField("assigned", isEqualTo: false)
            .getDocuments(completion: { (querySnapshot, err) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                } else {
                    print("Init Names count: ",querySnapshot!.documents.count)
                    var assignedNames: [[String:Any]] = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue["id"] = $0.documentID
                        self.firebseNames.append(documentValue)
                        if let assigned = documentValue["assigned"] as? Bool, assigned == true {
                            assignedNames.append(documentValue)
                        }
                    }
                    if let documents = querySnapshot?.documents, documents.count > 0 {
                        self.batches[index]["data"] = documents
                        self.batches[index]["status"] = "Total Names: \(documents.count) (Assigned: \(assignedNames.count) & Not-Assigned: \(documents.count - assignedNames.count))"
                        self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    } else {
                        self.batches[index]["data"] = []
                        self.batches[index]["status"] = "No Names available. Please add some names."
                        self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                    }
                }
            })
                    
    }
    
    func runBatchForMoveTopic(_ data: [DocumentSnapshot],_ index: Int) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let batch = Firestore.firestore().batch()
        let db = Firestore.firestore()
        let oldtopicsRef = db.collection(FirebaseConst.Collections.OldTopics)
        for document in data {
            if let data = document.data() {
                let aDoc = oldtopicsRef.document()
                //Add topic to old collection
                batch.setData(data, forDocument: aDoc)
                //delete same topic from Topic collection
                batch.deleteDocument(document.reference)
            }
        }
        batch.commit { (error) in
            if let err = error {
                print("Add topic in old topic batch failed",err)
            }
            print("Add topic in old topic batch success")
            MBProgressHUD.hide(for: self.view, animated: true)
            self.batches[index].removeValue(forKey: "data")
            self.batches[index]["status"] = "Topics moved. Try to fecth net topics."
            self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            CommonUtility.showAlert(title: "Batch executed!", message: "All Selected topics moved to OldTopics collection and removed from Topics collection.", controller: self)
        }
    }
    
    func runBatchForMoveSubTopic(_ data: [DocumentSnapshot],_ index: Int) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let batch = Firestore.firestore().batch()
        let db = Firestore.firestore()
        let oldtopicsRef = db.collection(FirebaseConst.Collections.OldSubTopics)
        for document in data {
            if let data = document.data() {
                let aDoc = oldtopicsRef.document()
                //Add sub topic to old collection
                batch.setData(data, forDocument: aDoc)
                //delete same sub topic from SubTopic collection
                batch.deleteDocument(document.reference)
            }
        }
        batch.commit { (error) in
            if let err = error {
                print("Add Sub Topic in old subtopic batch failed",err)
            }
            print("Add sub topic in old sub topic batch success")
            MBProgressHUD.hide(for: self.view, animated: true)
            self.batches[index].removeValue(forKey: "data")
            self.batches[index]["status"] = "Sub Topics moved. Try to fecth net sub topics."
            self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            CommonUtility.showAlert(title: "Batch executed!", message: "All Selected sub topics moved to OldSubTopics collection and removed from SubTopics collection.", controller: self)
        }
    }
    
    func runBatchToInsertStringDate(_ data: [DocumentSnapshot],_ index: Int) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        // Get a new write batch
        let batch = Firestore.firestore().batch()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        for document in data {
            if let data = document.data(), data["topic_date_str"] == nil {
                let topicDate = (data["topic_date"] as AnyObject).dateValue()
                let topicDateStr = dateFormatter.string(from: topicDate)
                batch.updateData(["topic_date_str": topicDateStr], forDocument: document.reference)
            }
        }
        
        batch.commit { (error) in
            if let err = error {
                print("update date batch failed",err)
            }
            print("Update date batch success")
            MBProgressHUD.hide(for: self.view, animated: true)
            self.batches[index].removeValue(forKey: "data")
            self.batches[index]["status"] = "Topics dates inserted. Try to fecth new topics."
            self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            CommonUtility.showAlert(title: "Batch executed!", message: "All Selected topics inserted string date.", controller: self)
        }
    }
    
    func runBatchForAddNames(_ index: Int) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let batch = Firestore.firestore().batch()
        let db = Firestore.firestore()
        let namesRef = db.collection(FirebaseConst.Collections.Names)
        
        //Check name existist on firebse
        print("Random names:",self.randomNames.count)
        for nameObj in self.firebseNames {
            if let name = nameObj["name"] as? String {
                self.randomNames.remove(name)
            }
        }
        print("Remaining names:",self.randomNames.count)
        for (index,name) in self.randomNames.enumerated() {
            let aDoc = namesRef.document()
            //Add topic to old collection
            batch.setData(["name": name, "assigned": false], forDocument: aDoc)
            if index >= 499 {
                print("Last Index: ", index)
                break
            }
        }
    
        batch.commit { (error) in
            if let err = error {
                print("Add names batch failed",err)
            }
            print("Add names batch success")
            MBProgressHUD.hide(for: self.view, animated: true)
            self.batches[index].removeValue(forKey: "data")
            self.batches[index]["status"] = "New Names added. Try to fecth updated names."
            self.batchTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            CommonUtility.showAlert(title: "Batch executed!", message: "500 New names added on firebse.", controller: self)
        }
    }
    
    //Reset assigment before upload final version on app store
    func batchUpdatesResetAssignNames(_ names: [[String:Any]]) {
        
        // Get a new write batch
        let batch = Firestore.firestore().batch()
        for name in names {
            let nameDoc = Firestore.firestore().collection(FirebaseConst.Collections.Names).document(name["id"] as! String)
            batch.updateData(["assigned": false], forDocument: nameDoc)
        }
        
        batch.commit { (error) in
            if let err = error {
                print("update name assign batch failed",err)
            }
            print("Update name assign batch success")
        }
    }
    
    func generateRandListOfUsers() {
        for i in 0...StaticData.randomName.count - 1 {
            for j in 0...StaticData.animalNames.count - 1 {
                let fullName = StaticData.randomName[i].capitalizingFirstLetter() + " " + StaticData.animalNames[j].capitalizingFirstLetter()
                self.randomNames.insert(fullName)
                if self.randomNames.count == 50000 {
                    break
                }
            }
        }
        print(self.randomNames.count)
    }
}
