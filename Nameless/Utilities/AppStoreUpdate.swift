//
//  AppStoreUpdate.swift
//  Herrd
//
//  Created by DG on 03/11/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit

enum CustomError: Error {
   case jsonReading
   case invalidIdentifires
   case invalidURL
   case invalidVersion
   case invalidAppName
}

class AppStoreUpdate: NSObject {
    
    static let shared = AppStoreUpdate()

    func showAppStoreVersionUpdateAlert(isForceUpdate: Bool) {
        
        do {
            //Get Bundle Identifire from Info.plist
            guard let bundleIdentifire = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String else {
                print("No Bundle Info found.")
                throw CustomError.invalidIdentifires
            }
            
            // Build App Store URL
            guard let url = URL(string:APPURL.iTunesAppBaseUrl + bundleIdentifire) else {
                print("Isse with generating URL.")
                throw CustomError.invalidURL
            }
            let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
            
            let serviceTask = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
                
                do {
                    // Check error
                    if let error = error { throw error }
                    //Parse response
                    guard let data = responseData else { throw CustomError.jsonReading }
                    let result = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    let itunes = ItunesAppInfoItunes.init(fromDictionary: result as! [String : Any])
//                    print(itunes.results)
                    if let itunesResult = itunes.results.first {
//                        print("App Store Varsion: ",itunesResult.version)
                        
                        //Get Bundle Version from Info.plist
                        guard let appShortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
                            print("No Short Version Info found.")
                            throw CustomError.invalidVersion
                        }
                        
                        let compareResult = appShortVersion.versionCompare(itunesResult.version)
                        switch compareResult {
                        case .orderedAscending:
                            print("Itune version grater than local version")
                            //Show Update alert
                            var message = ""
                            //Get Bundle Version from Info.plist
                            if let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
                                message = "\(appName) has a new version(\(itunesResult.version!)) available on the App Store."
                            } else {
                                message = "This app has a new version(\(itunesResult.version!)) available on the App Store."
                            }
                            
                            //Show Alert on main thread
                            DispatchQueue.main.async {
                                self.showUpdateAlert(message: message, appStoreURL: itunesResult.trackViewUrl, isForceUpdate: isForceUpdate)
                            }
                        case .orderedDescending:
                            print("Itune version less than local version")
                        case .orderedSame:
                            //App Store & Local App Have same Version.
                            print("Current Version is the latest version of the app.")
                        default:
                            print("No Result")
                        }
                    }
                } catch {
                    print(error)
                }
            }
            serviceTask.resume()
        } catch {
            print(error)
        }
    }
    
    func checkStringToFloat(versionString : String) -> Float {
        let finalString = versionString.replacingOccurrences(of: ".", with: "")
        return Float(finalString)!
    }
    
    func showUpdateAlert(message : String, appStoreURL: String, isForceUpdate: Bool) {
        
        let controller = UIAlertController(title: "New Version", message: message, preferredStyle: .alert)
        
        //Optional Button
        if !isForceUpdate {
            controller.addAction(UIAlertAction(title: "Later", style: .cancel, handler: { (_) in }))
        }
        
        controller.addAction(UIAlertAction(title: "Update", style: .default, handler: { (_) in
            guard let url = URL(string: appStoreURL) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }))
        
        let applicationDelegate = UIApplication.shared.delegate as? AppDelegate
        applicationDelegate?.window?.rootViewController?.present(controller, animated: true)
        
    }
}

extension String {
    func versionCompare(_ otherVersion: String) -> ComparisonResult {
        return self.compare(otherVersion, options: .numeric)
    }
}
