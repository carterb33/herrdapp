//
//  UIDeviceExtension.swift
//  Herrd
//
//  Created by DG on 23/04/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit

import Foundation
import UIKit
public extension UIDevice {

    class var isPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }

    class var isPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }

    class var isTV: Bool {
        return UIDevice.current.userInterfaceIdiom == .tv
    }

    class var isCarPlay: Bool {
        return UIDevice.current.userInterfaceIdiom == .carPlay
    }

}
