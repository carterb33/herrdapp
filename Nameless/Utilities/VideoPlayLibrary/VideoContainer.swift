//
//  VideoContainer.swift
//  Herrd
//
//  Created by DG on 17/05/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import AVFoundation
class VideoContainer {
    var url: String
    var playOn: Bool {
        didSet {
//            if UserDefaults.standard.bool(forKey: "isVideoMute") {
//                player.isMuted = true
//            } else {
//                player.isMuted = false
//            }
            player.isMuted = true
            playerItem.preferredPeakBitRate = VideoPlayerController.sharedVideoPlayer.preferredPeakBitRate
            if playOn && playerItem.status == .readyToPlay {
                player.play()
            }
            else{
                player.pause()
            }
        }
    }
    
    let player: AVPlayer
    let playerItem: AVPlayerItem
    
    init(player: AVPlayer, item: AVPlayerItem, url: String) {
        self.player = player
        self.playerItem = item
        self.url = url
        playOn = false
    }
}
