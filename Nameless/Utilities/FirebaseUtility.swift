//
//  FirebaseUtility.swift
//  Herrd
//
//  Created by DG on 27/07/19.
//  Copyright © 2019 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class FirebaseUtility: NSObject {
    
    func getUserDetails(completion : @escaping(UserInfo?, Error?) ->()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection("UserInfo").document("\(uid ?? "")").getDocument { (documentSnapShot, error) in
            if let err = error, ((documentSnapShot?.data()?.isEmpty) != nil) {
                print("Error getting documents: \(err)")
                completion(nil,error)
            } else {
                do {
                    let user = try UserInfo(data: documentSnapShot?.data() ?? [:])
                    completion(user,nil)
                } catch {
                    print(error)
                    completion(nil,error)
                }
            }
        }
    }
    
    func getUserDetails(userId: String,completion: @escaping(UserInfo?, Error?) -> ()) {
        let uRef = Firestore.firestore()
            .collection(FirebaseConst.Collections.Users)
            .document(userId)

        uRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let documentValue = document.data()
                do {
                    let user = try UserInfo(data: documentValue ?? [:])
                    completion(user,nil)
                } catch {
                    completion(nil,error)
                }
            } else {
                completion(nil,nil)
            }
        }
    }
    
    func getUserDetailsList(userIds: [String],completion: @escaping([UserInfo]?, Error?) -> ()) {
        let uRef = Firestore.firestore()
            .collection(FirebaseConst.Collections.Users)

        uRef.getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting VenueChannels documents: \(err)")
                completion(nil,err)
            } else {
                var userList : [UserInfo]  = []
                querySnapshot!.documents.forEach {
                    if userIds.contains($0.documentID) {
                        if let userInfo = UserInfo(document: $0) {
                            userList.append(userInfo)
                        }
                    }
                }
                completion(userList,nil)
            }
        })
    }
    
    // Add or update nickName using user id in "Users" Collection
    func addOrUpdateNickNameWithUserId(userId: String, nickName: String,completion: @escaping(Bool?, Error?) -> ()) {
        let uRef = Firestore.firestore().collection(FirebaseConst.Collections.Users)
        uRef.document(userId).updateData([
            "nickName": nickName
        ]) { err in
            if let err = err {
                completion(false,err)
            } else {
                completion(true,nil)
            }
        }
    }
    
    // Add or update nickName using user id in "Users" Collection
    func updateLastMessageWithChannelId(channelId: String, lastMsg: String, msgTime:Date,completion: @escaping(Bool?, Error?) -> ()) {
        let uRef = Firestore.firestore().collection(FirebaseConst.Collections.TopicChannels)
        uRef.document(channelId).updateData([
            "lastMsg": lastMsg,
            "lastMsgDate": msgTime
        ]) { err in
            if let err = err {
                completion(false,err)
            } else {
                completion(true,nil)
            }
        }
    }
    
    // update Name assigned object using id in "Names" Collection
    func updateNickNameAssignedWithId(Id: String,completion: @escaping(Bool?, Error?) -> ()) {
        let uRef = Firestore.firestore().collection(FirebaseConst.Collections.Names)
        uRef.document(Id).updateData([
            "assigned": true
        ]) { err in
            if let err = err {
                completion(false,err)
            } else {
                completion(true,nil)
            }
        }
    }
    
    func getUserFavoriteLocations(completion : @escaping([String:Any]?, Error?) ->()) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection(FirebaseConst.Collections.Users)
            .whereField("UID", isEqualTo: uid!)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    if let data = querySnapshot!.documents.first?.data(){
                        completion(data,nil)
                    }else{
                        completion(nil,err)
                    }
                }
            })
    }
    
    func getChannels(completion: @escaping([[String:Any]]?, Error?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Venues)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting VenueChannels documents: \(err)")
                completion(nil,err)
            } else {
                var channelList : [[String:Any]]  = []
                querySnapshot!.documents.forEach {
                    let documentValue = $0.data()
                    channelList.append(documentValue)
                }
                completion(channelList,nil)
            }
        })
    }
    
    func getNames(completion: @escaping([[String:Any]]?, Error?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Names)
            .whereField("assigned", isEqualTo: false)
            .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting VenueChannels documents: \(err)")
                completion(nil,err)
            } else {
                var names : [[String:Any]]  = []
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue["id"] = $0.documentID
                    names.append(documentValue)
                }
                print("Names remaining to assign: ",names.count)
                completion(names,nil)
                
            }
        })
    }
    
    func getEvents(completion: @escaping([[String:Any]]?, Error?) -> ()) {
        
        let todayDate = Date()
        let components = Calendar.current.dateComponents([.year, .month, .day], from: todayDate)
        guard
            let start = Calendar.current.date(from: components),
            let end = Calendar.current.date(byAdding: .day, value: 1, to: start)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        
        let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.Events)
        eventsRef.whereField("event_date", isGreaterThan: start)
//            .whereField("event_date", isLessThan: end)
            .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Events documents: \(err)")
                completion(nil,err)
            } else {
                var eventList : [[String:Any]]  = []
                querySnapshot!.documents.forEach {
                    let documentValue = $0.data()
                    eventList.append(documentValue)
                }
                completion(eventList,nil)
            }
        })
    }
    
    func getUserChatReadCount(completion: @escaping([[String:Any]]?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        Firestore.firestore().collection(FirebaseConst.Collections.UserChatCount)
            .whereField("uniqueId", isGreaterThan: uid ?? "")
            .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting UserChatReadCount documents: \(err)")
                completion(nil,err)
            } else {
                var userChatCountList : [[String:Any]]  = []
                querySnapshot!.documents.forEach {
                    let documentValue = $0.data()
                    userChatCountList.append(documentValue)
                }
                completion(userChatCountList,nil)
            }
        })
    }
    
    func addOrUpdateBlockedUserIds(blockeduserId: String,completion: @escaping(Bool?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        var newBlockedUids: [String] = []
        if let blockedUsers = appDelegate.currentUserDetails?.blockedUserIds, blockedUsers.count > 0  {
            newBlockedUids.append(contentsOf: blockedUsers)
        }
        //Add Blocked Id
        if !newBlockedUids.contains(blockeduserId) {
            newBlockedUids.append(blockeduserId)
        }
        let uRef = Firestore.firestore().collection(FirebaseConst.Collections.Users)
        uRef.document(uid).updateData([
            "blockedUserIds": newBlockedUids
        ]) { err in
            if let err = err {
                completion(false,err)
            } else {
                //Add new blocked id in current userInfo blocked user list
                appDelegate.currentUserDetails?.blockedUserIds = newBlockedUids
                completion(true,nil)
            }
        }
    }
    
    //Old Method
//    func addOrUpdateBlockedUserIds(blockeduserId: String,completion: @escaping(Bool?, Error?) -> ()) {
//
//        let user = Auth.auth().currentUser
//        let uid = user?.uid
//
//        let buRef = Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
//        buRef.whereField("uid", isEqualTo: uid!)
//        .getDocuments(completion: { (querySnapshot, err) in
//            if let err = err {
//                print("Error getting BlockedUsers documents: \(err)")
//                completion(false,err)
//            } else {
//                if let document = querySnapshot?.documents.first{
//                    let documentValue = document.data()
//                    if let blockedUserIdsStr = documentValue["blockedUserIds"] as? String {
//                        var blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
//                        blockedUserIdsArr.append(blockeduserId)
//                        document.reference.updateData([
//                            "blockedUserIds": blockedUserIdsArr.joined(separator: ","),
//                        ])
//                        completion(true,nil)
//                    } else {
//                        document.reference.updateData([
//                            "blockedUserIds": blockeduserId,
//                        ])
//                        completion(true,nil)
//                    }
//                } else {
//                    let newBlockUserData = [
//                    "blockedUserIds": blockeduserId,
//                    "uid": uid!] as [String : Any]
//                    let aDoc = buRef.document() //this creates a document with a documentID
//                    print(aDoc.documentID) //prints the documentID, no database interaction
//                    aDoc.setData(newBlockUserData) { (err) in
//                        if let error = err {
//                            print(error)
//                            completion(false,error)
//                        } else {
//                            completion(true,nil)
//                        }
//                    }
//                }
//            }
//        })
//    }
    
    func addOrUpdateSubTopicBlockedUserIds(blockeduserId: String,completion: @escaping(Bool?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        let buRef = Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
        buRef.whereField("uid", isEqualTo: uid!)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Sub Topic BlockedUsers documents: \(err)")
                completion(false,err)
            } else {
                if let document = querySnapshot?.documents.first{
                    let documentValue = document.data()
                    if let blockedUserIdsStr = documentValue["blockedSubTopicUserIds"] as? String {
                        var blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                        blockedUserIdsArr.append(blockeduserId)
                        document.reference.updateData([
                            "blockedSubTopicUserIds": blockedUserIdsArr.joined(separator: ","),
                        ])
                        completion(true,nil)
                    } else {
                        document.reference.updateData([
                            "blockedSubTopicUserIds": blockeduserId,
                        ])
                        completion(true,nil)
                    }
                } else {
                    let newBlockUserData = [
                    "blockedSubTopicUserIds": blockeduserId,
                    "uid": uid!] as [String : Any]
                    let aDoc = buRef.document() //this creates a document with a documentID
                    print(aDoc.documentID) //prints the documentID, no database interaction
                    aDoc.setData(newBlockUserData) { (err) in
                        if let error = err {
                            print(error)
                            completion(false,error)
                        } else {
                            completion(true,nil)
                        }
                    }
                }
            }
        })
    }
    
    func deleteBlockedUserIds(completion: @escaping(Bool?, Error?) -> ()) {
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        let buRef = Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
        buRef.whereField("uid", isEqualTo: uid!)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting BlockedUsers documents: \(err)")
                completion(false,err)
            } else {
                if let document = querySnapshot?.documents.first{
                    buRef.document(document.documentID).delete(){ err in
                        if let err = err {
                            print("Error removing document: \(err)")
                            completion(false,err)
                        } else {
                            print("Block Users Document removed successfully!")
                            completion(true,nil)
                        }
                    }
                } else {
                    completion(true,nil)
                }
            }
        })
    }
    
    // Add or update device id using user id in "Users" Collection
    func addOrUpdateDeviceTokenWithUserId(userId: String, deviceToken: String,completion: @escaping(Bool?, Error?) -> ()) {
        let uRef = Firestore.firestore().collection(FirebaseConst.Collections.Users)
        uRef.document(userId).updateData([
            "device_id": deviceToken
        ]) { err in
            if let err = err {
                completion(false,err)
            } else {
                completion(true,nil)
            }
        }
    }
    
    // Get device id using user id from "Users" Collection
    func getDeviceTokenUsingUserId(userId: String,completion: @escaping(String?, Error?) -> ()) {
        let uRef = Firestore.firestore()
            .collection(FirebaseConst.Collections.Users)
            .document(userId)

        uRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let documentValue = document.data()
                if let deviceId = documentValue?["device_id"] as? String {
                    completion(deviceId,nil)
                } else {
                    completion(nil,nil)
                }
            } else {
                completion(nil,nil)
            }
        }
    }
    
    // Get device id using user id from "Users" Collection
    func getAppSettings(completion: @escaping(Error?, [String : Any]?) -> ()) {
        guard let bundleID = Bundle.main.bundleIdentifier else {
            completion(nil,nil)
            return
        }
        let uRef = Firestore.firestore()
            .collection(FirebaseConst.Collections.AppSettings)
            .document(bundleID)

        uRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let documentValue = document.data()
                if let topicTimelineLimit = documentValue?["topicTimelineLimit"] as? Int {
                    let adminList = documentValue?["adminlist"] as? [String]
                    let numberOfAds = documentValue?["numberOfAds"] as? Int
                    UserDefaults.standard.setValue(topicTimelineLimit, forKey: "topicTimelineLimit")
                    UserDefaults.standard.setValue(adminList, forKey: "adminlist")
                    UserDefaults.standard.setValue(numberOfAds, forKey: "numberOfAds")
                    UserDefaults.standard.synchronize()
                    completion(nil,documentValue)
                } else {
                    completion(nil,documentValue)
                }
            } else {
                completion(nil,nil)
            }
        }
    }
    
    // Get device id using user id from "Users" Collection
    func updateAppSettings(data:[AnyHashable:AnyHashable], completion: @escaping(Error?) -> ()) {
        guard let bundleID = Bundle.main.bundleIdentifier else {
            completion(nil)
            return
        }
        let uRef = Firestore.firestore()
            .collection(FirebaseConst.Collections.AppSettings)
            .document(bundleID)
        
        uRef.updateData(data) { error in
            completion(error)
        }
    }
    
    // Send notification
    func sendNotificationAPI(param: [String:Any]) {
        // create the request
        let url = URL(string: APPURL.FCMNotifictionUrl)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue(Key.Google.FCM, forHTTPHeaderField: "authorization")
        request.setValue(Key.Headers.ContentType, forHTTPHeaderField: "Content-Type")
        
//        print(param)
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let dataTask = session.dataTask(with: request as URLRequest) { data,response,error in
//            let httpResponse = response as? HTTPURLResponse
            if (error != nil) {
                print(error!)
            } else {
//                print(httpResponse!)
            }
//            guard let responseData = data else {
//                print("Error: did not receive data")
//                return
//            }
//            do {
//                guard let responseDictionary = try JSONSerialization.jsonObject(with: responseData, options: [])
//                    as? [String: Any] else {
//                        print("error trying to convert data to JSON")
//                        return
//                }
//                print("The responseDictionary is: " + responseDictionary.description)
//            } catch  {
//                print("error trying to convert data to JSON")
//                return
//            }
//            DispatchQueue.main.async {
//                //Update your UI here
//            }
        }
        dataTask.resume()
    }

}
