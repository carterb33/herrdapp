//
//  SubTopicFirebaseUtility.swift
//  Herrd
//
//  Created by DG on 05/04/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SubTopicFirebaseUtility: NSObject {
    
    func addNewSubTopic(title: String, topic_id: Double, isReplySubTopic: Bool, completion: @escaping([String:Any]?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        let db = Firestore.firestore()
        let topicsRef = db.collection(FirebaseConst.Collections.SubTopics)
        let todayDateTime = Date()
        let newTopicData = [
            "subtopic_date": todayDateTime,
            "subtopic_desc": "",
            "subtopic_title": title,
            "subtopic_id": Date().timeIntervalSince1970,
            "topic_id": topic_id,
            "difference": 0,
            "created_by": uid!,
            "upVoteIds":"",
            "downVoteIds":"",
            "channelId": "",
            "msgcount": 0,
            "isReplySubTopic": isReplySubTopic
            ] as [String : Any]

        let aDoc = topicsRef.document() //this creates a document with a documentID
        print(aDoc.documentID) //prints the documentID, no database interaction
        aDoc.setData(newTopicData) { (err) in
            if let error = err {
                print(error)
                completion(nil,error)
            } else {
                completion(newTopicData,nil)
            }
        }
        
    }
    
    func getAllSubTopics(completion: @escaping([SubTopic]?, Error?) -> ()) {
        
        let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
        eventsRef.getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                completion(nil,err)
            } else {
                var subTopicList : [SubTopic]  = []
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue.merge(["documentId" : $0.documentID]) { (test, test1) -> Any in
                        print(test,test1)
                    }
                    do {
                        let subtopic = try SubTopic(data: documentValue)
                        subTopicList.append(subtopic)
                    } catch {
                        print(error)
                    }
                }
                completion(subTopicList,nil)
            }
        })
        
    }
    
    func getReportedSubTopicsFirstPage(completion: @escaping([SubTopic]?, Error?, DocumentSnapshot?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
//            .whereField("subtopic_date", isGreaterThan: start)
            .whereField("reportedIds", isNotEqualTo: "")
            .limit(to: Constants.pageSize)
            .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Reported Sub Topics documents: \(err)")
                completion(nil,err,nil)
            } else {
                var subtopicList : [SubTopic]  = []
                print("Reported Sub Topic count: ",querySnapshot!.documents.count)
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue.updateValue($0.documentID, forKey: "documentId")
                    do {
                        let subtopic = try SubTopic(data: documentValue)
                        subtopicList.append(subtopic)
                    } catch {
                        print(error)
                    }
                }
                print("Reported Sub Topic count Local: ",subtopicList.count)
                if let lastDoc = querySnapshot?.documents.last {
                    completion(subtopicList,nil,lastDoc)
                } else {
                    completion(subtopicList,nil,nil)
                }
            }
        })
    }
    
    func getReportedSubTopicsNextPage(_ lastDoc: DocumentSnapshot ,_ completion: @escaping([SubTopic]?, Error?, DocumentSnapshot?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
//            .whereField("subtopic_date", isGreaterThan: start)
            .whereField("reportedIds", isNotEqualTo: "")
            .limit(to: Constants.pageSize)
            .start(afterDocument: lastDoc)
            .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Reported Sub Topics documents: \(err)")
                completion(nil,err,nil)
            } else {
                var subtopicList : [SubTopic]  = []
                print("Reported Sub Topic count: ",querySnapshot!.documents.count)
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue.updateValue($0.documentID, forKey: "documentId")
                    do {
                        let subtopic = try SubTopic(data: documentValue)
                        subtopicList.append(subtopic)
                    } catch {
                        print(error)
                    }
                }
                print("Reported Sub Topic count Local: ",subtopicList.count)
                if let lastDoc = querySnapshot?.documents.last {
                    completion(subtopicList,nil,lastDoc)
                } else {
                    completion(subtopicList,nil,nil)
                }
            }
        })
    }
    
    func updateSubTopicCount(_ topic : Topic, subTopicCount: Int, completion: @escaping(Topic?, Error?) -> ()) {
        
        guard let documentId = topic.documentId else {
            return
        }
        
        let topicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicRef.document(documentId)
            .updateData([
                "subTopicsCount": subTopicCount
            ], completion: { (error) in
                if let err = error {
                    completion(nil,err)
                }
                completion(topic,nil)
            })
    }
    
    func updateReplySubTopicCount(_ subtopic : SubTopic, subReplyCount: Int, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        guard let documentId = subtopic.documentId else {
            return
        }
        
        let subTopicRef = Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
        subTopicRef.document(documentId)
            .updateData([
                "subReplyCount": subReplyCount
            ], completion: { (error) in
                if let err = error {
                    completion(nil,err)
                }
                completion(subtopic,nil)
            })
    }
    
    func getSubTopics(_ topicId : Any ,completion: @escaping([SubTopic]?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        let buRef = Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
        buRef.whereField("uid", isEqualTo: uid!)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Blocked Users documents: \(err)")
                 completion(nil,err)
            } else {
                var blockedUserIdsArr : [String] = []
                if let document = querySnapshot?.documents.first{
                    if let blockedUserIdsStr = document.data()["blockedSubTopicUserIds"] as? String {
                        blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                    }
                }
                
                let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
                eventsRef.whereField("topic_id", isEqualTo: topicId).getDocuments(completion: { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting Topics documents: \(err)")
                        completion(nil,err)
                    } else {
                        var subTopicList : [SubTopic]  = []
                        querySnapshot!.documents.forEach {
                            var documentValue = $0.data()
                            documentValue.merge(["documentId" : $0.documentID]) { (test, test1) -> Any in
                                print(test,test1)
                            }
                            do {
                                let subtopic = try SubTopic(data: documentValue)
                                //Check Hide post
                                let hideUserIdsArr = subtopic.hidePostsUserIds?.components(separatedBy: ",")
                                let isHidePost = hideUserIdsArr?.contains(uid!)
                                //Check Block User post
                                let isBlockedUserPost = blockedUserIdsArr.contains(subtopic.created_by!)
                                if !(isHidePost ?? false) &&
                                    !isBlockedUserPost {
                                    subTopicList.append(subtopic)
                                }
                                
                            } catch {
                                print(error)
                            }
                        }
                        
                        completion(subTopicList,nil)
                    }
                })
            }
        })
        
    }
    
    func getSubTopicsByTopicId(_ subtopicId : Any ,completion: @escaping(SubTopic?, Error?) -> ()) {
        
        let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
        eventsRef.whereField("subtopic_id", isEqualTo: subtopicId).getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                completion(nil,err)
            } else {
                if let data = querySnapshot!.documents.first{
                    var documentValue = data.data()
                    documentValue.merge(["documentId" : data.documentID]) { (test, test1) -> Any in
                        print(test,test1)
                    }
                    do {
                        let subtopic = try SubTopic(data: documentValue)
                        completion(subtopic,nil)
                    } catch {
                        completion(nil,error)
                    }
                    
                } else {
                    completion(nil,nil)
                }
            }
        })
        
    }
    
    func getMySubTopics(completion: @escaping([SubTopic]?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else {
            completion(nil,nil)
            return
        }
        
        var limitDays = 0
        if let topicTimelineLimit = UserDefaults.standard.value(forKey: "topicTimelineLimit") as? Int {
            limitDays = topicTimelineLimit/24
        } else {
            limitDays = Constants.topicTimelineLimit/24
        }
        
        let todayDate = Date()
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: todayDate)
        guard
            let end = Calendar.current.date(from: components),
            let start = Calendar.current.date(byAdding: .day, value: -limitDays, to: end)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        
        let buRef = Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
        buRef.whereField("uid", isEqualTo: uid)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Blocked Users documents: \(err)")
                 completion(nil,err)
            } else {
                var blockedUserIdsArr : [String] = []
                if let document = querySnapshot?.documents.first{
                    if let blockedUserIdsStr = document.data()["blockedSubTopicUserIds"] as? String {
                        blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                    }
                }
                
                let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
                eventsRef.whereField("created_by", isEqualTo: uid).getDocuments(completion: { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting Topics documents: \(err)")
                        completion(nil,err)
                    } else {
                        var subTopicList : [SubTopic]  = []
                        querySnapshot!.documents.forEach {
                            var documentValue = $0.data()
                            documentValue.merge(["documentId" : $0.documentID]) { (test, test1) -> Any in
                                print(test,test1)
                            }
                            do {
                                let subtopic = try SubTopic(data: documentValue)
                                //Check Hide post
                                let hideUserIdsArr = subtopic.hidePostsUserIds?.components(separatedBy: ",")
                                let isHidePost = hideUserIdsArr?.contains(uid)
                                //Check Block User post
                                let isBlockedUserPost = blockedUserIdsArr.contains(subtopic.created_by!)
                                let interval = Date() - (subtopic.subtopic_date?.dateValue())!
                                if !(isHidePost ?? false) &&
                                    !isBlockedUserPost &&
                                    interval.hour ?? 0 < Constants.topicTimelineLimit {
                                    subTopicList.append(subtopic)
                                }
                                
                            } catch {
                                print(error)
                            }
                        }
                        
                        completion(subTopicList,nil)
                    }
                })
            }
        })
        
    }
    
    func addReportedIdstoSubTopic(_ subtopic : SubTopic, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .whereField("subtopic_id", isEqualTo: subtopic.subtopic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                    completion(subtopic,err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    if let document = querySnapshot!.documents.first{
                        var documentValue = document.data()
                        documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                            print(test,test1)
                        }
                        do {
                            let firebaseTopic = try SubTopic(data: documentValue)
                            document.reference.updateData([
                                "reportedIds": subtopic.reportedIds ?? "",
                            ])
                            firebaseTopic.reportedIds = subtopic.reportedIds
                            completion(firebaseTopic,nil)
                        } catch {
                            completion(subtopic,error)
                        }
                    }
                }
        }
    }
    
    func addHidePostUserIdstoSubTopic(_ subtopic : SubTopic, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .whereField("subtopic_id", isEqualTo: subtopic.subtopic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                    completion(subtopic,err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    if let document = querySnapshot?.documents.first{
                        var documentValue = document.data()
                        documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                            print(test,test1)
                        }
                        do {
                            let firebaseTopic = try SubTopic(data: documentValue)
                            document.reference.updateData([
                                "hidePostsUserIds": subtopic.hidePostsUserIds ?? "",
                            ])
                            firebaseTopic.hidePostsUserIds = subtopic.hidePostsUserIds
                            completion(firebaseTopic,nil)
                        } catch {
                            completion(subtopic,error)
                        }
                    }
                }
        }
    }

    func upVoteSubTopicNew(_ subTopic : SubTopic, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        var upvoteStringArr : [String] = []
        var downvoteStringArr : [String] = []
        if subTopic.upVoteIds!.count > 0 {
            upvoteStringArr = subTopic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        if subTopic.downVoteIds!.count > 0 {
            downvoteStringArr = subTopic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        
        if (upvoteStringArr.contains(uid)) {
            print("uid is in upvote")
            if let index = upvoteStringArr.firstIndex(of: uid) {
                upvoteStringArr.remove(at: index)
            }
        }
        
        if (downvoteStringArr.contains(uid)) {
            print("uid is in downvote")
            if let index = downvoteStringArr.firstIndex(of: uid) {
                downvoteStringArr.remove(at: index)
            }
        }
        
        //New record added
        upvoteStringArr.append(uid)
        
        let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
        let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
        let difference = upvoteStringArr.count - downvoteStringArr.count
        
        guard let documentId = subTopic.documentId else {
            return
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .document(documentId)
            .updateData([
                "upVoteIds": upvoteString,
                "downVoteIds": downvoteString,
                "difference": difference
            ]) { (error) in
                if let err = error {
                    completion(subTopic,err)
                }
                subTopic.upVoteIds = upvoteString
                subTopic.downVoteIds = downvoteString
                subTopic.difference = difference
                completion(subTopic,nil)
            }
    }
    
    func upVoteSubTopicRemove(_ subTopic : SubTopic, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        var upvoteStringArr : [String] = []
        if subTopic.upVoteIds!.count > 0 {
            upvoteStringArr = subTopic.upVoteIds?.components(separatedBy: ",") ?? []
        }
    
        if (upvoteStringArr.contains(uid)) {
            print("uid is in upvote")
            if let index = upvoteStringArr.firstIndex(of: uid) {
                upvoteStringArr.remove(at: index)
            }
        }
        
        let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
        let difference = (subTopic.difference ?? 0) - 1
        
        guard let documentId = subTopic.documentId else {
            return
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .document(documentId)
            .updateData([
                "upVoteIds": upvoteString,
                "difference": difference
            ]) { (error) in
                if let err = error {
                    completion(subTopic,err)
                }
                subTopic.upVoteIds = upvoteString
                subTopic.difference = difference
                completion(subTopic,nil)
            }
    }
    
    func downVoteSubTopicNew(_ subTopic : SubTopic, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        var upvoteStringArr : [String] = []
        var downvoteStringArr : [String] = []
        if subTopic.upVoteIds!.count > 0 {
            upvoteStringArr = subTopic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        if subTopic.downVoteIds!.count > 0 {
            downvoteStringArr = subTopic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (upvoteStringArr.contains(uid)) {
            print("uid is in upvote")
            if let index = upvoteStringArr.firstIndex(of: uid) {
                upvoteStringArr.remove(at: index)
            }
        }
        
        if (downvoteStringArr.contains(uid)) {
            print("uid is in downvote")
            if let index = downvoteStringArr.firstIndex(of: uid) {
                downvoteStringArr.remove(at: index)
            }
        }
        
        //New record added
        downvoteStringArr.append(uid)
        
        let upvoteString = (upvoteStringArr.count > 1 ? (upvoteStringArr.joined(separator: ",") ) : upvoteStringArr.first ?? "")
        let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
        let difference = upvoteStringArr.count - downvoteStringArr.count
        
        guard let documentId = subTopic.documentId else {
            return
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .document(documentId)
            .updateData([
                "upVoteIds": upvoteString,
                "downVoteIds": downvoteString,
                "difference": difference
            ]) { (error) in
                if let err = error {
                    completion(subTopic,err)
                }
                subTopic.upVoteIds = upvoteString
                subTopic.downVoteIds = downvoteString
                subTopic.difference = difference
                completion(subTopic,nil)
            }
    }
    
    func downVoteSubTopicRemove(_ subTopic : SubTopic, completion: @escaping(SubTopic?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        var upvoteStringArr : [String] = []
        var downvoteStringArr : [String] = []
        if subTopic.upVoteIds!.count > 0 {
            upvoteStringArr = subTopic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        if subTopic.downVoteIds!.count > 0 {
            downvoteStringArr = subTopic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (downvoteStringArr.contains(uid)) {
            print("uid is in downvote")
            if let index = downvoteStringArr.firstIndex(of: uid) {
                downvoteStringArr.remove(at: index)
            }
        }
        
        let downvoteString = (downvoteStringArr.count > 1 ? (downvoteStringArr.joined(separator: ",") ) : downvoteStringArr.first ?? "")
        let difference = upvoteStringArr.count - downvoteStringArr.count
        
        guard let documentId = subTopic.documentId else {
            return
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
            .document(documentId)
            .updateData([
                "downVoteIds": downvoteString,
                "difference": difference
            ]) { (error) in
                if let err = error {
                    completion(subTopic,err)
                }
                subTopic.downVoteIds = downvoteString
                subTopic.difference = difference
                completion(subTopic,nil)
            }
    }
    
    // Fetch particular sub topic from "SubTopics" collection using sub tpoic id
    func getSubTopicsBySubTopicId(_ subTopicId : Any ,completion: @escaping(SubTopic?, Error?) -> ()) {
        
        let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.SubTopics)
        eventsRef.whereField("subtopic_id", isEqualTo: subTopicId).getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                completion(nil,err)
            } else {
                if let data = querySnapshot!.documents.first{
                    var documentValue = data.data()
                    documentValue.merge(["documentId" : data.documentID]) { (test, test1) -> Any in
                        print(test,test1)
                    }
                    do {
                        let subtopic = try SubTopic(data: documentValue)
                        completion(subtopic,nil)
                    } catch {
                        completion(nil,error)
                    }
                    
                } else {
                    completion(nil,nil)
                }
            }
        })
        
    }
    
}
