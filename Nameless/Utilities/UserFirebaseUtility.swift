//
//  UserFirebaseUtility.swift
//  Herrd
//
//  Created by DG on 16/08/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import MBProgressHUD

class UserFirebaseUtility: NSObject {
    
    func getAdminUsers(completion: @escaping([Admin]?, Error?) -> ()) {
        
        let eventsRef = Firestore.firestore().collection(FirebaseConst.Collections.Admin)
        eventsRef.getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Admin documents: \(err)")
                completion(nil,err)
            } else {
                var adminList : [Admin]  = []
                querySnapshot!.documents.forEach {
                    do {
                        let admin = try Admin(data: $0.data())
                        adminList.append(admin)
                    } catch {
                        print(error)
                    }
                }
                completion(adminList,nil)
            }
        })
    }
    
}
