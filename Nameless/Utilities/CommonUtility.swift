//
//  CommonUtility.swift
//  Herrd
//
//  Created by Dhanesh Gosai on 23/12/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import Foundation
import UIKit
import ImageViewer

class CommonUtility {
    static func showAlert(title: String, message: String, controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlert(title: String,
                          message: String,
                          controller: UIViewController,
                          alertStyle:UIAlertController.Style,
                          actionTitles:[String],
                          actionStyles:[UIAlertAction.Style],
                          actions: [((UIAlertAction) -> Void)]){

        let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        for(index, indexTitle) in actionTitles.enumerated(){
            let action = UIAlertAction(title: indexTitle, style: actionStyles[index], handler: actions[index])
            alertController.addAction(action)
        }
        controller.present(alertController, animated: true)
    }
    
    static func galleryConfiguration() -> GalleryConfiguration {
        return [
            GalleryConfigurationItem.deleteButtonMode(.none),
            GalleryConfigurationItem.seeAllCloseButtonMode(.none),
            GalleryConfigurationItem.videoAutoPlay(true),
            GalleryConfigurationItem.continuePlayVideoOnEnd(true)
        ]
    }
    
    static func getChannelRefFromTopicRefList(_ privateChannelRef: [String], currentUid: String) -> String {
        for channelRef in privateChannelRef {
            let ids = channelRef.components(separatedBy: "+")
            if ids.last == currentUid {
                return channelRef
            }
        }
        // No channel found
        return ""
    }
    
    static func addLogoInNavigationBar(_ navigationController: UINavigationController?, _ navigationItem: UINavigationItem) {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 72, height: 36))

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 72, height: 36))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "new-nav-logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
}
