//
//  TopicFirebaseUtility.swift
//  Herrd
//
//  Created by DG on 10/04/20.
//  Copyright © 2020 Carter Beaulieu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import MBProgressHUD

class TopicFirebaseUtility: NSObject {
    
    func addNewTopic(topicId: Double,
                     title: String,
                     buttonTitle: String?,
                     buttonUrl: String?,
                     imageUrl:String,
                     videoUrl:String,
                     gifUrl:String?,
                     gifId:String?,
                     logoUrl:String?,
                     mediaType:String,
                     completion: @escaping([String:Any]?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let db = Firestore.firestore()
        let topicsRef = db.collection(FirebaseConst.Collections.Topics)
        let todayDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var newTopicData = [
            "topic_date": todayDateTime,
            "topic_date_str": dateFormatter.string(from: todayDateTime),
            "topic_desc": "",
            "topic_title": title,
            "buttonTitle": buttonTitle ?? "",
            "buttonUrl": buttonUrl ?? "",
            "topic_id": topicId,
            "difference": 0,
            "created_by": uid,
            "upVoteIds":"",
            "downVoteIds":"",
            "channelId": "",
            "msgcount": 0,
            "subTopicsCount": 0,
            "imageUrl": imageUrl,
            "videoUrl": videoUrl,
            "gifUrl": gifUrl ?? "",
            "gifId": gifId ?? "",
            "logoUrl": logoUrl ?? "",
            "mediaType": mediaType
            ] as [String : Any]

        let aDoc = topicsRef.document() //this creates a document with a documentID
        print(aDoc.documentID) //prints the documentID, no database interaction
        aDoc.setData(newTopicData) { (err) in
            if let error = err {
                print(error)
                completion(nil,error)
            } else {
                newTopicData["documentId"] = aDoc.documentID
                if let topicDate = newTopicData["topic_date"] as? Date {
                    newTopicData["topic_date"] = Timestamp(date: topicDate)
                }
                completion(newTopicData,nil)
            }
        }
        
    }
    
    func uploadImageToTopicFolder(imageName: String, imageData:Data, completion: @escaping(String?, Error?) -> ()) {
        let path = "Topic Images" + "/" + imageName
        let storageRef = Storage.storage().reference().child(path)
        storageRef.putData(imageData, metadata: nil) { (metadata, err) in
            if let error = err {
                print(error)
                completion(nil,error)
            } else {
                // You can also access to download URL after upload.
                storageRef.downloadURL { (url, error) in
                    guard let downloadURL = url else { return }
                    completion(downloadURL.absoluteString,nil)
                }
                
            }
        }
    }
    
    func uploadVideoToTopicFolder(videoName: String, videoData:Data, completion: @escaping(String?, Error?) -> ()) {
        let path = "Topic Videos" + "/" + videoName
        let storageRef = Storage.storage().reference().child(path)
        storageRef.putData(videoData, metadata: nil) { (metadata, err) in
            if let error = err {
                print(error)
                completion(nil,error)
            } else {
                // You can also access to download URL after upload.
                storageRef.downloadURL { (url, error) in
                    guard let downloadURL = url else { return }
                    completion(downloadURL.absoluteString,nil)
                }
            }
        }
    }
    
    func uploadDcoToTopicFolder(docName: String, docData:Data, completion: @escaping(String?, Error?) -> ()) {
        let path = "Docs" + "/" + docName
        let storageRef = Storage.storage().reference().child(path)
        storageRef.putData(docData, metadata: nil) { (metadata, err) in
            if let error = err {
                print(error)
                completion(nil,error)
            } else {
                // You can also access to download URL after upload.
                storageRef.downloadURL { (url, error) in
                      guard let downloadURL = url else {
                        // Uh-oh, an error occurred!
                        return
                      }
                    completion(downloadURL.absoluteString,nil)
                }
            }
        }
    }
    
    func getAllPinnedTopics(_ completion: @escaping([Topic]?, Error?) -> ()) {
        
        guard let adminlist = UserDefaults.standard.value(forKey: "adminlist") as? [String] else {
            FirebaseUtility().getAppSettings { (error, doc) in
                if let err = error {
                    print("Got error while reading app settings",err)
                }
                //Call Again to get admin list
                self.getAllPinnedTopics(completion)
            }
            return
        }
        
        //Get all dates array from timelimit
        let datesArray = self.getDatesArray()
        //Create topic firebse ref
        let topicsRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicsRef
            .whereField("created_by", in: adminlist)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                } else {
                    var topicList : [Topic]  = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            if datesArray.contains(topic.topic_date_str ?? "") {
                                topicList.append(topic)
                            }
                        } catch {
                            print(error)
                        }
                    }
                    DispatchQueue.main.async {
                        completion(topicList,nil)
                    }
                }
        })
    }

    func getTopTopicsFirstPage(_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
//        Analytics.logEvent("APICallTofFirstPage", parameters: [
//          "reachAt": "StartFunction",
//          "function": "getTopTopicsFirstPage"
//          ])
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let blockedUserIdsArr = appDelegate?.blockedUserIdsArr  {
            self.getTopTopicsFirstPageSub(uid,blockedUserIdsArr,completion)
        } else {
            Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
            .whereField("uid", isEqualTo: uid)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Blocked Users documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var blockedUserIdsArr : [String] = []
                    if let document = querySnapshot?.documents.first{
                        if let blockedUserIdsStr = document.data()["blockedUserIds"] as? String {
                            blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                        }
                    }
                    
                    appDelegate?.blockedUserIdsArr = blockedUserIdsArr
                    self.getTopTopicsFirstPageSub(uid,blockedUserIdsArr,completion)
                }
            })
        }
    }
    
    func getTopTopicsFirstPageSub(_ uid: String,
                                  _ blockedUserIdsArr: [String],
                                  _ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ())
    {
        
        //Get all dates array from timelimit
        let datesArray = self.getDatesArray()
        Firestore.firestore().collection(FirebaseConst.Collections.Topics).whereField("topic_date_str", in: datesArray)
            .order(by: "difference", descending: true)
            .limit(to: Constants.pageSize)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var topicList : [Topic]  = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            //Check Hide post
                            let isHidePost = topic.hidePostsUserIdsArr.contains(uid)
                            //Check Block User post
                            let isBlockedUserPost = blockedUserIdsArr.contains(topic.created_by!)
                            if !isHidePost && !isBlockedUserPost {
                                topicList.append(topic)
                            }
                        } catch {
                            print(error)
                        }
                    }
                    if let lastDoc = querySnapshot?.documents.last {
                        DispatchQueue.main.async {
                            completion(topicList,nil,lastDoc)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(topicList,nil,nil)
                        }
                    }
                }
            })
    }
    
    func getTopTopicsNextPage(_ lastDoc: DocumentSnapshot,
                              _ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ())
    {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let blockedUserIdsArr = appDelegate?.blockedUserIdsArr  {
            self.getTopTopicsNextPageSub(uid,lastDoc,blockedUserIdsArr,completion)
        } else {
            Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
            .whereField("uid", isEqualTo: uid)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Blocked Users documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var blockedUserIdsArr : [String] = []
                    if let document = querySnapshot?.documents.first{
                        if let blockedUserIdsStr = document.data()["blockedUserIds"] as? String {
                            blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                        }
                    }
                    appDelegate?.blockedUserIdsArr = blockedUserIdsArr
                    self.getTopTopicsNextPageSub(uid,lastDoc,blockedUserIdsArr,completion)
                }
            })
        }
    }
    
    func getTopTopicsNextPageSub(_ uid: String,
                                 _ lastDoc: DocumentSnapshot,
                                 _ blockedUserIdsArr: [String],
                                 _ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ())
    {
        //Get all dates array from timelimit
        let datesArray = self.getDatesArray()
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_date_str", in: datesArray)
            .order(by: "difference", descending: true)
            .limit(to: Constants.pageSize)
            .start(afterDocument: lastDoc)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var topicList : [Topic]  = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            //Check Hide post
                            let isHidePost = topic.hidePostsUserIdsArr.contains(uid)
                            //Check Block User post
                            let isBlockedUserPost = blockedUserIdsArr.contains(topic.created_by!)
                            if !isHidePost && !isBlockedUserPost {
                                topicList.append(topic)
                            }
                        } catch {
                            print(error)
                        }
                    }
                    if let lastDoc = querySnapshot?.documents.last {
                        DispatchQueue.main.async {
                            completion(topicList,nil,lastDoc)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(topicList,nil,nil)
                        }
                    }
                }
            })
    }
    
    func getNewTopicsFirstPage(_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let blockedUserIdsArr = appDelegate?.blockedUserIdsArr  {
            self.getNewTopicsFirstPageSub(uid,blockedUserIdsArr,completion)
        } else {
            Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
            .whereField("uid", isEqualTo: uid)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Blocked Users documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var blockedUserIdsArr : [String] = []
                    if let document = querySnapshot?.documents.first{
                        if let blockedUserIdsStr = document.data()["blockedUserIds"] as? String {
                            blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                        }
                    }
                    appDelegate?.blockedUserIdsArr = blockedUserIdsArr
                    self.getNewTopicsFirstPageSub(uid,blockedUserIdsArr,completion)
                }
            })
        }
    }
    
    func getNewTopicsFirstPageSub(_ uid: String,_ blockedUserIdsArr:[String] ,_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .order(by: "topic_date", descending: true)
            .limit(to: Constants.pageSize)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var topicList : [Topic]  = []
//                    print("Init Topic count: ",querySnapshot!.documents.count)
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            //Check Hide post
                            let isHidePost = topic.hidePostsUserIdsArr.contains(uid)
                            //Check Block User post
                            let isBlockedUserPost = blockedUserIdsArr.contains(topic.created_by!)
                            if !isHidePost && !isBlockedUserPost {
                                topicList.append(topic)
                            }
                        } catch {
                            print(error)
                        }
                    }
                    if let lastDoc = querySnapshot?.documents.last {
                        DispatchQueue.main.async {
                            completion(topicList,nil,lastDoc)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(topicList,nil,nil)
                        }
                    }
                }
            })
    }
    
    func getNewTopicsNextPage(_ lastDoc: DocumentSnapshot,_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let blockedUserIdsArr = appDelegate?.blockedUserIdsArr  {
            self.getNewTopicsNextPageSub(uid,blockedUserIdsArr,lastDoc,completion)
        } else {
            Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
            .whereField("uid", isEqualTo: uid)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Blocked Users documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var blockedUserIdsArr : [String] = []
                    if let document = querySnapshot?.documents.first{
                        if let blockedUserIdsStr = document.data()["blockedUserIds"] as? String {
                            blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                        }
                    }
                    appDelegate?.blockedUserIdsArr = blockedUserIdsArr
                    self.getNewTopicsNextPageSub(uid,blockedUserIdsArr,lastDoc,completion)
                }
            })
        }
    }
    
    func getNewTopicsNextPageSub(_ uid: String,_ blockedUserIdsArr:[String] ,_ lastDoc: DocumentSnapshot,_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .order(by: "topic_date", descending: true)
            .limit(to: Constants.pageSize)
            .start(afterDocument: lastDoc)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err,nil)
                    }
                } else {
                    var topicList : [Topic]  = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            //Check Hide post
                            let isHidePost = topic.hidePostsUserIdsArr.contains(uid)
                            //Check Block User post
                            let isBlockedUserPost = blockedUserIdsArr.contains(topic.created_by!)
                            if !isHidePost && !isBlockedUserPost {
                                topicList.append(topic)
                            }
                        } catch {
                            print(error)
                        }
                    }
                    if let lastDoc = querySnapshot?.documents.last {
                        DispatchQueue.main.async {
                            completion(topicList,nil,lastDoc)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion(topicList,nil,nil)
                        }
                    }
                }
            })
        
    }
    
    func getDatesArray() -> [String] {
        var limitDays = 0
        if let topicTimelineLimit = UserDefaults.standard.value(forKey: "topicTimelineLimit") as? Int {
            limitDays = topicTimelineLimit/24
        } else {
            limitDays = Constants.topicTimelineLimit/24
        }
        let todayDate = Date()
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: todayDate)
        var datesArray: [String] = []
        guard let end = Calendar.current.date(from: components) else { return [] }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        datesArray.append(dateFormatter.string(from: end))
        for day in 1..<limitDays + 1 {
            guard let start = Calendar.current.date(byAdding: .day,
                                                    value: -day,
                                                    to: end) else {
                fatalError("Could not find start date or calculate end date.")
            }
            datesArray.append(dateFormatter.string(from: start))
        }
        print(datesArray)
        return datesArray
        
    }
    
    func getStartDateBasedOnTimeLimit() -> Date? {
        let limitDays = getLimitDays()
        let todayDate = Date()
        let components = Calendar.current.dateComponents([.year, .month, .day,
                                                          .hour, .minute, .second],
                                                         from: todayDate)
        guard
            let end = Calendar.current.date(from: components),
            let start = Calendar.current.date(byAdding: .day,
                                              value: -limitDays, to: end)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        
        return start
        
    }
    
    func getLimitDays() -> Int {
        var limitDays = 0
        if let topicTimelineLimit = UserDefaults.standard.value(forKey: "topicTimelineLimit") as? Int {
            limitDays = topicTimelineLimit/24
        } else {
            limitDays = Constants.topicTimelineLimit/24
        }
        return limitDays
    }
    
    func batchUpdates(_ topics: [AnyObject]) {
        
        // Get a new write batch
        let batch = Firestore.firestore().batch()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        for (index,topic) in topics.enumerated() {
            guard let topic = topic as? Topic else { continue }
            if topic.topic_date_str == nil {
                print(index,": ",topic.topic_id as Any)
                let topicDoc = Firestore.firestore().collection(FirebaseConst.Collections.Topics).document(topic.documentId!)
                guard let topicDate = topic.topic_date?.dateValue() else { continue }
                let topicDateStr = dateFormatter.string(from: topicDate)
                batch.updateData(["topic_date_str": topicDateStr], forDocument: topicDoc)
            }
        }
        
        batch.commit { (error) in
            if let err = error {
                print("update date batch failed",err)
            }
            print("Update date batch success")
        }
    }
    
    func batchUpdatesForHidePostIds(_ topics: [AnyObject]) {
        
        // Get a new write batch
        let batch = Firestore.firestore().batch()
        for (index,topic) in topics.enumerated() {
            guard let topic = topic as? Topic else { continue }
            if topic.hidePostsUserIdsArr.count == 0 && topic.hidePostsUserIds?.count ?? 0 > 0 {
                print(index,": ",topic.topic_id as Any)
                let topicDoc = Firestore.firestore()
                    .collection(FirebaseConst.Collections.Topics).document(topic.documentId!)
                if let hidePostsUserIdsArr = topic.hidePostsUserIds?.components(separatedBy: ","), hidePostsUserIdsArr.count > 0 {
                    batch.updateData(["hidePostsUserIdsArr": hidePostsUserIdsArr], forDocument: topicDoc)
                }
            }
        }
        
        batch.commit { (error) in
            if let err = error {
                print("update date batch failed",err)
            }
            print("Update date batch success")
        }
    }
    
    func getTopicsByPrivateChannel(_ completion: @escaping([Topic]?, Error?) -> ()) {
    
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let blockedUserIdsArr = appDelegate?.blockedUserIdsArr  {
            self.getTopicsByPrivateChannelSub(uid,blockedUserIdsArr,completion)
        } else {
            Firestore.firestore().collection(FirebaseConst.Collections.BlockedUsers)
            .whereField("uid", isEqualTo: uid)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Blocked Users documents: \(err)")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                } else {
                    var blockedUserIdsArr : [String] = []
                    if let document = querySnapshot?.documents.first{
                        if let blockedUserIdsStr = document.data()["blockedUserIds"] as? String {
                            blockedUserIdsArr = blockedUserIdsStr.components(separatedBy: ",")
                        }
                    }
                    appDelegate?.blockedUserIdsArr = blockedUserIdsArr
                    self.getTopicsByPrivateChannelSub(uid,blockedUserIdsArr,completion)
                }
            })
        }
    }
    
    func getTopicsByPrivateChannelSub(_ uid: String,_ blockedUserIdsArr:[String],_ completion: @escaping([Topic]?, Error?) -> ()) {
        
        //Get all dates array from timelimit
        let datesArray = self.getDatesArray()
        
        let privateTopicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        privateTopicRef.whereField("connected_users", arrayContains: uid)
//        privateTopicRef.whereField("created_by", isEqualTo: uid)
//        privateTopicRef.whereField("privateChannelRef", notIn: [""])
//        privateTopicRef.whereField("privateMsgCount", notIn: [""])
            .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                DispatchQueue.main.async {
                    completion(nil,err)
                }
            } else {
                var topicList : [Topic]  = []
                print("Private chat Initial Topic count: ",querySnapshot!.documents.count)
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue.updateValue($0.documentID, forKey: "documentId")
                    do {
                        let topic = try Topic(data: documentValue)
//                                let interval = Date() - (topic.topic_date?.dateValue())!
                        //Check Hide post
                        let isHidePost = topic.hidePostsUserIdsArr.contains(uid)
                        //Check Block User post
                        let isBlockedUserPost = blockedUserIdsArr.contains(topic.created_by!)
                        if datesArray.contains(topic.topic_date_str ?? "") &&
                            !isHidePost &&
                            !isBlockedUserPost &&
                            self.checkPrivateChannelIds(topic,uid) {
                            topicList.append(topic)
                        }
                    } catch {
                        print(error)
                    }
                }
                print("Private chat final Topic count: ",topicList.count)
                DispatchQueue.main.async {
                    completion(topicList,nil)
                }
            }
        })
        
    }
    
    func checkPrivateChannelIds(_ topic: Topic,_ uid: String) -> Bool {
        let channelRef1 = "\(topic.topic_id ?? 0)+\(uid)+\(topic.created_by ?? "")"
        let channelRef2 = "\(topic.topic_id ?? 0)+\(topic.created_by ?? "")+\(uid)"
        guard let privateChannelRef =  topic.privateChannelRef else {
            return false
        }
    
        if (privateChannelRef.contains(channelRef1) ||
                privateChannelRef.contains(channelRef2)) {
            return true
            
        } else {
            //This logic for own topic private chats
            for channelRef in privateChannelRef {
                let channelArray = channelRef.components(separatedBy: "+")
                if channelArray.count == 3 &&
                    channelArray[0] == "\(topic.topic_id ?? 0)" &&
                    channelArray[1] == "\(uid)" {
                    return true
                }
            }
            return false
        }
    }
    
    func getMyTopics(completion: @escaping([Topic]?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let dates = self.getDatesArray()
        
        let topicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicRef
            .whereField("created_by", isEqualTo: uid)
            .whereField("topic_date_str", in: dates)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    completion(nil,err)
                } else {
                    var topicList : [Topic]  = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            topicList.append(topic)
                        } catch {
                            print(error)
                        }
                    }
                    completion(topicList,nil)
                }
            })
    }
    
    func getMyTopicsWithPrivateChannels(completion: @escaping([Topic]?, Error?) -> ()) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let dates = self.getDatesArray()
        
        let topicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicRef
            .whereField("created_by", isEqualTo: uid)
            .whereField("topic_date_str", in: dates)
            .getDocuments(completion: { (querySnapshot, err) in
                if let err = err {
                    print("Error getting Topics documents: \(err)")
                    completion(nil,err)
                } else {
                    var topicList : [Topic]  = []
                    querySnapshot!.documents.forEach {
                        var documentValue = $0.data()
                        documentValue.updateValue($0.documentID, forKey: "documentId")
                        do {
                            let topic = try Topic(data: documentValue)
                            if self.checkPrivateChannelIds(topic,uid) {
                                topicList.append(topic)
                            }
                        } catch {
                            print(error)
                        }
                    }
                    completion(topicList,nil)
                }
            })
    }
    
    func getReportedTopicsFirstPage(_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
        
//        guard let start = self.getStartDateBasedOnTimeLimit() else { return }
        
//        let datesArray = self.getDatesArray()
        
        let topicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicRef
//            .whereField("topic_date_str", in: datesArray)
//            .order(by: "topic_date" ,descending: true)
            .whereField("reportedIds", isNotEqualTo: "")
            
            .limit(to: Constants.pageSize)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                completion(nil,err,nil)
            } else {
                var topicList : [Topic]  = []
                print("Reported Topic count: ",querySnapshot!.documents.count)
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue.updateValue($0.documentID, forKey: "documentId")
                    do {
                        let topic = try Topic(data: documentValue)
                        topicList.append(topic)
                    } catch {
                        print(error)
                    }
                }
                print("Reported Topic count Local: ",topicList.count)
                if let lastDoc = querySnapshot?.documents.last {
                    completion(topicList,nil,lastDoc)
                } else {
                    completion(topicList,nil,nil)
                }
            }
        })
    }
    
    func getReportedTopicsNextPage(_ lastDoc: DocumentSnapshot,_ completion: @escaping([Topic]?, Error?, DocumentSnapshot?) -> ()) {
        
//        guard let start = self.getStartDateBasedOnTimeLimit() else { return }
        
        let topicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicRef
//            .whereField("topic_date", isGreaterThan: start)
//            .order(by: "topic_date" ,descending: true)
            .whereField("reportedIds", isNotEqualTo: "")
//            .order(by: "reportedIds")
            .limit(to: Constants.pageSize)
            .start(afterDocument: lastDoc)
        .getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                completion(nil,err,nil)
            } else {
                var topicList : [Topic]  = []
                print("Reported Topic count: ",querySnapshot!.documents.count)
                querySnapshot!.documents.forEach {
                    var documentValue = $0.data()
                    documentValue.updateValue($0.documentID, forKey: "documentId")
                    do {
                        let topic = try Topic(data: documentValue)
                        topicList.append(topic)
                    } catch {
                        print(error)
                    }
                }
                print("Reported Topic count Local: ",topicList.count)
                if let lastDoc = querySnapshot?.documents.last {
                    completion(topicList,nil,lastDoc)
                } else {
                    completion(topicList,nil,nil)
                }
            }
        })
    }
    
    func getTopicsByTopicId(_ topicId : Any ,completion: @escaping(Topic?, Error?) -> ()) {
        
        let topicRef = Firestore.firestore().collection(FirebaseConst.Collections.Topics)
        topicRef.whereField("topic_id", isEqualTo: topicId).getDocuments(completion: { (querySnapshot, err) in
            if let err = err {
                print("Error getting Topics documents: \(err)")
                completion(nil,err)
            } else {
                if let data = querySnapshot!.documents.first{
                    var documentValue = data.data()
                    documentValue.updateValue(data.documentID, forKey: "documentId")
                    do {
                        let topic = try Topic(data: documentValue)
                        completion(topic,nil)
                    } catch {
                        completion(nil,error)
                    }
                    
                } else {
                    completion(nil,nil)
                }
            }
        })
        
    }
    
    func addReportedIdstoTopic(_ topic : Topic, completion: @escaping(Topic?, Error?) -> ()) {
        
        guard let documentId = topic.documentId else { return }
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .document(documentId)
            .updateData([
                "reportedIds": topic.reportedIds ?? "",
            ], completion: { (error) in
                if let err = error {
                    print("Got error while hide post",err)
                    completion(topic,err)
                }
                completion(topic,nil)
            })
    }
    
    func addHidePostUserIdstoTopic(_ topic : Topic, completion: @escaping(Topic?, Error?) -> ()) {
        
        guard let documentId = topic.documentId else { return }
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .document(documentId)
            .updateData([
                "hidePostsUserIdsArr": topic.hidePostsUserIdsArr,
            ], completion: { (error) in
                if let err = error {
                    print("Got error while hide post",err)
                    completion(topic,err)
                }
                completion(topic,nil)
        })
    }
    
    func upVoteTopicNew(_ topic : Topic, completion: @escaping(Topic?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        var upvoteStringArr : [String] = []
        if topic.upVoteIds!.count > 0 {
            upvoteStringArr = topic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var downvoteStringArr : [String] = []
        if topic.downVoteIds!.count > 0 {
            downvoteStringArr = topic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (upvoteStringArr.contains(uid!)) {
            print("uid is in upvote")
            if let index = upvoteStringArr.firstIndex(of: uid!) {
                upvoteStringArr.remove(at: index)
            }
        }
        
        if (downvoteStringArr.contains(uid!)) {
            print("uid is in downvote")
            if let index = downvoteStringArr.firstIndex(of: uid!) {
                downvoteStringArr.remove(at: index)
            }
        }
        
        //New record added
        upvoteStringArr.append(uid!)
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: topic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                    completion(topic,err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    if let document = querySnapshot!.documents.first{
                        var documentValue = document.data()
                        documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                            print(test,test1)
                        }
                        do {
                            let topic = try Topic(data: documentValue)
                            let upvoteString = (upvoteStringArr.count > 1 ?
                                                    upvoteStringArr.joined(separator: ",") :
                                                    upvoteStringArr.first ?? "")
                            let downvoteString = (downvoteStringArr.count > 1 ?
                                                    downvoteStringArr.joined(separator: ",") :
                                                    downvoteStringArr.first ?? "")
                             let difference = upvoteStringArr.count - downvoteStringArr.count
                            document.reference.updateData([
                                "upVoteIds": upvoteString,
                                "downVoteIds": downvoteString,
                                "difference": difference
                            ])
                            topic.upVoteIds = upvoteString
                            topic.downVoteIds = downvoteString
                            topic.difference = difference
                            completion(topic,nil)
                        } catch {
                            completion(topic,error)
                        }
                    }
                }
        }
    }
    
    func upVoteTopicRemove(_ topic : Topic, completion: @escaping(Topic?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        var upvoteStringArr : [String] = []
        if topic.upVoteIds!.count > 0 {
            upvoteStringArr = topic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var downvoteStringArr : [String] = []
        if topic.downVoteIds!.count > 0 {
            downvoteStringArr = topic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (upvoteStringArr.contains(uid!)) {
            print("uid is in upvote")
            if let index = upvoteStringArr.firstIndex(of: uid!) {
                upvoteStringArr.remove(at: index)
            }
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: topic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                    completion(topic,err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    if let document = querySnapshot!.documents.first{
                        var documentValue = document.data()
                        documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                            print(test,test1)
                        }
                        do {
                            let topic = try Topic(data: documentValue)
                            let upvoteString = (upvoteStringArr.count > 1 ?
                                                    upvoteStringArr.joined(separator: ",") :
                                                    upvoteStringArr.first ?? "")
                            let difference = upvoteStringArr.count - downvoteStringArr.count
                            document.reference.updateData([
                                "upVoteIds": upvoteString,
                                "difference": difference
                            ])
                            topic.upVoteIds = upvoteString
                            topic.difference = difference
                            completion(topic,nil)
                        } catch {
                            completion(topic,error)
                        }
                    }
                }
        }
    }
    
    func downVoteTopicNew(_ topic : Topic, completion: @escaping(Topic?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        var upvoteStringArr : [String] = []
        if topic.upVoteIds!.count > 0 {
            upvoteStringArr = topic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var downvoteStringArr : [String] = []
        if topic.downVoteIds!.count > 0 {
            downvoteStringArr = topic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (upvoteStringArr.contains(uid!)) {
            print("uid is in upvote")
            if let index = upvoteStringArr.firstIndex(of: uid!) {
                upvoteStringArr.remove(at: index)
            }
        }
        
        if (downvoteStringArr.contains(uid!)) {
            print("uid is in downvote")
            if let index = downvoteStringArr.firstIndex(of: uid!) {
                downvoteStringArr.remove(at: index)
            }
        }
        
        //New record added
        downvoteStringArr.append(uid!)
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: topic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                    completion(topic,err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    if let document = querySnapshot!.documents.first{
                    var documentValue = document.data()
                    documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                        print(test,test1)
                    }
                    do {
                        let topic = try Topic(data: documentValue)
                        let upvoteString = (upvoteStringArr.count > 1 ?
                                                upvoteStringArr.joined(separator: ",") :
                                                upvoteStringArr.first ?? "")
                        let downvoteString = (downvoteStringArr.count > 1 ?
                                                downvoteStringArr.joined(separator: ",") :
                                                downvoteStringArr.first ?? "")
                        let difference = upvoteStringArr.count - downvoteStringArr.count
                        document.reference.updateData([
                            "upVoteIds": upvoteString,
                            "downVoteIds": downvoteString,
                            "difference": difference
                        ])
                        topic.upVoteIds = upvoteString
                        topic.downVoteIds = downvoteString
                        topic.difference = difference
                        completion(topic,nil)
                    } catch {
                        completion(topic,error)
                    }
                }
            }
        }
    }
    
    func downVoteTopicRemove(_ topic : Topic, completion: @escaping(Topic?, Error?) -> ()) {
        
        let user = Auth.auth().currentUser
        let uid = user?.uid
        
        var upvoteStringArr : [String] = []
        if topic.upVoteIds!.count > 0 {
            upvoteStringArr = topic.upVoteIds?.components(separatedBy: ",") ?? []
        }
        
        var downvoteStringArr : [String] = []
        if topic.downVoteIds!.count > 0 {
            downvoteStringArr = topic.downVoteIds?.components(separatedBy: ",") ?? []
        }
        
        if (downvoteStringArr.contains(uid!)) {
            print("uid is in downvote")
            if let index = downvoteStringArr.firstIndex(of: uid!) {
                downvoteStringArr.remove(at: index)
            }
        }
        
        Firestore.firestore().collection(FirebaseConst.Collections.Topics)
            .whereField("topic_id", isEqualTo: topic.topic_id ?? "")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print(err)
                    completion(topic,err)
                } else if querySnapshot!.documents.count != 1 {
                    // Perhaps this is an error for you?
                } else {
                    
                    if let document = querySnapshot!.documents.first{
                        var documentValue = document.data()
                        documentValue.merge(["documentId" : document.documentID]) { (test, test1) -> Any in
                            print(test,test1)
                        }
                        do {
                            let topic = try Topic(data: documentValue)
                            let document = querySnapshot!.documents.first
                            let downvoteString = (downvoteStringArr.count > 1 ?
                                                    downvoteStringArr.joined(separator: ",") :
                                                    downvoteStringArr.first ?? "")
                            let difference = upvoteStringArr.count - downvoteStringArr.count
                            document?.reference.updateData([
                                "downVoteIds": downvoteString,
                                "difference": difference
                            ])
                            topic.downVoteIds = downvoteString
                            topic.difference = difference
                            completion(topic,nil)
                        } catch {
                            completion(topic,error)
                        }
                    }
                }
        }
    }
}
